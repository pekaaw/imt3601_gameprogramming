
#include "common.h"

uniform float zrange;
uniform int color_r;
uniform int color_g;

in float vertex_depth;

out vec3 fragment_color;

void main()
{
	fragment_color = vec3(color_r / 255.0, color_g / 255.0, 0);

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


