
#include "common.h"

uniform mat4 MVP;
uniform float time;

in vec3 position;

out vec3 vertex_position;
out vec4 vertex_uvs;
out vec2 vertex_noise;

const float scale_0 = 0.001;
const float scale_1 = 0.0004;
const float scale_2 = 0.0008;
const vec2 dir_0 = vec2(0.2, 0.5) * 0.1;
const vec2 dir_1 = vec2(0.3, 0.17) * 0.03;
const vec2 dir_2 = vec2(0.02, 0.2) * 0.6;

void main()
{
	gl_Position = MVP * vec4(position, 1.0);

	vertex_position = position;

	vertex_uvs.xy = vertex_position.xy * scale_0 + dir_0 * time;
	vertex_uvs.zw = vertex_position.xy * scale_1 + dir_1 * time;
	
	vertex_noise = vertex_position.xy * scale_2 + dir_2 * time;
}
