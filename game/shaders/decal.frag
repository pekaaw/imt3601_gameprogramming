
#include "common.h"

uniform sampler2D albedoSampler;

uniform float zrange;
uniform vec4 diffuseModulation;

in vec3 vertex_normal;
in vec2 vertex_uv;
in float vertex_depth;

out vec4 fragment_color[2];


void main()
{
	vec4 albedo = texture2D(albedoSampler, vertex_uv);
	vec3 normal = vertex_normal;

	// hack: decal geometry isn't perfect yet, force face up
	//normal *= ((normal.z < 0.0)?-1:1);

	normal = normalize(normal);
	normal = vec3(0, 0, 1);
	
	//if (albedo.a < 0.5)
	//{
	//	discard;
	//}
	
	albedo *= diffuseModulation;

	fragment_color[0] = albedo;
	
	fragment_color[1] = vec4(normal * 0.5 + 0.5, albedo.a * diffuseModulation.a);

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


