
//COMBO: ALPHATESTING

#include "common.h"

uniform mat4 VP;
uniform mat4 bones[64];

in vec3 position;
//in vec3 normal;
in vec3 boneindices;

#if ALPHATESTING == 1
in vec2 texcoord;
out vec2 vertex_uv;
#endif

//out vec3 vertex_position;

void main()
{
	vec4 position4 = vec4(position, 1.0);

	vec4 worldSpacePosition =  bones[int(boneindices.x)] * position4 * 0.333333
					+ bones[int(boneindices.y)] * position4 * 0.333333
					+ bones[int(boneindices.z)] * position4 * 0.333333;

	gl_Position = VP * worldSpacePosition;

#if ALPHATESTING == 1
	vertex_uv = texcoord;
#endif

	//vertex_position = worldSpacePosition.xyz;
	
	//vertex_normal = rotate3x3(bones[int(boneindices.x)], normal) * 0.333333
	//				+ rotate3x3(bones[int(boneindices.y)], normal) * 0.333333
	//				+ rotate3x3(bones[int(boneindices.z)], normal) * 0.333333;
}
