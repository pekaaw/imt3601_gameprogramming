
uniform sampler2D albedoSampler;
uniform vec2 texelSizeHalf;
uniform vec2 texelSize3Half;

in vec2 vertex_texcoord;

out vec3 fragment_color;

void main()
{
	vec3 albedo = texture2D(albedoSampler, vertex_texcoord + texelSizeHalf).rgb
				+ texture2D(albedoSampler, vertex_texcoord + texelSize3Half).rgb
				+ texture2D(albedoSampler, vertex_texcoord + vec2(texelSizeHalf.x, texelSize3Half.y)).rgb
				+ texture2D(albedoSampler, vertex_texcoord + vec2(texelSize3Half.x, texelSizeHalf.y)).rgb;
	
	fragment_color = albedo * 0.25;
}