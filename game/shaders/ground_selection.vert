
#include "common.h"

uniform mat4 MVP;
uniform float znear;

in vec3 position;


out float vertex_depth;

void main()
{
	gl_Position = MVP * vec4(position, 1.0);
	
	vertex_depth = gl_Position.z - znear;
}
