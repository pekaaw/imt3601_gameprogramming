
//COMBO: MODEL
//COMBO: SKINNING
//COMBO: VERTEXCOLOR
//COMBO: VERTEXALPHA
//COMBO: TEXTURED

#include "common.h"

#if SKINNING
uniform mat4 VP;
uniform mat4 bones[64];
in vec3 boneindices;
#elif MODEL
uniform mat4 MVP;
#endif
uniform float znear;

in vec3 position;

#if VERTEXCOLOR
in vec3 color;
out vec3 vertex_color;
#elif VERTEXALPHA
in vec4 color;
out vec4 vertex_color;
#endif

#if TEXTURED
in vec2 texcoord;
out vec2 vertex_uv;
#endif

out float vertex_depth;

void main()
{
#if SKINNING
	vec4 position4 = vec4(position, 1.0);

	vec4 worldSpacePosition =  bones[int(boneindices.x)] * position4 * 0.333333
					+ bones[int(boneindices.y)] * position4 * 0.333333
					+ bones[int(boneindices.z)] * position4 * 0.333333;

	gl_Position = VP * worldSpacePosition;
#elif MODEL
	gl_Position = MVP * vec4(position, 1.0);
#else
	gl_Position = vec4(position, 1.0);
#endif

#if VERTEXCOLOR || VERTEXALPHA
	vertex_color = color;
#endif

#if TEXTURED
	vertex_uv = texcoord;
#endif

	vertex_depth = gl_Position.z - znear;
}
