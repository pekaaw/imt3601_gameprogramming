
uniform sampler2D albedoSampler;
uniform sampler2D bloomSampler;
uniform vec2 texelSize;

in vec2 vertex_texcoord;

out vec3 fragment_color;

void main()
{
	vec3 albedo = texture2D(albedoSampler, vertex_texcoord).rgb;
	vec3 bloom = texture2D(bloomSampler, vertex_texcoord - texelSize).rgb;
	
	fragment_color = albedo + bloom;
}