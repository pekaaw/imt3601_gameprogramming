
#include "common.h"

uniform sampler2D albedoSampler;
uniform sampler2D normalSampler;
uniform float zrange;
uniform vec3 color;


//in vec3 vertex_position;
in vec3 vertex_normal;
in vec3 vertex_tangent;
in vec3 vertex_binormal;
in vec2 vertex_uv;
in float vertex_depth;

out vec3 fragment_color[2];


void main()
{
	vec4 albedo = texture2D(albedoSampler, vertex_uv);
	vec3 normal = texture2D(normalSampler, vertex_uv).rgb * 2 - 1;
	
	mat3x3 tangentSpace = mat3x3(vertex_tangent, vertex_binormal, vertex_normal);
	normal = tangentSpace * normal;
	//normal = normalize(normal);
	
	
	fragment_color[0] = albedo.rgb * color;
	
	
	fragment_color[1] = normal * 0.5 + 0.5;
	
	//fragment_color[0] = normal * 0.5 + 0.5;

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


