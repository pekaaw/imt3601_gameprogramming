
#include "common.h"

uniform sampler2D normalSampler;
uniform sampler2D depthSampler;

uniform vec3 frustum_fwd;
uniform vec3 frustum_top;
uniform vec3 frustum_right;
uniform vec3 view_origin;

uniform int light_count;
uniform mat4x3 light_data[2];

in vec2 vertex_texcoord;
in vec2 vertex_position;

out vec3 fragment_color;


vec3 DoPointLight( in mat4x3 data, in vec3 worldPos, in vec3 normal )
{
	vec3 lightPos = data[0];
	vec3 color = data[1];
	float radius = data[3][0];
	
	vec3 delta = lightPos - worldPos;
	float dist = length(delta);
	delta /= dist;
	
	float lum = dot(normal, delta);
	lum = clamp(lum,0,1);
	
	float falloff = (1.0 - clamp( dist / radius, 0, 1 ) );
	
	
	vec3 viewDir = view_origin - worldPos;
	viewDir = normalize(viewDir);
	
	float phong = BlinnPhong( viewDir, delta, normal, 20.0 ) * falloff;
	
	return color * ( lum * falloff * falloff + phong );
}

void main()
{
	float depth = texture2D(depthSampler, vertex_texcoord).r;
	
	if ( depth >= 0.99 )
	{
		discard;
	}
	
	vec3 normal = texture2D(normalSampler, vertex_texcoord).rgb * 2 - 1;
	
	//float zNear = 20.0;
	//float zFar = 1000.0;
	//float z_n = 2.0 * depth - 1.0;
	//depth = 2.0 * zNear * zFar / (zFar + zNear - z_n * (zFar - zNear)) - zNear;
	
	//fragment_color = vec3(vertex_texcoord.xy, 0);
	
#if COMBINE_ORTHOGRAPHIC == 0
	vec3 worldRay = frustum_fwd + frustum_top * vertex_position.y + frustum_right * vertex_position.x;
	vec3 worldPosition = view_origin + worldRay * depth;
#else
	vec3 worldRay = frustum_top * vertex_position.y + frustum_right * vertex_position.x;
	vec3 worldPosition = view_origin + worldRay + frustum_fwd * depth;
#endif
	
	fragment_color = vec3( depth, depth, depth);
	fragment_color = worldPosition;
	
	fragment_color = vec3(0,0,0);
	
	for (int i = 0; i < light_count; i++)
	{
		fragment_color += DoPointLight(light_data[i], worldPosition, normal);
	}
}