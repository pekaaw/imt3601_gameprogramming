
#include "common.h"

uniform mat4 MVP;
uniform mat4 M;
uniform float znear;
uniform float time;

in vec3 position;
in vec2 texcoord;
in vec3 normal;
in vec3 tangent;
in vec3 binormal;


out vec3 vertex_position;
out vec3 vertex_normal;
out vec3 vertex_tangent;
out vec3 vertex_binormal;
out vec2 vertex_uv;
out vec4 vertex_animatedUv;
out vec3 vertex_screenUv;
out float vertex_depth;

const float vertexAnimSpeed0 = 3.36;
const float vertexAnimSpeed1 = 4.11;
const float vertexAnimScale = 3;

const float speed_0 = 0.45;
const float speed_1 = 0.3;
const vec2 dir_0 = vec2(0.1, 0.23) * speed_0;
const vec2 dir_1 = vec2(-0.3, -0.1) * speed_1;
const float scale_0 = 1.77;
const float scale_1 = 1.38;

void main()
{
	float animA = sin(position.x * 2 + time * vertexAnimSpeed0) * vertexAnimScale;
	float animB = cos(position.y + time * vertexAnimSpeed1) * vertexAnimScale;

	vertex_position = position;
	vertex_position.z += animA + animB;
	vertex_position.x += animA - animB;
	vertex_position.y += animB - animA;
	gl_Position = MVP * vec4(vertex_position, 1.0);

	vertex_depth = gl_Position.z - znear;

	vertex_uv = texcoord;

	vertex_normal = rotate3x3(M, normal);
	vertex_tangent = rotate3x3(M, tangent);
	vertex_binormal = rotate3x3(M, binormal);
	
	vertex_animatedUv.zw = scale_0 * texcoord + dir_0 * time + animA * 0.002 + animB * 0.003;
	vertex_animatedUv.xy = scale_1 * texcoord + dir_1 * time;
	
	vertex_screenUv = gl_Position.xyw;
}
