
#include "common.h"

uniform sampler2D normalSampler;
uniform sampler2D normal2Sampler;
uniform sampler2D depthSampler;
uniform sampler2D refractSampler;
uniform sampler2D reflectSampler;
uniform float zrange;
uniform vec3 view_origin;


in vec3 vertex_position;
in vec3 vertex_normal;
in vec3 vertex_tangent;
in vec3 vertex_binormal;
in vec2 vertex_uv;
in vec4 vertex_animatedUv;
in vec3 vertex_screenUv;
in float vertex_depth;

out vec4 fragment_color[2];

const vec3 refRay = vec3(0.57, -0.42, -0.707);

void main()
{
	vec2 screenCoord = vertex_screenUv.xy/vertex_screenUv.z * 0.5 + 0.5;
	float finalDepth = vertex_depth / zrange;

	vec4 albedo = vec4(0.4, 0.6, 0.7, 1.0); //texture2D(albedoSampler, vertex_uv);
	float depth = texture2D(depthSampler, screenCoord);
	
	float fogAmount = (depth - finalDepth) * 0.021 * zrange;
	fogAmount = clamp(fogAmount, 0, 1);
	
	vec3 normal = texture2D(normalSampler, vertex_animatedUv.xy).rgb * 2 - 1;
	normal += texture2D(normal2Sampler, vertex_animatedUv.zw).rgb * 2 - 1;
	normal = normalize(normal);
	
	mat3x3 tangentSpace = mat3x3(vertex_tangent, vertex_binormal, vertex_normal);
	normal = tangentSpace * normal;
	//normal = normalize(normal);
	
	// chromatic aberration
	vec3 refractSample = vec3(texture2D(refractSampler, screenCoord
							+ normal.xy * 0.018 * fogAmount).r,
						texture2D(refractSampler, screenCoord
							+ normal.xy * 0.025 * fogAmount).g,
						texture2D(refractSampler, screenCoord
							+ normal.xy * 0.035 * fogAmount).b);
	//refractSample *= 0.8;
	refractSample *= 1.8;
	
	
	vec3 viewVector = view_origin - vertex_position;
	viewVector = normalize(viewVector);
	vec3 reflected = reflect(refRay, normal);
	
	vec2 reflectUV = screenCoord;
	reflectUV.x = 1.0 - reflectUV.x;
	vec3 reflectSample = texture2D(reflectSampler, reflectUV + normal.xy * 0.045).rgb;
	reflectSample += texture2D(reflectSampler, reflectUV + normal.yx * 0.015).rgb;
	reflectSample += 0.5;
	
	float fresnel = clamp(dot(normal, viewVector), 0, 1);
	fresnel = smoothstep(1.1, 0.8, fresnel);
	//fresnel = 1.0 - pow(fresnel, 2.5);
	reflectSample = mix(vec3(0.5, 0.5, 0.5), reflectSample, fresnel);

	
	albedo.rgb = mix(refractSample * vec3(0.4, 0.6, 0.8), vec3(0.35, 0.6, 0.72) * 1.2, fogAmount * 0.32);
	//albedo.rgb = mix(refractSample * vec3(0.6, 0.8, 1) * 2, albedo.rgb * reflectSample, fogAmount);
	albedo.rgb = mix(albedo.rgb, albedo.rgb * reflectSample, fogAmount);
	
	
	float dotAmt = dot(viewVector, reflected);
	vec3 highlight = pow(clamp(dotAmt, 0, 1), 800) * vec3(1.2, 0.9, 0.7) * 100;
	highlight = clamp(highlight, 0, 1) * 0.45;
	highlight += pow(clamp(dotAmt, 0, 1), 40) * 0.25 * vec3(0.7, 0.9, 1.0);
	albedo.rgb += highlight * pow(fogAmount, 5);

	
	fragment_color[0] = vec4(albedo.rgb, fogAmount);
	fragment_color[1] = vec4(normal * 0.5 + 0.5, fogAmount);
	
	//fragment_color[0] = normal * 0.5 + 0.5;
	//fragment_color[0] = vec3(vertex_screenUv.xy, 0);
	//fragment_color[1] = vec3(0, 0, 1);

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = finalDepth;
#endif
}


