
#include "common.h"

uniform sampler2D albedoSampler;
uniform vec2 texelSize;

in vec2 vertex_texcoord;

out vec3 fragment_color;


const vec2 offsets13[13] = vec2[]
(
	vec2(0,-6),
	vec2(0,-5),
	vec2(0,-4),
	vec2(0,-3),
	vec2(0,-2),
	vec2(0,-1),
	vec2(0,0),
	vec2(0,1),
	vec2(0,2),
	vec2(0,3),
	vec2(0,4),
	vec2(0,5),
	vec2(0,6)
);

void main()
{

				
		vec3 albedo = vec3(0);
				
	for (int i = 0; i < 13; i++)
	{
		albedo += texture2D(albedoSampler, vertex_texcoord + texelSize * offsets13[i]).rgb * gauss13[i];
	}
	
	fragment_color = albedo;
}