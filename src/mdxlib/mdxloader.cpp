
#include "pch.h"

#include <stack>

#include "mdxlib\mdxformat.h"
#include "stringutil.h"


using namespace std;
using namespace Eigen;


static const char *MDXModel_Header = "MDLX";
static const char *MDXVersion_Header = "VERS";
static const char *MDXModelChunk_Header = "MODL";
static const char *MDXSequence_Header = "SEQS";

static const char *MDXMaterialAlpha_Header = "KMTA";
static const char *MDXMaterialTextureId_Header = "KMTF";

static const char *MDXLayerChunk_Header = "LAYS";
static const char *MDXMaterialChunk_Header = "MTLS";
static const char *MDXTextureChunk_Header = "TEXS";

static const char *MDXGeosetChunk_Header = "GEOS";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedVRTX = "VRTX";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedNRMS = "NRMS";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedPTYP = "PTYP";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedPCNT = "PCNT";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedPVTX = "PVTX";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedGNDX = "GNDX";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedMTGC = "MTGC";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedMATS = "MATS";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedUVAS = "UVAS";
const char *MDXGeosetChunk::MDXGeoset::pszExpectedUVBS = "UVBS";

static const char *MDXGeosetAlpha_Header = "KGAO";
static const char *MDXGeosetColor_Header = "KGAC";

static const char *MDXGeosetAnimationChunk_Header = "GEOA";

static const char *MDXGeosetTranslation_Header = "KGTR";
static const char *MDXGeosetRotation_Header = "KGRT";
static const char *MDXGeosetScaling_Header = "KGSC";

static const char *MDXBoneChunk_Header = "BONE";
static const char *MDXHelperChunk_Header = "HELP";
static const char *MDXAttachmentChunk_Header = "ATCH";
static const char *MDXAttachmentVisibility_Header = "KATV";
static const char *MDXPivotPointChunk_Header = "PIVT";
static const char *MDXGlobalSequenceChunk_Header = "GLBS";

static const char *MDXLightChunk_Header = "LITE";
const char *MDXLightChunk::MDXLightData::pszExpectedKLAV = "KLAV";
const char *MDXLightChunk::MDXLightData::pszExpectedKLAC = "KLAC";
const char *MDXLightChunk::MDXLightData::pszExpectedKLAI = "KLAI";
const char *MDXLightChunk::MDXLightData::pszExpectedKLBC = "KLBC";
const char *MDXLightChunk::MDXLightData::pszExpectedKLBI = "KLBI";


static bool CheckHeader(ifstream &file, const char *pszHeader)
{
	std::streamoff iLast = file.tellg();

	char szText[5] = { 0 };
	file.read(szText, sizeof(szText) - 1);
	szText[4] = 0;

	bool bMatch = _stricmp(pszHeader, szText) == 0;
	if (!bMatch)
		file.seekg(iLast);

	return bMatch;
}

static string ReadHeader(ifstream &file)
{
	char szText[5] = { 0 };
	file.read(szText, sizeof(szText) - 1);
	szText[4] = 0;

	return string( szText );
}

static bool ExpectHeader(ifstream &file, const char *pszHeader)
{
	bool bFound = CheckHeader(file, pszHeader);

	Assert(bFound);

	return bFound;
}

static Uint32 ReadUINT32(ifstream &file)
{
	Uint32 value;
	file.read((char*) &value, sizeof(Uint32));
	return value;
}

static float ReadFLOAT(ifstream &file)
{
	float value;
	file.read((char*) &value, sizeof(float));
	return value;
}

static vec3 ReadVEC3(ifstream &file)
{
	vec3 value;
	file.read((char*) &value, sizeof(vec3));
	return value;
}

static vec4 ReadVEC4(ifstream &file)
{
	vec4 value;
	file.read((char*) &value, sizeof(vec4));
	return value;
}

static quat ReadQUAT(ifstream &file)
{
	Assert(sizeof(quat) == sizeof(vec4));

	quat value;
	file.read((char*) &value, sizeof(quat));
	return value;
}

static MDXAnimationTrackUINT32 *ReadAnimationTrackUINT32(ifstream &file)
{
	MDXAnimationTrackUINT32 *pAnimation = new MDXAnimationTrackUINT32();

	pAnimation->NrOfTracks = ReadUINT32(file);
	pAnimation->InterpolationType = ReadUINT32(file);
	pAnimation->GlobalSequenceId = ReadUINT32(file);

	for (Uint32 iTrack = 0; iTrack < pAnimation->NrOfTracks; iTrack++)
	{
		MDXAnimationNodeUINT32 track;
		track.Time = ReadUINT32(file);
		track.Value = ReadUINT32(file);

		if (pAnimation->InterpolationType > 1)
		{
			track.InTan = ReadUINT32(file);
			track.OutTan = ReadUINT32(file);
		}

		pAnimation->Tracks.push_back(track);
	}

	return pAnimation;
}

static MDXAnimationTrackFLOAT *ReadAnimationTrackFLOAT(ifstream &file)
{
	MDXAnimationTrackFLOAT *pAnimation = new MDXAnimationTrackFLOAT();

	pAnimation->NrOfTracks = ReadUINT32(file);
	pAnimation->InterpolationType = ReadUINT32(file);
	pAnimation->GlobalSequenceId = ReadUINT32(file);

	for (Uint32 iTrack = 0; iTrack < pAnimation->NrOfTracks; iTrack++)
	{
		MDXAnimationNodeFLOAT track;
		track.Time = ReadUINT32(file);
		track.Value = ReadFLOAT(file);

		if (pAnimation->InterpolationType > 1)
		{
			track.InTan = ReadFLOAT(file);
			track.OutTan = ReadFLOAT(file);
		}

		pAnimation->Tracks.push_back(track);
	}

	return pAnimation;
}

static MDXAnimationTrackVEC3 *ReadAnimationTrackVEC3(ifstream &file)
{
	MDXAnimationTrackVEC3 *pAnimation = new MDXAnimationTrackVEC3();

	pAnimation->NrOfTracks = ReadUINT32(file);
	pAnimation->InterpolationType = ReadUINT32(file);
	pAnimation->GlobalSequenceId = ReadUINT32(file);

	for (Uint32 iTrack = 0; iTrack < pAnimation->NrOfTracks; iTrack++)
	{
		MDXAnimationNodeVEC3 track;
		track.Time = ReadUINT32(file);
		track.Value = ReadVEC3(file);

		if (pAnimation->InterpolationType > 1)
		{
			track.InTan = ReadVEC3(file);
			track.OutTan = ReadVEC3(file);
		}

		pAnimation->Tracks.push_back(track);
	}

	return pAnimation;
}

static MDXAnimationTrackQUAT *ReadAnimationTrackQUAT(ifstream &file)
{
	MDXAnimationTrackQUAT *pAnimation = new MDXAnimationTrackQUAT();

	pAnimation->NrOfTracks = ReadUINT32(file);
	pAnimation->InterpolationType = ReadUINT32(file);
	pAnimation->GlobalSequenceId = ReadUINT32(file);

	for (Uint32 iTrack = 0; iTrack < pAnimation->NrOfTracks; iTrack++)
	{
		MDXAnimationNodeQUAT track;
		track.Time = ReadUINT32(file);
		track.Value = ReadQUAT(file);

		if (pAnimation->InterpolationType > 1)
		{
			track.InTan = ReadQUAT(file);
			track.OutTan = ReadQUAT(file);
		}

		pAnimation->Tracks.push_back(track);
	}

	return pAnimation;
}

static void ExtractStruct(ifstream &file, void *pDst, int iSize)
{
	file.read((char*) pDst, iSize);
}

static MDXNode *ReadMDXNode(ifstream &file)
{
	MDXNode *pNode = new MDXNode();

	int iSize = sizeof(MDXNode);
	iSize -= sizeof(void*) * 3;

	ExtractStruct(file, pNode, iSize);

	for (;;)
	{
		if (CheckHeader(file, MDXGeosetTranslation_Header))
			pNode->pTranslation = ReadAnimationTrackVEC3(file);
		else if (CheckHeader(file, MDXGeosetRotation_Header))
			pNode->pRotation = ReadAnimationTrackQUAT(file);
		else if (CheckHeader(file, MDXGeosetScaling_Header))
			pNode->pScaling = ReadAnimationTrackVEC3(file);
		else
			break;
	}

	return pNode;
}

MDXDynamicChunk::MDXGeosetHelper *CreateGeosetHelper(MDXGeosetChunk::MDXGeoset *pGeoset)
{
	MDXDynamicChunk::MDXGeosetHelper *pHelper = new MDXDynamicChunk::MDXGeosetHelper();

	// use node index to figure out local index for this geoset
	vector<int> boneGlobalToLocal;

	auto &localBoneList = pHelper->NodeIndices;
	pHelper->NodeBoneIndices.resize(pGeoset->NrOfMatrixGroups);

	int matrixLookupIndex = 0;

	auto LbdAddGlobalBone = [&boneGlobalToLocal, &localBoneList](Uint32 globalBone) -> int
	{
		// TODO: optimize bones? (costs more on cpu)
		// no bone optimization, send all bones
		return globalBone;

		// remap to used bones only
		if (globalBone >= boneGlobalToLocal.size())
		{
			unsigned int oldSize = boneGlobalToLocal.size();
			boneGlobalToLocal.resize(globalBone + 1);
			unsigned int newSize = boneGlobalToLocal.size();

			Assert(newSize > oldSize);

			for (; oldSize < newSize; oldSize++)
				boneGlobalToLocal[oldSize] = -1;
		}

		if (boneGlobalToLocal[globalBone] == -1)
		{
			localBoneList.push_back(globalBone);
			boneGlobalToLocal[globalBone] = localBoneList.size() - 1;
		}

		return boneGlobalToLocal[globalBone];
	};

	for (Uint32 group = 0; group < pGeoset->NrOfMatrixGroups; group++)
	{
		Uint32 size = pGeoset->MatrixGroups[group].MatrixGroupSize;
		Assert(size >= 1 && size <= 3);

		int index = LbdAddGlobalBone(pGeoset->MatrixIndices[matrixLookupIndex++].MatrixIndex);
		pHelper->NodeBoneIndices[group] = vec3((float)index, (float)index, (float)index);

		if (size > 1)
		{
			index = LbdAddGlobalBone(pGeoset->MatrixIndices[matrixLookupIndex++].MatrixIndex);
			pHelper->NodeBoneIndices[group][1] = (float)index;
		}

		if (size > 2)
		{
			index = LbdAddGlobalBone(pGeoset->MatrixIndices[matrixLookupIndex++].MatrixIndex);
			pHelper->NodeBoneIndices[group][2] = (float) index;
		}
	}

	return pHelper;
}

template< typename T >
void CreateNodeAnimationHelpers(MDXAnimationTrack<T> &Node, const MDXSequenceChunk &Sequences)
{
	for (auto s : Sequences.Sequences)
	{
		MDXAnimationHelper helper;
		helper.startIndex = -1;
		helper.endIndex = -1;

		for (unsigned int i = 0; i < Node.Tracks.size(); i++)
		{
			MDXAnimationNode<T> &n = Node.Tracks[i];

			if (n.Time <= s.IntervalStart
				/*&& helper.startIndex < 0*/)
			{
				helper.startIndex = i;

				if (n.Time < s.IntervalStart
					&& i == Node.Tracks.size() - 1)
				{
					helper.startIndex = -1;
				}
			}

			if (n.Time <= s.IntervalEnd)
			{
				helper.endIndex = i;
			}
		}

		if (helper.endIndex <= helper.startIndex)
			helper.endIndex = -1;

		Node.Helpers.push_back(helper);
	}
}

void FinalizeModel(MDXModelData *model)
{
	Assert(model->pDynamic == nullptr);

	model->pDynamic = new MDXDynamicChunk();

	// create node list
	const auto &Bones = model->pBones->Bones;
	const auto &Helpers = model->pHelpers->Helpers;
	auto &Nodes = model->pDynamic->Nodes;

	for (unsigned int i = 0; i < Bones.size(); i++)
	{
		Nodes.push_back(Bones[i]->pNode);
	}

	for (unsigned int i = 0; i < Helpers.size(); i++)
	{
		for (unsigned int p = 0; p < Nodes.size(); p++)
		{
			if (i == p)
			{
				continue;
			}

			if (Nodes[p]->ParentId == Helpers[i]->ObjectId)
			{
				Nodes[p]->ParentId = model->pDynamic->Nodes.size();
			}
		}

		//Assert(model->pDynamic->Nodes.size() == Helpers[i]->ObjectId);
		Helpers[i]->ObjectId = Nodes.size();

		Nodes.push_back(Helpers[i]);
	}

	// create geoset node indices
	for (auto g : model->pGeosets->Geosets)
	{
		MDXDynamicChunk::MDXGeosetHelper *pGeosetHelper = CreateGeosetHelper(g);

		model->pDynamic->GeosetHelpers.push_back(pGeosetHelper);
	}

	// adjust texture paths
	for (auto &m : model->pTextures->Textures)
	{
		if (strlen(m.FileName) < 1)
			continue;

		char newFilename[ARRAYSIZE(m.FileName)];

		G_StrReplaceExtension(m.FileName, "xml");

		G_StrStripDirectory(m.FileName, "Textures");

		//G_StrSnPrintf(newFilename, sizeof(newFilename), "materials%cmodels%c%s",
		G_StrSnPrintf(newFilename, sizeof(newFilename), "models%c%s",
			PATHSEPARATOR, m.FileName);

		G_StrLower(newFilename);

		G_StrCpy(m.FileName, newFilename);
	}

	// create sequence helpers
	for (auto n : model->pDynamic->Nodes)
	{
		if (n->pRotation != nullptr)
			CreateNodeAnimationHelpers(*n->pRotation, *model->pSequences);
		if (n->pTranslation != nullptr)
			CreateNodeAnimationHelpers(*n->pTranslation, *model->pSequences);
		if (n->pScaling != nullptr)
			CreateNodeAnimationHelpers(*n->pScaling, *model->pSequences);
		
	}

	for (auto g : model->pGeosetAnimations->GeosetAnimations)
	{
		if (g->pGeosetAlpha != nullptr)
			CreateNodeAnimationHelpers(*g->pGeosetAlpha, *model->pSequences);
		if (g->pGeosetColor != nullptr)
			CreateNodeAnimationHelpers(*g->pGeosetColor, *model->pSequences);
	}
}

MDXModelData *LoadMDX(const char *filename)
{
	Assert(sizeof(vec2) == sizeof(float) * 2);
	Assert(sizeof(vec3) == sizeof(float) * 3);
	Assert(sizeof(vec4) == sizeof(float) * 4);
	Assert(sizeof(quat) == sizeof(float) * 4);
	Assert(sizeof(mat4) == sizeof(float) * 16);

	char fullPath[MAX_PATH_GAME];
	G_StrAbsContentPath(ContentDirectories::MODELS, filename, fullPath, sizeof(fullPath));

	ifstream file(fullPath, ios::in | ios::binary);

	if (!file.is_open())
		return nullptr;

	MDXModelData *pModel = new MDXModelData;

	if (!CheckHeader(file, MDXModel_Header))
		return nullptr;

	MDXModelData &model = *pModel; // TODO: cleanup

	stack<std::streamoff> offsetStack;

	for (;;)
	{
		string header = ReadHeader(file);
		const char *pszHeader = header.c_str();

		if (G_StrEq(pszHeader, MDXVersion_Header))
		{
			Assert(model.pVersion == nullptr);

			model.pVersion = new MDXVersionChunk();
			ExtractStruct(file, model.pVersion, sizeof(MDXVersionChunk));

			Assert(model.pVersion->Version == 800);
		}
		else if (G_StrEq(pszHeader, MDXModelChunk_Header))
		{
			Assert(model.pModel == nullptr);

			model.pModel = new MDXModelChunk();
			ExtractStruct(file, model.pModel, 376);
		}
		else if (G_StrEq(pszHeader, MDXSequence_Header))
		{
			Assert(model.pSequences == nullptr);

			model.pSequences = new MDXSequenceChunk();
			model.pSequences->ChunkSize = ReadUINT32(file);

			const int iSequenceCount = model.pSequences->ChunkSize / 132;

			model.pSequences->Sequences.resize(iSequenceCount);

			for (int i = 0; i < iSequenceCount; i++)
			{
				ExtractStruct(file, &model.pSequences->Sequences[i], 132);
			}
		}
		else if (G_StrEq(pszHeader, MDXMaterialChunk_Header))
		{
			Assert(model.pMaterials == nullptr);

			model.pMaterials = new MDXMaterialChunk();
			model.pMaterials->ChunkSize = ReadUINT32(file);

			int test = sizeof(MDXMaterialChunk::MDXMaterial);
			int test2 = sizeof(void*);

			if (model.pMaterials->ChunkSize > 0)
			{
				streamoff iMaterialChunkLeft = model.pMaterials->ChunkSize;

				while (iMaterialChunkLeft > 0)
				{
					offsetStack.push(file.tellg());

					MDXMaterialChunk::MDXMaterial *pMaterial = new MDXMaterialChunk::MDXMaterial();
					model.pMaterials->Materials.push_back(pMaterial);

					pMaterial->InclusiveSize = ReadUINT32(file);
					pMaterial->PriorityPlane = ReadUINT32(file);
					pMaterial->Flags = ReadUINT32(file);

					if (CheckHeader(file, MDXLayerChunk_Header))
					{
						MDXLayerChunk *pLayerChunk = new MDXLayerChunk();
						pMaterial->pLayer = pLayerChunk;

						pLayerChunk->NrOfLayers = ReadUINT32(file);

						for (unsigned int iLayer = 0; iLayer < pLayerChunk->NrOfLayers; iLayer++)
						{
							MDXLayerChunk::MDXLayer *pLayer = new MDXLayerChunk::MDXLayer();
							pLayerChunk->Layers.push_back(pLayer);

							int iLayerSize = sizeof(MDXLayerChunk::MDXLayer);
							iLayerSize -= sizeof(void*) * 2; // remove two pointers

							ExtractStruct(file, pLayer, iLayerSize);

							for (;;)
							{
								if (CheckHeader(file, MDXMaterialAlpha_Header))
									pLayer->pMaterialAlpha = ReadAnimationTrackFLOAT(file);
								else if (CheckHeader(file, MDXMaterialTextureId_Header))
									pLayer->pMaterialTexture = ReadAnimationTrackUINT32(file);
								else
									break;
							}
						}
					}

					iMaterialChunkLeft -= file.tellg() - offsetStack.top();
					offsetStack.pop();

					Assert(iMaterialChunkLeft >= 0);
				}
			}
		}
		else if (G_StrEq(pszHeader, MDXTextureChunk_Header))
		{
			Assert(model.pTextures == nullptr);

			model.pTextures = new MDXTextureChunk();
			model.pTextures->ChunkSize = ReadUINT32(file);

			streamoff iTextureChunkLeft = model.pTextures->ChunkSize;

			while (iTextureChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXTextureChunk::MDXTexture texture;
				ExtractStruct(file, &texture, sizeof(MDXTextureChunk::MDXTexture));
				model.pTextures->Textures.push_back(texture);

				iTextureChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iTextureChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXGeosetChunk_Header))
		{
			Assert(model.pGeosets == nullptr);

			model.pGeosets = new MDXGeosetChunk();
			model.pGeosets->ChunkSize = ReadUINT32(file);

			streamoff iGeosetChunkLeft = model.pGeosets->ChunkSize;

			while (iGeosetChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXGeosetChunk::MDXGeoset *pGeoset = new MDXGeosetChunk::MDXGeoset();
				model.pGeosets->Geosets.push_back(pGeoset);

				pGeoset->InclusiveSize = ReadUINT32(file);

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedVRTX);
				pGeoset->NrOfVertexPositions = ReadUINT32(file);

				for (Uint32 iVertexPosition = 0; iVertexPosition < pGeoset->NrOfVertexPositions; iVertexPosition++)
				{
					MDXGeosetChunk::MDXGeoset::MDXVertexPosition position;
					ExtractStruct(file, &position, 12);
					pGeoset->VertexPositions.push_back(position);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedNRMS);
				pGeoset->NrOfVertexNormals = ReadUINT32(file);

				for (Uint32 iVertexNormal = 0; iVertexNormal < pGeoset->NrOfVertexNormals; iVertexNormal++)
				{
					MDXGeosetChunk::MDXGeoset::MDXVertexNormal normal;
					ExtractStruct(file, &normal, 12);
					pGeoset->VertexNormals.push_back(normal);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedPTYP);
				pGeoset->NrOfFaceTypeGroups = ReadUINT32(file);

				for (Uint32 iFaceTypeGroup = 0; iFaceTypeGroup < pGeoset->NrOfFaceTypeGroups; iFaceTypeGroup++)
				{
					MDXGeosetChunk::MDXGeoset::MDXFaceTypeGroup faceTypeGroup;
					ExtractStruct(file, &faceTypeGroup, sizeof(MDXGeosetChunk::MDXGeoset::MDXFaceTypeGroup));
					pGeoset->FaceTypeGroups.push_back(faceTypeGroup);

					Assert(faceTypeGroup.FaceType == 4); // only full triangles are supposed to be supported
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedPCNT);
				pGeoset->NrOfFaceGroups = ReadUINT32(file);

				for (Uint32 iFaceGroup = 0; iFaceGroup < pGeoset->NrOfFaceGroups; iFaceGroup++)
				{
					MDXGeosetChunk::MDXGeoset::MDXFaceGroup faceGroup;
					ExtractStruct(file, &faceGroup, sizeof(MDXGeosetChunk::MDXGeoset::MDXFaceGroup));
					pGeoset->FaceGroups.push_back(faceGroup);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedPVTX);
				pGeoset->TotalNrOfIndexes = ReadUINT32(file);

				const Uint32 iTotalNrOfFaces = pGeoset->TotalNrOfIndexes / 3;
				Assert(pGeoset->TotalNrOfIndexes % 3 == 0);

				for (Uint32 iFace = 0; iFace < iTotalNrOfFaces; iFace++)
				{
					MDXGeosetChunk::MDXGeoset::MDXFace face;
					ExtractStruct(file, &face, sizeof(MDXGeosetChunk::MDXGeoset::MDXFace));
					pGeoset->Faces.push_back(face);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedGNDX);
				pGeoset->NrOfVertexGroups = ReadUINT32(file);

				for (Uint32 iVertexGroup = 0; iVertexGroup < pGeoset->NrOfVertexGroups; iVertexGroup++)
				{
					MDXGeosetChunk::MDXGeoset::MDXVertexGroup vertexGroup;
					ExtractStruct(file, &vertexGroup, sizeof(MDXGeosetChunk::MDXGeoset::MDXVertexGroup));
					pGeoset->VertexGroups.push_back(vertexGroup);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedMTGC);
				pGeoset->NrOfMatrixGroups = ReadUINT32(file);

				for (Uint32 iMatrixGroup = 0; iMatrixGroup < pGeoset->NrOfMatrixGroups; iMatrixGroup++)
				{
					MDXGeosetChunk::MDXGeoset::MDXMatrixGroup matrixGroup;
					ExtractStruct(file, &matrixGroup, sizeof(MDXGeosetChunk::MDXGeoset::MDXMatrixGroup));
					pGeoset->MatrixGroups.push_back(matrixGroup);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedMATS);
				pGeoset->NrOfMatrixIndexes = ReadUINT32(file);

				for (Uint32 iMatrixIndex = 0; iMatrixIndex < pGeoset->NrOfMatrixIndexes; iMatrixIndex++)
				{
					MDXGeosetChunk::MDXGeoset::MDXMatrixIndex matrixIndex;
					ExtractStruct(file, &matrixIndex, sizeof(MDXGeosetChunk::MDXGeoset::MDXMatrixIndex));
					pGeoset->MatrixIndices.push_back(matrixIndex);
				}

				pGeoset->MaterialId = ReadUINT32(file);
				pGeoset->SelectionGroup = ReadUINT32(file);
				pGeoset->SelectionFlags = ReadUINT32(file);

				pGeoset->BoundsRadius = ReadFLOAT(file);
				pGeoset->MinimumExtent = ReadVEC3(file);
				pGeoset->MaximumExtent = ReadVEC3(file);

				Assert(sizeof(vec3) == sizeof(float) * 3);

				pGeoset->NrOfExtents = ReadUINT32(file);
				for (Uint32 iExtent = 0; iExtent < pGeoset->NrOfExtents; iExtent++)
				{
					MDXGeosetChunk::MDXGeoset::MDXExtent extent;
					ExtractStruct(file, &extent, 28);
					pGeoset->Extents.push_back(extent);
				}

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedUVAS);
				pGeoset->NrOfTextureVertexGroups = ReadUINT32(file);

				ExpectHeader(file, MDXGeosetChunk::MDXGeoset::pszExpectedUVBS);
				pGeoset->NrOfVertexTexturePositions = ReadUINT32(file);

				for (Uint32 iUV = 0; iUV < pGeoset->NrOfVertexTexturePositions; iUV++)
				{
					MDXGeosetChunk::MDXGeoset::MDXVertexTexturePosition uv;
					ExtractStruct(file, &uv, 8);
					pGeoset->VertexTexturePositions.push_back(uv);
				}

				iGeosetChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iGeosetChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXGeosetAnimationChunk_Header))
		{
			Assert(model.pGeosetAnimations == nullptr);

			model.pGeosetAnimations = new MDXGeosetAnimationChunk();
			model.pGeosetAnimations->ChunkSize = ReadUINT32(file);

			streamoff iGeoAnimChunkLeft = model.pGeosetAnimations->ChunkSize;

			while (iGeoAnimChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXGeosetAnimationChunk::MDXGeosetAnimation *pGeoAnim = new MDXGeosetAnimationChunk::MDXGeosetAnimation();
				model.pGeosetAnimations->GeosetAnimations.push_back(pGeoAnim);

				int iGeoAnimSize = 28;

				ExtractStruct(file, pGeoAnim, iGeoAnimSize);

				for (;;)
				{
					if (CheckHeader(file, MDXGeosetAlpha_Header))
						pGeoAnim->pGeosetAlpha = ReadAnimationTrackFLOAT(file);
					else if (CheckHeader(file, MDXGeosetColor_Header))
						pGeoAnim->pGeosetColor = ReadAnimationTrackVEC3(file);
					else
						break;
				}

				iGeoAnimChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iGeoAnimChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXBoneChunk_Header))
		{
			Assert(model.pBones == nullptr);

			model.pBones = new MDXBoneChunk();
			model.pBones->ChunkSize = ReadUINT32(file);

			streamoff iBoneChunkLeft = model.pBones->ChunkSize;

			while (iBoneChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXBoneChunk::MDXBone *pBone = new MDXBoneChunk::MDXBone();
				model.pBones->Bones.push_back(pBone);

				pBone->pNode = ReadMDXNode(file);
				pBone->GeosetId = ReadUINT32(file);
				pBone->GeosetAnimationId = ReadUINT32(file);

				iBoneChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iBoneChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXHelperChunk_Header))
		{
			Assert(model.pHelpers == nullptr);

			model.pHelpers = new MDXHelperChunk();
			model.pHelpers->ChunkSize = ReadUINT32(file);

			streamoff iHelperChunkLeft = model.pHelpers->ChunkSize;

			while (iHelperChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXNode *pHelper = ReadMDXNode(file);
				model.pHelpers->Helpers.push_back(pHelper);

				iHelperChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iHelperChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXAttachmentChunk_Header))
		{
			Assert(model.pAttachments == nullptr);

			model.pAttachments = new MDXAttachmentChunk();
			model.pAttachments->ChunkSize = ReadUINT32(file);

			streamoff iAtchChunkLeft = model.pAttachments->ChunkSize;

			while (iAtchChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXAttachmentChunk::MDXAttachment *pAtch = new MDXAttachmentChunk::MDXAttachment();
				model.pAttachments->Attachments.push_back(pAtch);

				pAtch->InclusiveSize = ReadUINT32(file);
				pAtch->pNode = ReadMDXNode(file);
				file.read(pAtch->Path, sizeof(pAtch->Path));
				pAtch->AttachmentId = ReadUINT32(file);

				if (CheckHeader(file, MDXAttachmentVisibility_Header))
				{
					pAtch->pVisibility = ReadAnimationTrackFLOAT(file);
				}

				iAtchChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iAtchChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXPivotPointChunk_Header))
		{
			Assert(model.pPivots == nullptr);

			model.pPivots = new MDXPivotPointChunk();
			model.pPivots->ChunkSize = ReadUINT32(file);

			streamoff iPivtChunkLeft = model.pPivots->ChunkSize;

			while (iPivtChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				vec3 point = ReadVEC3(file);
				model.pPivots->Pivots.push_back(point);

				iPivtChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iPivtChunkLeft >= 0);
			}
		}
		else if (G_StrEq(pszHeader, MDXGlobalSequenceChunk_Header))
		{
			Assert(model.pGlobalSequences == nullptr);

			model.pGlobalSequences = new MDXGlobalSequence();
			model.pGlobalSequences->ChunkSize = ReadUINT32(file);

			int count = model.pGlobalSequences->ChunkSize / 4;

			for (int i = 0; i < count; i++)
			{
				model.pGlobalSequences->Durations.push_back(ReadUINT32(file));
			}
		}
		else if (G_StrEq(pszHeader, MDXLightChunk_Header))
		{
			Assert(model.pLights == nullptr);

			model.pLights = new MDXLightChunk();
			model.pLights->ChunkSize = ReadUINT32(file);

			streamoff iLiteChunkLeft = model.pLights->ChunkSize;

			while (iLiteChunkLeft > 0)
			{
				offsetStack.push(file.tellg());

				MDXLightChunk::MDXLightData *pLite = new MDXLightChunk::MDXLightData();
				model.pLights->Lights.push_back(pLite);

				pLite->InclusiveSize = ReadUINT32(file);

				pLite->pNode = ReadMDXNode(file);

				pLite->Type = ReadUINT32(file);
				pLite->AttenuationStart = ReadUINT32(file);
				pLite->AttenuationEnd = ReadUINT32(file);
				pLite->Color = ReadVEC3(file);
				pLite->Intensity = ReadFLOAT(file);
				pLite->AmbientColor = ReadVEC3(file);
				pLite->AmbientIntensity = ReadFLOAT(file);

				while (file.tellg() - offsetStack.top() < pLite->InclusiveSize)
				{
					if (CheckHeader(file, MDXLightChunk::MDXLightData::pszExpectedKLAV))
					{
						Assert(pLite->pLightVisibility == nullptr);
						pLite->pLightVisibility = ReadAnimationTrackFLOAT(file);
					}
					else if (CheckHeader(file, MDXLightChunk::MDXLightData::pszExpectedKLAI))
					{
						Assert(pLite->pLightIntensity == nullptr);
						pLite->pLightIntensity = ReadAnimationTrackFLOAT(file);
					}
					else if (CheckHeader(file, MDXLightChunk::MDXLightData::pszExpectedKLBI))
					{
						Assert(pLite->pLightAmbientIntensity == nullptr);
						pLite->pLightAmbientIntensity = ReadAnimationTrackFLOAT(file);
					}
					else if (CheckHeader(file, MDXLightChunk::MDXLightData::pszExpectedKLAC))
					{
						Assert(pLite->pLightColor == nullptr);
						pLite->pLightColor = ReadAnimationTrackVEC3(file);
					}
					else if (CheckHeader(file, MDXLightChunk::MDXLightData::pszExpectedKLBC))
					{
						Assert(pLite->pLightAmbientColor == nullptr);
						pLite->pLightAmbientColor = ReadAnimationTrackVEC3(file);
					}
				}

				iLiteChunkLeft -= file.tellg() - offsetStack.top();
				offsetStack.pop();

				Assert(iLiteChunkLeft >= 0);
			}
		}
		else
		{
			break;
		}
	}

	// check required mdx chunks
	Assert(pModel->pAttachments);
	Assert(pModel->pBones);
	Assert(pModel->pGeosetAnimations);
	Assert(pModel->pGeosets);
	Assert(pModel->pHelpers);
	Assert(pModel->pMaterials);
	Assert(pModel->pModel);
	Assert(pModel->pPivots);
	Assert(pModel->pSequences);
	Assert(pModel->pTextures);
	Assert(pModel->pVersion);

	if (pModel->pAttachments == nullptr
		|| pModel->pBones == nullptr
		|| pModel->pGeosetAnimations == nullptr
		|| pModel->pGeosets == nullptr
		|| pModel->pHelpers == nullptr
		|| pModel->pMaterials == nullptr
		|| pModel->pModel == nullptr
		|| pModel->pPivots == nullptr
		|| pModel->pSequences == nullptr
		|| pModel->pTextures == nullptr
		|| pModel->pVersion == nullptr)
		return nullptr;

	FinalizeModel(pModel);

	return pModel;
}