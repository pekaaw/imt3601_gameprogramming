#include "pch.h"
#include "util\testutil.h"


BEGIN_TEST_SUITE()
{
	RUN_TEST(ByteBufferDynamic);
	RUN_TEST(ByteBufferStatic);
	RUN_TEST(ByteBufferSwap);

	RUN_TEST(MathTestEulerAnglesUnit);
	RUN_TEST(MathTestEulerAnglesOffset);

	RUN_TEST(ThreadLocking);
	RUN_TEST(ThreadLockingUtility);

	RUN_TEST(EventTest);
	RUN_TEST(EventScopeTest);
	RUN_TEST(EventCopyTest);
}
END_TEST_SUITE()
