#include "pch.h"
#include "gui/layoutpanel.h"

using namespace GUI;

LayoutPanel::~LayoutPanel()
{
}

LayoutPanel::LayoutPanel()
{
}

void LayoutPanel::InvokeResize()
{
	Resize(renderContext->GetScreenWidth(), renderContext->GetScreenHeight());
}

void LayoutPanel::Resize(int width, int height)
{
	Control::Resize(width, height);

	SetPositions(width, height);
}
