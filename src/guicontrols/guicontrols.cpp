
#include "pch.h"

#include "gui/igui.h"

namespace GUI
{
	IRenderContext *renderContext;
	IFontManager *fontManager;
	IGraphics *graphics;
	IGUI *gui;
	IInput *input;

	void Init()
	{
		//AssertCall(GetAppInterface()->LoadSharedLibrary("gui"));

		gui = (IGUI *)GetAppInterface()->QueryApp(INTERFACE_IGUI_VERSION);

		Assert(gui);

		gui->Init();
		renderContext = (IRenderContext *)GetAppInterface()->QueryApp(INTERFACE_IRENDERCONTEXT_VERSION);
		fontManager = (IFontManager *)GetAppInterface()->QueryApp(INTERFACE_IFONTMANAGER_VERSION);
		graphics = (IGraphics *)GetAppInterface()->QueryApp(INTERFACE_IGRAPHICS_VERSION);
		input = (IInput *)GetAppInterface()->QueryApp(INTERFACE_IINPUT_VERSION);

		Assert(renderContext);
		Assert(fontManager);
		Assert(graphics);
		Assert(input);
	}

	void Shutdown()
	{
		Assert(gui);

		gui->Shutdown();
	}
}
