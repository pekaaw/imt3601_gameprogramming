#include "pch.h"
#include "gui/panel.h"

using namespace GUI;

Panel::Panel() : borderColor(0, 0, 0, 0), mouseIn(false), leftMouseDown(false), mat(nullptr)
{
}

void Panel::UpdateSelf()
{
	Rect rect = GetRecursiveRectPos();

	//Enter / Leave checks
	if (rect.Contains(input->GetMousePosX(), input->GetMousePosY()))
	{
		if (!mouseIn)
		{
			mouseIn = true;
			Enter(this);
		}
	}
	else
	{
		if (mouseIn)
		{
			mouseIn = false;
			Leave(this);
		}
	}
}

void Panel::DrawSelf()
{
	int w = renderContext->GetScreenWidth();
	int h = renderContext->GetScreenHeight();
	
	if (color.a > 0)
	{
		if (mat == nullptr)
		{
			graphics->SetDrawColor(color.r, color.g, color.b, color.a);
			graphics->DrawFilledRect(0, 0, rect.w, rect.h);
		}
		else
		{
			graphics->DrawTexturedRect(0, 0, rect.w, rect.h, mat);
		}
	}

	//Draw border
	if (borderColor.a != 0)
	{
		graphics->SetDrawColor(borderColor.r, borderColor.g, borderColor.b, borderColor.a);
		graphics->DrawRect(0, 0, rect.w, rect.h);
	}
}

void Panel::SetMaterial(IMaterial *mat)
{
	if (this->mat != nullptr)
	{
		this->mat->DecrementReferenceCount();
	}

	this->mat = mat;
	this->mat->IncrementReferenceCount();
}

Panel::~Panel()
{
	if (this->mat != nullptr)
	{
		this->mat->DecrementReferenceCount();
	}
}

void Panel::OnMouseDown(int mouseButton)
{
	if (mouseButton == SDL_BUTTON_LEFT)
	{
		Click(this);
	}
}

void Panel::OnMouseUp(int mouseButton)
{
}
