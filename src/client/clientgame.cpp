
#include "pch.h"
#include "clientgame.h"
#include "c_towerhint.h"

SINGLETON_INSTANCE(ClientGame);

ClientGame::ClientGame()
	: overlayMode(OVERLAY_NONE)
	, overlayTowerType(0)
	, towerHint(nullptr)
	, lastOverlayTile(-1)
	, pathFullyBlocked(false)
{
}

ClientGame::~ClientGame()
{
}

void ClientGame::Update(float frametime)
{
	const bool shouldShowTower = overlayMode == OVERLAY_PLACE_TOWER;
	bool isShowingTower = towerHint != nullptr;

	if (shouldShowTower != isShowingTower)
	{
		if (shouldShowTower)
		{
			Assert(VECTOR_IS_INDEX_VALID(Resources::GetInstance()->GetTowers(), overlayTowerType));

			towerHint = assert_cast<C_TowerHint*>(ClientEntityFactory::GetInstance()->CreateClientEntity("towerhint", -1));
			Assert(towerHint != nullptr);

			towerHint->SetVisible(false);

			towerHint->InitFromScript(Resources::GetInstance()->GetTowers()[overlayTowerType]);

			lastOverlayTile = -1;
			isShowingTower = true;
		}
		else
		{
			towerHint->Release();
			towerHint = nullptr;
		}
	}

	IWorld *world = engine->GetWorld();

	if (shouldShowTower
		&& world != nullptr)
	{
		vec3 point;

		if (IntersectScreenRayWithWorld(input->GetMousePosX(), input->GetMousePosY(),
			renderContext->GetScreenWidth(), renderContext->GetScreenHeight(),
			renderContext->GetMainViewProjectionMatrix(), point))
		{
			int selectedTile = world->GetTileIndexAtPosition(point);
			const bool selectedTileIsValid = selectedTile >= 0;

			if (selectedTile != lastOverlayTile)
			{
				lastOverlayTile = selectedTile;

				vec3 tileCenter = world->GetTilePosition(selectedTile);
				tileCenter += vec3(WORLD_TILE_SCALE * 0.5f, WORLD_TILE_SCALE * 0.5f, 0.0f);
				tileCenter.z() = world->GetElevationHeight(tileCenter, true);

				towerHint->SetOrigin(tileCenter);
				towerHint->SetVisible(selectedTile >= 0);

				if (selectedTileIsValid)
				{
					const bool wasBlocked = pathFinder.IsTileBlocked(selectedTile);

					pathFinder.SetTileBlocked(selectedTile, true);
					pathFullyBlocked = pathFinder.IsAnyPairBlocked();
					pathFinder.SetTileBlocked(selectedTile, wasBlocked);
				}
			}

			if (selectedTileIsValid)
			{
				Assert(towerHint != nullptr);

				const bool isBlocked = IsTileBlocked(selectedTile);

				towerHint->SetColor(isBlocked ? vec3(1.0f, 0.2f, 0.05f) : vec3(0.1f, 1.0f, 0.15f));
			}
		}
	}
}

void ClientGame::OnLevelInit()
{
	overlayMode = OVERLAY_NONE;

	pathFinder.Init(engine->GetWorld());
}

void ClientGame::OnLevelShutdown()
{
	overlayMode = OVERLAY_NONE;

	if (towerHint != nullptr)
	{
		towerHint->Release();
		towerHint = nullptr;
	}
}

void ClientGame::SetOverlayMode(OverlayMode_e mode)
{
	// don't set this if not in-game!
	if (engine->GetWorld() == nullptr)
	{
		Assert(0);
		return;
	}

	overlayMode = mode;
}

void ClientGame::SetOverlayTowerType(int type)
{
	// cause ent to respawn so the new type is applied
	if (overlayTowerType != type
		&& towerHint != nullptr)
	{
		towerHint->Release();
		towerHint = nullptr;
	}

	overlayTowerType = type;
}

bool ClientGame::IsTileBlocked(int tileIndex)
{
	return pathFinder.GetNodeCreepCount(tileIndex) > 0
			|| pathFinder.IsTileBlocked(tileIndex)
			|| pathFullyBlocked;
}

void ClientGame::SetTileBlocked(int tileIndex, bool isBlocked)
{
	pathFinder.SetTileBlocked(tileIndex, isBlocked);
}

void ClientGame::ModifyNodeCreepCount(int tileIndex, int delta)
{
	pathFinder.ModifyNodeCreepCount(tileIndex, delta);
}