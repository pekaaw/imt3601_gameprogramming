#ifndef CLIENT_H
#define CLIENT_H

#include "client\iclient.h"
#include "util\singleton.h"

class Client : public IClient
{
	DECLARE_SINGLETON(Client);

public:

	virtual bool Init(bool enableGUI);

	virtual void Shutdown();

	virtual void Simulate();

	virtual void OnLevelInit();
	virtual void OnLevelShutdown();

	virtual void ApplyNetworkSymbolDefinitions(ByteBuffer &buffer);
	virtual void CreateNetworkEntity(int networkId, Uint8 networkTableIndex, ByteBuffer &buffer);
	virtual void DestroyNetworkEntity(int networkId);

	virtual void ApplyWorldStateUpdates(ByteBuffer &buffer);
	virtual void ApplyWorldStateComplete(ByteBuffer &buffer);

	virtual void SetLocalClientIndex(int clientIndex);
	int GetLocalClientIndex();

	virtual void RenderBegin();
	virtual void RenderScene();
	virtual void RenderEnd();

private:

	int localClientIndex;
};


#endif