#include "pch.h"
#include "c_creepbase.h"
#include "clientgame.h"
#include "view.h"
#include "engine/iworld.h"

REGISTER_CLIENTENTITY_CLASS(creep, C_CreepBase);

IMPLEMENT_NETWORKTABLE_BASE(C_CreepBase)
NETWORK_VARIABLE(creepState);
NETWORK_PROCEDURE(BloodDecal);
NETWORK_VARIABLE(scale);
NETWORK_VARIABLE(maxSpeed);
NETWORK_VARIABLE(color);
END_NETWORKTABLE();

C_CreepBase::C_CreepBase()
	: lastRenderOrigin(vec3::Zero())
	  , yaw(0.0f)
	  , yawDesired(0.0f)
	  , speed(0.0f)
	  , animatedAngles(euler::Zero())
	  , CreepStateChanged(this, &C_CreepBase::OnCreepStateChanged)
	  , elevationHeight(0.0f)
	  , lastTileIndex(-1)
	  , BloodDecal(this, &C_CreepBase::BloodDecalCallback)
{
	creepState.AddCallback(&CreepStateChanged);
}

C_CreepBase::~C_CreepBase()
{
}

void C_CreepBase::BloodDecalCallback(const vec3 &origin)
{
	DBGMSGF("Received blood decal: %f %f %f\n", XYZ(origin));

	vec3 delta = origin - GetRenderOrigin();
	delta.norm();

	float pitch = randfRange(-60.0f, -45.0f);
	float yaw = RAD2DEG(atan2(delta.y(), delta.x()));
	float roll = randfRange(0.0f, 360.0f);

	float offsetZ = randfRange(28.0f, 48.0f);
	float size = randfRange(32.0f, 64.0f);

	IMesh *mesh = engine->GetWorld()->CreateProjectedDecal(origin + vec3(0, 0, offsetZ),
		euler(pitch, yaw, roll),
		vec3(500, size, size));

	if (mesh == nullptr)
	{
		return;
	}

	IMaterial *material = materialDict->FindMaterial("decals/blood_00");
	material->IncrementReferenceCount();

	View::GetInstance()->AddDecal(mesh, material, 10.0f);
}

void C_CreepBase::OnSpawn()
{
	BaseClass::OnSpawn();

	SetModel("units/footman.mdx");
}

void C_CreepBase::OnRelease()
{
	BaseClass::OnRelease();

	if (lastTileIndex >= 0)
	{
		ClientGame::GetInstance()->ModifyNodeCreepCount(lastTileIndex, -1);
	}
}

void C_CreepBase::Simulate()
{
	const vec3 &renderOrigin = GetRenderOrigin();

	if (renderOrigin != lastRenderOrigin)
	{
		vec3 delta = renderOrigin - lastRenderOrigin;

		speed = (delta.norm() * 0.005f) / globals->GetFrametime();

		if (lastRenderOrigin.isZero())
		{
			speed = 0.0f;
		}
		else
		{
			speed = MIN(2.0f, speed);
		}

		if (speed > 0.01f)
		{
			yawDesired = (atan2(delta.y(), delta.x()));
		}

		lastRenderOrigin = renderOrigin;
	}

	if (yaw != yawDesired)
	{
		yaw = ApproachAngleRadians(yawDesired, yaw, globals->GetFrametime() * speed * 1.5f);
	}

	animatedAngles[1] = RAD2DEG(yaw);

	if (creepState.Get() == CreepStates::WALKING)
	{
		GetModel()->SetPlaybackRate(speed * maxSpeed.Get() / 500.0f);
	}
	else
	{
		GetModel()->SetPlaybackRate(1.0f);
	}

	UpdateTileChange();

	BaseClass::Simulate();

	elevationHeight = engine->GetWorld()->GetElevationHeight(BaseClass::GetRenderOrigin());
}

void C_CreepBase::Render()
{
	renderContext->SetRenderParamVec3(RenderParams::VEC3_MODEL_TINT_COLOR, color.Get());

	BaseClass::Render();
}

const vec3 &C_CreepBase::GetRenderAngles()
{
	return animatedAngles;
}

const vec3 &C_CreepBase::GetRenderOrigin()
{
	elevatedOrigin = BaseClass::GetRenderOrigin();

	elevatedOrigin.z() = elevationHeight;

	return elevatedOrigin;
}

void C_CreepBase::OnCreepStateChanged(Uint8 *value)
{
	IMDXRenderable *model = GetModel();

	Assert(model != nullptr);

	switch (*value)
	{
	case CreepStates::WALKING:
		model->SetSequence("Walk");
		break;
	case CreepStates::IDLE:
		if (randRange(0, 5))
		{
			model->SetSequence("Stand - 1");
		}
		else
		{
			model->SetSequence("Stand Victory");
		}

		model->SetCycle(randf());
		break;
	case CreepStates::DEAD:
		model->SetSequence("Death");
		break;
	}
}

void C_CreepBase::UpdateTileChange()
{
	int newTileIndex = engine->GetWorld()->GetTileIndexAtPosition(GetOrigin());

	if (newTileIndex == lastTileIndex)
	{
		return;
	}

	if (lastTileIndex >= 0)
	{
		ClientGame::GetInstance()->ModifyNodeCreepCount(lastTileIndex, -1);
	}

	if (newTileIndex >= 0)
	{
		ClientGame::GetInstance()->ModifyNodeCreepCount(newTileIndex, 1);
	}

	lastTileIndex = newTileIndex;
}
