#include "pch.h"
#include "view.h"
#include "camera.h"
#include "engine\iworld.h"

SINGLETON_INSTANCE(View);

//inline void VectorAngles(const vec3 &fwd, euler &angles)
//{
//	angles.y() = RAD2DEG(atan2(fwd.x(), fwd.y()));
//	angles.x() = RAD2DEG(acos(fwd.z()/fwd.norm()));
//	angles.z() = 0.0f;
//}

CON_COMMAND(dbg_toggleentitybounds)
{
	View::GetInstance()->SetShowingEntityBounds(!View::GetInstance()->IsShowingEntityBounds());
}

View::View()
	: fboGBuffer(FBO_HANDLE_INVALID)
	  , frameBufferCopy(FBO_HANDLE_INVALID)
	  , frameBufferQuarter(FBO_HANDLE_INVALID)
	  , frameBufferQuarter2(FBO_HANDLE_INVALID)
	  , lightBuffer(FBO_HANDLE_INVALID)
	  , shadowmapBuffer(FBO_HANDLE_INVALID)
	  , fboDepthCopy(FBO_HANDLE_INVALID)
	  , showEntityBounds(false)
	  , requestScreenshot(false)
	  , OnScreenshotCallback(this, &View::OnScreenshot, "r_screenshot", CvarExecutionTypes::CLIENT)
	  , OnCycleDebugRendererCallback(this, &View::OnCycleDebugRenderer, "dbg_renderer", CvarExecutionTypes::CLIENT)
	  , currentDebugIndex(-1)
{
}

View::~View()
{
}

void View::Init()
{
	// create all required FBOs
	const int rtFlags = TextureFlags::CLAMP_S | TextureFlags::CLAMP_T | TextureFlags::NO_MIPMAP | TextureFlags::NO_FILTERING;

	// main scene gbuffer: albedo (rgb), world space normals (rgb), depth (r16f)
	fboGBuffer = fboDict->CreateFBO();
	RT_HANDLE rt0 = fboDict->CreateRenderTarget("__framebuffer", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);
	RT_HANDLE rt1 = fboDict->CreateRenderTarget("__framebuffer_1", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);
	RT_HANDLE rt2 = fboDict->CreateRenderTarget("__framebuffer_depth", TextureFormats::DEPTH_16F, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(fboGBuffer);
	fboDict->BindRenderTargetColor(rt0, 0);
	fboDict->BindRenderTargetColor(rt1, 1);
	fboDict->BindRenderTargetDepth(rt2);
	fboDict->PopFBO();


	fboGBufferSmall = fboDict->CreateFBO();
	fboDict->PushFBO(fboGBufferSmall);
	fboDict->BindRenderTargetColor(rt0, 0);
	fboDict->BindRenderTargetDepth(rt2);
	fboDict->PopFBO();


	// framebuffer copy fbo for complete scene (useful for post processing)
	frameBufferCopy = fboDict->CreateFBO();
	RT_HANDLE frameBufferCopyRt0 = fboDict->CreateRenderTarget("__framebuffer_copy", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferCopy);
	fboDict->BindRenderTargetColor(frameBufferCopyRt0, 0);
	fboDict->BindRenderTargetDepth(rt2);
	fboDict->PopFBO();


	// quarter sized framebuffer, 1 of 2
	frameBufferQuarter = fboDict->CreateFBO();
	RT_HANDLE frameBufferQuarterRt0 = fboDict->CreateRenderTarget("__framebuffer_quarter", TextureFormats::RGB_8, FramebufferResizeModes::QUARTER_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferQuarter);
	fboDict->BindRenderTargetColor(frameBufferQuarterRt0, 0);
	fboDict->PopFBO();


	// quarter sized framebuffer, 2 of 2 (for ping-pong effects i.e. gaussian blur in two passes)
	frameBufferQuarter2 = fboDict->CreateFBO();
	RT_HANDLE frameBufferQuarter2Rt0 = fboDict->CreateRenderTarget("__framebuffer_quarter_2", TextureFormats::RGB_8, FramebufferResizeModes::QUARTER_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferQuarter2);
	fboDict->BindRenderTargetColor(frameBufferQuarter2Rt0, 0);
	fboDict->PopFBO();


	// copy of gbuffer depth
	fboDepthCopy = fboDict->CreateFBO();
	RT_HANDLE depthCopyRt0 = fboDict->CreateRenderTarget("__framebuffer_depth_copy", TextureFormats::DEPTH_16F, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(fboDepthCopy);
	fboDict->BindRenderTargetColor(frameBufferCopyRt0, 0);
	fboDict->BindRenderTargetDepth(depthCopyRt0);
	fboDict->PopFBO();



	fboReflection = fboDict->CreateFBO();
	RT_HANDLE fboReflectionRt0 = fboDict->CreateRenderTarget("__framebuffer_reflection", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(fboReflection);
	fboDict->BindRenderTargetColor(fboReflectionRt0, 0);
	fboDict->BindRenderTargetDepth(depthCopyRt0);
	fboDict->PopFBO();


	// light accumulation buffer for deferred lights
	lightBuffer = fboDict->CreateFBO();
	RT_HANDLE lightRt0 = fboDict->CreateRenderTarget("__lightbuffer", TextureFormats::RGB_16F, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(lightBuffer);
	fboDict->BindRenderTargetColor(lightRt0, 0);
	fboDict->PopFBO();


	// directional shadow map for sun light
	shadowmapBuffer = fboDict->CreateFBO();
	RT_HANDLE shadowmapRt0 = fboDict->CreateRenderTarget("__shadowmap_global", TextureFormats::DEPTH_16F, FramebufferResizeModes::OFFSCREEN,
														 SHADOWMAP_RESOLUTION, SHADOWMAP_RESOLUTION, rtFlags);

	fboDict->PushFBO(shadowmapBuffer);
	fboDict->BindRenderTargetDepth(shadowmapRt0);
	fboDict->PopFBO();


	sceneMaterials[MATERIAL_BLOOM_COMBINE] = materialDict->FindMaterial("postprocessing/bloom_combine");
	sceneMaterials[MATERIAL_LIGHT_COMBINE] = materialDict->FindMaterial("postprocessing/light_combine");
	sceneMaterials[MATERIAL_LIGHT_POINT] = materialDict->FindMaterial("postprocessing/light_point_world");
	sceneMaterials[MATERIAL_DOWNSAMPLE4] = materialDict->FindMaterial("postprocessing/downsample4");
	sceneMaterials[MATERIAL_GAUSSBLUR_13V] = materialDict->FindMaterial("postprocessing/gaussblur13v");
	sceneMaterials[MATERIAL_GAUSSBLUR_13H] = materialDict->FindMaterial("postprocessing/gaussblur13h");
	sceneMaterials[MATERIAL_PASSTHROUGH_FRAMEBUFFER] = materialDict->FindMaterial("postprocessing/debug_gbuffer_albedo");
	sceneMaterials[MATERIAL_CLOUDS] = materialDict->FindMaterial("maps/clouds");
	sceneMaterials[MATERIAL_CLOUDS_REFLECTION] = materialDict->FindMaterial("maps/clouds_reflected");

	sceneMaterials[MATERIAL_DBG_LIGHT] = materialDict->FindMaterial("postprocessing/light_combine");
	sceneMaterials[MATERIAL_DBG_BLOOM] = materialDict->FindMaterial("postprocessing/debug_bloom");
	sceneMaterials[MATERIAL_DBG_NORMALS] = materialDict->FindMaterial("postprocessing/debug_gbuffer_normals");
	sceneMaterials[MATERIAL_DBG_DEPTH] = materialDict->FindMaterial("postprocessing/debug_gbuffer_depth");
	sceneMaterials[MATERIAL_DBG_ALBEDO] = materialDict->FindMaterial("postprocessing/debug_gbuffer_albedo");
	sceneMaterials[MATERIAL_DBG_SHADOWMAP] = materialDict->FindMaterial("postprocessing/debug_shadowmap");

	for (int i = 0; i < MATERIAL_COUNT; i++)
	{
		sceneMaterials[i]->IncrementReferenceCount();
	}
}

void View::Clear()
{
	DestroyAllDecals();
}

void View::Shutdown()
{
	DestroyAllDecals();

	for (int i = 0; i < MATERIAL_COUNT; i++)
	{
		sceneMaterials[i]->DecrementReferenceCount();
	}
}

bool View::IsShowingEntityBounds() const
{
	return showEntityBounds;
}

void View::SetShowingEntityBounds(bool isShowing)
{
	showEntityBounds = isShowing;
}

void View::OnCycleDebugRenderer(ICommandArgs *args)
{
	if (currentDebugIndex < MATERIAL_DBG_FIRST)
	{
		currentDebugIndex = MATERIAL_DBG_FIRST;
	}
	else
	{
		currentDebugIndex++;
	}

	if (currentDebugIndex > MATERIAL_DBG_LAST)
	{
		currentDebugIndex = -1;
	}
}

void View::RenderBegin()
{
	UpdateDecals();

	renderContext->SetClearColor(0.0f, 0.2f, 0.4f, 0.0f);
	renderContext->ClearBuffers(true, true);

	const Camera *camera = Camera::GetInstance();

	float fov = 45.0f + (camera->GetDistance()) * 0.01f;

	//renderContext->PushViewMatrix(camera->GetReflectionViewMatrix());
	renderContext->PushViewMatrix(camera->GetViewMatrix());
	renderContext->PushProjectionMatrix(camera->GetProjectionMatrix());
	renderContext->SetDepthRange(camera->GetNearZ(), camera->GetFarZ());

	renderContext->SetMainMatrices();

	fboDict->PushFBO(fboGBufferSmall);
}

void View::RenderScene()
{
	DrawScene();

	//debugDraw->DrawLine(vec3::Zero(), vec3::UnitX() * 200, vec3(1, 0, 0), 1, -1, true);
	//debugDraw->DrawLine(vec3::Zero(), vec3::UnitY() * 200, vec3(0, 1, 0), 1, -1, true);
	//debugDraw->DrawLine(vec3::Zero(), vec3::UnitZ() * 200, vec3(0, 0, 1), 1, -1, true);
}

void View::RenderEnd()
{
	fboDict->PopFBO();

	renderContext->PopViewMatrix();
	renderContext->PopProjectionMatrix();

	if (engine->GetWorld() == nullptr)
	{
		renderContext->SetClearColor(0.0f, 0.04f, 0.1f, 1.0f);

		fboDict->PushFBO(fboGBuffer);
		renderContext->ClearBuffers(true, false);
		fboDict->PopFBO();
	}

	renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_PASSTHROUGH_FRAMEBUFFER]);

	if (requestScreenshot)
	{
		requestScreenshot = false;

		int nextIndex = 0;

#ifdef PLATFORM_WIN32
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind = FindFirstFile("screenshots/screenshot_*.png", &FindFileData);

		if (hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				DBGMSGF("found: %s\n", FindFileData.cFileName);

				if (G_StrLen(FindFileData.cFileName) > 12)
				{
					nextIndex = MAX(nextIndex, atoi(FindFileData.cFileName + 12) + 1);
				}
			}
			while (FindNextFile(hFind, &FindFileData));
		}

		_mkdir("screenshots");

		FindClose(hFind);
#else
#error implement_me
#endif

		char screenshotName[MAX_PATH_GAME];
		G_StrSnPrintf(screenshotName, sizeof(screenshotName), "screenshots/screenshot_%03i.png", nextIndex);

		renderContext->TakeScreenshot(screenshotName);
	}
}

void View::DrawScene()
{
	vec3 directionalLightDir = vec3(-1, -1, -1);
	directionalLightDir.normalize();

	vec3 shadowOffset = vec3::Zero();
	shadowOffset = directionalLightDir * -512;
	shadowOffset.x() -= 512;
	shadowOffset.y() += 300;
	shadowOffset.z() = 512;

	vec3 shadowOrigin = Camera::GetInstance()->GetOrigin() + shadowOffset;

	mat4 shadowMatrix = mat4::Identity();
	shadowMatrix = rotate(shadowMatrix, DEG2RAD(-45), vec3::UnitY());
	shadowMatrix = rotate(shadowMatrix, DEG2RAD(135), vec3::UnitZ());
	shadowMatrix = translate(shadowMatrix, shadowOrigin);

	float shadowMapOrthoSize = 300.0f + Camera::GetInstance()->GetDistance() * 1.3f;
	//vec3 fogColor = vec3(0.039f, 0.039f, 0.04f);
	vec3 fogColor = vec3(0.04f, 0.12f, 0.2f) * 2.0f;

	//vec3 v00, v01, v10, v11;

	//v00 = shadowOrigin - shadowMapOrthoSize * left + shadowMapOrthoSize * up;
	//v10 = shadowOrigin + shadowMapOrthoSize * left + shadowMapOrthoSize * up;
	//v01 = shadowOrigin - shadowMapOrthoSize * left - shadowMapOrthoSize * up;
	//v11 = shadowOrigin + shadowMapOrthoSize * left - shadowMapOrthoSize * up;

	//Ray r00, r01, r10, r11;
	//r00.Init(v00, directionalLightDir * 2000.0f);
	//r01.Init(v01, directionalLightDir * 2000.0f);
	//r10.Init(v10, directionalLightDir * 2000.0f);
	//r11.Init(v11, directionalLightDir * 2000.0f);
	//AssertCall(IntersectRayWithPlane(r00, Plane(), &v00));
	//AssertCall(IntersectRayWithPlane(r01, Plane(), &v01));
	//AssertCall(IntersectRayWithPlane(r10, Plane(), &v10));
	//AssertCall(IntersectRayWithPlane(r11, Plane(), &v11));

	//debugDraw->DrawLine(v00, v10, vec3(1, 0, 0), 1, -1);
	//debugDraw->DrawLine(v10, v11, vec3(1, 0, 0), 1, -1);
	//debugDraw->DrawLine(v11, v01, vec3(1, 0, 0), 1, -1);
	//debugDraw->DrawLine(v01, v00, vec3(1, 0, 0), 1, -1);
	//debugDraw->DrawLine(shadowOrigin, shadowOrigin + directionalLightDir * 2000.0f, vec3(1, 0, 0), 1, -1);
	//debugDraw->DrawLine(shadowOrigin, shadowOrigin + left * 40, vec3(0, 1, 0), 1, -1);
	//debugDraw->DrawLine(shadowOrigin, shadowOrigin + up * 40, vec3(0, 0, 1), 1, -1);

	// draw gbuffer

	fboDict->PushFBO(fboGBuffer);

	renderContext->ClearBuffers(false, true);

	DrawWorld();

	DrawEntities();

	DrawWater();

	fboDict->PopFBO();

	// draw lights
	fboDict->PushFBO(lightBuffer);

	renderContext->SetClearColor(fogColor.x(), fogColor.y(), fogColor.z(), 1);
	renderContext->ClearBuffers(true, false);

	renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_LIGHT_POINT]);

	fboDict->PopFBO();

	// draw shadow map
	fboDict->PushFBO(shadowmapBuffer);
	renderContext->SetRenderStage(RenderStages::SHADOW);

	renderContext->PushViewport(0, 0, SHADOWMAP_RESOLUTION, SHADOWMAP_RESOLUTION);
	renderContext->PushProjectionMatrix(orthographic(-shadowMapOrthoSize, shadowMapOrthoSize,
		shadowMapOrthoSize, -shadowMapOrthoSize,
		0.0f, 4000.0f));
	renderContext->PushViewMatrix(shadowMatrix);

	renderContext->ClearBuffers(false, true);

	renderContext->UpdateMatrices();
	renderContext->SetRenderParamMat4(RenderParams::MAT4_SHADOW_VIEWPROJECTION, shadowRemapMatrix() * renderContext->GetOGLViewProjectionMatrix());
	renderContext->SetRenderParamFloat(RenderParams::FLOAT_SHADOWMAP_SIZE, (float)SHADOWMAP_RESOLUTION);
	renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_DIR, directionalLightDir);
	renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_COLOR, vec3(1.2f, 1.1f, 0.9f));
	renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_AMBIENT, vec3(0.08f, 0.12f, 0.14f));

	DrawEntities(ENTITY_DRAW_SHADOW);

	renderContext->PopViewMatrix();
	renderContext->PopProjectionMatrix();
	renderContext->PopViewport();

	renderContext->SetRenderStage(RenderStages::NORMAL);
	fboDict->PopFBO();

	// compose scene
	// these matrices are used during worldspace reconstruction too
	if (currentDebugIndex != MATERIAL_DBG_ALBEDO)
	{
		fboDict->PushFBO(frameBufferCopy);

		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_AMOUNT, 1.0f);
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_START, 512.0f);
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_RANGE, 1500.0f);
		renderContext->SetRenderParamVec3(RenderParams::VEC3_FOG_COLOR, fogColor);

		renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_LIGHT_COMBINE]);

		DrawParticles();
		DrawEntities(ENTITY_DRAW_TRANSLUCENT);

		DrawClouds(MATERIAL_CLOUDS);

		fboDict->PopFBO();
	}

	// post processing
	renderContext->PushViewport(0,0,renderContext->GetScreenWidth()/4,renderContext->GetScreenHeight()/4);

	// downsample scene
	fboDict->PushFBO(frameBufferQuarter);
	renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_DOWNSAMPLE4]);
	fboDict->PopFBO();

	// blur vertical
	fboDict->PushFBO(frameBufferQuarter2);
	renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_GAUSSBLUR_13V]);
	fboDict->PopFBO();

	// blur horizontal and do bloom mapping
	fboDict->PushFBO(frameBufferQuarter);
	renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_GAUSSBLUR_13H]);
	fboDict->PopFBO();

	renderContext->PopViewport();

	// output result
	// combine bloom with scene

	//renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_DBG_SHADOWMAP]);

	if (currentDebugIndex >= MATERIAL_DBG_FIRST)
	{
		renderContext->DrawFullscreenQuad(sceneMaterials[currentDebugIndex]);
	}
	else
	{
		renderContext->DrawFullscreenQuad(sceneMaterials[MATERIAL_BLOOM_COMBINE]);
	}

	if (showEntityBounds)
	{
		DrawBounds();
	}
}

void View::DrawWorld()
{
	const IWorld *world = engine->GetWorld();
	const Plane *frustum = Camera::GetInstance()->GetFrustum();

	if (world != nullptr)
	{
		const IWorldGeometry *geometry = world->GetGeometry();
		IMaterial *material = world->GetMaterial();

		vec3 mins, maxs;

		for (int i = 0; i < geometry->GetWorldListCount(); i++)
		{
			geometry->GetWorldListBounds(i, mins, maxs);

			if (FrustumCullAABB(frustum, mins, maxs))
			{
				continue;
			}

			renderContext->BindMesh(geometry->GetWorldListMesh(i));
			material->Draw();
		}
	}
	else
	{
		renderContext->ClearBuffers(true, true);
	}
}

void View::DrawWater()
{
	const IWorld *world = engine->GetWorld();
	const Plane *frustum = Camera::GetInstance()->GetFrustum();

	bool drewReflection = false;

	if (world != nullptr)
	{
		const IWorldGeometry *geometry = world->GetGeometry();
		IMaterial *material = world->GetWaterMaterial();

		vec3 mins, maxs;

		for (int i = 0; i < geometry->GetWaterListCount(); i++)
		{
			geometry->GetWaterListBounds(i, mins, maxs);

			if (FrustumCullAABB(frustum, mins, maxs))
			{
				continue;
			}

			if (!drewReflection)
			{
				fboDict->PushFBO(fboReflection);
				renderContext->SetClearColor(0, 0, 0, 1);
				renderContext->ClearBuffers(true, true);

				DrawReflectionView();
				fboDict->PopFBO();

				fboDict->PushFBO(fboDepthCopy);
				fboDict->CopyFBOtoFBO(fboGBuffer, fboDepthCopy, FramebufferResizeModes::FULL_FRAMEBUFFER);
				fboDict->PopFBO();

				drewReflection = true;
			}

			renderContext->BindMesh(geometry->GetWaterListMesh(i));
			material->Draw();
		}
	}
}

void View::DrawReflectionView()
{
	renderContext->PushViewMatrix(Camera::GetInstance()->GetReflectionViewMatrix());
	renderContext->SetClipPlaneEnabled(0, true);

	DrawEntities(ENTITY_DRAW_REFLECTION);

	DrawClouds(MATERIAL_CLOUDS_REFLECTION);

	renderContext->SetClipPlaneEnabled(0, false);
	renderContext->PopViewMatrix();
}

void View::DrawEntities(EntityDraw_e mode)
{
	Assert(translucentEntities.empty());

	const Plane *frustum = Camera::GetInstance()->GetFrustum();
	auto entities = ClientEntityFactory::GetInstance()->GetEntities();

	for (auto e : entities)
	{
		if (!e->IsVisible())
		{
			continue;
		}

		const bool isTranslucent = e->IsTranslucent();

		switch (mode)
		{
		case ENTITY_DRAW_REFLECTION:
			if (!e->ShouldReflect())
				continue;
			break;
		case ENTITY_DRAW_SHADOW:
			if (!e->ShouldCastShadow())
				continue;
			break;
		case ENTITY_DRAW_TRANSLUCENT:
			if (!isTranslucent)
				continue;
			break;
		}

		vec3 mins, maxs;

		if (e->GetRenderBounds(mins, maxs)
			&& FrustumCullAABB(frustum, mins, maxs))
		{
			continue;
		}

		if (isTranslucent)
		{
			if (mode == ENTITY_DRAW_TRANSLUCENT)
			{
				translucentEntities.push(e);
			}
		}
		else
		{
			e->Render();
		}
	}

	if (mode == ENTITY_DRAW_NORMAL
		|| mode == ENTITY_DRAW_REFLECTION)
	{
		DrawDecals();
	}

	while (!translucentEntities.empty())
	{
		translucentEntities.top()->Render();
		translucentEntities.pop();
	}
}

void View::DrawParticles()
{
}

void View::DrawClouds(SceneMaterials_e material)
{
	vec3 positionMin = Camera::GetInstance()->GetOrigin();
	positionMin.z() = 1050.0f;
	positionMin -= vec3(3000, 3000, 0);
	vec3 positionMax = positionMin + vec3(3000, 6000, 0);

	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::TRIANGLES,
		VertexFormat::POSITION_3F, 2);

	MeshBuilder builder;
	builder.AttachModify(mesh);

	builder.Position3f(XYZ(positionMin));
	builder.AdvanceVertex();
	builder.Position3f(positionMax.x(), positionMin.y(), positionMin.z());
	builder.AdvanceVertex();
	builder.Position3f(positionMin.x(), positionMax.y(), positionMin.z());
	builder.AdvanceVertex();

	builder.Position3f(positionMin.x(), positionMax.y(), positionMin.z());
	builder.AdvanceVertex();
	builder.Position3f(positionMax.x(), positionMin.y(), positionMin.z());
	builder.AdvanceVertex();
	builder.Position3f(XYZ(positionMax));
	builder.AdvanceVertex();

	builder.Detach();

	renderContext->BindMesh(mesh);
	sceneMaterials[material]->Draw();
}

void View::DrawBounds()
{
	const IWorld *world = engine->GetWorld();

	if (world != nullptr)
	{
		const IWorldGeometry *geometry = world->GetGeometry();
		IMaterial *material = world->GetMaterial();

		vec3 mins, maxs;

		for (int i = 0; i < geometry->GetWorldListCount(); i++)
		{
			geometry->GetWorldListBounds(i, mins, maxs);
			debugDraw->DrawBox(mins, maxs + vec3(0, 0, 5), vec3(1, 0, 0), 0.5f, -1);
		}

		for (int i = 0; i < geometry->GetWaterListCount(); i++)
		{
			geometry->GetWaterListBounds(i, mins, maxs);
			debugDraw->DrawBox(mins, maxs + vec3(0, 0, 25), vec3(0, 0, 1), 0.5f, -1);
		}
	}

	auto entities = ClientEntityFactory::GetInstance()->GetEntities();

	for (auto e : entities)
	{
		vec3 mins, maxs;

		if (e->GetRenderBounds(mins, maxs))
		{
			debugDraw->DrawBox(mins, maxs, vec3(1, 1, 0), 0.5f, -1);
		}
	}
}

void View::AddDecal(IMesh *decalMesh, IMaterial *decalMaterial, float lifetime)
{
	Assert(decalMesh != nullptr);
	Assert(decalMaterial != nullptr);

	DecalData *data = new DecalData;
	data->lifetime = lifetime;
	data->material = decalMaterial;
	data->mesh = decalMesh;

	decals.push_back(data);
}

void View::DestroyAllDecals()
{
	for (auto itr = decals.begin();
		itr != decals.end();
		itr++)
	{
		(*itr)->mesh->Release();
		(*itr)->material->DecrementReferenceCount();
		delete (*itr);
	}

	decals.clear();
}

void View::UpdateDecals()
{
	const float frametime = globals->GetFrametime();

	for (auto itr = decals.begin();
		itr != decals.end();)
	{
		auto &decal = (*itr);

		if (decal->lifetime < 0.0f)
		{
			itr++;
			continue;
		}

		decal->lifetime -= frametime;

		if (decal->lifetime <= 0.0f)
		{
			decal->mesh->Release();
			decal->material->DecrementReferenceCount();
			delete decal;

			itr = decals.erase(itr);
		}
		else
		{
			itr++;
		}
	}
}

void View::DrawDecals()
{
	for (const auto &decal : decals)
	{
		float decalAlpha = RemapValClamped(decal->lifetime, 0.0f, 1.5f, 0.0f, 1.0f);
		renderContext->SetRenderParamVec4(RenderParams::VEC4_DIFFUSE_MODULATION, vec4(1, 1, 1, decalAlpha));

		renderContext->BindMesh(decal->mesh);
		decal->material->Draw();
	}
}

void View::OnScreenshot(ICommandArgs *args)
{
	requestScreenshot = true;
}

int View::SelectEntity(int mousex, int mousey, int entitySelectionMask)
{
	mousey = renderContext->GetScreenHeight() - mousey;

	if (mousex < 0 || mousex >= renderContext->GetScreenWidth()
		|| mousey < 0 || mousey >= renderContext->GetScreenHeight())
	{
		return -1;
	}

	renderContext->SetRenderStage(RenderStages::SELECTION);

	renderContext->SetClearColor(1, 1, 1, 1);
	renderContext->PushProjectionMatrix(Camera::GetInstance()->GetProjectionMatrix());
	renderContext->PushViewMatrix(Camera::GetInstance()->GetViewMatrix());

	renderContext->ClearBuffers(true, true);

	DrawWorld();

	const Plane *frustum = Camera::GetInstance()->GetFrustum();
	auto entities = ClientEntityFactory::GetInstance()->GetEntities();

	for (auto e : entities)
	{
		if (!e->IsVisible())
		{
			continue;
		}

		if (e->IsTranslucent())
		{
			continue;
		}

		vec3 mins, maxs;

		if (e->GetRenderBounds(mins, maxs)
			&& FrustumCullAABB(frustum, mins, maxs))
		{
			continue;
		}

		if (e->IsCreep()
			&& (entitySelectionMask & ESMASK_CREEPS) == 0)
		{
			continue;
		}

		if (e->IsTower()
			&& (entitySelectionMask & ESMASK_TOWERS) == 0)
		{
			continue;
		}

		renderContext->SetRenderParamInt(RenderParams::INT_ENTINDEX, e->GetNetworkId());

		e->Render();
	}

	renderContext->PopViewMatrix();
	renderContext->PopProjectionMatrix();

	Uint8 color[4] = { 0 };
	fboDict->ReadFBOColor(color, mousex, mousey, 1, 1, 0);

	renderContext->SetRenderStage(RenderStages::NORMAL);

	// world hit
	if (color[2] > 0)
	{
		return -1;
	}

	// background hit
	if (color[0] == 255
		&& color[1] == 255)
	{
		return -1;
	}

	return color[0] + color[1] * 256;
}