#ifndef CONSOLE_H
#define CONSOLE_H

#include "pch.h"
#include "gui/layoutpanel.h"

class Console : public GUI::LayoutPanel, IInputKeyListener
{
	DECLARE_CLASS(Console, GUI::LayoutPanel);
public:
	Console();
	~Console();

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);

	virtual bool OnKeyDown(SDL_Keycode sdlKey);
	virtual bool OnKeyUp(SDL_Keycode sdlKey);
	virtual void OnTextEvent(const char *text);

	virtual void SetVisible(bool visible);
private:
	void ResetAutoComplete();

	GUI::Label *text;

	char curCmd[255];
	std::vector<std::string> curAutoComplete;
	int curAutoCompleteIndex;

	bool textInputEnabled;
};

#endif