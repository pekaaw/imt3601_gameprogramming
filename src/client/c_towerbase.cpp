
#include "pch.h"
#include "clientgame.h"
#include "c_towerbase.h"

REGISTER_CLIENTENTITY_CLASS(tower, C_TowerBase);

IMPLEMENT_NETWORKTABLE_BASE(C_TowerBase)
	NETWORK_VARIABLE(towerType);
END_NETWORKTABLE();

C_TowerBase::C_TowerBase()
	: towerScript(nullptr)
	, outlineVisible(false)
	, TowerTypeChanged(this, &C_TowerBase::OnTowerTypeChanged)
{
	towerType.AddCallback(&TowerTypeChanged);
}

C_TowerBase::~C_TowerBase()
{
}

void C_TowerBase::Render()
{
	if (outlineVisible)
	{
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_OUTLINE_WIDTH, 5.0f);
	}

	vec3 color;

	if (GetOwnerId() == 0)
	{
		color = PLAYER_0_COLOR;
	}
	else
	{
		color = PLAYER_1_COLOR;
	}

	renderContext->SetRenderParamVec3(RenderParams::VEC3_MODEL_TINT_COLOR, color);

	BaseClass::Render();

	if (outlineVisible)
	{
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_OUTLINE_WIDTH, 0.0f);
	}
}

void C_TowerBase::OnNetworkedSpawn()
{
	Assert(VECTOR_IS_INDEX_VALID(Resources::GetInstance()->GetTowers(), towerType.Get()));

	towerScript = &Resources::GetInstance()->GetTowers()[towerType.Get()];

	BaseClass::OnNetworkedSpawn();
	
	SetModel(towerScript->model.c_str());

	Assert(GetModel() != nullptr);

	GetModel()->SetSequence(towerScript->animation.c_str());

	GetModel()->SetPlaybackRate(0.0f);

	const int tileIndex = engine->GetWorld()->GetTileIndexAtPosition(GetOrigin());
	ClientGame::GetInstance()->SetTileBlocked(tileIndex, true);

	SetRenderAngles(euler(0.0f, towerScript->rotation, 0.0f));
}

void C_TowerBase::OnTowerTypeChanged(Uint8 *value)
{
	if (!VECTOR_IS_INDEX_VALID(Resources::GetInstance()->GetTowers(), *value))
	{
		return;
	}

	towerScript = &Resources::GetInstance()->GetTowers()[*value];
	SetModel(towerScript->model.c_str());

	Assert(GetModel() != nullptr);

	GetModel()->SetSequence(towerScript->animation.c_str());
	GetModel()->SetPlaybackRate(0.0f);
	SetRenderAngles(euler(0.0f, towerScript->rotation, 0.0f));
}

void C_TowerBase::OnRelease()
{
	BaseClass::OnRelease();

	const int tileIndex = engine->GetWorld()->GetTileIndexAtPosition(GetOrigin());
	ClientGame::GetInstance()->SetTileBlocked(tileIndex, false);
}

bool C_TowerBase::GetRenderBounds(vec3 &min, vec3 &max)
{
	AssertCall(BaseClass::GetRenderBounds(min, max));

	const vec3 boundsBloat(100.0f, 100.0f, 100.0f);
	min -= boundsBloat;
	max += boundsBloat;
	return true;
}

ResourceTower C_TowerBase::GetTowerScript()
{
	return *towerScript;
}

void C_TowerBase::SetOutlineVisible(bool visible)
{
	outlineVisible = visible;
}