#include "pch.h"
#include "clientbutton.h"


ClientButton::ClientButton(const char *command, int bit)
		: internalState(0)
		, previousState(0)
		, buttonState(ButtonStates::RELEASED)
{
	this->bit = bit;

	char fullCommand[64];

	G_StrSnPrintf(fullCommand, sizeof(fullCommand), "+%s", command);
	callbackDown.Set(this, &ClientButton::FunctionDown, fullCommand);

	G_StrSnPrintf(fullCommand, sizeof(fullCommand), "-%s", command);
	callbackUp.Set(this, &ClientButton::FunctionUp, fullCommand);
}

void ClientButton::FunctionDown(ICommandArgs *args)
{
	if (internalState == 0)
	{
		internalState = 1;
	}
}

void ClientButton::FunctionUp(ICommandArgs *args)
{
	if (internalState == 1)
	{
		internalState = 2;
	}
}

void ClientButton::UpdateState()
{
	// no state change
	if (previousState == internalState)
	{
		if (previousState == 1)
		{
			buttonState = ButtonStates::DOWN;
		}

		return;
	}

	// was pressed during the last frame
	if (previousState == 0)
	{
		previousState = 1;

		buttonState = ButtonStates::PRESSED | ButtonStates::DOWN;
	}
	else if (previousState == 1) // was released
	{
		buttonState = ButtonStates::RELEASED;

		previousState = 0;
		internalState = 0;
	}
}

int ClientButton::GetBit() const
{
	return bit;
}

Uint8 ClientButton::GetButtonState() const
{
	return buttonState;
}