#ifndef TOWERSELECTIONWINDOW_H
#define TOWERSELECTIONWINDOW_H

#include "gui/layoutpanel.h"
#include "towerinfo.h"
#include "towerbutton.h"

class TowerSelectionWindow : public GUI::LayoutPanel
{
public:
	TowerSelectionWindow(void);
	~TowerSelectionWindow(void);

	void SetTowerScript(ResourceTower towerScript);
	ResourceTower GetTowerScript() { return towerScript; }

	void SetTowerNetworkId(const int id) { towerId = id; }

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);

	void OnTowerUpgradeClick(Control* ctrl);
	LISTENER(TowerSelectionWindow, OnTowerUpgradeClick, Control *);

private:
	ResourceTower towerScript;
	TowerInfo *info;
	Panel *pTowerIcon;
	TowerButton* bSell;
	GUI::Label *lUpgrade;

	int towerId;

	std::vector<TowerButton*> upgrades;
	
};
#endif
