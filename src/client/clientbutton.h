#ifndef CLIENTBUTTON_H
#define CLIENTBUTTON_H

/// Helper that caches button state on events so that a pressed
/// and immediately released button will at least be held down for one frame.
class ClientButton
{
public:
	ClientButton(const char *command, int bit);

	/// Callback for key press
	void FunctionDown(ICommandArgs *args);
	/// Callback for key release
	void FunctionUp(ICommandArgs *args);

	/// Called each frame to advance cached state
	void UpdateState();

	/// Get the button game bit this instance is associated with
	int GetBit() const;
	/// Get the current cached button state
	Uint8 GetButtonState() const;

private:

	int bit;
	int internalState;
	int previousState;

	Uint8 buttonState;

	CommandCallback<ClientButton> callbackDown;
	CommandCallback<ClientButton> callbackUp;
};


#endif