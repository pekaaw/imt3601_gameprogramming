#ifndef CLIENTINPUT_H
#define CLIENTINPUT_H

/// Client helper class that can be called from anywhere
/// to figure out which buttons are down, have been pressed or released this frame
class ClientInput : public IGameSystem
{
	DECLARE_SINGLETON(ClientInput);
public:

	virtual GameSystemPriorityId GetPriority() {return GameSystemPriorites::CLIENT_INPUT;}

	virtual bool Init();
	virtual void Shutdown();

	virtual void Update(float frametime);

	int GetButtonsPressed() const;
	int GetButtonsDown() const;
	int GetButtonsReleased() const;

	/// Was a button pressed last frame?
	bool WasButtonPressed(ButtonId button) const;
	/// Is a button down this frame?
	bool IsButtonDown(ButtonId button) const;
	/// Was a button released this frame?
	bool WasButtonReleased(ButtonId button) const;

private:
	int buttonsPressed;
	int buttonsDown;
	int buttonsReleased;
};

#endif