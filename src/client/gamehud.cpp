#include "pch.h"
#include "gamehud.h"
#include "c_creepbase.h"
#include "c_towerbase.h"
#include "c_player.h"
#include "clientgame.h"
#include "towerbutton.h"
#include "view.h"

using namespace GUI;

//TODO Refactor into seperate components
GameHud::GameHud()
	: buildSelection(-1)
	, selectedTower(-1)
	, LOnTowerButtonClick(this)
	, lastGold(-1)
{
	InitComponents();
	InvokeResize();
	SetVisible(false);
}

void GameHud::DrawSelf()
{
	LayoutPanel::DrawSelf();

	DrawHealthbars();
}

void GameHud::OnMouseDown(int mouseButton)
{
	if (mouseButton == SDL_BUTTON_LEFT)
	{
		if (buildSelection >= 0)
		{
			if (pTowerShop->GetRecursiveRectPos().Contains(input->GetMousePosX(), input->GetMousePosY()))
			{
				return;
			}

			vec3 point;
			if (IntersectScreenRayWithWorld(input->GetMousePosX(), input->GetMousePosY(),
											renderContext->GetScreenWidth(), renderContext->GetScreenHeight(),
											renderContext->GetMainViewProjectionMatrix(), point))
			{
				ClientGame::GetInstance()->SetOverlayMode(ClientGame::OverlayMode_e::OVERLAY_NONE);


				int posX = int(point.y() / WORLD_TILE_SCALE);
				int posY = int(point.x() / WORLD_TILE_SCALE);

				char command[256];

				G_StrSnPrintf(command, sizeof(command), "tower_create %i %i %i",
							  posX, posY, buildSelection);

				// pos game x, pos game y, type index
				engine->SendClientCommand(command);

				buildSelection = -1;
			}
		}
		else
		{
			TowerSelection();
		}
	}
	else if (mouseButton == SDL_BUTTON_RIGHT)
	{
		if (buildSelection >= 0)
		{
			buildSelection = -1;
			ClientGame::GetInstance()->SetOverlayMode(ClientGame::OverlayMode_e::OVERLAY_NONE);
		}

		DeselectTower();
	}
}

void GameHud::TowerSelection()
{
	int entityIndex = View::GetInstance()->SelectEntity(input->GetMousePosX(), input->GetMousePosY(), View::ESMASK_TOWERS);

	C_TowerBase *tower = dynamic_cast<C_TowerBase *>(ClientEntityFactory::GetInstance()->GetNetworkEntityByIndex(entityIndex));

	if (tower != nullptr)
	{
		DeselectTower();

		if (tower->GetOwner() == C_Player::GetLocalPlayer())
		{
			tower->SetOutlineVisible(true);

			selectedTower = tower->GetNetworkId();
			towerSelectionWindow->SetTowerScript(tower->GetTowerScript());
			towerSelectionWindow->SetTowerNetworkId(tower->GetNetworkId());
			towerSelectionWindow->SetVisible(true);
		}
	}
	else
	{
		DeselectTower();
	}
}

void GameHud::UpdateSelf()
{
	C_Player *player = C_Player::GetLocalPlayer();

	if (player != nullptr)
	{
		if (lastGold != player->GetGold())
		{
			lastGold = player->GetGold();

			char goldText[256];
			G_StrSnPrintf(goldText, sizeof(goldText), "%i", lastGold);
			lGold->SetText(goldText);
		}
	}
}

void GameHud::DrawHealthbars()
{
	auto entities = ClientEntityFactory::GetInstance()->GetEntities();

	mat4 viewProjectionMatrix = renderContext->GetMainViewProjectionMatrix();

	for (auto e : entities)
	{
		C_CreepBase *creep = dynamic_cast<C_CreepBase *>(e);
		if (creep != nullptr)
		{
			const int healthMax = creep->GetHealthMax();
			const int health = creep->GetHealth();

			if ((health >= healthMax) ||
				(health <= 0))
			{
				continue;
			}

			vec3 creepPos = creep->GetRenderOrigin();
			creepPos.z() += 140;

			vec4 pos = vec4(creepPos.x(), creepPos.y(), creepPos.z(), 1);
			vec4 point3D = viewProjectionMatrix * pos;

			float x = (float)(point3D.x() * 0.5 / point3D.w() + 0.5);
			float y = (float)((-point3D.y()) * 0.5 / point3D.w() + 0.5);

			int winX = (int)(x * renderContext->GetScreenWidth());
			int winY = (int)(y * renderContext->GetScreenHeight());

			winX -= 15;	//Draw centered

			Assert(healthMax > 0);
			const float lifePercentage = (float)health / healthMax;

			graphics->SetDrawColor(0, 1, 0, 0.5f);
			int width = winX + SCALE((int)(lifePercentage * 30));
			graphics->DrawFilledRect(winX, winY, width, winY + SCALE(5));
			graphics->SetDrawColor(0.2f, 0.2f, 0.2f, 1);
			graphics->DrawRect(winX, winY, winX + SCALE(30), winY + SCALE(5));
		}
	}
}

void GameHud::OnTowerButtonClick(Control *ctrl)
{
	for (unsigned int i = 0; i < towerButtons.size(); i++)
	{
		if (towerButtons[i] == ctrl)
		{
			buildSelection = i;
			ClientGame::GetInstance()->SetOverlayMode(ClientGame::OverlayMode_e::OVERLAY_PLACE_TOWER);
			ClientGame::GetInstance()->SetOverlayTowerType(buildSelection);
			return;
		}
	}
}

GameHud::~GameHud()
{
}

void GameHud::InitComponents()
{
	color = Color(0, 0, 0, 0);

	//Tower shop
	pTowerShop = new Panel();
	pTowerShop->color = Color(0.15f, 0.15f, 0.15f, 0.85f);
	pTowerShop->borderColor = Color(0.15f, 0.15f, 0.15f, 1.0f);

	for (unsigned int i = 0; i < 3; i++)
	{
		TowerInfo *tooltip = new TowerInfo();
		TowerButton *btn = new TowerButton(tooltip);
		btn->SetTowerScript(Resources::GetInstance()->GetTowers()[i]);
		btn->Click += LOnTowerButtonClick;
		btn->color = Color(1, 0, 1, 1);

		tooltip->AddStat(TowerInfoStat::TowerCost);
		tooltip->AddStat(TowerInfoStat::Damage);
		tooltip->AddStat(TowerInfoStat::Range);

		pTowerShop->AddChild(btn);
		towerButtons.push_back(btn);
	}

	AddChild(pTowerShop);

	//Player Info panel
	lGold = new Label();
	lGold->color = Color(1, 1, 1, 1);
	lGold->SetAlignment(Alignment::Left);

	pGold = new Panel();
	pGold->SetMaterial(materialDict->FindMaterial("materials/gui/icons/gold"));

	pInfo = new Panel();
	pInfo->color = Color(0.15f, 0.15f, 0.15f, 0.85f);
	pInfo->borderColor = Color(0.15f, 0.15f, 0.15f, 1.0f);

	AddChild(pInfo);
	AddChild(pGold);
	AddChild(lGold);

	//Tower selection
	towerSelectionWindow = new TowerSelectionWindow();
	towerSelectionWindow->SetTowerScript(Resources::GetInstance()->GetTowers()[0]);
	AddChild(towerSelectionWindow);
}

void GameHud::SetPositions(int width, int height)
{
	Rect rect;

	rect.x = 0;
	rect.y = 0;
	rect.w = width;
	rect.h = height;
	SetRect(rect);

	//Info panel
	rect.x = 0;
	rect.y = 0;
	rect.w = (int)(width * 0.11f);
	rect.h = (int)(height * 0.05f);
	pInfo->SetRect(rect);

	rect.x = 0;
	rect.y = SCALE(2);
	rect.w = SCALE(20);
	rect.h = SCALE(20);
	pGold->SetRect(rect);

	lGold->SetPos(SCALE(23), SCALE(1));

	//Tower shop
	int shopWidth = SCALE(6 + (towerButtons.size() * 63));
	rect.x = width - shopWidth;
	rect.y = (int)(height * 0.85f);
	rect.w = shopWidth;
	rect.h = SCALE(70);
	pTowerShop->SetRect(rect);

	int xOffset = 5, y = SCALE(5), size = SCALE(60);
	for (unsigned int i = 0; i < towerButtons.size(); i++)
	{
		towerButtons[i]->SetRect(Rect(SCALE(xOffset), y, size, size));
		xOffset += 63;
	}
}

void GameHud::DeselectTower()
{
	if (selectedTower >= 0)
	{
		C_TowerBase *tower = dynamic_cast<C_TowerBase *>(ClientEntityFactory::GetInstance()->GetNetworkEntityByIndex(selectedTower));

		if (tower != nullptr)
		{
			tower->SetOutlineVisible(false);
		}

		towerSelectionWindow->SetVisible(false);
		selectedTower = -1;
	}
}