#include "pch.h"
#include "guisystem.h"

SINGLETON_INSTANCE_LAZY(GUISystem);

GUISystem::GUISystem()
	: callbackEvent(this, &GUISystem::OnEvent)
	, mouseDownPanel(nullptr)
{
}

GUISystem::~GUISystem()
{
}

bool GUISystem::Init()
{
	events->AddListener("game_started", &callbackEvent);
	events->AddListener("game_ended", &callbackEvent);

	CreateGUI();
	ToggleMenu();
	//ToggleStats();
	return true;
}

void GUISystem::Shutdown()
{
	GUI::Shutdown();

	input->RemoveMouseListener(dynamic_cast<IInputMouseListener *>(this));
	delete rootPanel;
}

void GUISystem::CreateGUI()
{
	w = renderContext->GetScreenWidth();
	h = renderContext->GetScreenHeight();

	rootPanel = new GUI::Panel();
	rootPanel->color.a = 0;
	rootPanel->SetRect(GUI::Rect(0, 0, w, h));
	input->AddMouseListener(dynamic_cast<IInputMouseListener *>(this));

	//Create all gui elements
	statsWindow = new StatsWindow();
	console = new Console();
	mainMenu = new MainMenu();
	gameHud = new GameHud();

	//Order is important as there is no draw order system
	rootPanel->AddChild(gameHud);
	rootPanel->AddChild(statsWindow);
	rootPanel->AddChild(mainMenu);
	rootPanel->AddChild(console);
}

void GUISystem::Update(float frametime)
{
	rootPanel->Update();
}

void GUISystem::Draw()
{
	if ((w != renderContext->GetScreenWidth()) ||
		(h != renderContext->GetScreenHeight()))
	{
		w = renderContext->GetScreenWidth();
		h = renderContext->GetScreenHeight();

		rootPanel->SetRect(GUI::Rect(0, 0, w, h));
		rootPanel->Resize(w, h);
	}

	rootPanel->Draw();
}

void GUISystem::ToggleStats()
{
	statsWindow->SetVisible(!statsWindow->IsVisible());
}

void GUISystem::ToggleConsole()
{
	console->SetVisible(!console->IsVisible());
}

void GUISystem::ToggleGameHud()
{
	gameHud->SetVisible(!gameHud->IsVisible());
}

void GUISystem::ToggleMenu()
{
	mainMenu->SetVisible(!mainMenu->IsVisible());
}

void GUISystem::OnMouseDown(int mouseButton)
{
	GUI::Control *focus = rootPanel->GetFocusPanel(input->GetMousePosX(), input->GetMousePosY());
	GUI::Panel *focusPanel = dynamic_cast<GUI::Panel *>(focus);

	if (focusPanel != nullptr)
	{
		mouseDownPanel = focus;

		focusPanel->OnMouseDown(mouseButton);
	}
}

void GUISystem::OnMouseUp(int mouseButton)
{
	GUI::Control *focus = rootPanel->GetFocusPanel(input->GetMousePosX(), input->GetMousePosY());
	GUI::Panel *focusPanel = dynamic_cast<GUI::Panel *>(focus);

	if (focusPanel != nullptr)
	{
		if (mouseDownPanel == focus)
		{
			focusPanel->OnMouseUp(mouseButton);
		}

		mouseDownPanel = nullptr;
	}
}

void GUISystem::OnEvent(KeyValues *data)
{
	const char *name = data->GetName();

	if (G_StrEq("game_started", name))
	{
		mainMenu->SetVisible(false);
		gameHud->SetVisible(true);
	}
	else if (G_StrEq("game_ended", name))
	{
		mainMenu->SetVisible(true);
		gameHud->SetVisible(false);
	}
}

CON_COMMAND(gui_togglemenu)
{
	GUISystem::GetInstance()->ToggleMenu();
}

CON_COMMAND(gui_togglestats)
{
	GUISystem::GetInstance()->ToggleStats();
}

CON_COMMAND(gui_togglehud)
{
	GUISystem::GetInstance()->ToggleGameHud();
}

CON_COMMAND(gui_toggleconsole)
{
	GUISystem::GetInstance()->ToggleConsole();
}
