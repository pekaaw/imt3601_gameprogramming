#include "pch.h"
#include "mainMenu.h"
#include <iostream>
#include <math.h>
#include "client.h"

using namespace GUI;

MainMenu::MainMenu() : LOnPlayClick(this), LOnExitClick(this)
{
	InitComponents();
	InvokeResize();
	SetVisible(false);
}

MainMenu::~MainMenu()
{
}

void MainMenu::SetPositions(int width, int height)
{
	Rect menuRect;

	menuRect.x = width / 4;
	menuRect.y = (int)(height * 0.1f);
	menuRect.w = width / 2;
	menuRect.h = (int)(height * 0.8f);
	SetRect(menuRect);

	lTitle->SetPos(menuRect.w / 2, 20);

	bPlay->SetRect(Rect((menuRect.w / 2) - (SCALE(150) / 2), (int)(height * 0.15f), SCALE(150), SCALE(40)));
	bExit->SetRect(Rect((menuRect.w / 2) - (SCALE(150) / 2), (int)(height * 0.3f), SCALE(150), SCALE(40)));
}

void MainMenu::InitComponents()
{
	color = Color(0.15f, 0.15f, 0.15f, 0.8f);
	borderColor = Color(0.15f, 0.15f, 0.15f, 1.0f);

	lTitle = new Label();
	lTitle->SetText("Main menu");
	lTitle->color.Init(0.7f, 0.7f, 0.7f, 1.0f);

	bExit = new Button();
	bExit->label->SetText("Exit");
	bExit->color = Color(0.1f, 0.1f, 0.1f, 1);
	bExit->borderColor = Color(0.3f, 0.3f, 0.3f, 1);
	bExit->hoverColor = Color(0.5f, 0, 0, 1);
	bExit->Click += LOnExitClick;

	bPlay = new Button();
	bPlay->label->SetText("Play");
	bPlay->color = Color(0.1f, 0.1f, 0.1f, 1);
	bPlay->borderColor = Color(0.3f, 0.3f, 0.3f, 1);
	bPlay->hoverColor = Color(0, 0.5f, 0, 1);
	bPlay->Click += LOnPlayClick;

	AddChild(lTitle);
	AddChild(bExit);
	AddChild(bPlay);
}

void MainMenu::OnPlayClick(Control *ctrl)
{
	cvar->ExecuteBuffer("map Level1");
}

void MainMenu::OnExitClick(Control *ctrl)
{
	cvar->ExecuteBuffer("shutdown");
}
