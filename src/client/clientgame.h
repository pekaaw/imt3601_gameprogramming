#ifndef CLIENTGAME_H
#define CLIENTGAME_H

#include "shared/pathfinder.h"

class C_TowerHint;

class ClientGame : public IGameSystem
{
	DECLARE_SINGLETON(ClientGame);

public:
	~ClientGame();

	enum OverlayMode_e
	{
		OVERLAY_NONE = 0,
		OVERLAY_PLACE_TOWER,
	};

	virtual void Update(float frametime);

	virtual void OnLevelInit();
	virtual void OnLevelShutdown();

	void SetOverlayMode(OverlayMode_e mode);
	void SetOverlayTowerType(int type);

	bool IsTileBlocked(int tileIndex);

	// path finding
	void SetTileBlocked(int tileIndex, bool isBlocked);
	void ModifyNodeCreepCount(int tileIndex, int delta);

private:

	OverlayMode_e overlayMode;
	int overlayTowerType;

	C_TowerHint *towerHint;
	int lastOverlayTile;

	PathFinder pathFinder;
	bool pathFullyBlocked;
};


#endif