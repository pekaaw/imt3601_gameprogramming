#ifndef VIEW_H
#define VIEW_H

#include <queue>

struct RenderOrderLess
{
	bool operator()(ClientEntity *left, ClientEntity *right) const
	{
		return left->GetRenderOrigin().x() < right->GetRenderOrigin().x();
	}
};

class View
{
	DECLARE_SINGLETON(View);

public:
	~View();

	void Init();
	void Clear();
	void Shutdown();

	void RenderBegin();
	void RenderScene();
	void RenderEnd();

	bool IsShowingEntityBounds() const;
	void SetShowingEntityBounds(bool isShowing);

	void AddDecal(IMesh *decalMesh, IMaterial *decalMaterial, float lifetime);

	enum EntitySelectionMask_e
	{
		ESMASK_CREEPS = (1 << 0),
		ESMASK_TOWERS = (1 << 1),
		ESMASK_ALL = 0xFFFFFFFF,
	};

	int SelectEntity(int mousex, int mousey, int entitySelectionMask);

	CON_COMMAND_OBJECT(View, OnScreenshot);
	CON_COMMAND_OBJECT(View, OnCycleDebugRenderer);

private:

	enum EntityDraw_e
	{
		ENTITY_DRAW_NORMAL = 0,
		ENTITY_DRAW_REFLECTION,
		ENTITY_DRAW_SHADOW,
		ENTITY_DRAW_TRANSLUCENT,
	};

	enum SceneMaterials_e
	{
		MATERIAL_BLOOM_COMBINE = 0,
		MATERIAL_LIGHT_COMBINE,
		MATERIAL_LIGHT_POINT,
		MATERIAL_DOWNSAMPLE4,
		MATERIAL_GAUSSBLUR_13V,
		MATERIAL_GAUSSBLUR_13H,

		MATERIAL_PASSTHROUGH_FRAMEBUFFER,

		MATERIAL_CLOUDS,
		MATERIAL_CLOUDS_REFLECTION,

		MATERIAL_DBG_LIGHT,
		MATERIAL_DBG_BLOOM,
		MATERIAL_DBG_NORMALS,
		MATERIAL_DBG_DEPTH,
		MATERIAL_DBG_ALBEDO,
		MATERIAL_DBG_SHADOWMAP,

		MATERIAL_COUNT,

		MATERIAL_DBG_FIRST = MATERIAL_DBG_LIGHT,
		MATERIAL_DBG_LAST = MATERIAL_DBG_SHADOWMAP,
	};

	void DrawScene();

	void DrawWorld();
	void DrawReflectionView();
	void DrawWater();
	void DrawEntities(EntityDraw_e mode = ENTITY_DRAW_NORMAL);
	void DrawParticles();
	void DrawClouds(SceneMaterials_e material);

	void DrawBounds();

	// tiny gbuffer for passthrough drawing (albedo + depth, shared rts)
	FBO_HANDLE fboGBufferSmall;

	// albedo, normals, depth
	FBO_HANDLE fboGBuffer;

	// copy of depth buffer to avoid read/write conflicts
	FBO_HANDLE fboDepthCopy;

	FBO_HANDLE fboReflection;

	// only albedo, faster than using wide gbuffer again
	FBO_HANDLE frameBufferCopy;

	// quarter sized fb for post processing
	FBO_HANDLE frameBufferQuarter;

	// ping pong rt for previous one
	FBO_HANDLE frameBufferQuarter2;

	// light accumulation
	FBO_HANDLE lightBuffer;

	// orthogonal shadowmap
	FBO_HANDLE shadowmapBuffer;

	IMaterial *sceneMaterials[MATERIAL_COUNT];

	std::priority_queue<ClientEntity*, std::vector<ClientEntity*>, RenderOrderLess> translucentEntities;
	bool showEntityBounds;

	struct DecalData
	{
		IMesh *mesh;
		IMaterial *material;
		float lifetime;
	};

	std::vector<DecalData*> decals;

	void DestroyAllDecals();
	void UpdateDecals();
	void DrawDecals();

	bool requestScreenshot;
	int currentDebugIndex;
};

#endif