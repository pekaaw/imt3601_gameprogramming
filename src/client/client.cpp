#include "pch.h"
#include "client.h"
#include "view.h"
#include "appinterface\iappinterface.h"
#include "camera.h"
#include "guisystem.h"

SINGLETON_INSTANCE(Client);

EXPOSE_APP(INTERFACE_ICLIENT_VERSION, Client::GetInstance());

IRenderContext *renderContext;
IMaterialDict *materialDict;
ITextureDict *textureDict;
IMDXModelDict *modelDict;
IMDXRenderableDict *renderableDict;
IFBODict *fboDict;
IGlobals *globals;
IEngineClient *engine;
IInput *input;
IDebugDraw *debugDraw;
ICvarInterface *cvar;
IEvents *events;
IGraphics *graphics;

Client::Client()
	: localClientIndex(-1)
{
}

bool Client::Init(bool enableGUI)
{
	// pull all interfaces used by the client
	if ((renderContext = (IRenderContext *)GetAppInterface()->QueryApp(INTERFACE_IRENDERCONTEXT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((materialDict = (IMaterialDict *)GetAppInterface()->QueryApp(INTERFACE_IMATERIALDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((textureDict = (ITextureDict *)GetAppInterface()->QueryApp(INTERFACE_ITEXTUREDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((modelDict = (IMDXModelDict *)GetAppInterface()->QueryApp(INTERFACE_IMDXMODELDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((renderableDict = (IMDXRenderableDict *)GetAppInterface()->QueryApp(INTERFACE_IMDXRENDERABLEDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((fboDict = (IFBODict *)GetAppInterface()->QueryApp(INTERFACE_IFBODICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((globals = (IGlobals *)GetAppInterface()->QueryApp(INTERFACE_IGLOBALS_CLIENT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((debugDraw = (IDebugDraw *)GetAppInterface()->QueryApp(INTERFACE_IDEBUGDRAW_VERSION)) == nullptr)
	{
		return false;
	}

	if ((engine = (IEngineClient *)GetAppInterface()->QueryApp(INTERFACE_IENGINECLIENT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((input = (IInput *)GetAppInterface()->QueryApp(INTERFACE_IINPUT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((cvar = (ICvarInterface *)GetAppInterface()->QueryApp(INTERFACE_ICVAR_VERSION)) == nullptr)
	{
		return false;
	}

	if ((events = (IEvents *)GetAppInterface()->QueryApp(INTERFACE_IEVENTS_VERSION)) == nullptr)
	{
		return false;
	}

	// setup FBOs
	View::GetInstance()->Init();

	if (enableGUI)
	{
		if ((graphics = (IGraphics *)GetAppInterface()->QueryApp(INTERFACE_IGRAPHICS_VERSION)) == nullptr)
		{
			return false;
		}

		GUI::Init();
		GUISystem::InitSingleton();
	}

	if (!GameSystemManager::InitAllGameSystems())
	{
		return false;
	}

	return true;
}

void Client::Shutdown()
{
	GameSystemManager::ShutdownAllGameSystems();

	View::GetInstance()->Shutdown();
}

void Client::Simulate()
{
	GameSystemManager::UpdateAllGameSystems(globals->GetFrametime());

	if (GUISystem::GetInstance() != nullptr)
	{
		GUISystem::GetInstance()->Update(globals->GetFrametime());
	}
}

void Client::OnLevelInit()
{
	GameSystemManager::LevelInitAllGameSystems();
}

void Client::OnLevelShutdown()
{
	GameSystemManager::LevelShutdownAllGameSystems();

	View::GetInstance()->Clear();

	localClientIndex = -1;
}

void Client::ApplyNetworkSymbolDefinitions(ByteBuffer &buffer)
{
	ClientEntityFactory::GetInstance()->ApplyNetworkSymbolDefinitions(buffer);
}

void Client::CreateNetworkEntity(int networkId, Uint8 networkTableIndex, ByteBuffer &buffer)
{
	ClientEntityFactory::GetInstance()->CreateNetworkEntity(networkId, networkTableIndex, buffer);
}

void Client::DestroyNetworkEntity(int networkId)
{
	ClientEntityFactory::GetInstance()->DestroyNetworkEntity(networkId);
}

void Client::ApplyWorldStateUpdates(ByteBuffer &buffer)
{
	ClientEntityFactory::GetInstance()->ApplyWorldStateUpdates(buffer);
}

void Client::ApplyWorldStateComplete(ByteBuffer &buffer)
{
	ClientEntityFactory::GetInstance()->ApplyWorldStateComplete(buffer);
}

void Client::RenderBegin()
{
	View::GetInstance()->RenderBegin();
}

void Client::RenderScene()
{
	View::GetInstance()->RenderScene();
}

void Client::RenderEnd()
{
	View::GetInstance()->RenderEnd();

	if (GUISystem::GetInstance() != nullptr)
	{
		GUISystem::GetInstance()->Draw();
	}

	renderContext->SwapBuffers();
	renderContext->CheckForErrors();
}

void Client::SetLocalClientIndex(int clientIndex)
{
	localClientIndex = clientIndex;
}

int Client::GetLocalClientIndex()
{
	return localClientIndex;
}