#ifndef C_TOWERHINT_H
#define C_TOWERHINT_H

#include "c_basemodel.h"

class C_TowerHint : public C_BaseModel
{
	DECLARE_CLASS(C_TowerHint, C_BaseModel);
public:

	C_TowerHint();

	virtual void OnSpawn();
	virtual void OnRelease();

	virtual void Render();
	
	virtual bool IsTranslucent() const { return true; }
	virtual bool ShouldCastShadow() const { return false; }

	void InitFromScript(const ResourceTower &script);
	void SetColor(const vec3 &color);

private:
	~C_TowerHint();

	//virtual void OnRelease();

	IMaterial *hintMaterial;
	IMaterial *rangeMaterial;
	IMesh *rangeMesh;

	vec3 lastOrigin;
	vec3 color;
	float range;
};


#endif