
#include "pch.h"
#include "c_towerhint.h"

REGISTER_CLIENTENTITY_CLASS(towerhint, C_TowerHint);

C_TowerHint::C_TowerHint()
	: hintMaterial(nullptr)
	, rangeMesh(nullptr)
	, lastOrigin(vec3::Zero())
	, range(0.0f)
{
}

C_TowerHint::~C_TowerHint()
{
	Assert(hintMaterial == nullptr);
	Assert(rangeMesh == nullptr);
}

void C_TowerHint::OnSpawn()
{
	BaseClass::OnSpawn();

	hintMaterial = materialDict->FindMaterial("models/towerhint");
	rangeMaterial = materialDict->FindMaterial("ground/towerrange");
	hintMaterial->IncrementReferenceCount();
	rangeMaterial->IncrementReferenceCount();
}

void C_TowerHint::OnRelease()
{
	BaseClass::OnRelease();

	hintMaterial->DecrementReferenceCount();
	hintMaterial = nullptr;

	rangeMaterial->DecrementReferenceCount();
	rangeMaterial = nullptr;

	if (rangeMesh != nullptr)
	{
		rangeMesh->Release();
		rangeMesh = nullptr;
	}
}

void C_TowerHint::Render()
{
	if (GetOrigin() != lastOrigin)
	{
		lastOrigin = GetOrigin();

		if (rangeMesh != nullptr)
		{
			rangeMesh->Release();
		}

		if (range > 0.0f)
		{
			rangeMesh = engine->GetWorld()->CreateProjectedDecal(lastOrigin + vec3(0, 0, 200),
				euler(-90, 0.0f, 0.0f), vec3(500, range, range), true);
		}
	}

	renderContext->SetRenderParamVec4(RenderParams::VEC4_DIFFUSE_MODULATION, vec4(XYZ(color), 0.4f));

	if (rangeMesh != nullptr)
	{
		renderContext->BindMesh(rangeMesh);
		rangeMaterial->Draw();
	}

	renderContext->SetMaterialOverride(hintMaterial);

	BaseClass::Render();

	renderContext->SetMaterialOverride(nullptr);
}

void C_TowerHint::SetColor(const vec3 &color)
{
	this->color = color;
}

void C_TowerHint::InitFromScript(const ResourceTower &script)
{
	SetModel(script.model.c_str());

	Assert(GetModel() != nullptr);

	GetModel()->SetSequence(script.animation.c_str());

	range = script.range;

	SetRenderAngles(euler(0.0f, script.rotation, 0.0f));
}