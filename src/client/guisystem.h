#ifndef GUISYSTEM_H
#define GUISYSTEM_H

#include "mainMenu.h"
#include "statswindow.h"
#include "gamehud.h"
#include "console.h"

class GUISystem : public IGameSystem,
				  public IInputMouseListener,
				  public SingletonLazy<GUISystem>
{
	DECLARE_SINGLETON_LAZY(GUISystem);
	DECLARE_CLASS_NOBASE(GUISystem);
public:

	~GUISystem();

	virtual GameSystemPriorityId GetPriority() { return GameSystemPriorites::CLIENT_GUI; }

	bool Init();
	void Shutdown();

	void CreateGUI();

	void Update(float frametime);
	void Draw();

	void ToggleMenu();
	void ToggleStats();
	void ToggleGameHud();
	void ToggleConsole();

	// IInputMouseListener
	virtual void OnMouseDown(int mouseButton);
	virtual void OnMouseUp(int mouseButton);

private:
	void OnEvent(KeyValues *data);

	DECLARE_CALLBACK(callbackEvent, KeyValues);

	int w, h;
	GUI::Panel *rootPanel;
	MainMenu *mainMenu;
	StatsWindow *statsWindow;
	GameHud *gameHud;
	Console *console;

	GUI::Control *mouseDownPanel;
};
#endif
