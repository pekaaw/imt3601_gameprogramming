#ifndef CLIENTENTITYFACTORY_H
#define CLIENTENTITYFACTORY_H

#define REGISTER_CLIENTENTITY_CLASS(scriptName, className)											   \
	static ClientEntity * __createInstance ## scriptName()											   \
	{																								   \
		return new className();																		   \
	}																								   \
	static class __entityRegister ## scriptName														   \
	{																								   \
public:																								   \
		__entityRegister ## scriptName()															   \
		{																							   \
			ClientEntityFactory::RegisterEntityFactory(# scriptName, &__createInstance ## scriptName); \
		}																							   \
	}																								   \
	__entityRegister ## scriptName ## Instance;

class ClientEntityFactory : public IGameSystem
{
	DECLARE_SINGLETON(ClientEntityFactory);
public:

	typedef ClientEntity *(*CreateEntityFunc)();

	virtual GameSystemPriorityId GetPriority() { return GameSystemPriorites::CLIENT_ENTITY_FACTORY; }

	virtual bool Init();
	virtual void Shutdown();

	virtual void Update(float frametime);

	virtual void OnLevelShutdown();

	static void RegisterEntityFactory(const char *name, CreateEntityFunc function);

	ClientEntity *CreateClientEntity(const char *className, int networkId);

	void OnEntityRemoved(ClientEntity *entity);

	void ApplyNetworkSymbolDefinitions(ByteBuffer &buffer);
	void CreateNetworkEntity(int networkId, Uint8 networkTableIndex, ByteBuffer &buffer);
	void DestroyNetworkEntity(int networkId);

	void ApplyWorldStateUpdates(ByteBuffer &buffer);
	void ApplyWorldStateComplete(ByteBuffer &buffer);

	inline std::vector<ClientEntity *> &GetEntities()
	{
		return activeEntities;
	}

	inline ClientEntity *GetNetworkEntityByIndex(int index)
	{
		if (index < 0 || index >= NETWORKED_ENTITY_COUNT)
		{
			return nullptr;
		}

		return networkEntities[index];
	}

private:
	void FreeAllEntities();

	std::unordered_map<std::string, CreateEntityFunc> functions;

	ClientEntity *networkEntities[NETWORKED_ENTITY_COUNT];
	std::vector<ClientEntity *> activeEntities;

	struct NetworkTableDefinition
	{
		char				className[MAX_CLASS_NAME];

		std::vector<int>	memberLookup;
		std::vector<int>	memberSize;
		std::vector<bool>	memberIsDynamic;
	};

	std::vector<NetworkTableDefinition> networkTableDefinitions;
};
#endif
