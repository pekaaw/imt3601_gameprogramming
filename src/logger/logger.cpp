#include "logger.h"
#include "appInterface/iAppInterface.h"

SINGLETON_INSTANCE(Logger);

EXPOSE_APP( INTERFACE_ILOGGER_VERSION,  Logger::GetInstance() );

Logger::Logger()
{
}

void Logger::Log(const char *message, LogType type) {

}

void Logger::Log(const char *message, LogType type, const char *file, int line) {

	OutputDebugStringA(message);
}