#ifndef ENGINE_H
#define ENGINE_H

#include "engine\iengine.h"
#include "engine\iengineclient.h"
#include "engine\iengineserver.h"
#include "singleton.h"
#include "globals.h"

class World;
class INetworking;

class Engine
	: public IEngine					// exposed to launcher
	  , public IEngineClient			// for client dll
	  , public IEngineServer			// for server dll
{
	DECLARE_SINGLETON(Engine);
public:

	enum RunState_e
	{
		STATE_INIT = 0,
		STATE_RUNNING,
		STATE_SHUTDOWN,
	};

	~Engine();

	IGlobals *GetClientGlobals();
	IGlobals *GetServerGlobals();
	IGlobals *GetGUIGlobals();

	IEngineClient *ToIEngineClient() { return dynamic_cast<IEngineClient*>(this); }
	IEngineServer *ToIEngineServer() { return dynamic_cast<IEngineServer*>(this); }

	void LoadMap(const char *filename);
	ThreadLock *GetThreadLock();

	inline bool IsInitializationMode(InitializationMode mode) const { return initializationMode == mode; }
	inline bool IsInState(RunState_e state) const { return runState == state; }
	inline void ChangeState(RunState_e state) { runState = state; }

	void ShutdownMap();

	inline void TogglePhysicsDebug() { drawPhysicsDebug = !drawPhysicsDebug; }

	// IEngine
	virtual bool Init(InitializationMode mode, void *windowHandle, int width, int height);
	virtual void Shutdown();

	virtual void Run();
	virtual void ResizeWindow(int width, int height);

	// IEngineClient & IEngineServer
	virtual IWorld *GetWorld();
	virtual bool IsInToolsMode() const;

	// IEngineClient
	virtual void SendClientCommand(const char *command);

	// IEngineServer

private:
	void ProcessSDLEvents();

	void OnLevelShutdown();
	void OnLevelInit();

	void ResetGlobals();

	void DispatchMessages();

	InitializationMode initializationMode;
	RunState_e runState;
	void *windowHandle;

	ThreadLock *threadLock;

	Globals clientGlobals;
	Globals guiGlobals;
	GlobalsFixedInterval simulationGlobals;

	World *currentMap;

	bool drawPhysicsDebug;
};
#endif
