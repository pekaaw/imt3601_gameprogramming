#ifndef WORLD_H
#define WORLD_H

#include "engine\iworld.h"

#include <vector>

class World;
class WorldGeometry;
class IMaterial;

//class WorldEntity
//{
//	friend class World;
//public:
//	WorldEntity() : className(nullptr) {}
//	~WorldEntity() { delete [] className; }
//	WorldEntity( const WorldEntity &other) { className = G_StrCreateCopy(other.className); }
//
//private:
//	char *className;
//};

struct WorldVertex
{
	vec3 position;
};

class World : public IWorld
{
	World();
	NO_COPY(World);
	~World();
public:

	void Release();

	static World *LoadFromFile(const char *filename);

	virtual WorldGeometry *GetGeometryForModify();

	virtual void Init();
	virtual void Shutdown();

	// IWorld
	virtual int GetVersion() const;
	virtual const char *GetName() const;

	virtual int GetWidth() const;
	virtual int GetHeight() const;

	virtual int GetCameraWidth() const;
	virtual int GetCameraHeight() const;

	virtual const GlobalLightParams &GetGlobalLightParams() const;
	virtual const FogParams &GetFogParams() const;

	virtual int GetTileCount() const;
	virtual const WorldTile &GetTile(int index) const;

	virtual int GetWaveDuration() const;
	virtual int GetPauseDuration() const;
	virtual int GetWaveFlags() const;

	virtual int GetWaveCount() const;
	virtual const WorldWave &GetWave(int index) const;

	virtual const IWorldGeometry *GetGeometry() const;
	virtual IMaterial *GetMaterial() const;
	virtual IMaterial *GetWaterMaterial() const;

	virtual int GetTileIndexAtPosition(const vec3 &worldPosition) const;
	virtual bool GetTileIndexAtPosition(const vec3 &worldPosition, int &index_x, int &index_y) const;
	virtual vec3 GetTilePosition(int index) const;

	virtual void GetElevationHeights(int index, vec4 &heights, bool ignoreWater) const;
	virtual float GetElevationHeight(const vec3 &worldPosition, bool ignoreWater) const;
	virtual IMesh *CreateProjectedDecal(const vec3 &origin, const euler &angles, const vec3 &size, bool ignoreWater);

private:
	inline int GetTileVertexIndex(int tileIndex)
	{
		return tileIndex % width
				+ (tileIndex / width) * (width + 1);
	};

	inline int GetTileVertexIndex(int tileIndexX, int tileIndexY)
	{
		return tileIndexX + tileIndexY * (width + 1);
	};

	enum TileNeightbours
	{
		TN_MOUNTAIN_UPPER_LEFT = (1 << 0),
		TN_MOUNTAIN_UPPER = (1 << 1),
		TN_MOUNTAIN_UPPER_RIGHT = (1 << 2),
		TN_MOUNTAIN_LEFT = (1 << 3),
		TN_MOUNTAIN = (1 << 4),
		TN_MOUNTAIN_RIGHT = (1 << 5),
		TN_MOUNTAIN_LOWER_LEFT = (1 << 6),
		TN_MOUNTAIN_LOWER = (1 << 7),
		TN_MOUNTAIN_LOWER_RIGHT = (1 << 8),

		TN_MOUNTAIN_MASK = 0x0001FF,

		TN_WATER_UPPER_LEFT = (1 << 9),
		TN_WATER_UPPER = (1 << 10),
		TN_WATER_UPPER_RIGHT = (1 << 11),
		TN_WATER_LEFT = (1 << 12),
		TN_WATER = (1 << 13),
		TN_WATER_RIGHT = (1 << 14),
		TN_WATER_LOWER_LEFT = (1 << 15),
		TN_WATER_LOWER = (1 << 16),
		TN_WATER_LOWER_RIGHT = (1 << 17),

		TN_WATER_MASK = 0x03FE00,
	};

	void GenerateWorldGeometry();
	void GenerateNeighbours();
	void GenerateVertexPositions();

	float GetMountainOffset(int tile, int corner);
	float GetWaterOffset(int tile, int corner);
	void GetElevationPoints(int tile, vec4 &offsets) const;

	Uint32 version;
	char *name;

	Uint16 width;
	Uint16 height;

	Uint16 camWidth;
	Uint16 camHeight;

	GlobalLightParams globalLight;
	FogParams fog;

	Uint8 defaultTileType;
	int tileCount;
	WorldTile *tiles;

	int vertexCount;
	int *tileNeighbours;
	WorldVertex *tileVertices;

	Uint16 waveDuration;
	Uint16 pauseDuration;
	Uint8 waveFlags;

	std::vector<WorldWave> waves;
	WorldGeometry *geometry;
	IMaterial *material;
	IMaterial *waterMaterial;

	std::vector<PhysicsBody> solidTiles;
};
#endif
