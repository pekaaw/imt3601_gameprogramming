
#include "pch.h"
#include "servercontroller.h"
#include "engine.h"
#include "engine/iworld.h"
#include "networking\inetworking.h"
#include "networking\iserver.h"

SINGLETON_INSTANCE(ServerController);


ServerController::ServerController()
	: networkServer(nullptr)
{
}

ServerController::~ServerController()
{
}

bool ServerController::HandlePackage(const NetworkAddress &source, const int& clientReference, Uint8 type, ByteBuffer &buffer)
{
	switch (type)
	{
	case MessageTypes::CS_CONNECTION_ATTEMPT:
		{
			HandleClientConnectionAttempt(source, clientReference, buffer);
		}
		break;
	case MessageTypes::CS_CONNECTION_PRECACHED:
		{
			HandleClientConnectionPrecache(source, clientReference, buffer);
		}
		break;
	case MessageTypes::CS_CONNECTION_FINISHED:
		{
			HandleClientConnectionFinished(source, clientReference, buffer);
		}
		break;
	case MessageTypes::CS_CLIENT_COMMAND:
		{
			HandleClientCommand(source, clientReference, buffer);
		}
		break;
	default:
		return false;
	}

	return true;
}

void ServerController::Simulate()
{
	// Handle network packages
	if (networkServer != nullptr)
	{
		Byte* data;
		Uint32 size;
		int reference;
		NetworkAddress address;

		while(data = networkServer->HasNewMessage(reference, size, address.ip, address.port))
		{
			ByteBuffer buffer;
			buffer.Attach(data, size);
				
			DBGMSGF("SERVER << %i (%i bytes) \n", data[0], size);

			HandlePackage(address, reference, buffer.ReadUCharSafe(MessageTypes::INVALID), buffer);
		}

		for(int i = 0; i < MAX_PLAYERS; i++)
		{
			if(networkServer->HasDisconnected(clients[i].clientReference, true))
			{
				DropNetworkClient(clients[i].clientReference);
				break;
			}
		}
	}

	ByteBuffer constructionBuffer(-1);

	if (server->WriteEntityConstruction(constructionBuffer))
	{
		ByteBuffer messageBuffer;

		messageBuffer.WriteUChar(MessageTypes::SC_ENTITY_CONSTRUCTION);
		messageBuffer.Write(constructionBuffer.GetBase(), constructionBuffer.GetWritePosition());

		SendMessageToAllClients(messageBuffer.GetBase(), messageBuffer.GetWritePosition());

		messageBuffer.Detach();
	}

	static Byte worldStateData[(1 << 15)] = { MessageTypes::SC_WORLDSTATE_UPDATE };

	ByteBuffer worldStateBuffer(worldStateData, sizeof(worldStateData), 1);

	if (server->WriteEntityWorldStateUpdate(worldStateBuffer))
	{
		SendMessageToAllClients(worldStateBuffer.GetBase(), worldStateBuffer.GetWritePosition(), true);
	}
}

void ServerController::ShutdownServer()
{
	// special handling for local clients otherwise they'd kick themselves on reconnect?
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		ClientData &client = clients[i];

		client.state = ClientStates::DISCONNECTED;
	}

	if (networkServer != nullptr)
	{
		networkServer->Stop();
		networkServer->Release();
		networkServer = nullptr;
	}
}

void ServerController::SendMessageTo(const NetworkAddress &destAddress, const int &destIndex, Byte *data, int sizeInBytes)
{
	Message *message = new Message();
	message->data = data;
	message->size = sizeInBytes;
	message->dest = destAddress;
	message->destclientReference = destIndex;

	// some packages need to  be delayed until after the client
	// fully connected and synchronized the world state
	if (!MessageTypes::IsHandshakeMessage((MessageTypeId)*data))
	{
		int clientIndex = GetClientIndexFromReference(destIndex);

		if (clientIndex >= 0 && !clients[clientIndex].IsFullyConnected())
		{
			QueueClientMessage(clientIndex, message);
			return;
		}
	}

	messages.push_back(message);
}

void ServerController::SendMessageToClient(int clientIndex, Byte *data, int sizeInBytes)
{
	Assert(clientIndex >= 0 && clientIndex < MAX_PLAYERS);

	const ClientData &client = clients[clientIndex];

	if (client.state == ClientStates::DISCONNECTED)
	{
		Assert(0);

		delete [] data;
		return;
	}

	SendMessageTo(client.address, client.clientReference, data, sizeInBytes);
}

void ServerController::SendMessageToAllClients(Byte *data, int sizeInBytes, bool bufferIsStatic)
{
	Byte *originalData = bufferIsStatic ? nullptr : data;

	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		const ClientData &client = clients[i];

		if (client.IsFree())
		{
			continue;
		}

		Byte *currentData = data;

		// first client takes original buffer
		// following clients need to make a copy
		if (originalData == nullptr)
		{
			currentData = new Byte[sizeInBytes];
			memcpy(currentData, data, sizeInBytes);
		}

		SendMessageToClient(i, currentData, sizeInBytes);

		originalData = nullptr;
	}

	delete [] originalData;
}

void ServerController::QueueClientMessage(int clientIndex, Message *message)
{
	Assert(clientIndex >= 0 && clientIndex < MAX_PLAYERS);
	Assert(!clients[clientIndex].IsFree());

	clients[clientIndex].queuedMessages.push_back(message);
}

Message *ServerController::PollMessage()
{
	if (messages.size() < 1)
	{
		return nullptr;
	}

	Message *message = messages.at(0);
	messages.erase(messages.begin());

	return message;
}

void ServerController::HandleClientConnectionAttempt(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer)
{
	int freeSlot = -1;
	bool doRespond = true;

	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		ClientData &client = clients[i];

		// free slot for this client
		if (client.IsFree() && freeSlot < 0)
		{
			freeSlot = i;
		}
		else if (!client.IsFree()
			&& client.address == source) // client already connected
		{
			freeSlot = -1;
			break;
		}
	}

	ByteBuffer responseBuffer;
	responseBuffer.WriteUChar(MessageTypes::SC_CONNECTION_ATTEMPT_RESPONSE);

	// refuse connection attempt
	if (freeSlot < 0)
	{
		if(clientReference != -1)
		{
			networkServer->Drop(clientReference);
			doRespond = false;
		}
		else
		{
			responseBuffer.WriteBool(false);
		}
	}
	else // accept connection attempt
	{
		responseBuffer.WriteBool(true);
		responseBuffer.WriteUChar(freeSlot);

		if (source.IsSet())
		{
			IWorld *world = Engine::GetInstance()->GetWorld();

			const char *mapName = world ? world->GetName() : "";

			Assert(*mapName);

			responseBuffer.WriteString(mapName);
		}
		else
		{
			responseBuffer.WriteString("");
		}

		// explain networktables + classes to client
		server->WriteNetworkSymbolDefinitions(responseBuffer);

		clients[freeSlot].address = source;
		clients[freeSlot].clientReference = clientReference;
		clients[freeSlot].state = ClientStates::LOADING;
	}

	SendMessageTo(source, clientReference, responseBuffer.GetBase(), responseBuffer.GetWritePosition());

	responseBuffer.Detach();
}

void ServerController::HandleClientConnectionPrecache(const NetworkAddress &source, const int &clientReference, ByteBuffer &buffer)
{
	int clientIndex = GetClientIndexFromReference(clientReference);

	if (clientIndex < 0)
	{
		return;
	}

	if (clients[clientIndex].state != ClientStates::LOADING)
	{
		Assert(0);
		return;
	}

	clients[clientIndex].state = ClientStates::SYNC;

	ByteBuffer response;
	response.WriteUChar(MessageTypes::SC_CONNECTION_PRECACHE_RESPONSE);

	server->WriteEntityWorldStateComplete(response);

	SendMessageToClient(clientIndex, response.GetBase(), response.GetWritePosition());

	response.Detach();
}

void ServerController::HandleClientConnectionFinished(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer)
{
	int clientIndex = GetClientIndexFromReference(clientReference);

	if (clientIndex < 0)
	{
		return;
	}

	if (clients[clientIndex].state != ClientStates::SYNC)
	{
		Assert(0);
		return;
	}

	ClientData &client = clients[clientIndex];

	client.state = ClientStates::CONNECTED;

	ByteBuffer response;
	response.WriteUChar(MessageTypes::SC_CONNECTION_FINISHED_RESPONSE);

	SendMessageToClient(clientIndex, response.GetBase(), response.GetWritePosition());

	response.Detach();

	// after synching, send any queued up messages
	for (auto m : client.queuedMessages)
	{
		messages.push_back(m);
	}

	client.queuedMessages.clear();

	server->OnClientConnected(clientIndex);
}

void ServerController::HandleClientCommand(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer)
{
	int clientIndex = GetClientIndexFromReference(clientReference);
	
	if (clientIndex < 0)
	{
		return;
	}

	const ClientData &client = clients[clientIndex];

	if (client.state != ClientStates::CONNECTED)
	{
		Assert(0);
		return;
	}

	char command[1024];
	buffer.ReadString(command, sizeof(command));

	server->SetClientCommandHost(clientIndex);
	cvar->ExecuteBuffer(command, CvarExecutionTypes::CLIENTCOMMAND);
	server->SetClientCommandHost(-1);
}

int ServerController::GetClientIndexFromReference(const int &destIndex)
{
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		const ClientData &client = clients[i];

		if (!client.IsFree()
			&& client.clientReference == destIndex)
		{
			return i;
		}
	}

	return -1;
}

bool ServerController::StartNetworkServer(unsigned short port)
{
	if (networkServer != nullptr)
	{
		networkServer->Release();
	}

	networkServer = networking->CreateServer(port);

	if (networkServer->Start())
	{
		return true;
	}
	else
	{
		Assert(0);
		// TODO: Error logging.
		networkServer->Release();
		networkServer = nullptr;
		return false;
	}
}

void ServerController::DropNetworkClient(const int clientReference)
{
	int client = GetClientIndexFromReference(clientReference);

	server->OnClientDisconnected(client);
	networkServer->Drop(clientReference);

	clients[client].Reset();
}

void ServerController::DispatchOutgoingMessage(const int clientReference, ByteBuffer& data, bool reliable)
{
	networkServer->QueueForSending(clientReference, data.GetBase(), data.GetSizeAllocated(), reliable ? SYNCMODE_RELIABLE_NOORDER : SYNCMODE_UNRELIABLE_NOORDER);
	networkServer->SendQueuedMessages();
}

Byte *ServerController::PollIncomingMessages(int clientReference, Uint32& sizeOutput)
{
	return networkServer->HasNewMessage(clientReference, sizeOutput);
}
