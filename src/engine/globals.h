#ifndef GLOBALS_H
#define GLOBALS_H

#include "engine\iglobals.h"

/// Global time/frame information.
/// Might be instantiated multiple times for different modules or contexts
class Globals : public IGlobals
{
public:
	Globals();

	// IGlobals
	/// Get time passed for last frame
	virtual float GetFrametime();
	/// Get frame count
	virtual int GetFrameCount();
	/// Get absolute time
	virtual float GetTime();
	virtual double GetTimeHighPrecision();

	/// Advance time by specific interval
	void Advance(float frametime);
	/// Reset all time information
	void Reset();

	/// Overwrite frame time
	void SetFrametime(float frametime);
	/// Overwrite frame count
	void SetFrameCount(int framecount);
	/// Overwrite absolute time
	void SetTime(double time);

private:
	float frametime;
	int framecount;
	double time;
	float timeFloat;

};

#endif