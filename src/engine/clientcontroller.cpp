
#include "pch.h"
#include "clientcontroller.h"
#include "engine.h"
#include "events.h"
#include "networking\inetworking.h"
#include "networking\iclient.h"


SINGLETON_INSTANCE(ClientController);


ClientController::ClientController()
	: state(ClientStates::DISCONNECTED)
	, networkClient(nullptr)
	, pingTimer(0.0f)
{
}

ClientController::~ClientController()
{
	for (auto m : messages)
	{
		delete m;
	}
}

void ClientController::ShowLoadingScreen()
{
}

void ClientController::HideLoadingScreen()
{
}

void ClientController::ConnectToServer(const NetworkAddress &address)
{
	connectedServer = address;

	ChangeState(ClientStates::CONNECTION_ATTEMPT);

	ByteBuffer buffer;
	buffer.WriteUChar(MessageTypes::CS_CONNECTION_ATTEMPT);

	// If there's a network client already, destroy it.
	if (networkClient != nullptr)
	{
		networkClient->Release();
		networkClient = nullptr;
	}

	bool success = true;

	// It'll connect online if the address is non-zero.
	if (address.IsSet())
	{
		networkClient = networking->CreateClient();

		if (!ConnectToNetworkServer(address, true, 3))
		{
			success = false;
		}
	}
	
	if (success)
	{
		SendMessageToServer(buffer);

		Events::GetInstance()->FireEvent(new KeyValues("game_started"));
	}
}

void ClientController::Disconnect()
{
	connectedServer.Reset();

	if (networkClient != nullptr)
	{
		networkClient->Disconnect();
		networkClient->Release();
		networkClient = nullptr;
	}
}

void ClientController::SendClientCommand(const char *command)
{
	if (state != ClientStates::CONNECTED)
	{
		return;
	}

	ByteBuffer buffer;

	buffer.WriteUChar(MessageTypes::CS_CLIENT_COMMAND);
	buffer.WriteString(command);

	SendMessageToServer(buffer);
}

bool ClientController::HandlePackage(const NetworkAddress &source, Uint8 type, ByteBuffer &buffer)
{
	// unexpected source address
	if (source != connectedServer)
	{
		return false;
	}

	switch (type)
	{
	case MessageTypes::SC_CONNECTION_ATTEMPT_RESPONSE:
		{
			HandleConnectionAttemptResponse(buffer);
		}
		break;
	case MessageTypes::SC_CONNECTION_PRECACHE_RESPONSE:
		{
			HandleConnectionPrecacheResponse(buffer);
		}
		break;
	case MessageTypes::SC_CONNECTION_FINISHED_RESPONSE:
		{
			HandleConnectionFinishedResponse(buffer);
		}
		break;
	case MessageTypes::SC_ENTITY_CONSTRUCTION:
		{
			HandleEntityConstruction(buffer);
		}
		break;
	case MessageTypes::SC_WORLDSTATE_UPDATE:
		{
			HandleWorldStateUpdate(buffer);
		}
		break;
	default:
		return false;
	}

	return true;
}

void ClientController::Simulate()
{
	if (networkClient != nullptr)
	{
		Byte *data;
		Uint32 size;

		while (data = PollIncomingMessages(size))
		{
			//DBGMSGF("CLIENT << %i (%i bytes) \n", data[0], size);

			ByteBuffer buffer;
			buffer.Attach(data, size);
			HandlePackage(connectedServer, buffer.ReadUCharSafe(MessageTypes::INVALID), buffer);

			// I can just put connectedServer as address, because PollIncomingMessages wouldn't return a packet if the address wasn't the Server's
		}

		if (networkClient->GetConnectionStatus() == ConnectionStatus::CSTATUS_DROPPED
			|| networkClient->HasTimedOut())
		{
			ChangeState(ClientStates::DISCONNECTED);

			Engine::GetInstance()->ShutdownMap();

			KeyValues *event = new KeyValues("game_ended");
			Events::GetInstance()->FireEvent(event);

			if (networkClient != nullptr)
			{
				networkClient->Release();
				networkClient = nullptr;
			}
		}
		else if (pingTimer < Engine::GetInstance()->GetClientGlobals()->GetTime())
		{
			networkClient->Ping();
			pingTimer = Engine::GetInstance()->GetClientGlobals()->GetTime() + 1.0f;
		}
	}
}

void ClientController::SendMessageToServer(Byte *data, int sizeInBytes)
{
	Message *message = new Message();
	message->data = data;
	message->size = sizeInBytes;

	messages.push_back(message);
}

void ClientController::SendMessageToServer(ByteBuffer &buffer)
{
	Assert(buffer.GetWritePosition() > 0);

	SendMessageToServer(buffer.GetBase(), buffer.GetWritePosition());
	buffer.Detach();
}

Message *ClientController::PollMessage()
{
	if (messages.size() < 1)
	{
		return nullptr;
	}

	Message *message = messages.at(0);
	messages.erase(messages.begin());

	message->dest = connectedServer; //TODO: Reconsider
	return message;
}

bool ClientController::IsInState(ClientStateId state)
{
	return this->state == state;
}

void ClientController::ChangeState(ClientStateId state)
{
	this->state = state;
}

void ClientController::HandleConnectionAttemptResponse(ByteBuffer &buffer)
{
	// connection was refused
	if (!buffer.ReadBool())
	{
		return;
	}

	ChangeState(ClientStates::LOADING);

	int localClientIndex = buffer.ReadUChar();

	char mapName[128];
	buffer.ReadString(mapName, sizeof(mapName));

	if (*mapName)
	{
		Engine::GetInstance()->LoadMap(mapName);
	}

	client->ApplyNetworkSymbolDefinitions(buffer);
	client->SetLocalClientIndex(localClientIndex);

	ByteBuffer response;
	response.WriteUChar(MessageTypes::CS_CONNECTION_PRECACHED);

	SendMessageToServer(response);
}

void ClientController::HandleConnectionPrecacheResponse(ByteBuffer &buffer)
{
	ChangeState(ClientStates::SYNC);

	client->ApplyWorldStateComplete(buffer);

	ByteBuffer response;
	response.WriteUChar(MessageTypes::CS_CONNECTION_FINISHED);

	SendMessageToServer(response);
}

void ClientController::HandleConnectionFinishedResponse(ByteBuffer &buffer)
{
	ChangeState(ClientStates::CONNECTED);

	pingTimer = 0.0f;

	HideLoadingScreen();
}

void ClientController::HandleEntityConstruction(ByteBuffer &buffer)
{
	if (!IsInState(ClientStates::CONNECTED))
	{
		return;
	}

	Uint16 count = buffer.ReadUShort();

	//DBGMSGF("Received entity construction change set with count %u\n", count);

	for (int i = 0; i < count; i++)
	{
		Uint16 networkId = buffer.ReadUShort();
		bool spawned = buffer.ReadBool();

		if (spawned)
		{
			Uint8 networkTableIndex = buffer.ReadUChar();

			Assert(networkTableIndex >= 0 && networkTableIndex < 0xFF);

			//DBGMSGF("Spawning client entity with network id %u from type '%s'\n",
			//	networkId, className);

			client->CreateNetworkEntity(networkId, networkTableIndex, buffer);
		}
		else
		{
			//DBGMSGF("Destroying client entity with network id %u\n",
			//	networkId);

			client->DestroyNetworkEntity(networkId);
		}
	}
}

void ClientController::HandleWorldStateUpdate(ByteBuffer &buffer)
{
	if (!IsInState(ClientStates::CONNECTED))
	{
		return;
	}

	client->ApplyWorldStateUpdates(buffer);
}

bool ClientController::ConnectToNetworkServer(const char* host, unsigned short port, bool block, size_t blockTimeout)
{
	networkClient->SetTimeoutThreshold(blockTimeout);
	return networkClient->Connect(host, port, block);
}

bool ClientController::ConnectToNetworkServer(NetworkAddress host, bool block, size_t blockTimeout)
{
	networkClient->SetTimeoutThreshold(blockTimeout);
	return networkClient->Connect(host.ip, host.port, block);
}

void ClientController::DisconnectFromServer()
{
	networkClient->Disconnect();
}

ConnectionStatus ClientController::GetConnectionStatus()
{
	return networkClient->GetConnectionStatus();
}

void ClientController::DispatchOutgoingMessage(ByteBuffer &data, bool reliable)
{
	networkClient->QueueForSending(data.GetBase(), data.GetSizeAllocated(), reliable);
	networkClient->SendMessages();
}

Byte *ClientController::PollIncomingMessages(Uint32 &sizeOutput)
{
	return networkClient->HasNewMessage(sizeOutput);
}

void ClientController::ShutdownClient()
{
	Disconnect();
}