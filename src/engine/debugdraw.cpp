
#include "pch.h"
#include "engine.h"
#include "debugdraw.h"


SINGLETON_INSTANCE(DebugDraw);

EXPOSE_APP(INTERFACE_IDEBUGDRAW_VERSION, DebugDraw::GetInstance());

DebugDraw::DebugDraw()
	: debugMaterial(nullptr)
	, debugMaterialNoZ(nullptr)
{
}

DebugDraw::~DebugDraw()
{
	Assert(debugMaterial == nullptr);
	Assert(debugMaterialNoZ == nullptr);
}

void DebugDraw::Init()
{
	Assert(debugMaterial == nullptr);
	Assert(debugMaterialNoZ == nullptr);

	debugMaterial = materialDict->FindMaterial("debug/flat");
	debugMaterialNoZ = materialDict->FindMaterial("debug/flatnoz");

	Assert(debugMaterial != nullptr);
	Assert(debugMaterialNoZ != nullptr);

	debugMaterial->IncrementReferenceCount();
	debugMaterialNoZ->IncrementReferenceCount();
}

void DebugDraw::Shutdown()
{
	Assert(debugMaterial != nullptr);
	Assert(debugMaterialNoZ != nullptr);

	debugMaterial->DecrementReferenceCount();
	debugMaterial = nullptr;

	debugMaterialNoZ->DecrementReferenceCount();
	debugMaterialNoZ = nullptr;
}

void DebugDraw::DrawLine(const vec3 &start, const vec3 &end, const vec3 &color, float alpha, float lifetime, bool ignoreZ)
{
	DebugLine line;
	line.start = start;
	line.end = end;
	line.color = color;
	line.alpha = alpha;
	line.timer = lifetime;
	line.ignoreZ = ignoreZ;

	lines.push_back(line);
}

void DebugDraw::DrawLineLoop(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime, bool ignoreZ)
{
	if (vertexCount < 1)
	{
		return;
	}

	for (int i = 0; i < vertexCount; i++)
	{
		DebugLine line;
		line.start = vertices[i];
		line.end = (i + 1 < vertexCount) ? vertices[i + 1] : vertices[0];
		line.color = color;
		line.alpha = alpha;
		line.timer = lifetime;
		line.ignoreZ = ignoreZ;

		lines.push_back(line);
	}
}

void DebugDraw::DrawTriangleFan(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime, bool ignoreZ)
{
	if (vertexCount < 3)
	{
		return;
	}

	for (int i = 2; i < vertexCount; i++)
	{
		DebugTriangle triangle;

		triangle.vertices[0] = vertices[0];
		triangle.vertices[1] = vertices[i - 1];
		triangle.vertices[2] = vertices[i];

		triangle.alpha = alpha;
		triangle.color = color;
		triangle.ignoreZ = ignoreZ;
		triangle.timer = lifetime;

		triangles.push_back(triangle);
	}
}

void DebugDraw::DrawBox(const vec3 &mins, const vec3 &maxs, const vec3 &color, float alpha, float lifetime, bool ignoreZ)
{
	DrawLine(mins, SWIZZLE_VECTOR(maxs, mins, mins), color, alpha, lifetime, ignoreZ);
	DrawLine(mins, SWIZZLE_VECTOR(mins, maxs, mins), color, alpha, lifetime, ignoreZ);
	DrawLine(mins, SWIZZLE_VECTOR(mins, mins, maxs), color, alpha, lifetime, ignoreZ);

	DrawLine(SWIZZLE_VECTOR(maxs, mins, mins), SWIZZLE_VECTOR(maxs, maxs, mins), color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(mins, maxs, mins), SWIZZLE_VECTOR(maxs, maxs, mins), color, alpha, lifetime, ignoreZ);

	DrawLine(SWIZZLE_VECTOR(maxs, mins, mins), SWIZZLE_VECTOR(maxs, mins, maxs), color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(maxs, maxs, mins), maxs, color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(mins, maxs, mins), SWIZZLE_VECTOR(mins, maxs, maxs), color, alpha, lifetime, ignoreZ);

	DrawLine(SWIZZLE_VECTOR(mins, mins, maxs), SWIZZLE_VECTOR(mins, maxs, maxs), color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(mins, mins, maxs), SWIZZLE_VECTOR(maxs, mins, maxs), color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(maxs, mins, maxs), maxs, color, alpha, lifetime, ignoreZ);
	DrawLine(SWIZZLE_VECTOR(mins, maxs, maxs), maxs, color, alpha, lifetime, ignoreZ);
}

void DebugDraw::Render()
{
	const float frametime = Engine::GetInstance()->GetClientGlobals()->GetFrametime();

	if (!lines.empty())
	{
		int lineCount = lines.size();

		while (lineCount > 0)
		{
			int drawCount = MIN(lineCount, DYNAMIC_MESH_MAX_VERTICES / 2);
			int linesLeft = drawCount;

			IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::LINES, VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, drawCount);

			MeshBuilder builder;
			builder.AttachModify(mesh);

			for (int i = lineCount - 1; linesLeft > 0; linesLeft--, i--)
			{
				const auto &line = lines[i];

				builder.Position3v(line.start);
				builder.Color4f(line.color.x(), line.color.y(), line.color.z(), line.alpha);
				builder.AdvanceVertex();

				builder.Position3v(line.end);
				builder.Color4f(line.color.x(), line.color.y(), line.color.z(), line.alpha);
				builder.AdvanceVertex();
			}
		
			builder.Detach();

			renderContext->BindMesh(mesh);

			debugMaterialNoZ->Draw();
			//debugMaterial->Draw();

			lineCount -= drawCount;
		}

		auto itrFirst = lines.end();

		// batch erase objects for optimal perf
		for (auto itr = lines.begin(); itr != lines.end();)
		{
			itr->timer -= frametime;

			if (itr->timer <= 0.0f)
			{
				if (itrFirst == lines.end())
				{
					itrFirst = itr;
				}

				itr++;
			}
			else
			{
				if (itrFirst != lines.end())
				{
					itr = lines.erase(itrFirst, itr);

					itrFirst = lines.end();
				}
				else
				{
					itr++;
				}
			}
		}

		if (itrFirst != lines.end())
		{
			lines.erase(itrFirst, lines.end());
		}
	}

	if (!triangles.empty())
	{
		int triangleCount = triangles.size();

		while (triangleCount > 0)
		{
			int drawCount = MIN(triangleCount, DYNAMIC_MESH_MAX_VERTICES / 3);
			int trianglesLeft = drawCount;

			IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::TRIANGLES, VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, drawCount);

			MeshBuilder builder;
			builder.AttachModify(mesh);

			for (int i = triangleCount - 1; trianglesLeft > 0; trianglesLeft--, i--)
			{
				const auto &tri = triangles[i];
				
				builder.Position3v(tri.vertices[0]);
				builder.Color4f(tri.color.x(), tri.color.y(), tri.color.z(), tri.alpha);
				builder.AdvanceVertex();

				builder.Position3v(tri.vertices[1]);
				builder.Color4f(tri.color.x(), tri.color.y(), tri.color.z(), tri.alpha);
				builder.AdvanceVertex();

				builder.Position3v(tri.vertices[2]);
				builder.Color4f(tri.color.x(), tri.color.y(), tri.color.z(), tri.alpha);
				builder.AdvanceVertex();
			}
			
			builder.Detach();

			renderContext->BindMesh(mesh);

			debugMaterialNoZ->Draw();
			//debugMaterial->Draw();

			triangleCount -= drawCount;
		}

		auto itrFirst = triangles.end();

		// batch erase objects for optimal perf
		for (auto itr = triangles.begin(); itr != triangles.end();)
		{
			itr->timer -= frametime;

			if (itr->timer <= 0.0f)
			{
				if (itrFirst == triangles.end())
				{
					itrFirst = itr;
				}

				itr++;
			}
			else
			{
				if (itrFirst != triangles.end())
				{
					itr = triangles.erase(itrFirst, itr);

					itrFirst = triangles.end();
				}
				else
				{
					itr++;
				}
			}
		}

		if (itrFirst != triangles.end())
		{
			triangles.erase(itrFirst, triangles.end());
		}
	}
}