#ifndef PCH_H
#define PCH_H


#include "util\platform.h"

#pragma warning( disable: 4996 )
#pragma warning( disable: 4530 )

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//#include <fstream>
//#include <direct.h>
//#include <vector>
//#include <unordered_map>
//#include <stack>
#include <memory>
#include <unordered_set>

#include <SDL.h>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"
#include "util/bytebuffer.h"
#include "util/collisionutil.h"
#include "util/threadlock.h"
#include "util/keyvalues.h"

#include <shareddefs.h>

#include "protocol.h"
#include "message.h"
#include "appinterface\iappinterface.h"
#include "logger\ilogger.h"
#include "materialsystem\materialsystem.h"
#include "materialsystem\irendercontext.h"
#include "materialsystem\imaterialdict.h"
#include "materialsystem\imaterial.h"
#include "materialsystem\itexturedict.h"
#include "materialsystem\itexture.h"
#include "materialsystem\imdxrenderabledict.h"
#include "materialsystem\ifbodict.h"
#include "materialsystem\imesh.h"
#include "materialsystem\meshbuilder.h"
#include "mdxlib\imdxmodeldict.h"
#include "client\iclient.h"
#include "server\iserver.h"
#include "cvar\icvarinterface.h"
#include "physics/iphysics.h"
#include "globals.h"
#include "globalsfixedinterval.h"
#include "networking\inetworking.h"


extern ILogger *logger;
extern IRenderContext *renderContext;
extern IMaterialDict *materialDict;
extern ITextureDict *textureDict;
extern IMDXModelDict *modelDict;
extern IMDXRenderableDict *renderableDict;
extern IFBODict *fboDict;
extern IClient *client;
extern IServer *server;
extern ICvarInterface *cvar;
extern IPhysics *physics;
extern INetworking *networking;


#endif