#ifndef PROTOCOL_H
#define PROTOCOL_H


namespace MessageTypes
{
	/// CS = client to server
	/// SC = server to client
	enum MessageType_e
	{
		INVALID = 0,

		// connect to server
		CS_CONNECTION_ATTEMPT,				/// Try to connect to server
		CS_CONNECTION_PRECACHED,			/// Load required resources, e.g. map
		CS_CONNECTION_FINISHED,				/// Tell server that we're ready to play

		SC_CONNECTION_ATTEMPT_RESPONSE,		/// Send resources to be loaded or reject client
		SC_CONNECTION_PRECACHE_RESPONSE,	/// Send world state
		SC_CONNECTION_FINISHED_RESPONSE,	/// Tell client to begin rendering game

		SC_ENTITY_CONSTRUCTION,
		SC_WORLDSTATE_UPDATE,

		CS_CLIENT_COMMAND,					/// Player actions like placing towers

		COUNT,
	};

	inline bool IsHandshakeMessage(MessageType_e type)
	{
		return type < SC_ENTITY_CONSTRUCTION;
	}
}
typedef MessageTypes::MessageType_e MessageTypeId;


namespace ClientStates
{
	enum ClientState_e
	{
		/// In main menu
		DISCONNECTED = 0,

		/// Asking server if there's a free slot etc
		CONNECTION_ATTEMPT,

		/// Loading map, resources
		LOADING,

		/// Receiving server world state
		SYNC,

		/// Connected to server, playing the game
		CONNECTED,
	};
}
typedef ClientStates::ClientState_e ClientStateId;

#endif