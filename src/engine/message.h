#ifndef MESSAGE_H
#define MESSAGE_H

struct NetworkAddress
{
	NetworkAddress()
		: ip(0)
		, port(0)
	{
	}

	inline void Reset()
	{
		ip = 0;
		port = 0;
	}

	inline bool IsSet() const
	{
		return ip != 0;
	}

	inline bool operator==(const NetworkAddress &other) const
	{
		return ip == other.ip && port == other.port;
	}

	inline bool operator!=(const NetworkAddress &other) const
	{
		return !(*this == other);
	}

	Uint32 ip;
	Uint16 port;
};

struct Message
{
	Message()
		: data(nullptr)
		, size(0)
		, destclientReference(0)
	{
	}

	~Message()
	{
		delete [] data;
	}

	Byte *data;
	int size;

	NetworkAddress source;
	NetworkAddress dest;
	int destclientReference;

private:
	Message(const Message &other);
	Message(Message &&other);
};


#endif