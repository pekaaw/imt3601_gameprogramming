#ifndef INPUT_H
#define INPUT_H

#include "engine\iinput.h"

class Input : public IInput
{
	DECLARE_SINGLETON(Input);
public:
	~Input();

	void MouseMove(int x, int y);

	void ExecuteCommandBuffer();

	// IInput
	virtual int GetMousePosX();
	virtual int GetMousePosY();

	virtual void KeyDown(SDL_Keycode sdlKey);
	virtual void KeyUp(SDL_Keycode sdlKey);
	virtual void OnTextInput(const char *text);

	virtual void MouseDown(int mouseButton);
	virtual void MouseUp(int mouseButton);
	virtual void MouseWheel(int delta);

	virtual void StartTextInput(IInputKeyListener *listener);
	virtual void StopTextInput();

	virtual void AddKeyListener(IInputKeyListener *listener);
	virtual void RemoveKeyListener(IInputKeyListener *listener);

	virtual void AddMouseListener(IInputMouseListener *listener);
	virtual void RemoveMouseListener(IInputMouseListener *listener);

	virtual void SetBinding(SDL_Keycode sdlKey, const char *command);
	virtual const char *GetBinding(SDL_Keycode sdlKey);

	virtual SDL_Keycode GetKeyOfBinding(const char *command);

	void ReleaseAllKeys();

private:

	bool EvaluateKeyDown(SDL_Keycode sdlKey);
	void EvaluateKeyUp(SDL_Keycode sdlKey);

	int mousePosX;
	int mousePosY;

	std::vector<IInputKeyListener*> keyListeners;
	std::vector<IInputMouseListener*> mouseListeners;

	std::vector<SDL_Keycode> pressedBoundKeys;
	std::unordered_map<SDL_Keycode, IInputKeyListener*> activeKeyListeners;
	std::unordered_map<SDL_Keycode, std::string> bindings;

	std::string commandBuffer;

	IInputKeyListener *activeTextListener;
};



#endif