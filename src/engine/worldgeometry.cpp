#include "pch.h"
#include "worldgeometry.h"

WorldGeometry::WorldGeometry()
{
}

WorldGeometry::~WorldGeometry()
{
	for (auto e : worldList)
	{
		e.mesh->Release();
	}

	for (auto e : waterList)
	{
		e.mesh->Release();
	}
}

int WorldGeometry::GetWorldListCount() const
{
	return worldList.size();
}

const IMesh *WorldGeometry::GetWorldListMesh(int index) const
{
	Assert(index >= 0 && index < (int)worldList.size());
	return worldList[index].mesh;
}

void WorldGeometry::GetWorldListBounds(int index, vec3 &min, vec3 &max) const
{
	Assert(index >= 0 && index < (int)worldList.size());

	const ListEntry &entry = worldList[index];
	min = entry.min;
	max = entry.max;
}

int WorldGeometry::GetWaterListCount() const
{
	return waterList.size();
}

const IMesh *WorldGeometry::GetWaterListMesh(int index) const
{
	Assert(index >= 0 && index < (int)waterList.size());
	return waterList[index].mesh;
}

void WorldGeometry::GetWaterListBounds(int index, vec3 &min, vec3 &max) const
{
	Assert(index >= 0 && index < (int)waterList.size());

	const ListEntry &entry = waterList[index];
	min = entry.min;
	max = entry.max;
}

void WorldGeometry::AddWorldMesh(ListEntry &entry)
{
	worldList.push_back(entry);
}

void WorldGeometry::AddWaterMesh(ListEntry &entry)
{
	waterList.push_back(entry);
}

void WorldGeometry::ChangeTileUVs(int tile_x, int tile_y, vec2 uvs)
{
	IMesh *mesh = nullptr;
	int tileIndex = 0;

	for (unsigned int i = 0; i < worldList.size(); i++)
	{
		ListEntry &entry = worldList[i];

		if (tile_x >= entry.startIndex_x
			&& tile_x < entry.startIndex_x + entry.width
			&& tile_y >= entry.startIndex_y
			&& tile_y < entry.startIndex_y + entry.height)
		{
			mesh = entry.mesh;

			tileIndex = tile_x - entry.startIndex_x
				+ (tile_y - entry.startIndex_y) * entry.width;

			if (tileIndex >= entry.width * entry.height)
			{
				Assert(0);
				return;
			}
			break;
		}
	}

	if (mesh == nullptr)
	{
		return;
	}

	Assert(tileIndex * 6 < mesh->GetVertexCount());

	MeshBuilder builder;
	builder.AttachModify(mesh, tileIndex * 6, 6);

	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();
	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();
	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();

	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();
	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();
	builder.Texcoord2f(uvs.x() + WORLD_TILE_UV_INSET, uvs.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
	builder.AdvanceVertex();

	builder.Detach();
}