#include "pch.h"
#include "engine.h"
#include "enginetools.h"
#include "world.h"
#include "worldgeometry.h"
#include "input.h"
#include "debugdraw.h"
#include "events.h"
#include "clientcontroller.h"
#include "servercontroller.h"
#include "gui/igui.h"

SINGLETON_INSTANCE(Engine);

EXPOSE_APP(INTERFACE_IENGINE_VERSION, Engine::GetInstance());

EXPOSE_APP(INTERFACE_IGLOBALS_CLIENT_VERSION, Engine::GetInstance()->GetClientGlobals());
EXPOSE_APP(INTERFACE_IGLOBALS_SERVER_VERSION, Engine::GetInstance()->GetServerGlobals());
EXPOSE_APP(INTERFACE_IGLOBALS_GUI_VERSION, Engine::GetInstance()->GetGUIGlobals());


ILogger *logger;
IRenderContext *renderContext;
IMaterialDict *materialDict;
ITextureDict *textureDict;
IMDXModelDict *modelDict;
IMDXRenderableDict *renderableDict;
IFBODict *fboDict;
IClient *client;
IServer *server;
ICvarInterface *cvar;
static IGUI *gui;
IPhysics *physics;
INetworking *networking;

CON_COMMAND(map)
{
	// TODO: read map name from cmd args
	const char *mapName = "Level1";
	unsigned short port = 23962;

	if (args->GetArgCount() > 0)
	{
		mapName = args->GetArg(0);
	}

	if (args->GetArgCount() > 1)
	{
		port = atoi(args->GetArg(1));
	}

	ServerController::GetInstance()->ShutdownServer();
	ServerController::GetInstance()->StartNetworkServer(port);

	// start a local server and connect to it
	ClientController::GetInstance()->ShowLoadingScreen();

	Engine::GetInstance()->LoadMap(mapName);

	ClientController::GetInstance()->ConnectToServer(NetworkAddress());
}

CON_COMMAND(joinserver)
{
	NetworkAddress addr;

	addr.ip = 0x0100007fU;
	addr.port = networking->GetNetworkOrderShort(23962U);

	if (args->GetArgCount() > 0)
	{
		const char *ipString = args->GetArg(0);

		if (ipString && *ipString)
		{
			addr.ip = 0;

			while (ipString != nullptr)
			{
				addr.ip |= atoi(ipString);

				ipString = G_StrStr(ipString, ".");

				if (ipString != nullptr)
				{
					ipString++;
					addr.ip <<= 8;
				}
			}

			addr.ip = networking->GetNetworkOrderUInt(addr.ip);
		}
	}

	if (args->GetArgCount() > 1)
	{
		addr.port = networking->GetNetworkOrderShort(atoi(args->GetArg(1)));
	}

	ServerController::GetInstance()->ShutdownServer();

	ClientController::GetInstance()->ShowLoadingScreen();
	ClientController::GetInstance()->ConnectToServer(addr);
}

CON_COMMAND(r_fullscreen)
{
	renderContext->SetFullscreen(!renderContext->IsFullscreen());
}

CON_COMMAND(dbg_physics)
{
	Engine::GetInstance()->TogglePhysicsDebug();
}

CON_COMMAND(shutdown)
{
	Engine::GetInstance()->ChangeState(Engine::STATE_SHUTDOWN);
}

Engine::Engine()
	: initializationMode(INIT_INVALID)
	  , runState(STATE_INIT)
	  , windowHandle(nullptr)
	  , currentMap(nullptr)
	  , threadLock(nullptr)
	  , drawPhysicsDebug(false)
{
	simulationGlobals.SetFixedInterval(SIMULATION_FRAMESPEED);
}

Engine::~Engine()
{
	Assert(currentMap == nullptr);
}

IGlobals *Engine::GetClientGlobals()
{
	return &clientGlobals;
}

IGlobals *Engine::GetServerGlobals()
{
	return &simulationGlobals;
}

IGlobals *Engine::GetGUIGlobals()
{
	return &guiGlobals;
}

void Engine::ShutdownMap()
{
	if (currentMap != nullptr)
	{
		OnLevelShutdown();

		currentMap->Release();

		materialDict->ReleaseUnreferencedMaterials();
		textureDict->ReleaseUnreferencedTextures();

		currentMap = nullptr;
	}
}

void Engine::LoadMap(const char *filename)
{
	ShutdownMap();

	currentMap = World::LoadFromFile(filename);

	if (currentMap != nullptr)
	{
		OnLevelInit();
	}
}

void Engine::OnLevelShutdown()
{
	client->OnLevelShutdown();
	server->OnLevelShutdown();
}

void Engine::OnLevelInit()
{
	ResetGlobals();

	server->OnLevelInit();
	client->OnLevelInit();
}

void Engine::ResetGlobals()
{
	clientGlobals.Reset();
	guiGlobals.Reset();
	simulationGlobals.Reset();
}

ThreadLock *Engine::GetThreadLock()
{
	Assert(IsInitializationMode(INIT_TOOLSMODE));
	Assert(threadLock != nullptr);

	return threadLock;
}

void Engine::ProcessSDLEvents()
{
	SDL_Event sdlEvent;

	while (SDL_PollEvent(&sdlEvent))
	{
		switch (sdlEvent.type)
		{
		case SDL_QUIT:
			ChangeState(STATE_SHUTDOWN);
			break;
		case SDL_WINDOWEVENT:
			if (sdlEvent.window.event == SDL_WINDOWEVENT_RESIZED)
			{
				ResizeWindow(sdlEvent.window.data1, sdlEvent.window.data2);
			}
			break;
		// TODO: translate these events to key-values
		// and pump them through a common event controller
		case SDL_MOUSEMOTION:
			{
				Input::GetInstance()->MouseMove(sdlEvent.motion.x, sdlEvent.motion.y);
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			{
				Input::GetInstance()->MouseDown(sdlEvent.button.button);
			}
			break;
		case SDL_MOUSEBUTTONUP:
			{
				Input::GetInstance()->MouseUp(sdlEvent.button.button);
			}
			break;
		case SDL_MOUSEWHEEL:
			{
				Input::GetInstance()->MouseWheel(sdlEvent.wheel.y);
			}
			break;
		case SDL_KEYDOWN:
			{
				Input::GetInstance()->KeyDown(sdlEvent.key.keysym.sym);
			}
			break;
		case SDL_KEYUP:
			{
				Input::GetInstance()->KeyUp(sdlEvent.key.keysym.sym);
			}
			break;
		case SDL_TEXTINPUT:
			{
				Input::GetInstance()->OnTextInput(sdlEvent.text.text);
			}
			break;
		}
	}
}

void Engine::DispatchMessages()
{
	ClientController *clientController = ClientController::GetInstance();
	ServerController *serverController = ServerController::GetInstance();

	Message *message;

	while ((message = clientController->PollMessage()) != nullptr)
	{
		if (message->dest.IsSet())
		{
			/// Send over network
			ByteBuffer buffer;
			buffer.Attach(message->data, message->size);
			clientController->DispatchOutgoingMessage(buffer);
		}
		else
		{
			/// Dispatch in local server
			ByteBuffer buffer;
			buffer.Attach(message->data, message->size);

			serverController->HandlePackage(message->source, -1, buffer.ReadUCharSafe(MessageTypes::INVALID), buffer);
		}

		delete message;
	}

	while ((message = serverController->PollMessage()) != nullptr)
	{
		if (message->dest.IsSet())
		{
			/// Send over network
			ByteBuffer buffer;
			buffer.Attach(message->data, message->size);
			serverController->DispatchOutgoingMessage(message->destclientReference, buffer);
		}
		else
		{
			/// Dispatch to local client
			ByteBuffer buffer;

			buffer.Attach(message->data, message->size);

			clientController->HandlePackage(message->source, buffer.ReadUCharSafe(MessageTypes::INVALID), buffer);
		}

		delete message;
	}
}

/////////////////////////////////
//
// IEngine
//
// exposed for authoritative dll (launcher, editor)
//
/////////////////////////////////

bool Engine::Init(InitializationMode mode, void *windowHandle, int width, int height)
{
	initializationMode = mode;
	this->windowHandle = windowHandle;

	const bool shouldLoadGui = IsInitializationMode(INIT_GAME);

	// all shared libraries we'll use
	if (!GetAppInterface()->LoadSharedLibrary("logger"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("cvar"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("mdxlib"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("materialsystem"))
	{
		return false;
	}

	if (shouldLoadGui &&
		!GetAppInterface()->LoadSharedLibrary("gui"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("physics"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("client"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("server"))
	{
		return false;
	}

	if (!GetAppInterface()->LoadSharedLibrary("networking"))
	{
		return false;
	}

	// pull all required interfaces
	// TODO: remove the ones we won't actually need...

	if ((logger = (ILogger *)GetAppInterface()->QueryApp(INTERFACE_ILOGGER_VERSION)) == nullptr)
	{
		return false;
	}

	if ((cvar = (ICvarInterface *)GetAppInterface()->QueryApp(INTERFACE_ICVAR_VERSION)) == nullptr)
	{
		return false;
	}

	if ((renderContext = (IRenderContext *)GetAppInterface()->QueryApp(INTERFACE_IRENDERCONTEXT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((materialDict = (IMaterialDict *)GetAppInterface()->QueryApp(INTERFACE_IMATERIALDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((textureDict = (ITextureDict *)GetAppInterface()->QueryApp(INTERFACE_ITEXTUREDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((modelDict = (IMDXModelDict *)GetAppInterface()->QueryApp(INTERFACE_IMDXMODELDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((renderableDict = (IMDXRenderableDict *)GetAppInterface()->QueryApp(INTERFACE_IMDXRENDERABLEDICT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((fboDict = (IFBODict *)GetAppInterface()->QueryApp(INTERFACE_IFBODICT_VERSION)) == nullptr)
	{
		return false;
	}

	if (shouldLoadGui &&
		((gui = (IGUI *)GetAppInterface()->QueryApp(INTERFACE_IGUI_VERSION)) == nullptr))
	{
		return false;
	}

	if ((physics = (IPhysics *)GetAppInterface()->QueryApp(INTERFACE_IPHYSICS_VERSION)) == nullptr)
	{
		return false;
	}

	if ((client = (IClient *)GetAppInterface()->QueryApp(INTERFACE_ICLIENT_VERSION)) == nullptr)
	{
		return false;
	}

	if ((server = (IServer *)GetAppInterface()->QueryApp(INTERFACE_ISERVER_VERSION)) == nullptr)
	{
		return false;
	}

	if ((networking = (INetworking *)GetAppInterface()->QueryApp(INTERFACE_NETWORKING_VERSION)) == nullptr)
	{
		return false;
	}

	if (!networking->Init())
	{
		Assert(0);
		return false;
	}

	// register all commands
	cvar->Init();

	// initializes child interfaces too
	renderContext->Init(windowHandle, width, height);

	// setup debug rendering
	DebugDraw::GetInstance()->Init();

	physics->Init();

	// need to expose this to client/server before initializing them
	GetAppInterface()->RegisterApp(INTERFACE_IENGINECLIENT_VERSION, ToIEngineClient());
	GetAppInterface()->RegisterApp(INTERFACE_IENGINESERVER_VERSION, ToIEngineServer());

	if (!client->Init(shouldLoadGui))
	{
		return false;
	}

	if (!server->Init())
	{
		return false;
	}

	// we're in tools mode (map editor)
	if (IsInitializationMode(INIT_TOOLSMODE))
	{
		Assert(threadLock == nullptr);

		threadLock = new ThreadLock();

		// create engine tools instance and expose interface
		EngineTools::InitSingletonInterface(INTERFACE_IENGINETOOLS_VERSION);

		EngineTools::GetInstance()->Init();
	}

	return true;
}

void Engine::Shutdown()
{
	if (IsInitializationMode(INIT_TOOLSMODE))
	{
		EngineTools::GetInstance()->Shutdown();
	}

	server->Shutdown();
	client->Shutdown();

	if (currentMap != nullptr)
	{
		currentMap->Release();
	}

	currentMap = nullptr;

	EngineTools::ShutdownSingleton();
	delete threadLock;
	threadLock = nullptr;

	physics->Shutdown();

	DebugDraw::GetInstance()->Shutdown();
	modelDict->Shutdown();
	renderContext->Shutdown();

	ServerController::GetInstance()->ShutdownServer();
	ClientController::GetInstance()->ShutdownClient();
}

void Engine::Run()
{
	ChangeState(STATE_RUNNING);

	float ticksPerSec = (float)SDL_GetPerformanceFrequency();
	Uint64 ticksOld = SDL_GetPerformanceCounter();

	float frametime = 0.0f;
	const bool isInToolsMode = IsInitializationMode(INIT_TOOLSMODE);

	// TODO: remove hardcoded bindings and load from script
	if (!isInToolsMode)
	{
		Input::GetInstance()->SetBinding(SDLK_RIGHT, "+camera_right");
		Input::GetInstance()->SetBinding(SDLK_LEFT, "+camera_left");
		Input::GetInstance()->SetBinding(SDLK_UP, "+camera_up");
		Input::GetInstance()->SetBinding(SDLK_DOWN, "+camera_down");
		Input::GetInstance()->SetBinding(SDLK_RSHIFT, "+camera_speed");
		Input::GetInstance()->SetBinding(SDLK_LSHIFT, "+camera_speed");
		Input::GetInstance()->SetBinding(SDLK_F1, "gui_toggleconsole");
		Input::GetInstance()->SetBinding(SDLK_F2, "r_screenshot");
		Input::GetInstance()->SetBinding(SDLK_F3, "dbg_toggledeathmap");
		Input::GetInstance()->SetBinding(SDLK_F4, "dbg_toggleentitybounds");
		Input::GetInstance()->SetBinding(SDLK_F5, "r_reloadallmaterials");
		Input::GetInstance()->SetBinding(SDLK_F6, "dbg_renderer");
		Input::GetInstance()->SetBinding(SDLK_F8, "joinserver 127.0.0.1 23962");
		Input::GetInstance()->SetBinding(SDLK_F9, "map Level1");
		Input::GetInstance()->SetBinding(SDLK_F10, "r_fullscreen");
		Input::GetInstance()->SetBinding(SDLK_F11, "gui_togglestats");
		Input::GetInstance()->SetBinding(SDLK_F12, "shutdown");
		Input::GetInstance()->SetBinding(SDLK_ESCAPE, "gui_togglemenu");
		Input::GetInstance()->SetBinding((SDL_Keycode)512, "+camera_zoom_in");
		Input::GetInstance()->SetBinding((SDL_Keycode)513, "+camera_zoom_out");
	}

	ClientController *clientController = ClientController::GetInstance();
	ServerController *serverController = ServerController::GetInstance();

	while (IsInState(STATE_RUNNING))
	{
		{
			Uint64 ticksNow = SDL_GetPerformanceCounter();

			frametime = (ticksNow - ticksOld) / ticksPerSec;

			// don't run empty frames
			if (frametime <= 0.0f)
			{
				continue;
			}

			ticksOld = ticksNow;

			// never let more than 100 ms pass
			// fixed framerate simulation would pick large intervals up and adjust accordingly
			frametime = MIN(0.1f, frametime);
		}

		// update client globals
		clientGlobals.Advance(frametime);

		// receive SDL events
		if (isInToolsMode)
		{
			EngineTools::GetInstance()->Update(frametime);
		}
		else
		{
			guiGlobals.Advance(frametime);

			ProcessSDLEvents();
		}

		// input handling
		Input::GetInstance()->ExecuteCommandBuffer();

		// simulate engine
		serverController->Simulate();
		clientController->Simulate();

		// networking
		DispatchMessages();

		client->Simulate();

		// update the game
		if (currentMap != nullptr)
		{
			int simulationSteps = simulationGlobals.AdvanceTime(frametime);

			for (int i = 0; i < simulationSteps; i++)
			{
				// simulate server
				server->Update();

				// simulate physics
				if (!isInToolsMode)
				{
					physics->Update(simulationGlobals.GetFrametime());
				}

				simulationGlobals.AdvanceStep();
			}

			if (drawPhysicsDebug)
			{
				physics->DrawDebug();
			}
		}

		// client sets up render matrices and clears buffers
		client->RenderBegin();

		// render client
		if (currentMap != nullptr)
		{
			client->RenderScene();
		}

		// render tool overlays
		if (isInToolsMode)
		{
			EngineTools::GetInstance()->Render();
		}

		// render debug overlays
		DebugDraw::GetInstance()->Render();

		// pop matrices, swap buffers
		client->RenderEnd();

		// lock thread for tools if requested
		if ((threadLock != nullptr)	&&
			threadLock->IsLocked())
		{
			renderContext->MakeCurrent(false);

			threadLock->CheckForLock();

			renderContext->MakeCurrent(true);
		}

		renderContext->SetShaderTime(clientGlobals.GetTime());
	}
}

void Engine::ResizeWindow(int width, int height)
{
	renderContext->Resize(width, height);

	if (gui != nullptr)
	{
		gui->Resize(width, height);
	}
}

/////////////////////////////////
//
//	IEngineClient/IEngineServer
//
//	exposed for client/server dll
//
/////////////////////////////////

IWorld *Engine::GetWorld()
{
	return currentMap;
}

bool Engine::IsInToolsMode() const
{
	return IsInitializationMode(INIT_TOOLSMODE);
}

void Engine::SendClientCommand(const char *command)
{
	ClientController::GetInstance()->SendClientCommand(command);
}
