#ifndef GAMESYSTEM_H
#define GAMESYSTEM_H

#include "util/singleton.h"

class IGameSystem;

// lower values are executed first
namespace GameSystemPriorites
{
	enum GameSystemPriority_e
	{
		RESOURCE = -11,
		CLIENT_INPUT = -10,
		CLIENT_GUI,
		CLIENT_ENTITY_FACTORY,

		SERVER_ENTITY_FACTORY = -10,

		NONE = 0,
	};
}
typedef GameSystemPriorites::GameSystemPriority_e GameSystemPriorityId;

/// Class that manages all game systems and
/// exposes event dispatching for them
class GameSystemManager
{
public:

	static void AddGameSystem(IGameSystem *gamesystem);

	static bool InitAllGameSystems();
	static void ShutdownAllGameSystems();

	static void UpdateAllGameSystems(float frametime);

	static void LevelInitAllGameSystems();
	static void LevelShutdownAllGameSystems();

private:
	static std::vector<IGameSystem*> gamesystems;
};

/// A game system is usually a singleton that has init/shutdown
/// functions executed on module load/unload and
// can be updated each frame or called for various events
class IGameSystem
{
protected:
	virtual ~IGameSystem() {}

public:
	IGameSystem()
	{
		GameSystemManager::AddGameSystem(this);
	}

	/// The priority defines the order the system are called in.
	/// Shutdown is called in reverse order to this
	virtual GameSystemPriorityId GetPriority() {return GameSystemPriorites::NONE;}

	/// Called on library load, return false to abort loading
	virtual bool Init() { return true; }
	/// Called on library shutdown
	virtual void Shutdown() {}

	/// Called each frame
	virtual void Update(float frametime) {}

	virtual void OnLevelInit() {}
	virtual void OnLevelShutdown() {}
};


#endif