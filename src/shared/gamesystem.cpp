
#include "pch.h"
#include "gamesystem.h"

static bool SortGameSystemPriority(IGameSystem *left, IGameSystem *right)
{
	return left->GetPriority() < right->GetPriority();
}

std::vector<IGameSystem*> GameSystemManager::gamesystems;

void GameSystemManager::AddGameSystem(IGameSystem *gamesystem)
{
	Assert(!VECTOR_CONTAINS(gamesystems, gamesystem));

	gamesystems.push_back(gamesystem);
}

bool GameSystemManager::InitAllGameSystems()
{
	std::sort(gamesystems.begin(), gamesystems.end(), SortGameSystemPriority);

	for (auto g : gamesystems)
	{
		if (!g->Init())
		{
			return false;
		}
	}

	return true;
}

void GameSystemManager::ShutdownAllGameSystems()
{
	// shutdown in reverse order
	for (int i = gamesystems.size() - 1;
		i >= 0; i-- )
	{
		gamesystems[i]->Shutdown();
	}
}

void GameSystemManager::UpdateAllGameSystems(float frametime)
{
	for (auto g : gamesystems)
	{
		g->Update(frametime);
	}
}

void GameSystemManager::LevelInitAllGameSystems()
{
	for (auto g : gamesystems)
	{
		g->OnLevelInit();
	}
}

void GameSystemManager::LevelShutdownAllGameSystems()
{
	// level shutdown in reverse order
	for (int i = gamesystems.size() - 1;
		i >= 0; i-- )
	{
		gamesystems[i]->OnLevelShutdown();
	}
}