#ifndef RESOURCE_H
#define RESOURCE_H

#include <vector>
#include "shared/gamesystem.h"

struct ResourceTowerUpgrade
{
	int id;
	int cost;
	float duration;
};

struct ResourceTower
{
	int id;
	std::string name;
	std::string icon;
	std::string model;
	std::string animation;
	int cost;
	//vec3 scale;
	//vec3 color;

	float fireDelay;
	float range;
	int damage;
	float rotation;

	std::vector<ResourceTowerUpgrade> upgrades;
};

struct ResourceCreep
{
	int id;

	std::string name;
	std::string model;

	std::string animationIdle;
	std::string animationWalk;
	std::string animationDeath;
};


class Resources : public IGameSystem
{
	DECLARE_SINGLETON(Resources);
public:

	virtual GameSystemPriorityId GetPriority() { return GameSystemPriorites::RESOURCE; }

	bool Init();

	const std::vector<ResourceTower> &GetTowers() const;

private:
	std::vector<ResourceTower> towers;
};


#endif