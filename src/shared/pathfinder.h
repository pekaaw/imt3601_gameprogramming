#ifndef PATHFINDER_H
#define PATHFINDER_H

class IWorld;

class PathFinder
{
public:
	PathFinder();
	~PathFinder();

	void Init(IWorld *world);

	int GetNodeCount() const;

	void SetTileBlocked(int tileIndex, bool isBlocked);
	bool IsTileBlocked(int tileIndex) const;

	void ModifyNodeCreepCount(int tileIndex, int delta);
	int GetNodeCreepCount(int tileIndex) const;

	void ModifyNodeDeathCost(int tileIndex, int radius, float delta);
	float GetNodeDeathCost(int tileIndex);
	void DecayDeathCost(float delta);

	bool IsAnyPairBlocked();

	struct AStarNode
	{
		int index;
		int x, y;
		float costReach;
		float costHeuristic;
		Uint8 flags;
		Uint16 creepCount;

		float costDeathMap;

		AStarNode *links[4];
		AStarNode *previous;
	};

	const AStarNode *ConstructPath(int tileStart, int tileGoal);

private:

	enum
	{
		PATHFIND_SOLID = (1 << 0),
		PATHFIND_BLOCKED = (1 << 1),
	};

	AStarNode *pathFindingNodes;
	bool *pathFindingVisited;

	int tileWidth;
	int tileHeight;
	int tileCount;

	struct PathPair
	{
		int start;
		int end;
	};
	std::vector<PathPair> pairs;
};


#endif