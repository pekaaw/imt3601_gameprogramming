
#include "pch.h"
#include "material.h"
#include "materialvar.h"
#include "texturedict.h"
#include "shaders\shaderfactory.h"
#include "util/xmlutil.h"


MATERIAL_VAR_HANDLE Material::lastInvalidVarHandle = 0;
std::vector<IMaterialVar*> Material::materialVarCache;

Material::Material()
	: shaderInstance(nullptr)
	, materialVarsBase(nullptr)
	, shaderContext(nullptr)
	, name(nullptr)
	, isErrorMaterial(false)
{
}

Material::~Material()
{
	delete [] name;

	Assert(materialVars.empty());
	Assert(shaderInstance == nullptr);
	Assert(CReferenceCounted::IsReferenceCountValid() && !CReferenceCounted::IsReferenced());
}

bool Material::LoadFromFile(const char *name)
{
	char pathShort[MAX_PATH_GAME];
	char pathFull[MAX_PATH_GAME];

	G_StrAbsContentPath(ContentDirectories::MATERIALS, name, pathShort, sizeof(pathShort));
	G_StrFixSlashes(pathShort);
	G_StrAddOrReplaceExtension(pathShort, "xml", pathFull, sizeof(pathFull));

	std::string file = LoadFile(pathFull);

	if (file.empty())
	{
		Assert(0);
		LoadErrorMaterial();
		return false;
	}

	return LoadFromString(name, file.c_str());
}

bool Material::LoadFromString(const char *name, const char *text)
{
	Assert(materialVars.empty());
	Assert(shaderInstance == nullptr);
	Assert(materialVarsBase == nullptr);
	Assert(shaderContext == nullptr);
	Assert(this->name != nullptr || name != nullptr);

	if (name != nullptr)
	{
		delete [] this->name;
		this->name = G_StrCreateCopy(name);
		G_StrFixSlashes(this->name);
	}

	// parse data from xml
	CXMLDocument doc;

	bool success = LoadXMLString(text, doc);

	if (!success)
	{
		LoadErrorMaterial();
		return false;
	}

	rapidxml::xml_node<> *root = doc.GetDocumentRoot();
	rapidxml::xml_node<> *shaderNode = root->first_node("shader", 0U, false);

	if (shaderNode == nullptr)
	{
		LoadErrorMaterial();
		return false;
	}

	std::vector<MaterialVar*> unsortedVars;

	for (rapidxml::xml_node<> *varNode = root->first_node("var", 0U, false);
		varNode != nullptr;
		varNode = varNode->next_sibling("var", 0U, false))
	{
		rapidxml::xml_attribute<> *nameAttribute = varNode->first_attribute("name", 0U, false);

		if (nameAttribute == nullptr)
			continue;

		const char *varName = nameAttribute->value();
		const char *varValue = varNode->value();

		MaterialVar *materialVar = new MaterialVar(varName);
		materialVar->Init(varValue);
		unsortedVars.push_back(materialVar);
	}

	// connect with shader
	const char *shaderName = shaderNode->value();
	shaderInstance = ShaderFactory::GetInstance()->CreateShader(shaderName);

	Assert(shaderInstance != nullptr);

	// order and initialize material vars
	const int shaderVarCount = shaderInstance->GetParamCount();
	materialVars.resize(shaderVarCount);

	for (int i = 0; i < shaderVarCount; i++)
	{
		IShaderVar *shaderVar = shaderInstance->GetParam(i);
		const char *shaderVarName = shaderVar->GetName();

		auto itr = unsortedVars.begin();

		for (; itr != unsortedVars.end(); itr++)
		{
			if (_stricmp((*itr)->GetName(), shaderVar->GetName()) == 0)
			{
				break;
			}
		}

		// take existing var over
		if (itr != unsortedVars.end()
			&& MaterialVarTypes::AreTypesMatching((*itr)->GetType(), shaderVar->GetType()))
		{
			materialVars[i] = (*itr);
			unsortedVars.erase(itr);
		}
		else // insert a default var
		{
			Assert(itr == unsortedVars.end()
				|| MaterialVarTypes::AreTypesMatching((*itr)->GetType(), shaderVar->GetType())); // type from script doesn't match expected type

			MaterialVar *defaultVar = new MaterialVar(shaderVar->GetName());
			defaultVar->Init(shaderVar->GetDefaultValue(), shaderVar->GetType());

			Assert(MaterialVarTypes::AreTypesMatching(defaultVar->GetType(), shaderVar->GetType())); // default string mismatching expected type
			materialVars[i] = defaultVar;
		}
	}

	for (auto v : unsortedVars)
		delete v;

	Assert(materialVars.size() == shaderVarCount);

	materialVarsBase = materialVars.data();
	shaderContext = new MaterialContext();

	// init params through shader, alloc draw passes
	InitShader();

	return true;
}

void Material::Release()
{
	ReleaseResources();

	delete this;
}

void Material::ReleaseResources(bool deleteTextures)
{
	if (shaderInstance != nullptr)
		shaderInstance->Shutdown(shaderContext, materialVarsBase);

	if (deleteTextures)
	{
		for (auto m : materialVars)
		{
			if (m->GetType() == MaterialVarTypes::TEXTURE
				&& m->GetTexture() != nullptr)
			{
				TextureDict::GetInstance()->ReleaseTextureIfUnreferenced(m->GetTexture());
			}
		}
	}

	for (auto m : materialVars)
	{
		delete m;
	}

	materialVars.clear();

	shaderInstance = nullptr;
	materialVarsBase = nullptr;

	delete shaderContext;
	shaderContext = nullptr;
}

const char *Material::GetName() const
{
	return name;
}

bool Material::IsErrorMaterial() const
{
	return isErrorMaterial;
}

void Material::InitShader()
{
	Assert(shaderInstance != nullptr);
	Assert(materialVarsBase != nullptr);
	Assert(shaderContext != nullptr);

	shaderInstance->Init(shaderContext, materialVarsBase);
}

void Material::Draw()
{
	Assert(shaderInstance != nullptr);
	Assert(materialVarsBase != nullptr);
	Assert(shaderContext != nullptr);

	RenderContext::GetInstance()->UpdateMatrices();

	shaderInstance->Draw(shaderContext, materialVarsBase);
}

IMaterialVar *Material::FindMaterialVar(const char *name)
{
	for (auto v : materialVars)
	{
		if (G_StrEq(v->GetName(), name))
		{
			return v;
		}
	}

	return nullptr;
}

IMaterialVar *Material::FindMaterialVarFast(MATERIAL_VAR_HANDLE &handle, const char *name)
{
	// lookup failed before, ignore
	if (handle == MATERIAL_VAR_HANDLE_INVALID)
	{
		return nullptr;
	}

	// lookup was successful before, access cache
	if (handle > lastInvalidVarHandle)
	{
		Assert((handle - lastInvalidVarHandle - 1) >= 0
			&& (handle - lastInvalidVarHandle - 1) < materialVarCache.size());

		return materialVarCache[handle - lastInvalidVarHandle - 1];
	}

	// otherwise do slow lookup
	IMaterialVar *var = FindMaterialVar(name);

	// lookup failed
	if (var == nullptr)
	{
		handle = MATERIAL_VAR_HANDLE_INVALID;
		return nullptr;
	}
	else // found var, cache it
	{
		materialVarCache.push_back(var);
		handle = lastInvalidVarHandle + materialVarCache.size();

		return var;
	}
}

void Material::FlushMaterialVarCache()
{
	lastInvalidVarHandle += materialVarCache.size();
	materialVarCache.clear();
}

void Material::LoadErrorMaterial()
{
	isErrorMaterial = true;

	const char *errorMaterial = "<material><shader>error</shader></material>";

	AssertCall(LoadFromString("error", errorMaterial));
}
