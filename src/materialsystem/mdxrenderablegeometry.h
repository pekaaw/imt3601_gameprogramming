#ifndef MDXRENDERABLEGEOMETRY_H
#define MDXRENDERABLEGEOMETRY_H

#include "util\referencecounted.h"
#include "mdxlib\mdxformat.h"

class IMDXModel;
class Mesh;
class IMaterial;

class MDXRenderableGeometry : public CReferenceCounted
{
	~MDXRenderableGeometry();
public:
	MDXRenderableGeometry();

	void Init(IMDXModel *model);
	void Release();

	void Draw(int animationtime, int sequence);

private:
	std::vector<Mesh *> meshes;
	std::vector<int> materialIds;
	std::vector<IMaterial *> materials;

	struct Layer
	{
		Mesh					*mesh;
		IMaterial				*material;
		MDXAnimationTrackFLOAT	*geosetAlphaAnimation;
	};

	std::vector<Layer> layers;
	Layer *layerBase;
};
#endif
