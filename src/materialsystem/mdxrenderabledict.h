#ifndef MDXRENDERABLEDICT_H
#define MDXRENDERABLEDICT_H

#include "util\singleton.h"
#include "materialsystem\imdxrenderabledict.h"
#include "mdxrenderabledict.h"

class IMDXModelDict;
class MDXRenderableGeometry;
class IMDXModel;

class MDXRenderableDict : public IMDXRenderableDict
{
	DECLARE_SINGLETON(MDXRenderableDict);
public:
	~MDXRenderableDict();

	void Init();
	void Shutdown();

	MDXRenderableGeometry *FindMDXRenderableGeometry(IMDXModel *modelData);

	//IMDXRenderableDict
	virtual IMDXRenderable *CreateMDXRenderableInstance(const char *path);

private:
	IMDXModelDict *modelDict;

	std::unordered_map<IMDXModel*, MDXRenderableGeometry*> geometryCache;
};

#endif