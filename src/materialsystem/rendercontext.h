#ifndef RENDERCONTEXT_H
#define RENDERCONTEXT_H

#include "materialsystem\materialsystem.h"
#include "materialsystem\irendercontext.h"

#include <util\eigen.h>
#include <stack>

class IMesh;
class Mesh;
class IMaterial;

class PRE_ALIGN_16 RenderContext : public IRenderContext
{
	DECLARE_SINGLETON(RenderContext);
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

	~RenderContext();

	void MakeMatricesDirty();

	// IRenderContext
	virtual void Init(void *windowHandle, int windowWidth, int windowHeight);
	virtual void Resize(int windowWidth, int windowHeight);
	virtual void Shutdown();

	virtual bool IsFullscreen();
	virtual void SetFullscreen(bool isFullscreen);
	
	// call with true to assign context to calling thread
	// call with false to release context. context MUST BE released before
	// assigning it elsewhere or opengl will explode
	virtual void MakeCurrent(bool assignContext);

	virtual bool CheckForErrors();

	virtual void SetRenderStage(RenderStageId stage);
	virtual RenderStageId GetRenderStage() const;

	virtual void SetRenderParamFloat(RenderParam_Float param, float value);
	virtual void SetRenderParamInt(RenderParam_Int param, int value);
	virtual void SetRenderParamVec3(RenderParam_Vec3 param, const vec3 &value);
	virtual void SetRenderParamVec4(RenderParam_Vec4 param, const vec4 &value);
	virtual void SetRenderParamMat4(RenderParam_Mat4 param, const mat4 &value);

	virtual float GetRenderParamFloat(RenderParam_Float param) const;
	virtual int GetRenderParamInt(RenderParam_Int param) const;
	virtual void GetRenderParamVec3(RenderParam_Vec3 param, vec3 &value) const;
	virtual void GetRenderParamVec4(RenderParam_Vec4 param, vec4 &value) const;
	virtual void GetRenderParamMat4(RenderParam_Mat4 param, mat4 &value) const;

	// see MODEL_MAX_BONES
	virtual void SetBoneCount(int boneCount);
	virtual int *GetBoneIndicesForModify();
	virtual void SetBoneIndices(int *boneIndices);
	virtual void SetBonePointer(mat4 *bones, int count);
	virtual mat4 *GetBoneTransformsForModify();

	virtual void PushModelMatrix(const mat4 &matrix);
	virtual void PushViewMatrix(const mat4 &matrix);
	virtual void PushProjectionMatrix(const mat4 &matrix);

	virtual void PopModelMatrix();
	virtual void PopViewMatrix();
	virtual void PopProjectionMatrix();

	virtual void LoadModelIdentity();
	virtual void LoadViewIdentity();
	virtual void LoadProjectionIdentity();

	virtual void SetWorldCoordinatesEnabled(bool enabled);
	virtual void UpdateMatrices();

	virtual void SetMainMatrices();

	virtual void SetDepthRange(float zNear, float zFar);
	virtual float GetZNear() const;
	virtual float GetZFar() const;
	virtual float GetZRange() const;

	virtual void PushLineWidth(float width);
	virtual void PopLineWidth();

	// currently pushed matrices which are used during rendering
	virtual const mat4 &GetModelViewProjectionMatrix() const;
	virtual const mat4 &GetViewProjectionMatrix() const;
	virtual const mat4 &GetModelMatrix() const;
	virtual const mat4 &GetViewMatrix() const;
	virtual const mat4 &GetProjectionMatrix() const;

	// same as above but include translation into OGL space
	virtual const mat4 &GetOGLViewProjectionMatrix() const;
	virtual const mat4 &GetOGLModelViewProjectionMatrix() const;

	// the last world view matrix that the client publicated
	virtual const mat4 &GetMainViewProjectionMatrix() const;

	virtual int GetScreenWidth() const;
	virtual int GetScreenHeight() const;

	virtual void PushViewport(int x = -1, int y = -1, int w = -1, int h = -1);
	virtual void SetViewPort(int x = -1, int y = -1, int w = -1, int h = -1);
	virtual void PopViewport();

	virtual void SetClearColor(float r, float g, float b, float a = 1.0f);
	virtual void ClearBuffers(bool color, bool depth);

	virtual void SetDepthTestEnabled(bool enabled);
	virtual void SetClipPlaneEnabled(int index, bool enabled);

	virtual void SwapBuffers();

	virtual IMesh *CreateStaticMesh(FaceTypeId type, int vertexFormat, int faceCount);
	virtual IMesh *GetDynamicMesh(FaceTypeId type, int vertexFormat, int faceCount);

	virtual void BindMesh(const IMesh *mesh);
	virtual const IMesh *GetActiveMesh() const;

	virtual void SetMaterialOverride(IMaterial *material);
	virtual IMaterial *GetMaterialOverride();

	virtual void DrawFullscreenQuad(IMaterial *material);

	virtual void SetShaderTime(float time);
	virtual float GetShaderTime() const;

	virtual void TakeScreenshot(const char *file);

	// internal to render implementation

	void SetScreenSize(int w, int h);

	void BindBones(GLuint uniformIndex);

	//void BindMaterial(Material *material);
	//Material *GetActiveMaterial();

private:

	void BuildFullscreenMesh();
	void DestroyFullscreenMesh();
	void DestroyDynamicMeshes();


	int stateMachineCheck;
	void *windowHandle;
	void *glContextHandle;

	RenderStageId activeRenderStage;

	float renderParamFloats[RenderParams::FLOAT_COUNT];
	int renderParamInts[RenderParams::INT_COUNT];
	vec3 renderParamVec3s[RenderParams::VEC3_COUNT];
	vec4 renderParamVec4s[RenderParams::VEC4_COUNT];
	mat4 renderParamMat4s[RenderParams::MAT4_COUNT];

	int boneCount;
	int boneIndices[MODEL_MAX_BONES];
	mat4 boneTransforms[MODEL_MAX_BONES];
	mat4 boneTransformsSorted[MODEL_MAX_BONES];
	mat4 *bonePointer;

	mat4 modelViewProjection;
	mat4 modelViewProjectionOGL;
	mat4 viewProjection;
	mat4 viewProjectionOGL;

	mat4 viewProjectionMain;

	std::stack< mat4, std::vector< mat4, Eigen::aligned_allocator< mat4 > > > matricesModel;
	std::stack< mat4, std::vector< mat4, Eigen::aligned_allocator< mat4 > > > matricesView;
	std::stack< mat4, std::vector< mat4, Eigen::aligned_allocator< mat4 > > > matricesProjection;
	std::stack< float > lineWidth;

	bool matricesValid;
	bool worldCoordinatesEnabled;

	float zNear, zFar, zRange;

	int screenWidth;
	int screenHeight;
	float time;
	bool fullscreen;

	struct ViewportData
	{
		int x, y, w, h;
	};
	std::stack<ViewportData> viewportStack;

	//Material *activeMaterial;
	const IMesh *activeMesh;

	IMaterial *materialOverride;
	Mesh *fullscreenMesh;

	struct DynamicMeshData
	{
		int faceCount;
		int ringbufferOffset;
		Mesh *mesh;
	};
	std::unordered_map<int, DynamicMeshData> dynamicMeshes[FaceTypes::COUNT];

} POST_ALIGN_16;

#endif