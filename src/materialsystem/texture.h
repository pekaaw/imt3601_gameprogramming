#ifndef TEXTURE_H
#define TEXTURE_H

#include "materialsystem\materialsystem.h"
#include "materialsystem\itexture.h"

class Texture : public ITexture, public CReferenceCounted
{
	~Texture();
public:
	Texture();

	void Init(const char *name,
		TextureFormatId format, TextureTypeId type, TextureGroupId group,
		int flags, int width, int height, void *data);

	void SetCubemapFace(int index, void *data);
	void FinalizeCubemap();

	void Release();

	GLuint GetTextureId() const;
	const bool IsReferenced() const;

	void Bind();

	// functions available through ITexture
	virtual const char *GetTextureName() const;

	virtual int GetWidth() const;
	virtual int GetHeight() const;

	virtual TextureFormatId GetFormat() const;
	virtual TextureTypeId GetType() const;
	virtual TextureGroupId GetGroup() const;

	virtual void IncrementReferenceCount();
	virtual void DecrementReferenceCount();

	virtual void SetContent(TextureFormatId format, int width, int height, void *data);
private:

	void GenerateMipmaps(GLuint target);

	char *name;

	int width, height;
	int flags;

	TextureFormatId format;
	TextureTypeId type;
	TextureGroupId group;

	GLuint textureId;
	GLuint glTextureType;
};

#endif