#ifndef MATERIALVAR_H
#define MATERIALVAR_H

#include "util/macros.h"
#include "materialsystem\materialsystem.h"
#include "texture.h"
#include "materialsystem\imaterialvar.h"

class PRE_ALIGN_16 MaterialVar : public IMaterialVar
{
public:
	MaterialVar(const char *name);
	~MaterialVar();

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

	void Init(const char *rawValue,
		MaterialVarTypeId defaultType = MaterialVarTypes::INT);

	// IMaterialVar
	virtual MaterialVarTypeId GetType() const;
	virtual const char *GetName() const;
	virtual bool IsDefined() const;

	virtual int GetInt() const;
	virtual bool GetBool() const;
	virtual float GetFloat() const;
	virtual vec2 GetVec2() const;
	virtual vec3 GetVec3() const;
	virtual vec4 GetVec4() const;
	virtual const char *GetString() const;
	virtual ITexture *GetTexture() const;

	virtual void SetInt(int value);
	virtual void SetBool(bool value);
	virtual void SetFloat(float value);
	virtual void SetVec2(const vec2 &value);
	virtual void SetVec3(const vec3 &value);
	virtual void SetVec4(const vec4 &value);
	virtual void SetString(const char *value);
	virtual void SetTexture(ITexture *value);

private:

	MaterialVarTypeId type;
	char *name;

	union
	{
		int value_int;
		float value_float;
		ITexture *value_texture;
		char *value_string;
	};

	// some of these can't be in a union
	vec2 value_vec2;
	vec3 value_vec3;
	vec4 value_vec4;

	bool isDefined;
} POST_ALIGN_16;


#endif