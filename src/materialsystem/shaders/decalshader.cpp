
#include "pch.h"

#include "baseshader.h"


BEGIN_SHADER(decal)
	BEGIN_SHADER_VARS
		SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	END_SHADER_VARS

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, ALBEDOTEXTURE);
	}

	SHADER_INIT_PASSES()
	{
		ShaderCombo vertexCombo("decal.vert");
		ShaderCombo fragmentCombo("decal.frag");

		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::NORMAL_3F
			| VertexFormat::TEXTURE_COORD_2F,
			std::move(vertexCombo), std::move(fragmentCombo));

		PushUniformVariable("albedoSampler");

		PushUniformVariable("MVP");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
		PushUniformVariable("diffuseModulation");
	}

	SHADER_DRAW_PASSES()
	{
		BeginPass();

		BindTexture(vars, ALBEDOTEXTURE, 0);

		SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
		SetUniform1f(context->GetZNear());
		SetUniform1f(context->GetZRange());

		vec4 diffuseModulation;
		context->GetRenderParamVec4(RenderParams::VEC4_DIFFUSE_MODULATION, diffuseModulation);
		SetUniform4fv(diffuseModulation.data());

		SetBlendMode(BlendModes::SRC_ALPHA);
		//SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK);
		SetDepthWriteEnabled(false);

		DrawPass();

		SetBlendMode(BlendModes::NONE);
		//SetPolygonMode(PolygonFaceModes::FRONT);
		SetDepthWriteEnabled(true);
	}
END_SHADER();