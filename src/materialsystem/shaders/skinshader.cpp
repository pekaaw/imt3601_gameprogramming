
#include "pch.h"
#include "baseshader.h"
#include "shadowpass.h"


BEGIN_SHADER(skin)
	BEGIN_SHADER_VARS
		SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(CUBEMAPTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(COLORMASKENABLED, MaterialVarTypes::INT, "0");
	END_SHADER_VARS

	void SetupShadowpassParams(MaterialVar **vars, ShadowPassData &data)
	{
		data.useAlphatesting = vars[ALPHATESTING]->GetBool();
		data.albedoTexture = ALBEDOTEXTURE;
		data.alphaTestRef = ALPHATESTREF;
		data.useNocull = vars[NOCULL]->GetBool();
		data.useSkinning = true;
	}

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, ALBEDOTEXTURE);
		//LoadCubemap(vars, CUBEMAPTEXTURE);
	}

	SHADER_INIT_PASSES()
	{
		// outline pass
		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::BONEINDICES_3F,
			"skin_outline.vert", "skin_outline.frag");
		
		PushUniformVariable("VP");
		PushUniformVariable("bones");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
		PushUniformVariable("time");

		const bool hasAlphaTesting = vars[ALPHATESTING]->GetBool();
		const bool hasColorMask = vars[COLORMASKENABLED]->GetBool();

		Assert(!hasColorMask || !hasAlphaTesting);

		ShaderCombo vertexCombo("skin.vert");
		ShaderCombo fragmentCombo("skin.frag");

		if (hasAlphaTesting)
		{
			fragmentCombo.SetCombo("alphatesting", 1);
		}
		else if (hasColorMask)
		{
			fragmentCombo.SetCombo("colormask", 1);
		}

		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::NORMAL_3F
			| VertexFormat::TEXTURE_COORD_2F
			| VertexFormat::BONEINDICES_3F,
			std::move(vertexCombo), std::move(fragmentCombo));

		PushUniformVariable("albedoSampler");
		//PushUniformVariable("cubemapSampler");

		PushUniformVariable("VP");
		PushUniformVariable("bones");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");

		if (hasAlphaTesting)
		{
			PushUniformVariable("alphaTestRef");
		}
		else if (hasColorMask)
		{
			PushUniformVariable("colorMask");
		}

		//PushUniformVariable("lightDir");
		//PushUniformVariable("viewOrigin");
		//PushUniformVariable("viewUp");


		ShadowPassData shadowPass;
		SetupShadowpassParams(vars, shadowPass);
		InitShadowPass(context, this, vars, shadowPass);

		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::BONEINDICES_3F,
			"skin_selection.vert", "skin_selection.frag");
		
		PushUniformVariable("VP");
		PushUniformVariable("bones");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
		PushUniformVariable("color_r");
		PushUniformVariable("color_g");
	}

	void RenderNormalPass(MaterialVar **vars, RenderContext *context)
	{
		const float outlineWidth = context->GetRenderParamFloat(RenderParams::FLOAT_OUTLINE_WIDTH);

		if (outlineWidth >= 0.1f)
		{
			BeginPass();

			SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
			SetBones();
			SetUniform1f(context->GetZNear());
			SetUniform1f(context->GetZRange());
			SetUniform1f(context->GetShaderTime());

			SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK, PolygonFillModes::WIREFRAME);
			SetDepthWriteEnabled(false);
			context->PushLineWidth(outlineWidth);

			DrawPass();

			context->PopLineWidth();
			SetDepthWriteEnabled(true);
			SetPolygonMode(PolygonFaceModes::FRONT);
		}
		else
		{
			SkipPass();
		}


		const bool isNocull = vars[NOCULL]->GetInt() != 0;
		const bool hasAlphaTesting = vars[ALPHATESTING]->GetBool();
		const bool hasColorMask = vars[COLORMASKENABLED]->GetBool();

		BeginPass();

		BindTexture(vars, ALBEDOTEXTURE, 0);
		//BindTexture(vars, CUBEMAPTEXTURE, 1);

		SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
		SetBones();
		//SkipUniform();
		SetUniform1f(context->GetZNear());
		SetUniform1f(context->GetZRange());

		if (hasAlphaTesting)
		{
			SetUniform1f(vars[ALPHATESTREF]->GetFloat());
		}
		else if (hasColorMask)
		{
			//SetUniform3fv(vars[COLOR]->GetVec3().data());

			vec3 color;
			context->GetRenderParamVec3(RenderParams::VEC3_MODEL_TINT_COLOR, color);
			SetUniform3fv(color.data());
		}

		//const mat4 &V = context->GetViewMatrix();

		//extern vec3 vLightDir;
		//extern vec3 vViewOrigin;
		//SetUniform3fv(&vLightDir[0], 1);
		//SetUniform3fv(&vViewOrigin[0], 1);

		//vec4 u4 = vec4(0, 1, 0, 1) * V;
		//vec3 u = vec3(u4.x, u4.y, u4.z);
		//SetUniform3fv(&u[0], 1);

		if (isNocull)
			SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK);

		DrawPass();

		if (isNocull)
			SetPolygonMode(PolygonFaceModes::FRONT);
	}

	SHADER_DRAW_PASSES()
	{
		switch (context->GetRenderStage())
		{
		case RenderStages::NORMAL:
			{
				RenderNormalPass(vars, context);

				// skip shadow pass
				SkipPass();

				// skip selection pass
				SkipPass();
			}
			break;
		case RenderStages::SHADOW:
			{
				// skip normal pass
				SkipPass();
				SkipPass();

				ShadowPassData shadowPass;
				SetupShadowpassParams(vars, shadowPass);
				DrawShadowPass(context, this, vars, shadowPass);

				// skip selection pass
				SkipPass();
			}
			break;
		case RenderStages::SELECTION:
			{
				// skip normal pass
				SkipPass();
				SkipPass();

				// skip shadow pass
				SkipPass();

				// draw selection pass
				BeginPass();

				SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
				SetBones();
				SetUniform1f(context->GetZNear());
				SetUniform1f(context->GetZRange());

				int entindex = context->GetRenderParamInt(RenderParams::INT_ENTINDEX);

				if (entindex < 0)
				{
					SetUniform1i(255);
					SetUniform1i(255);
				}
				else
				{
					SetUniform1i(entindex % 256);
					SetUniform1i(entindex / 256);
				}

				DrawPass();
			}
			break;
		}
	}
END_SHADER();