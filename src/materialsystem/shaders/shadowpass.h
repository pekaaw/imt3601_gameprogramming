#ifndef SHADOWPASS_H
#define SHADOWPASS_H

class BaseShader;
class MaterialVar;
class RenderContext;

struct ShadowPassData
{
	ShadowPassData()
		: albedoTexture(-1)
		, alphaTestRef(-1)
		, useSkinning(false)
		, useAlphatesting(false)
		, useNocull(false)
	{
	};

	int albedoTexture;
	int alphaTestRef;

	bool useSkinning;
	bool useAlphatesting;
	bool useNocull;
};

void InitShadowPass(RenderContext *context, BaseShader *shader, MaterialVar **vars, const ShadowPassData &data);
void DrawShadowPass(RenderContext *context, BaseShader *shader, MaterialVar **vars, const ShadowPassData &data);


#endif