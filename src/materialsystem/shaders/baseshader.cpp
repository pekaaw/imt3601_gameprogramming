
#include "pch.h"
#include "baseshader.h"
#include "shaderprogramdict.h"
#include "../rendercontext.h"
#include "..\texturedict.h"
#include "..\mesh.h"

using namespace std;


class CDrawPass
{
public:
	CDrawPass();
	~CDrawPass();

	void Init(int vertexFormat, ShaderCombo &&vertexCombo, ShaderCombo &&fragmentCombo);

	inline void PushUniformVariable(const char *name)
	{
		Assert(shaderProgram != GL_INVALID_INDEX);

		GLuint uniformIndex = glGetUniformLocation(shaderProgram, name);

		uniformQueue.push_back(uniformIndex);
	}

	inline void ResetUniformQueue()
	{
		if (uniformQueue.size() > 0)
		{
			currentUniform = &uniformQueue[0];
		}
		else
		{
			currentUniform = nullptr;
		}
	}

	inline GLuint NextUniformVariable()
	{
		Assert(currentUniform != nullptr);
		Assert(currentUniform >= &uniformQueue[0]);
		Assert((currentUniform - uniformQueue.data()) < int(uniformQueue.size()));

		GLuint value = *currentUniform;
		//Assert(value != GL_INVALID_INDEX);

		currentUniform++;

		return value;
	}

	inline int GetVertexFormat() const
	{
		return vertexFormat;
	}

	inline void Bind() const
	{
		glUseProgram(shaderProgram);
	}

private:
	std::vector<GLuint> uniformQueue;
	GLuint *currentUniform;

	GLuint shaderProgram;
	int vertexFormat;
};

CDrawPass::CDrawPass()
	: currentUniform(nullptr)
	, shaderProgram(GL_INVALID_INDEX)
	, vertexFormat(0)
{
}

CDrawPass::~CDrawPass()
{
	Assert(shaderProgram != GL_INVALID_INDEX);
}

void CDrawPass::Init(int vertexFormat, ShaderCombo &&vertexCombo, ShaderCombo &&fragmentCombo)
{
	assert(shaderProgram == GL_INVALID_INDEX);

	this->vertexFormat = vertexFormat;
	shaderProgram = ShaderProgramDict::GetInstance()->FindShaderProgram(vertexFormat, std::move(vertexCombo), std::move(fragmentCombo));
}

#define VALIDATE_CURRENT_PASS() \
	Assert(currentPass != nullptr); \
	Assert(activeMaterialData != nullptr); \
	Assert(currentPass >= &activeMaterialData->passes[0]); \
	Assert((currentPass - &activeMaterialData->passes[0]) < int(activeMaterialData->passes.size()))

BaseShader::BaseShader()
	: currentPass(nullptr)
	, activeMaterialData(nullptr)
	, activeMesh(nullptr)
{
}

BaseShader::~BaseShader()
{
}

void BaseShader::SetupVars()
{
	LinkStandardVars();
}

void BaseShader::Init(MaterialContext *materialData, MaterialVar **vars)
{
	activeMaterialData = materialData;
	RenderContext *context = RenderContext::GetInstance();

	OnInitParameters(vars, context);
	OnInitPasses(vars, context);
}

void BaseShader::Draw(MaterialContext *materialData, MaterialVar **vars)
{
	activeMaterialData = materialData;
	currentPass = materialData->passes.data();

	RenderContext *context = RenderContext::GetInstance();
	activeMesh = assert_cast<const Mesh*>(context->GetActiveMesh());

	OnDrawPasses(vars, context);

	activeMesh->Unbind();
	activeMesh = nullptr;

	glDisable(GL_TEXTURE_CUBE_MAP);
	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_GEN_R);

	glActiveTexture(GL_TEXTURE0);
	glUseProgram(0);
}

void BaseShader::Shutdown(MaterialContext *materialData, MaterialVar **vars)
{
	for (int i = 0; i < GetParamCount(); i++)
	{
		IMaterialVar *var = vars[i];

		if (var->IsDefined()
			&& var->GetType() == MaterialVarTypes::TEXTURE)
		{
			ITexture *texture = var->GetTexture();

			if (texture != nullptr)
			{
				texture->DecrementReferenceCount();
			}
		}
	}

	for (auto p : materialData->passes)
	{
		delete p;
	}
}

void BaseShader::AddPass(int vertexFormat, const char *vertexShaderName, const char *fragmentShaderName)
{
	ShaderCombo vertexCombo(vertexShaderName);
	ShaderCombo fragmentCombo(fragmentShaderName);

	AddPass(vertexFormat, std::move(vertexCombo), std::move(fragmentCombo));
}

void BaseShader::AddPass(int vertexFormat, ShaderCombo &&vertexCombo, ShaderCombo &&fragmentCombo)
{
	CDrawPass *pass = new CDrawPass();

	pass->Init(vertexFormat, std::move(vertexCombo), std::move(fragmentCombo));

	activeMaterialData->passes.push_back(pass);

	currentPass = &activeMaterialData->passes[activeMaterialData->passes.size() - 1];
}

void BaseShader::SkipPass()
{
	VALIDATE_CURRENT_PASS();

	currentPass++;
}

void BaseShader::BeginPass()
{
	VALIDATE_CURRENT_PASS();

	(*currentPass)->Bind();
	(*currentPass)->ResetUniformQueue();
}

void BaseShader::DrawPass()
{
	VALIDATE_CURRENT_PASS();

	Assert(activeMesh != nullptr);

	activeMesh->Bind((*currentPass)->GetVertexFormat());
	activeMesh->Draw();
	currentPass++;
}

void BaseShader::PushUniformVariable(const char *name)
{
	VALIDATE_CURRENT_PASS();

	(*currentPass)->PushUniformVariable(name);
}

void BaseShader::SkipUniform()
{
	VALIDATE_CURRENT_PASS();

	(*currentPass)->NextUniformVariable();
}

void BaseShader::SetUniform1i(int value)
{
	VALIDATE_CURRENT_PASS();
	glUniform1i((*currentPass)->NextUniformVariable(), value);
}

void BaseShader::SetUniform1iv(const int *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform1iv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform1f(float value)
{
	VALIDATE_CURRENT_PASS();
	glUniform1f((*currentPass)->NextUniformVariable(), value);
}

void BaseShader::SetUniform1fv(const float *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform1fv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform2i(int value0, int value1)
{
	VALIDATE_CURRENT_PASS();
	glUniform2i((*currentPass)->NextUniformVariable(), value0, value1);
}

void BaseShader::SetUniform2iv(const int *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform2iv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform2f(float value0, float value1)
{
	VALIDATE_CURRENT_PASS();
	glUniform2f((*currentPass)->NextUniformVariable(), value0, value1);
}

void BaseShader::SetUniform2fv(const float *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform2fv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform3i(int value0, int value1, int value2)
{
	VALIDATE_CURRENT_PASS();
	glUniform3i((*currentPass)->NextUniformVariable(), value0, value1, value2);
}

void BaseShader::SetUniform3iv(const int *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform3iv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform3f(float value0, float value1, float value2)
{
	VALIDATE_CURRENT_PASS();
	glUniform3f((*currentPass)->NextUniformVariable(), value0, value1, value2);
}

void BaseShader::SetUniform3fv(const float *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform3fv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform4i(int value0, int value1, int value2, int value3)
{
	VALIDATE_CURRENT_PASS();
	glUniform4i((*currentPass)->NextUniformVariable(), value0, value1, value2, value3);
}

void BaseShader::SetUniform4iv(const int *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform4iv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniform4f(float value0, float value1, float value2, float value3)
{
	VALIDATE_CURRENT_PASS();
	glUniform4f((*currentPass)->NextUniformVariable(), value0, value1, value2, value3);
}

void BaseShader::SetUniform4fv(const float *value, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniform4fv((*currentPass)->NextUniformVariable(), count, value);
}

void BaseShader::SetUniformMatrix4x4fv(const float *matrix, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniformMatrix4fv((*currentPass)->NextUniformVariable(), count, GL_FALSE, matrix);
}

void BaseShader::SetUniformMatrix4x3fv(const float *matrix, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniformMatrix4x3fv((*currentPass)->NextUniformVariable(), count, GL_FALSE, matrix);
}

void BaseShader::SetUniformMatrix3x4fv(const float *matrix, int count)
{
	VALIDATE_CURRENT_PASS();
	glUniformMatrix3x4fv((*currentPass)->NextUniformVariable(), count, GL_FALSE, matrix);
}

void BaseShader::SetBones()
{
	VALIDATE_CURRENT_PASS();
	RenderContext *context = RenderContext::GetInstance();
	context->BindBones((*currentPass)->NextUniformVariable());
}

void BaseShader::SetPolygonMode(PolygonFaceModeId mode, PolygonFillModeId fill)
{
	GLenum fillMode = (fill == PolygonFillModes::FILL) ? GL_FILL : GL_LINE;

	switch (mode)
	{
	default:
		Assert(0);
	case PolygonFaceModes::FRONT:
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		glPolygonMode(GL_FRONT, fillMode);
		break;
	case PolygonFaceModes::BACK:
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CW);
		glPolygonMode(GL_FRONT, fillMode);
		break;
	case PolygonFaceModes::FRONT_AND_BACK:
		glDisable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		glPolygonMode(GL_FRONT_AND_BACK, fillMode);
		break;
	}
}

void BaseShader::SetDepthWriteEnabled(bool enabled)
{
	if (enabled)
		glDepthMask(GL_TRUE);
	else
		glDepthMask(GL_FALSE);
}

void BaseShader::SetDepthTestEnabled(bool enabled)
{
	if (enabled)
		glEnable(GL_DEPTH_TEST);
	else
		glDisable(GL_DEPTH_TEST);
}

void BaseShader::SetBlendMode(BlendModeId mode)
{
	switch (mode)
	{
		Assert(0);
	case BlendModes::NONE:
		glDisable(GL_BLEND);
		break;
	case BlendModes::SRC_ALPHA:
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case BlendModes::ADDITIVE:
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		break;
	}
}

void BaseShader::LoadTexture(MaterialVar **vars, int materialParam, int flags)
{
	LoadTexture(vars, materialParam, TextureTypes::TEXTURE_2D, flags);
}

void BaseShader::LoadCubemap(MaterialVar **vars, int materialParam, int flags)
{
	LoadTexture(vars, materialParam, TextureTypes::TEXTURE_CUBEMAP, flags);
}

void BaseShader::LoadTexture(MaterialVar **vars, int materialParam, TextureTypeId type, int flags)
{
	const char *texturePath = vars[materialParam]->GetString();

	TextureFormatId format = TextureFormats::RGBA_8;

	if (G_StrHasExtension(texturePath, "tga"))
		format = TextureFormats::BGRA_8;

	// texturegroup none so it can be determined by path
	ITexture *texture = TextureDict::GetInstance()->FindOrLoadTexture(
		texturePath, TextureGroups::NONE, type, format, flags);

	vars[materialParam]->SetTexture(texture);
}

void BaseShader::BindTexture(MaterialVar **vars, int materialParam, int samplerIndex)
{
	VALIDATE_CURRENT_PASS();

	Assert(vars[materialParam]->GetType() == MaterialVarTypes::TEXTURE);

	Texture *texture = assert_cast<Texture*>(vars[materialParam]->GetTexture());

	glUniform1i((*currentPass)->NextUniformVariable(), samplerIndex);
	glActiveTexture(GL_TEXTURE0 + samplerIndex);

	texture->Bind();
}
