#ifndef SHADERPROGRAMDICT_H
#define SHADERPROGRAMDICT_H

#include <vector>
#include <map>
#include <string>

#include "util\singleton.h"

class ShaderProgramDict;

// hold combination for a single shader
class ShaderCombo
{
	friend class ShaderProgramDict;

public:
	ShaderCombo();
	ShaderCombo(const char *shaderName);
	ShaderCombo(const ShaderCombo &&other);

	void SetShaderName(const char *name);
	const char *GetShaderName() const;

	void SetCombo(const char *name, int value);
	bool HasCombo(const char *name) const;

	void CatHashString(char *out, int sizeInBytes) const;

	std::map<std::string, int>::const_iterator begin() const;
	std::map<std::string, int>::const_iterator end() const;

private:

	char shaderName[64];
	std::map<std::string, int> combos;
};

// holds shaders identified by their combo hashes
// holds programs identified by the combo hashes of both their shaders
class ShaderProgramDict
{
	DECLARE_SINGLETON(ShaderProgramDict);

public:
	~ShaderProgramDict();

	void Shutdown();

	GLuint FindShaderProgram(int vertexFormat, ShaderCombo &&comboVertex, ShaderCombo &&comboFragment);

private:
	GLuint FindShader(GLenum type, const char *hash, const ShaderCombo &combo);
	void GetShaderDefault(const char *path, ShaderCombo &comboOut);
	void ApplyMissingDefaults(const char *path, ShaderCombo &combo);

	std::unordered_map<std::string, GLuint> shaderPrograms;
	std::unordered_map<std::string, GLuint> shaders;
	std::unordered_map<std::string, ShaderCombo> shaderDefaults;

};

#endif