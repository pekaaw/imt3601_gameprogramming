#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(towerrange)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "");
	SHADER_VAR(INNERTEXTURE, MaterialVarTypes::TEXTURE, "");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
	LoadTexture(vars, INNERTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F,
		"towerrange.vert", "towerrange.frag");

	PushUniformVariable("MVP");
	PushUniformVariable("znear");
	PushUniformVariable("zrange");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("innerSampler");
	PushUniformVariable("diffusemodulation");
	PushUniformVariable("time");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
	SetUniform1f(context->GetZNear());
	SetUniform1f(context->GetZRange());

	BindTexture(vars, ALBEDOTEXTURE, 0);
	BindTexture(vars, INNERTEXTURE, 1);

	vec4 diffuseModulation;
	context->GetRenderParamVec4(RenderParams::VEC4_DIFFUSE_MODULATION, diffuseModulation);
	SetUniform4fv(diffuseModulation.data());
	SetUniform1f(context->GetShaderTime());

	//SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK); //, PolygonFillModes::WIREFRAME);

	SetDepthWriteEnabled(false);
	SetBlendMode(BlendModes::SRC_ALPHA);

	DrawPass();

	SetDepthWriteEnabled(true);
	SetBlendMode(BlendModes::NONE);
}
END_SHADER();
