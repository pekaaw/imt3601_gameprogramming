#ifndef SHADERFACTORY_H
#define SHADERFACTORY_H

#include "util/singleton.h"

class BaseShader;

class ShaderFactory
{
	DECLARE_SINGLETON(ShaderFactory);
public:

	void Init();

	typedef BaseShader* (*ShaderFactoryFunc)();
	static void RegisterShaderFactory(const char *name, ShaderFactoryFunc function);
	BaseShader *CreateShader(const char *name);

private:

	std::unordered_map<std::string, ShaderFactoryFunc> functions;
};

#endif