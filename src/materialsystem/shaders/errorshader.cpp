
#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(error)
	BEGIN_SHADER_VARS
		SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	END_SHADER_VARS

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, ALBEDOTEXTURE);
	}

	SHADER_INIT_PASSES()
	{
		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::NORMAL_3F
			| VertexFormat::TEXTURE_COORD_2F
			| VertexFormat::BONEINDICES_3F,
			"error.vert", "error.frag");

		PushUniformVariable("albedoSampler");

		PushUniformVariable("VP");
		PushUniformVariable("bones");

	}

	SHADER_DRAW_PASSES()
	{
		BeginPass();

		BindTexture(vars, ALBEDOTEXTURE, 0);

		SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
		SetBones();

		const bool isNocull = vars[NOCULL]->GetInt() != 0;

		if (isNocull)
			SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK);

		DrawPass();

		if (isNocull)
			SetPolygonMode(PolygonFaceModes::FRONT);
	}
END_SHADER();