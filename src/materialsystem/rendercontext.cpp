#include "pch.h"
#include "rendercontext.h"
#include "fbodict.h"
#include "shaders\shaderfactory.h"
#include "materialdict.h"
#include "texturedict.h"
#include "vbodict.h"
#include "mesh.h"
#include "material.h"
#include "mdxrenderabledict.h"
#include "shaders\shaderprogramdict.h"
#include "materialsystem\meshbuilder.h"
#include "appinterface\iappinterface.h"

#include <GL\glew.h>

using namespace std;


SINGLETON_INSTANCE(RenderContext);

EXPOSE_APP(INTERFACE_IRENDERCONTEXT_VERSION, RenderContext::GetInstance());


RenderContext::RenderContext()
	: boneCount(0)
		//, activeMaterial(nullptr)
	  , activeMesh(nullptr)
	  , fullscreenMesh(nullptr)
	  , bonePointer(nullptr)
	  , screenWidth(0)
	  , screenHeight(0)
	  , stateMachineCheck(0)
	  , zNear(0.0f)
	  , zFar(0.0f)
	  , zRange(0.0f)
	  , activeRenderStage(RenderStages::NORMAL)
	  , worldCoordinatesEnabled(true)
	  , matricesValid(false)
	  , fullscreen(false)
	  , time(0.0f)
	  , materialOverride(nullptr)
{
	matricesModel.push(mat4::Identity());
	matricesView.push(mat4::Identity());
	matricesProjection.push(mat4::Identity());
	lineWidth.push(1.0f);

	viewProjectionMain = mat4::Identity();
}

RenderContext::~RenderContext()
{
	Assert(stateMachineCheck == 2);

	Assert(fullscreenMesh == nullptr);
}

void RenderContext::Init(void *windowHandle, int windowWidth, int windowHeight)
{
	Assert(stateMachineCheck == 0);
	stateMachineCheck = 1;

	this->windowHandle = windowHandle;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	glContextHandle = SDL_GL_CreateContext((SDL_Window *)windowHandle);

	if (glContextHandle == nullptr)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Failed to initialize OpenGL context!", (SDL_Window*)windowHandle);
	}

	if (glewInit() != GLEW_OK)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Failed to init glew library!", (SDL_Window*)windowHandle);
	}

	if (!GLEW_VERSION_3_3)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "OpenGL version 3.3 not supported!", (SDL_Window*)windowHandle);
	}

	const char *glslVersion = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

	if (glslVersion && *glslVersion)
	{
		DBGMSGF("GLSL: %s\n", glslVersion);
	}

	//glEnable(GL_MULTISAMPLE);
	//glEnable(GL_LINE_SMOOTH);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glEnable(GL_POLYGON_SMOOTH);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_MULTISAMPLE);
	glDepthMask(GL_TRUE);

	ShaderFactory::GetInstance()->Init();
	SetScreenSize(windowWidth, windowHeight);
	PushViewport(0, 0, windowWidth, windowHeight);
	FBODict::GetInstance()->Init();
	MDXRenderableDict::GetInstance()->Init();

	BuildFullscreenMesh();
}

void RenderContext::Resize(int windowWidth, int windowHeight)
{
	Assert(stateMachineCheck == 1);

	SetScreenSize(windowWidth, windowHeight);
	FBODict::GetInstance()->OnResize(windowWidth, windowHeight);

	SetViewPort(0, 0, windowWidth, windowHeight);
}

void RenderContext::Shutdown()
{
	Assert(stateMachineCheck == 1);
	stateMachineCheck = 2;

	DestroyFullscreenMesh();
	DestroyDynamicMeshes();

	MDXRenderableDict::GetInstance()->Shutdown();
	MaterialDict::GetInstance()->Shutdown();
	ShaderProgramDict::GetInstance()->Shutdown();
	FBODict::GetInstance()->Shutdown();
	TextureDict::GetInstance()->Shutdown();

	VBODict::GetInstance()->Shutdown();

	SDL_GL_DeleteContext(glContextHandle);
}

bool RenderContext::IsFullscreen()
{
	return fullscreen;
	//return (SDL_GetWindowFlags((SDL_Window*)windowHandle) & SDL_WINDOW_FULLSCREEN) != 0;
}

void RenderContext::SetFullscreen(bool isFullscreen)
{
	if (isFullscreen)
	{
		SDL_SetWindowFullscreen((SDL_Window*)windowHandle, SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP);
	}
	else
	{
		SDL_SetWindowFullscreen((SDL_Window*)windowHandle,
			SDL_GetWindowFlags((SDL_Window*)windowHandle) & ~(SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP));
	}

	fullscreen = isFullscreen;
}

void RenderContext::MakeCurrent(bool assignContext)
{
	SDL_GL_MakeCurrent((SDL_Window*)windowHandle, (assignContext ? glContextHandle : nullptr));
}

bool RenderContext::CheckForErrors()
{
	GLenum err = glGetError();

	if (err != 0)
	{
		DBGMSGF("%i: %s\n", err, gluErrorString(err));

		static bool assertOnce = false;

		if (!assertOnce)
		{
			assertOnce = true;
			Assert(0);
		}
	}

	return err != 0;
}

void RenderContext::SetRenderStage(RenderStageId stage)
{
	activeRenderStage = stage;
}

RenderStageId RenderContext::GetRenderStage() const
{
	return activeRenderStage;
}

void RenderContext::SetRenderParamFloat(RenderParam_Float param, float value)
{
	Assert(param >= 0 && param < RenderParams::FLOAT_COUNT);
	renderParamFloats[param] = value;
}

void RenderContext::SetRenderParamInt(RenderParam_Int param, int value)
{
	Assert(param >= 0 && param < RenderParams::INT_COUNT);
	renderParamInts[param] = value;
}

void RenderContext::SetRenderParamVec3(RenderParam_Vec3 param, const vec3 &value)
{
	Assert(param >= 0 && param < RenderParams::VEC3_COUNT);
	renderParamVec3s[param] = value;
}

void RenderContext::SetRenderParamVec4(RenderParam_Vec4 param, const vec4 &value)
{
	Assert(param >= 0 && param < RenderParams::VEC4_COUNT);
	renderParamVec4s[param] = value;
}

void RenderContext::SetRenderParamMat4(RenderParam_Mat4 param, const mat4 &value)
{
	Assert(param >= 0 && param < RenderParams::MAT4_COUNT);
	renderParamMat4s[param] = value;
}

float RenderContext::GetRenderParamFloat(RenderParam_Float param) const
{
	Assert(param >= 0 && param < RenderParams::FLOAT_COUNT);
	return renderParamFloats[param];
}

int RenderContext::GetRenderParamInt(RenderParam_Int param) const
{
	Assert(param >= 0 && param < RenderParams::INT_COUNT);
	return renderParamInts[param];
}

void RenderContext::GetRenderParamVec3(RenderParam_Vec3 param, vec3 &value) const
{
	Assert(param >= 0 && param < RenderParams::VEC3_COUNT);
	value = renderParamVec3s[param];
}

void RenderContext::GetRenderParamVec4(RenderParam_Vec4 param, vec4 &value) const
{
	Assert(param >= 0 && param < RenderParams::VEC4_COUNT);
	value = renderParamVec4s[param];
}

void RenderContext::GetRenderParamMat4(RenderParam_Mat4 param, mat4 &value) const
{
	Assert(param >= 0 && param < RenderParams::MAT4_COUNT);
	value = renderParamMat4s[param];
}

void RenderContext::SetBoneCount(int boneCount)
{
	this->boneCount = boneCount;
}

int *RenderContext::GetBoneIndicesForModify()
{
	return boneIndices;
}

void RenderContext::SetBonePointer(mat4 *bones, int count)
{
	bonePointer = bones;
	boneCount = count;
}

mat4 *RenderContext::GetBoneTransformsForModify()
{
	return boneTransforms;
}

void RenderContext::SetBoneIndices(int *boneIndices)
{
	Assert(boneCount > 0);

	memcpy(this->boneIndices, boneIndices, sizeof(int) * boneCount);
}

void RenderContext::BindBones(GLuint uniformIndex)
{
	// TODO: optimize bones? (costs more on cpu)

	//if (boneCount < 1)
	//	return;

	// only send used bones?
	//for (int i = 0; i < boneCount; i++)
	//{
	//	boneTransformsSorted[i] = boneTransforms[boneIndices[i]];
	//}

	//glUniformMatrix4fv(uniformIndex, boneCount, GL_FALSE, (GLfloat*) &boneTransformsSorted[0][0]);

	//glUniformMatrix4fv(uniformIndex, MODEL_MAX_BONES, GL_FALSE, (GLfloat *)boneTransforms[0].data());

	Assert(bonePointer != nullptr);

	glUniformMatrix4fv(uniformIndex, MODEL_MAX_BONES, GL_FALSE, (GLfloat *)bonePointer);
}

void RenderContext::PushModelMatrix(const mat4 &matrix)
{
	matricesModel.push(matrix);

	MakeMatricesDirty();
}

void RenderContext::PushViewMatrix(const mat4 &matrix)
{
	matricesView.push(matrix);

	MakeMatricesDirty();
}

void RenderContext::PushProjectionMatrix(const mat4 &matrix)
{
	matricesProjection.push(matrix);

	MakeMatricesDirty();
}

void RenderContext::PopModelMatrix()
{
	matricesModel.pop();
	Assert(matricesModel.size() > 0);

	MakeMatricesDirty();
}

void RenderContext::PopViewMatrix()
{
	matricesView.pop();
	Assert(matricesView.size() > 0);

	MakeMatricesDirty();
}

void RenderContext::PopProjectionMatrix()
{
	matricesProjection.pop();
	Assert(matricesProjection.size() > 0);

	MakeMatricesDirty();
}

void RenderContext::LoadModelIdentity()
{
	matricesModel.top() = mat4::Identity();

	MakeMatricesDirty();
}

void RenderContext::LoadViewIdentity()
{
	matricesView.top() = mat4::Identity();

	MakeMatricesDirty();
}

void RenderContext::LoadProjectionIdentity()
{
	matricesProjection.top() = mat4::Identity();

	MakeMatricesDirty();
}

void RenderContext::SetWorldCoordinatesEnabled(bool enabled)
{
	if (worldCoordinatesEnabled != enabled)
	{
		worldCoordinatesEnabled = enabled;

		MakeMatricesDirty();
	}
}

void RenderContext::SetMainMatrices()
{
	UpdateMatrices();

	viewProjectionMain = viewProjectionOGL;
}

void RenderContext::SetDepthRange(float zNear, float zFar)
{
	this->zNear = zNear;
	this->zFar = zFar;

	zRange = zFar - zNear;
}

float RenderContext::GetZNear() const
{
	return zNear;
}

float RenderContext::GetZFar() const
{
	return zFar;
}

float RenderContext::GetZRange() const
{
	return zRange;
}

void RenderContext::PushLineWidth(float width)
{
	// doesn't work as expected, maybe because it's incompatible with shaders
	lineWidth.push(width);
	glLineWidth(width);
}

void RenderContext::PopLineWidth()
{
	lineWidth.pop();
	glLineWidth(lineWidth.top());
}

void RenderContext::UpdateMatrices()
{
	if (matricesValid)
	{
		return;
	}

	matricesValid = true;

	const mat4& model = matricesModel.top();
	const mat4& view = matricesView.top();
	const mat4& projection = matricesProjection.top();

	viewProjection = projection * view;
	modelViewProjection = viewProjection * model;

	// translate worldspace to device space
	if (worldCoordinatesEnabled)
	{
		mat4 toOGL = mat4::Zero();
		toOGL.col(0)(0) = -1.0f;
		toOGL.col(1)(1) = -1.0f;
		toOGL.col(2)(2) = -1.0f;
		toOGL.col(3)(3) = 1.0f;

		mat4 matView;
		matView.row(0) = view.row(1);
		matView.row(1) = -view.row(2);
		matView.row(2) = view.row(0);
		matView.row(3) = view.row(3);

		viewProjectionOGL = projection * matView * toOGL;
		modelViewProjectionOGL = viewProjectionOGL * model;
	}
	else
	{
		viewProjectionOGL = viewProjection;
		modelViewProjectionOGL = modelViewProjection;
	}
}

void RenderContext::MakeMatricesDirty()
{
	matricesValid = false;
}

const mat4 &RenderContext::GetModelViewProjectionMatrix() const
{
	return modelViewProjection;
}

const mat4 &RenderContext::GetViewProjectionMatrix() const
{
	return viewProjection;
}

const mat4 &RenderContext::GetModelMatrix() const
{
	return matricesModel.top();
}

const mat4 &RenderContext::GetViewMatrix() const
{
	return matricesView.top();
}

const mat4 &RenderContext::GetProjectionMatrix() const
{
	return matricesProjection.top();
}

const mat4 &RenderContext::GetOGLViewProjectionMatrix() const
{
	return viewProjectionOGL;
}

const mat4 &RenderContext::GetOGLModelViewProjectionMatrix() const
{
	return modelViewProjectionOGL;
}

const mat4 &RenderContext::GetMainViewProjectionMatrix() const
{
	return viewProjectionMain;
}

//void RenderContext::BindMaterial(Material *material)
//{
//	activeMaterial = material;
//}
//
//Material *RenderContext::GetActiveMaterial()
//{
//	return activeMaterial;
//}

int RenderContext::GetScreenWidth() const
{
	return screenWidth;
}

int RenderContext::GetScreenHeight() const
{
	return screenHeight;
}

void RenderContext::SetScreenSize(int w, int h)
{
	screenWidth = w;
	screenHeight = h;
}

void RenderContext::PushViewport(int x, int y, int w, int h)
{
	viewportStack.push(ViewportData());
	SetViewPort(x, y, w, h);
}

void RenderContext::SetViewPort(int x, int y, int w, int h)
{
	if (x < 0)
	{
		x = 0;
	}

	if (y < 0)
	{
		y = 0;
	}

	if (w < 0)
	{
		w = screenWidth;
	}

	if (h < 0)
	{
		h = screenHeight;
	}

	ViewportData &data = viewportStack.top();
	data.x = x;
	data.y = y;
	data.w = w;
	data.h = h;

	glViewport(x, y, w, h);
}

void RenderContext::PopViewport()
{
	viewportStack.pop();

	const ViewportData &data = viewportStack.top();
	glViewport(data.x, data.y, data.w, data.h);
}

void RenderContext::SetClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void RenderContext::ClearBuffers(bool color, bool depth)
{
	GLbitfield mask = 0;

	if (color)
	{
		mask |= GL_COLOR_BUFFER_BIT;
	}

	if (depth)
	{
		mask |= GL_DEPTH_BUFFER_BIT;
	}

	glClear(mask);
}

void RenderContext::SetDepthTestEnabled(bool enabled)
{
	if (enabled)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
}

void RenderContext::SetClipPlaneEnabled(int index, bool enabled)
{
	if (enabled)
	{
		glEnable(GL_CLIP_DISTANCE0 + index);
	}
	else
	{
		glDisable(GL_CLIP_DISTANCE0 + index);
	}
}

void RenderContext::SwapBuffers()
{
	Assert(stateMachineCheck == 1);

	SDL_GL_SwapWindow((SDL_Window *)windowHandle);
}

IMesh *RenderContext::CreateStaticMesh(FaceTypeId type, int vertexFormat, int faceCount)
{
	Mesh *mesh = new Mesh();
	mesh->Init(type, vertexFormat, faceCount);
	return mesh;
}

IMesh *RenderContext::GetDynamicMesh(FaceTypeId type, int vertexFormat, int faceCount)
{
	Assert(type >= 0 && type < FaceTypes::COUNT);

	auto &meshes = dynamicMeshes[type];

	// if the amount of dynamic meshes becomes too large
	// consider making the whole vertex attrib binding swappable
	// and use a single dynamic mesh
	Assert(meshes.size() < 10);

	auto &itr = meshes.find(vertexFormat);

	// ever cached a mesh with this format?
	if (itr != meshes.end())
	{
		// tell the mesh to draw a specific amount of faces
		itr->second.mesh->SetFaceCount(faceCount);

		// just return the cached mesh if it's large enough
		return itr->second.mesh;
	}

	// insert new dynamic mesh
	DynamicMeshData data;
	data.faceCount = faceCount;
	data.mesh = new Mesh();
	data.mesh->Init(type, vertexFormat, faceCount, true);
	data.mesh->Upload();

	meshes[vertexFormat] = data;
	return data.mesh;
}

void RenderContext::DestroyDynamicMeshes()
{
	for (int i = 0; i < ARRAYSIZE(dynamicMeshes); i++)
	{
		for (auto m : dynamicMeshes[i])
		{
			m.second.mesh->Release();
		}
	}
}

void RenderContext::BindMesh(const IMesh *mesh)
{
	activeMesh = mesh;
}

const IMesh *RenderContext::GetActiveMesh() const
{
	return activeMesh;
}

void RenderContext::BuildFullscreenMesh()
{
	Assert(fullscreenMesh == nullptr);

	fullscreenMesh = new Mesh();
	fullscreenMesh->Init(FaceTypes::TRIANGLE_STRIP, VertexFormat::POSITION_2F, 2);

	MeshBuilder builder;
	builder.Attach(fullscreenMesh);

	builder.Position2f(-1, 1);
	builder.AdvanceVertex();
	builder.Position2f(-1, -1);
	builder.AdvanceVertex();
	builder.Position2f(1, 1);
	builder.AdvanceVertex();
	builder.Position2f(1, -1);
	builder.AdvanceVertex();

	builder.Detach();
}

void RenderContext::DestroyFullscreenMesh()
{
	Assert(fullscreenMesh != nullptr);

	fullscreenMesh->Release();
	fullscreenMesh = nullptr;
}

void RenderContext::SetMaterialOverride(IMaterial *material)
{
	materialOverride = material;
}

IMaterial *RenderContext::GetMaterialOverride()
{
	return materialOverride;
}

void RenderContext::DrawFullscreenQuad(IMaterial *material)
{
	BindMesh(fullscreenMesh);
	material->Draw();
}

void RenderContext::SetShaderTime(float time)
{
	this->time = time;
}

float RenderContext::GetShaderTime() const
{
	return time;
}

static SDL_Surface *SurfaceFlipVert(SDL_Surface *sfc)
{
	SDL_Surface *result = SDL_CreateRGBSurface(sfc->flags, sfc->w, sfc->h,
		sfc->format->BytesPerPixel * 8, sfc->format->Rmask, sfc->format->Gmask,
		sfc->format->Bmask, sfc->format->Amask);

	Uint8 *pixels = (Uint8*) sfc->pixels;
	Uint8 *rpixels = (Uint8*) result->pixels;
	int pitch = sfc->pitch;
	int pxlength = pitch * sfc->h;

	Assert(result != nullptr);

	for (int line = 0; line < sfc->h; ++line)
	{
		int pos = line * pitch;

		memcpy(&rpixels[pos], &pixels[(pxlength-pos)-pitch], pitch);
	}

	return result;
}

void RenderContext::TakeScreenshot(const char *file)
{
	SDL_Surface *image = SDL_CreateRGBSurface(SDL_SWSURFACE, screenWidth, screenHeight, 24, 0x000000FF, 0x0000FF00, 0x00FF0000, 0);

	glReadBuffer(GL_FRONT);
	glReadPixels(0, 0, screenWidth, screenHeight, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);

	SDL_Surface *flipped = SurfaceFlipVert(image);

	IMG_SavePNG(flipped, file);

	SDL_FreeSurface(flipped);
	SDL_FreeSurface(image);
}