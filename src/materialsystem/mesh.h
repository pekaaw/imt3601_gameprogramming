#ifndef MESH_H
#define MESH_H

#include "vertexattributebinding.h"
#include "materialsystem\imesh.h"

class BaseShader;

class Mesh : public IMesh
{
	~Mesh();

	NO_COPY(Mesh);
public:
	Mesh();

	void Init(FaceTypeId type, int vertexFormat, int faceCount, bool isDynamic = false);
	void SetFaceCount(int newFaceCount);

	void Bind(int destinationVertexFormat) const;
	void Unbind() const;
	void Draw() const;

	// IMesh
	virtual void Release();
	virtual void FlushCache();

	virtual void Upload();
	virtual float *GetData() { return meshData; }

	virtual float *BindMap(int offset, int range);
	virtual void BindUnmap();

	virtual int GetVertexFormat() const { return vertexFormat; }
	virtual int GetVertexCount() const { return vertexCount; }

	virtual FaceTypeId GetFaceType() const { return faceType; }

private:

	int CalcVertexCount(int faceCount);

	FaceTypeId faceType;
	bool isDynamic;

	float *meshData;
	VertexAttribBinding vertexBinding;
	int vertexFormat;
	int vertexFormatSize;

	int vertexCount;
	int initialVertexCount;

	int arrayBufferSize;
	VBO_KEY vbo;

	// additional info for dynamic, ring buffer meshes
	int ringbufferSize;
	int ringbufferOffset;
	int renderOffset;
};

#endif