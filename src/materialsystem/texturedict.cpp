
#include "pch.h"
#include "materialsystem\materialsystem.h"
#include "texture.h"
#include "texturedict.h"
#include "util\xmlutil.h"
#include "appinterface\iappinterface.h"


SINGLETON_INSTANCE(TextureDict);

EXPOSE_APP(INTERFACE_ITEXTUREDICT_VERSION, TextureDict::GetInstance());

TextureDict::TextureDict()
	: errorTexture(nullptr)
{

}

void TextureDict::Shutdown()
{
	for (auto t : textures)
	{
		if (t.second != errorTexture)
		{
			Assert(!t.second->IsReferenced());

			t.second->Release();
		}
	}

	textures.clear();

	if (errorTexture != nullptr)
	{
		Assert(!errorTexture->IsReferenced());

		errorTexture->Release();
	}

	errorTexture = nullptr;
}

bool TextureDict::ReleaseTextureIfUnreferenced(ITexture *texture)
{
	Assert(texture != nullptr);

	Texture *concreteTexture = assert_cast<Texture*>(texture);

	if (concreteTexture->IsReferenced())
	{
		return false;
	}

	auto itr = textures.find(texture->GetTextureName());

	if (itr == textures.end())
	{
		Assert(0);
		return false;
	}

	textures.erase(itr);

	concreteTexture->Release();
	return true;
}

void TextureDict::ReleaseUnreferencedTextures(TextureGroupId group)
{
	for (auto itr = textures.begin(); itr != textures.end(); )
	{
		if (group != TextureGroups::NONE
			&& itr->second->GetGroup() != group)
		{
			itr++;
			continue;
		}

		if (!itr->second->IsReferenced()
			&& itr->second != errorTexture)
		{
			itr->second->Release();
			itr = textures.erase(itr);
		}
		else
		{
			itr++;
		}
	}
}

void TextureDict::InsertTexture(const char *path, Texture *texture)
{
	Assert(textures.find(path) == textures.end());

	textures[path] = texture;
}

ITexture *TextureDict::FindOrLoadTexture(const char *path,
	TextureGroupId group,
	TextureTypeId type,
	TextureFormatId format,
	int flags)
{
	StringScope pathFixed(path);

	G_StrFixSlashes(pathFixed);
	auto itr = textures.find(pathFixed.c_str());

	if (itr != textures.end())
	{
		return itr->second;
	}

	char fullPath[MAX_PATH_GAME];
	G_StrAbsContentPath(ContentDirectories::MATERIALS, pathFixed.c_str(), fullPath, sizeof(fullPath));

	// determine texturegroup by path
	if (group == TextureGroups::NONE)
	{
		group = G_StrGetTextureGroup(fullPath);
	}

	LoadTextureMetaData(fullPath, flags);

	const bool isCubemap = type == TextureTypes::TEXTURE_CUBEMAP;

	SDL_Surface *surface = isCubemap ? nullptr : IMG_Load(fullPath);
	Texture *texture = nullptr;

	if (surface != nullptr)
	{
		SDL_PixelFormat *pixel_format = surface->format;

		Uint8 bpp = pixel_format->BytesPerPixel;
		format = TextureFormats::GetCorrectedFormat(bpp, format);

		texture = new Texture();
		texture->Init(pathFixed.c_str(), format, type, group, flags,
			surface->w, surface->h, surface->pixels);

		SDL_FreeSurface(surface);
	}
	else if (isCubemap)
	{
		static const char *extension[6] = {
			"rt",
			"lt",
			"tp",
			"bt",
			"ft",
			"bk",
		};

		SDL_Surface *faces[6] = { nullptr };
		bool hadLoadError = false;

		for (int i = 0; i < 6; i++)
		{
			G_StrSnPrintf(fullPath, sizeof(fullPath), "materials" PATHSEPARATOR_STRING "%s%s", pathFixed.c_str(), extension[i]);
			faces[i] = IMG_Load(fullPath);

			hadLoadError = hadLoadError || faces[i] == nullptr;
		}

		if (!hadLoadError)
		{
			SDL_PixelFormat *pixel_format = faces[0]->format;

			Uint8 bpp = pixel_format->BytesPerPixel;
			format = TextureFormats::GetCorrectedFormat(bpp, format);

			Texture *texture = new Texture();
			texture->Init(pathFixed, format, type, group, flags,
				faces[0]->w, faces[0]->h, nullptr);

			for (int i = 0; i < 6; i++)
			{
				texture->SetCubemapFace(i, faces[i]->pixels);
			}

			texture->FinalizeCubemap();
		}

		for (int i = 0; i < 6; i++)
		{
			if (faces[i] != nullptr)
			{
				SDL_FreeSurface(faces[i]);
			}
		}
	}

	if (texture != nullptr)
	{
		Assert(textures.find(pathFixed.c_str()) == textures.end());

		textures[pathFixed.c_str()] = texture;

		return texture;
	}

	// reaching this means failed to load resources
	if (errorTexture == nullptr)
	{
		//Assert(0);

		errorTexture = assert_cast<Texture*>(FindOrLoadTexture("error.tga",
			TextureGroups::OTHER, TextureTypes::TEXTURE_2D, TextureFormats::BGRA_8));

		textures[pathFixed.c_str()] = errorTexture;
	}

	return errorTexture;
}

ITexture *TextureDict::FindTexture(const char *path)
{
	StringScope pathFixed(path);
	G_StrFixSlashes(pathFixed);

	// should at least have tried to load it once through materials
	Assert(textures.find(pathFixed.c_str()) != textures.end());

	if (textures.find(pathFixed.c_str()) != textures.end())
	{
		return nullptr;
	}

	return textures[pathFixed.c_str()];
}

ITexture *TextureDict::CreateTexture(const char *path,
		void *data, int width, int height, int flags,
		TextureFormatId format, TextureGroupId group)
{
	Assert(textures.find(path) == textures.end());

	if (textures.find(path) != textures.end())
	{
		return textures[path];
	}

	Texture *texture = new Texture();
	texture->Init(path, format, TextureTypes::TEXTURE_2D,
		group, flags, width, height, data );

	textures[path] = texture;

	return texture;
}

void TextureDict::LoadTextureMetaData(const char *textureName, int &flags)
{
	StringScope metadataName(G_StrCreateCopy(textureName));

	G_StrReplaceExtension(metadataName, "tex");

	CXMLDocument doc;

	if (!LoadXMLFile(metadataName, doc))
		return;

	struct MetaDataAssociation
	{
		const char *xmlname;
		const int flag;
	};
	static MetaDataAssociation s_MetaDataAssociation [] = {
		{ "nofilter", TextureFlags::NO_FILTERING },
		{ "nomipmap", TextureFlags::NO_MIPMAP },
		{ "clamps", TextureFlags::CLAMP_S },
		{ "clampt", TextureFlags::CLAMP_T },
		{ "clampr", TextureFlags::CLAMP_R },
	};
	static int associationCount = ARRAYSIZE(s_MetaDataAssociation);

	for (int i = 0; i < associationCount; i++)
	{
		const MetaDataAssociation &a = s_MetaDataAssociation[i];

		rapidxml::xml_node<> *node = doc.GetDocumentRoot()->first_node(a.xmlname);

		if (node == nullptr)
			continue;

		const char *value = node->value();
		if (value != nullptr && atoi(value) != 0)
			flags |= a.flag;
	}
}