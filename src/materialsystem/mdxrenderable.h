#ifndef MDXRENDERABLE_H
#define MDXRENDERABLE_H

#include "materialsystem\imdxrenderable.h"
#include "shareddefs.h"

class IMDXModel;
class MDXRenderableGeometry;
class IMaterial;

class PRE_ALIGN_16 MDXRenderable : public IMDXRenderable
{
public:
	MDXRenderable();
	~MDXRenderable();

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW()

	void Init(IMDXModel *modelData, MDXRenderableGeometry *geometry);

	//IMDXRenderable
	virtual void Release();

	virtual void Draw();
	virtual void DrawSkeleton();

	virtual void InvalidateBoneCache();

	virtual void SetSequence(const char *name);
	virtual void SetSequence(int index);
	virtual int GetSequence() const;
	virtual const char *GetSequenceName() const;

	virtual void SetCycle(float cycle);
	virtual float GetCycle() const;

	virtual void SetPlaybackRate(float rate);
	virtual float GetPlaybackRate(float rate) const;

	virtual void AdvanceFrame(float frametime);

	virtual void GetModelExtents(vec3 &min, vec3 &max);

private:
	void InitBones();
	void SetupBonesOnDemand();
	void SetupBones();

	void MakeSkeletonDirty();
	bool IsSkeletonDirty() const;

	IMDXModel *modelData;
	MDXRenderableGeometry *geometry;

	int boneCount;
	mat4 bindPose[MODEL_MAX_BONES];
	mat4 inversionPose[MODEL_MAX_BONES];
	mat4 bones[MODEL_MAX_BONES];

	float cycle;
	int absoluteCycle;
	float playbackRate;
	float sequenceRate;
	int sequence;
	bool isSequenceLooping;
	bool wasSequenceSet;

	bool isSkeletonDirty;

	struct SequenceTransitionLayer
	{
		int sequence;
		bool isLooping;
		float cycle;
		float sequenceRate;
		float weight;
	};
	std::vector<SequenceTransitionLayer> transitions;
	//SequenceTransitionLayer transitionLayers[MODEL_MAX_TRANSITION_LAYERS];

	IMaterial *skeletonMaterial;

} POST_ALIGN_16;


#endif