﻿namespace LevelEditor{
    #region

    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    #endregion

    public class Tile{
        public TileShareholder Diagonal;
        public TileShareholder Left;
        public List<Bitmap>[] Textures = new List<Bitmap>[2];
        public TileType TileType;
        public TileShareholder Top;
        public float Uvx;
        public float Uvy;

        private string combinationMask;
        private List<TileShareholder> tileShareholders;

        public TileFlags Flags { get; set; }

        public string CombinationMask { get { return combinationMask; } }
        public List<TileShareholder> ShareHolder{
            get{
                if (tileShareholders == null){
                    tileShareholders = new List<TileShareholder>();
                    tileShareholders.Add(new TileShareholder(TileEdge.DownRight, this));
                    tileShareholders.Add(Diagonal);
                    tileShareholders.Add(Left);
                    tileShareholders.Add(Top);
                }
                return tileShareholders;
            }
        }

        public Tile(){
            Flags = TileFlags.Buildable | TileFlags.Walkable;
            for (int i = 0; i < TileType.TexTypeCount; i++){
                Textures[i] = new List<Bitmap>();
            }
        }

        public Tile(TileType tileType)
            : this(){
            TileType = tileType;
        }

        public void Recalculate(){
            for (int i = 0; i < TileType.TexTypeCount; i++){
                Textures[i].Clear();
            }

            char[] tileMask = new char[TileManager.Count];
            Dictionary<TileType, List<TileShareholder>> texTileMap = new Dictionary<TileType, List<TileShareholder>>();

            foreach (TileShareholder curTile in ShareHolder){
                List<TileShareholder> sameTexTiles;
                if (!texTileMap.TryGetValue(curTile.Tile.TileType, out sameTexTiles)){
                    sameTexTiles = new List<TileShareholder>();
                    texTileMap.Add(curTile.Tile.TileType, sameTexTiles);
                }
                sameTexTiles.Add(curTile);
            }

            List<List<TileShareholder>> texTileList = texTileMap.Values.ToList();
            texTileList = texTileList.OrderBy(list => list.First().Tile.TileType.Depth).ToList();
            for (int index = 0; index < texTileList.Count; index++){
                List<TileShareholder> shareholders = texTileList[index];
                TileType tileType = shareholders.First().Tile.TileType;
                TileEdge mask;
                if (index == 0){
                    TileShareholder shareholder = shareholders.First();
                    mask = shareholder.Edge;
                    for (int i = 0; i < TileType.TexTypeCount; i++){
                        Textures[i].Add(tileType.GetTexture(i, tileType.FullMask));
                    }
                }
                else{
                    mask = 0;
                    shareholders.ForEach(shareholder => mask |= shareholder.Edge);
                    for (int i = 0; i < TileType.TexTypeCount; i++){
                        Textures[i].Add(tileType.GetTexture(i, mask));
                    }
                }
                tileMask[tileType.Id] = (char)mask;
            }
            combinationMask = new string(tileMask);
        }
    }
}