﻿namespace LevelEditor{
    #region

    using System;

    #endregion

    [Flags]
    public enum TileFlags{
        None = 0,
        Walkable = 1,
        Buildable = 2,
        Spawn = 4,
        End = 8,
		Mountain = 16,
		Water = 32,
    }
}