﻿namespace LevelEditor{
    public enum TileEdge{
        DownRight = 1,
        DownLeft = 2,
        UpRight = 4,
        UpLeft = 8,
    }
}