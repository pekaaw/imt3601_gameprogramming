﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.IO;

    #endregion

    public class MapData{
        public ushort CamBoundsHeight = 0;
        public ushort CamBoundsWidth = 0;
        public List<Entity> Entities = new List<Entity>();
        public float FogAmount; //0-1

        public ushort FogB = 0;
        public ushort FogDistance = 0; //0-90
        public ushort FogG = 0;
        public ushort FogR = 0;
        public ushort FogStart = 0; //0-360
        public ushort GlobalLightB = 0;
        public ushort GlobalLightG = 0;
        public ushort GlobalLightPitch = 0; //0-90
        public ushort GlobalLightR = 0;
        public ushort GlobalLightYaw = 0; //0-360
        public ushort Height = 0;
        public string Name = "Undefined";
        public ushort PauseBetweenWave = 30;

        public TileType DefaultTileType;
        public Tile[,] Tiles;

        public ushort TimePerWave = 60;
        private uint Version = 2;
        public WaveFlag WaveFlags = 0;
        public List<Wave> Waves = new List<Wave>();
        public ushort Width = 0;

        public byte[] Serialize(){
            MemoryStream memStream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memStream);

            writer.Write(Version);
            writer.Write(Name);

            writer.Write(Width);
            writer.Write(Height);

            writer.Write(CamBoundsHeight);
            writer.Write(CamBoundsWidth);

            writer.Write(GlobalLightR);
            writer.Write(GlobalLightG);
            writer.Write(GlobalLightB);
            writer.Write(GlobalLightYaw);
            writer.Write(GlobalLightPitch);

            writer.Write(FogR);
            writer.Write(FogG);
            writer.Write(FogB);
            writer.Write(FogStart);
            writer.Write(FogDistance);
            writer.Write(FogAmount);

            writer.Write(DefaultTileType.Id);
            WriteTiles(writer);

            writer.Write((ushort)Waves.Count);
            writer.Write(TimePerWave);
            writer.Write(PauseBetweenWave);
            writer.Write((byte)WaveFlags);
            WriteWaves(writer);

            WriteEntities(writer);

            return memStream.ToArray();
        }

        private void WriteEntities(BinaryWriter writer){
            writer.Write((ushort)Entities.Count);
            for (int i = 0; i < Entities.Count; i++){
                Entity entity = Entities[i];
                writer.Write(entity.GetType().Name);
            }
        }

        private void WriteWaves(BinaryWriter writer){
            for (int i = 0; i < Waves.Count; i++){
                Wave wave = Waves[i];
                writer.Write((byte)wave.Units.Count);

                for (int j = 0; j < wave.Units.Count; j++){
                    WaveUnit unit = wave.Units[j];
                    writer.Write(unit.Unit.Id);
                    writer.Write(unit.Count);
                }
            }
        }

        private void WriteTiles(BinaryWriter writer){
            for (int y = 0; y < Height; y++){
                for (int x = 0; x < Width; x++){
                    Tile tile = Tiles[x, y];
                    writer.Write(tile.Uvx);
                    writer.Write(tile.Uvy);
                    writer.Write((byte)tile.Flags);
                    writer.Write(tile.TileType.Id);
                }
            }
        }

        private void DeserializeFromArray(byte[] data){
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);

            Version = reader.ReadUInt32();
            Name = reader.ReadString();

            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();

            CamBoundsHeight = reader.ReadUInt16();
            CamBoundsWidth = reader.ReadUInt16();

            GlobalLightR = reader.ReadUInt16();
            GlobalLightG = reader.ReadUInt16();
            GlobalLightB = reader.ReadUInt16();
            GlobalLightYaw = reader.ReadUInt16();
            GlobalLightPitch = reader.ReadUInt16();

            FogR = reader.ReadUInt16();
            FogG = reader.ReadUInt16();
            FogB = reader.ReadUInt16();
            FogStart = reader.ReadUInt16();
            FogDistance = reader.ReadUInt16();
            FogAmount = reader.ReadSingle();

            DefaultTileType = TileManager.Get(reader.ReadByte());

            Tiles = new Tile[Width,Height];
            for (int y = 0; y < Height; y++){
                for (int x = 0; x < Width; x++){
                    Tile tile = new Tile();
                    tile.Uvx = reader.ReadSingle();
                    tile.Uvy = reader.ReadSingle();
                    tile.Flags = (TileFlags)reader.ReadByte();

                    byte tileTypeID = reader.ReadByte();
                    tile.TileType = TileManager.Get(tileTypeID);
                    Tiles[x, y] = tile;
                }
            }

            ushort WaveCount = reader.ReadUInt16();
            TimePerWave = reader.ReadUInt16();
            PauseBetweenWave = reader.ReadUInt16();
            WaveFlags = (WaveFlag)reader.ReadByte();

            for (int i = 0; i < WaveCount; i++){
                Wave wave = new Wave();
                wave.Units = new List<WaveUnit>();
                byte unitCount = reader.ReadByte();
                for (int j = 0; j < unitCount; j++){
                    WaveUnit unit = new WaveUnit();
                    unit.Unit = new Unit();
                    unit.Unit.Id = reader.ReadByte();
                    unit.Count = reader.ReadUInt16();
                    wave.Units.Add(unit);
                }
                Waves.Add(wave);
            }

            ushort entityCount = reader.ReadUInt16();
            for (int i = 0; i < entityCount; i++){
                string className = reader.ReadString();
                Type t = Type.GetType(className);
                Entity entity = (Entity)Activator.CreateInstance(t);
                Entities.Add(entity);
            }
        }

        public static MapData Deserialize(byte[] data){
            MapData mapData = new MapData();
            mapData.DeserializeFromArray(data);

            return mapData;
        }
    }
}