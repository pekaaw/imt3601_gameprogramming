﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using LevelEditor.Properties;

    #endregion

    internal static class TileManager{
        public static readonly Dictionary<string, TileType> Tiles = new Dictionary<string, TileType>();

        public static int Count { get { return Tiles.Count; } }
        public static IEnumerator<TileType> TileList { get { return Tiles.Values.GetEnumerator(); } }
        public static event Action Changed;

        public static bool Add(TileType tileType){
            if (Tiles.ContainsKey(tileType.Name)){
                return false;
            }

            Tiles.Add(tileType.Name, tileType);
            OnChanged();
            Save();
            return true;
        }

        public static bool Contains(string tileName){
            return Tiles.ContainsKey(tileName);
        }

        public static TileType Get(string name){
            if (Tiles.ContainsKey(name)){
                return Tiles[name];
            }
            return null;
        }

        public static TileType Get(byte tileTypeId){
            return Tiles.Values.ToList().Find(type => type.Id == tileTypeId);
        }

        public static TileType GetFirst(){
            if (Tiles.Count == 0) return null;

            return Tiles.First().Value;
        }

        public static void Load(){
            if (!File.Exists(Settings.Default.TileMetaFilePath)){
                return;
            }

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<TileType>));
            FileStream fileStream = File.Open(Settings.Default.TileMetaFilePath, FileMode.Open);
            List<TileType> tileTypeList = (List<TileType>)xmlSerializer.Deserialize(fileStream);
            fileStream.Close();

            foreach (TileType tileType in tileTypeList){
                string path = Settings.Default.TilesTexPath + "//" + tileType.Name + ".png";
                tileType.Load(path);
                Add(tileType);
            }
        }

        public static void Remove(TileType tileType){
            if (!Tiles.ContainsKey(tileType.Name)){
                return;
            }

            Remove(tileType.Name);
            OnChanged();
            Save();
        }

        public static void Remove(string tileName){
            Tiles.Remove(tileName);
        }

        public static void Save(){
            File.Delete(Settings.Default.TileMetaFilePath);

            List<TileType> types = Tiles.Values.ToList();
            if (types.Count == 0) return;

            XmlSerializer xmlSerializer = new XmlSerializer(types.GetType());
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;

            using (XmlWriter writer = XmlWriter.Create(Settings.Default.TileMetaFilePath, settings)){
                xmlSerializer.Serialize(writer, types);
            }
        }

        public static List<TileType> ToList(){
            return Tiles.Values.ToList();
        }

        public static void UpdatedTile(){
            OnChanged();
        }

        private static void OnChanged(){
            Action handler = Changed;
            if (handler != null){
                handler();
            }
        }
    }
}