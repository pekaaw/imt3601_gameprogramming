﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;

    #endregion

    public class Atlas{
        private readonly Graphics[] canvas = new Graphics[TileType.TexTypeCount];
        private readonly Stack<Point> freePositions = new Stack<Point>();
        private readonly Dictionary<string, UniqueTile> uniqueTiles = new Dictionary<string, UniqueTile>();
        public Bitmap[] Bitmap = new Bitmap[TileType.TexTypeCount];
        public int Size;
        public int TileSize;
        public Tile[,] Tiles;
        private Point nextFreePos;
        public float Increment { get { return 1f / MaxTilesPerLine; } }
        private int MaxTilesPerLine { get { return Size / TileSize; } }

        public Atlas(int size, int tileSize, Tile[,] tiles){
            Size = size;
            TileSize = tileSize;

            Tiles = tiles;
            for (int i = 0; i < TileType.TexTypeCount; i++){
                Bitmap[i] = new Bitmap(Size, Size);
                canvas[i] = Graphics.FromImage(Bitmap[i]);
            }

            nextFreePos = new Point(0, 0);

            for (int x = 0; x < tiles.GetLength(0); x++){
                for (int y = 0; y < tiles.GetLength(1); y++){
                    Add(x, y, tiles[x, y]);
                }
            }
        }

        public Atlas(Tile[,] world)
            : this(1024, 64, world){
        }

        public Tile GetTileFromPos(int x, int y){
            if (x < 0 || x >= Tiles.GetLength(0)){
                return null;
            }
            if (y < 0 || y >= Tiles.GetLength(1)){
                return null;
            }

            return Tiles[x, y];
        }

        private Point GetNextFreePos(){
            if (freePositions.Count >= 1){
                return freePositions.Pop();
            }

            Point freePos = nextFreePos;

            nextFreePos.X++;
            if (nextFreePos.X == MaxTilesPerLine){
                nextFreePos.X = 0;
                nextFreePos.Y++;
                if (nextFreePos.Y == MaxTilesPerLine){
                    MessageBox.Show("Atlas exceeded size");
                }
            }

            return freePos;
        }

        public void SaveAs(string path){
            string normalPath = path.Replace(".png", "");
            normalPath += "_normal.png";

            Bitmap[0].Save(path, ImageFormat.Png);
            Bitmap[1].Save(normalPath, ImageFormat.Png);
        }

        public unsafe byte[] GetRGB(int bitmapIndex){
            PixelFormat PixelFormat = PixelFormat.Format24bppRgb;

            BitmapData data = Bitmap[bitmapIndex].LockBits(new Rectangle(0, 0, Size, Size), ImageLockMode.ReadOnly, PixelFormat);
            byte[] rgbData = new Byte[3 * Size * Size];

            try{
                Byte* bb = (Byte*)data.Scan0.ToPointer();

                for (int i = 0; i < Size * Size; i++){
                    int startIndex = i * 3;

                    rgbData[startIndex + 0] = *(bb + startIndex + 2);
                    rgbData[startIndex + 1] = *(bb + startIndex + 1);
                    rgbData[startIndex + 2] = *(bb + startIndex + 0);
                }
            }finally{
                Bitmap[bitmapIndex].UnlockBits(data);
            }

            return rgbData;
        }

        public void Remove(Tile tile){
            if (!uniqueTiles.ContainsKey(tile.CombinationMask)){
                return;
            }

            UniqueTile uniqueTile = uniqueTiles[tile.CombinationMask];
            uniqueTile.Count--;
            if (uniqueTile.Count <= 0){
                freePositions.Push(new Point(uniqueTile.X, uniqueTile.Y));
                uniqueTiles.Remove(tile.CombinationMask);
                for (int x = uniqueTile.X * TileSize; x < (uniqueTile.X * TileSize) + 64; x++){
                    for (int y = uniqueTile.Y * TileSize; y < (uniqueTile.Y * TileSize) + 64; y++){
                        for (int i = 0; i < TileType.TexTypeCount; i++){
                            Bitmap[i].SetPixel(x, y, new Color());
                        }
                    }
                }
            }
        }

        public void Add(int x, int y, Tile tile){
            UniqueTile uniqueTile;
            if (!uniqueTiles.TryGetValue(tile.CombinationMask, out uniqueTile)){
                Point freePos = GetNextFreePos();
                uniqueTile = new UniqueTile(freePos.X, freePos.Y);
                uniqueTiles.Add(tile.CombinationMask, uniqueTile);
                for (int i = 0; i < TileType.TexTypeCount; i++){
                    tile.Textures[i].ForEach(
                        tex => canvas[i].DrawImage(tex, freePos.X * TileSize, freePos.Y * TileSize, TileSize, TileSize));
                }
            }
            uniqueTile.Count++;
            Tiles[x, y].Uvx = uniqueTile.X * Increment;
            Tiles[x, y].Uvy = uniqueTile.Y * Increment;
        }

        private class UniqueTile{
            public readonly int X;
            public readonly int Y;
            public uint Count;

            public UniqueTile(int x, int y){
                X = x;
                Y = y;
            }
        }
    }
}