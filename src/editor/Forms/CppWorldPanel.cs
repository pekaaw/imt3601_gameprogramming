﻿namespace LevelEditor.Forms{
    #region

    using System;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    #endregion

    internal class CppWorldPanel : WorldPanel{
        private readonly TileSelector tileSelector;
        private readonly EditorWrapper wrapper;
        private bool flagBrush = true;
        private World world;

        public CppWorldPanel(TileSelector selector, World world){
            this.world = world;
            tileSelector = selector;
            CheckForIllegalCrossThreadCalls = false;

            wrapper = new EditorWrapper();
            wrapper.TileSelected += wrapper_TileSelected;
            MouseDown += (sender, args) => wrapper.MouseDown(args);
            MouseUp += (sender, args) => wrapper.MouseUp(args);
            KeyDown += (sender, args) => wrapper.OnKeyDown(args);
            KeyDown += CppWorldPanel_KeyDown;
            KeyUp += (sender, args) => wrapper.OnKeyUp(args);
            Resize += CppRenderedPanelResize;
            LostFocus += CppWorldPanel_LostFocus;
            Load += (sender, args) => new Thread(Init).Start();
            Disposed += (sender, args) =>{
                world.Resized -= ReloadMap;
                Shutdown();
            };

            wrapper.Initalized += () => new Thread(() => wrapper.LoadMap(world.Name)).Start();
            world.Resized += ReloadMap;
        }

        private void CppWorldPanel_KeyDown(object sender, KeyEventArgs e){
            if (e.KeyCode == Keys.D){
                flagBrush = false;
            }
            if (e.KeyCode == Keys.E){
                flagBrush = true;
            }
        }

        private void CppWorldPanel_LostFocus(object sender, EventArgs e){
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Up));
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Down));
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Left));
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Right));
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Control));
            wrapper.OnKeyUp(new KeyEventArgs(Keys.Shift));
        }

        private void ReloadMap(){
            wrapper.Lock(true);

            wrapper.LoadMap(world.Name);
            wrapper.ChangeEditMode((int)EditMode); //Reset to reload highligting

            wrapper.Unlock();
        }

        public override void EditModeChanged(){
            wrapper.ChangeEditMode((int)EditMode);
        }

        private void wrapper_TileSelected(int x, int y){
            if (editMode != TileFlags.None){
                Tile tile = world.GetTileFromPos(x, y);

                if (flagBrush){
                    tile.Flags |= editMode;
                }
                else{
                    tile.Flags &= ~editMode;
                }

                wrapper.ChangeTileFlag(x, y, ((tile.Flags & editMode) != 0));
                wrapper.ChangeEditMode((int)editMode); //set editmode again for full redraw of the highlighting

                OnChanged();
            }
        }

        public override void Shutdown(){
            wrapper.Shutdown();
        }

        private void CppRenderedPanelResize(object sender, EventArgs e){
            wrapper.WindowResize(Width, Height);
        }

        public override void SetWorld(World world){
            if (world != null){
                world.Resized -= ReloadMap;
            }
            this.world = world;

            world.Resized += ReloadMap;

            ReloadMap();
        }

        public override unsafe void Init(){
            wrapper.Init(Handle.ToPointer(), Width, Height);
        }

        protected override void OnMouseClick(MouseEventArgs e){
            OnMouseMove(e);
        }

        protected override void OnMouseMove(MouseEventArgs e){
            Point pos = e.Location;

            if (e.Button == MouseButtons.Left){
                UpdateTileAtPos(pos.X, pos.Y);
                if (editMode != TileFlags.None){
                    wrapper.MouseDown(e);
                }
            }
        }

        private void ChangeTileAt(int x, int y){
            Tile tile = world.Atlas.GetTileFromPos(x, y);
            if (tile != null){
                wrapper.ChangeTile(x, y, tile.Uvx, tile.Uvy);
            }
        }

        private void UpdateTileAtPos(int mousex, int mousey){
            if (editMode != TileFlags.None){
                return;
            }

            int tileX = 0, tileY = 0;

            if (wrapper.GetTileAtPos(mousex, mousey, ref tileX, ref tileY)){
                Tile tile = world.GetTileFromPos(tileX, tileY);

                if (tile.TileType != tileSelector.Selection){
                    tile.TileType = tileSelector.Selection;

                    world.RecalculateAt(tileX, tileY);
                    world.RecalculateAt(tileX + 1, tileY);
                    world.RecalculateAt(tileX, tileY + 1);
                    world.RecalculateAt(tileX + 1, tileY + 1);

                    wrapper.Lock(true);
                    ChangeTileAt(tileX, tileY);
                    ChangeTileAt(tileX + 1, tileY);
                    ChangeTileAt(tileX, tileY + 1);
                    ChangeTileAt(tileX + 1, tileY + 1);

                    wrapper.ReloadAtlas(world.Atlas.GetRGB(0), world.Atlas.GetRGB(1),
						world.Atlas.Size, world.Atlas.Size);
                    wrapper.Unlock();

                    OnChanged();
                }
            }
        }

        protected override bool IsInputKey(Keys keyData){
            switch (keyData){
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                    return true;

                case Keys.ShiftKey:
                case Keys.ControlKey:
                    return true;
            }

            return base.IsInputKey(keyData);
        }
    }
}