﻿namespace LevelEditor.Forms{
    #region

    using System;
    using System.IO;
    using System.Windows.Forms;
    using LevelEditor.Properties;

    #endregion

    public partial class CreateLevel : Form{
        public CreateLevel(){
            InitializeComponent();
        }

        private void CreateLevel_Load(object sender, EventArgs e){
        }

        private void bCreate_Click(object sender, EventArgs e){
            if (string.IsNullOrEmpty(tbLevelName.Text)){
                MessageBox.Show("Level name must not be empty");
                return;
            }
            if (File.Exists(Settings.Default.LevelPath + tbLevelName.Text + ".level")){
                MessageBox.Show("File with that name already exists");
                return;
            }

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}