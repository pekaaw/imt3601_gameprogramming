﻿namespace LevelEditor.Forms{
    #region

    using System.Drawing;
    using System.Windows.Forms;

    #endregion

    internal class SharpWorldPanel : WorldPanel{
        private readonly TileSelector tileSelector;
        private World world;

        public SharpWorldPanel(TileSelector selector, World world){
            tileSelector = selector;
            this.world = world;
        }

        protected override void OnMouseMove(MouseEventArgs e){
            Point pos = e.Location;
            pos.X /= 64;
            pos.Y /= 64;
            
            if (e.Button == MouseButtons.Left){
                Tile tile = world.GetTileFromPos(pos.X, pos.Y);
                if (tile == null){
                    return;
                }

                if (tile.TileType != tileSelector.Selection){
                    tile.TileType = tileSelector.Selection;

                    world.RecalculateAt(pos.X, pos.Y);
                    world.RecalculateAt(pos.X + 1, pos.Y);
                    world.RecalculateAt(pos.X, pos.Y + 1);
                    world.RecalculateAt(pos.X + 1, pos.Y + 1);

                    OnChanged();
                    Refresh();
                }
            }
        }

        protected override void OnMouseDown(MouseEventArgs e){
            OnMouseMove(e);
        }

        public override void SetWorld(World world){
            this.world = world;
        }

        public override void Init(){
        }

        protected override void OnPaint(PaintEventArgs e){
            e.Graphics.Clear(Color.Gainsboro);
            
            Tile[,] tiles = world.Tiles;
            for (int x = 0; x < tiles.GetLength(0); x++){
                for (int y = 0; y < tiles.GetLength(1); y++){
                    Tile tile = tiles[x, y];
                    tile.Textures[0].ForEach(bitmap => e.Graphics.DrawImage(bitmap, x * 64, y * 64, 64, 64));
                }
            }
        }
    }
}