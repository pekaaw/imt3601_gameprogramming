﻿namespace LevelEditor.Forms{
    #region

    using System;
    using OpenTK;

    #endregion

    public abstract class WorldPanel : GLControl{
        protected TileFlags editMode;

        public TileFlags EditMode{
            get { return editMode; }
            set{
                if (editMode != value){
                    editMode = value;
                    EditModeChanged();
                }
            }
        }
        public event Action Changed;

        protected virtual void OnChanged(){
            Action handler = Changed;
            if (handler != null){
                handler();
            }
        }

        public abstract void SetWorld(World world);

        public abstract void Init();

        public virtual void Shutdown(){
        }

        public virtual void EditModeChanged(){
        }
    }
}