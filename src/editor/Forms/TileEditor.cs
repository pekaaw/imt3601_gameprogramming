﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using LevelEditor.Properties;

    #endregion

    public partial class TileEditor : Form{
        private TileType selectedTileType;

        public TileEditor(){
            InitializeComponent();
            pTileSelector.SelectionChanged += pTileSelector_SelectionChanged;
        }

        private void SelectionChanged(TileType tileType){
            if (tileType == selectedTileType){
                return;
            }

            selectedTileType = tileType;
            lTileName.Text = selectedTileType.Name;

            //Previews
            pBFull.Image = tileType.GetTexture(0, tileType.FullMask);

            pBUpLeft.Image = tileType.GetTexture(0, TileEdge.UpLeft);
            pBUpRight.Image = tileType.GetTexture(0, TileEdge.UpRight);
            pBDownRight.Image = tileType.GetTexture(0, TileEdge.DownRight);
            pBDownLeft.Image = tileType.GetTexture(0, TileEdge.DownLeft);

            pBDownFull.Image = tileType.GetTexture(0, TileEdge.DownLeft | TileEdge.DownRight);
            pBLeftFull.Image = tileType.GetTexture(0, TileEdge.DownLeft | TileEdge.UpLeft);
            pBRightFull.Image = tileType.GetTexture(0, TileEdge.DownRight | TileEdge.UpRight);
            pBUpFull.Image = tileType.GetTexture(0, TileEdge.UpLeft | TileEdge.UpRight);

            pbDREmpty.Image = tileType.GetTexture(0, TileEdge.DownLeft | TileEdge.UpLeft | TileEdge.UpRight);
            pBUREmpty.Image = tileType.GetTexture(0, TileEdge.UpLeft | TileEdge.DownRight | TileEdge.DownLeft);
            pBULEmpty.Image = tileType.GetTexture(0, TileEdge.DownLeft | TileEdge.DownRight | TileEdge.UpRight);
            pBDLEmpty.Image = tileType.GetTexture(0, TileEdge.DownRight | TileEdge.UpLeft | TileEdge.UpRight);

            nudDepth.Value = selectedTileType.Depth;
        }

        private void TileEditor_Load(object sender, EventArgs e){
            IEnumerator<TileType> enumerator = TileManager.TileList;
            if (enumerator.MoveNext()){
                SelectionChanged(enumerator.Current);
            }
        }

        private void bSave_Click(object sender, EventArgs e){
            TileManager.Add(selectedTileType);
        }

        private void menuButtonImportWC3_Click(object sender, EventArgs e){
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Multiselect = false;
            fileDialog.Filter = "Warcraft3 texture atlas (.png)|*.png";

            DialogResult dialogResult = fileDialog.ShowDialog();
            if (dialogResult != DialogResult.OK){
                return;
            }

            Bitmap atlas = new Bitmap(fileDialog.FileName);
            if (atlas.Width != 512 && atlas.Height != 256){
                MessageBox.Show("Invalid height/width. Import aborted");
                return;
            }

            string tileName = fileDialog.SafeFileName.Split('.')[0];
            if (TileManager.Contains(tileName)){
                MessageBox.Show("A Tile with the name '" + tileName + "' already exists. Import aborted");
                return;
            }

            File.Copy(fileDialog.FileName, Settings.Default.TilesTexPath + "\\" + tileName + ".png", true);
            SelectionChanged(new TileType(atlas, tileName));
            TileManager.Add(selectedTileType);
        }

        private void nudDepth_ValueChanged(object sender, EventArgs e){
            if (selectedTileType.Depth == nudDepth.Value){
                return;
            }

            IEnumerator<TileType> tileList = TileManager.TileList;
            while (tileList.MoveNext()){
                TileType tileType = tileList.Current;
                if (tileType.Depth == nudDepth.Value){
                    MessageBox.Show("Depth value already exists");
                    nudDepth.Value = selectedTileType.Depth;
                    return;
                }
            }

            selectedTileType.Depth = (int)nudDepth.Value;
            TileManager.UpdatedTile();
            TileManager.Save();
        }

        private void pTileSelector_SelectionChanged(TileType obj){
            SelectionChanged(obj);
        }

        protected override void OnFormClosing(FormClosingEventArgs e){
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown){
                return;
            }

            e.Cancel = true;
            Hide();
        }
    }
}