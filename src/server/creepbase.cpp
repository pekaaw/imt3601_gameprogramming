
#include "pch.h"
#include "creepbase.h"
#include "game.h"

REGISTER_SERVERENTITY_CLASS(creep, CreepBase);

IMPLEMENT_NETWORKTABLE_BASE(CreepBase)
	NETWORK_VARIABLE(creepState);
	NETWORK_VARIABLE(scale);
	NETWORK_VARIABLE(maxSpeed);
	NETWORK_VARIABLE(color);
	NETWORK_PROCEDURE(BloodDecal);
END_NETWORKTABLE();

CreepBase::CreepBase()
	: physicsObject(nullptr)
	, callbackEvent(this, &CreepBase::OnEvent)
	, lastTileIndex(-1)
	, delayedRemovalTime(0.0f)
	, pathDistanceMinor(0.0f)
	, pathDistanceMajor(FLT_MAX * 0.5f)
	, gold(0)
{
	scale.Set(1.0f);
	maxSpeed.Set(500.0f);
	color.Set(vec3(1.0f, 1.0f, 1.0f));
	SetHealthMax(12);

	//maxSpeed.Set(200.0f);
	//scale.Set(2.0f);
	//color.Set(vec3(1.0f, 0.1f, 0.0f));
}

CreepBase::~CreepBase()
{
	Assert(physicsObject == nullptr);
}

float CreepBase::GetRemainingDistance() const
{
	return pathDistanceMinor + pathDistanceMajor;
}

void CreepBase::OnSpawn()
{
	BaseClass::OnSpawn();

	SetHealth(GetHealthMax());

	Assert(physicsObject == nullptr);
	physicsObject = physics->CreateDynamicCircle(vec3::Zero(), 16.0f);
	Assert(physicsObject != nullptr);

	physics->SetObjectPosition(physicsObject, GetOrigin());
	physics->ApplyObjectImpulse(physicsObject, GetVelocity());

	events->AddListener("tower_created", &callbackEvent);
}

void CreepBase::OnRelease()
{
	if (lastTileIndex >= 0)
	{
		Game::GetInstance()->ModifyNodeCreepCount(lastTileIndex, -1);
	}

	events->RemoveListener(&callbackEvent);

	if (physicsObject != nullptr)
	{
		physics->DestroyObject(physicsObject);
		physicsObject = nullptr;
	}

	BaseClass::OnRelease();
}

void CreepBase::OnTakeDamage(const DamageInfo &info)
{
	BaseClass::OnTakeDamage(info);

	vec3 delta = GetOrigin() - info.origin;
	delta = GetOrigin() + delta * 0.2f;
	delta.z() = 0.0f;
	BloodDecal(delta);
}

void CreepBase::OnKilled(const DamageInfo &info)
{
	creepState.Set(CreepStates::DEAD);

	if (physicsObject != nullptr)
	{
		physics->SetObjectLinearDamping(physicsObject, 10.0f);
		physics->SetObjectCollisionGroup(physicsObject, 2);
	}

	delayedRemovalTime = 3.0f;

	KeyValues *creepDeathEvent = new KeyValues("creep_died");
	creepDeathEvent->SetInt("tileindex", lastTileIndex);
	creepDeathEvent->SetInt("playerkillerid", info.inflictorNetworkId);
	creepDeathEvent->SetInt("gold", gold);

	events->FireEvent(creepDeathEvent);
}

void CreepBase::SetOrigin(const vec3 &origin)
{
	BaseClass::SetOrigin(origin);

	if (physicsObject != nullptr)
	{
		physics->SetObjectPosition(physicsObject, origin);
	}
}

void CreepBase::SetVelocity(const vec3 &velocity)
{
	BaseClass::SetVelocity(velocity);

	if (physicsObject != nullptr)
	{
		physics->SetObjectVelocity(physicsObject, velocity);
	}
}

void CreepBase::Simulate()
{
	UpdatePhysicsState();

	if (delayedRemovalTime <= 0.0f)
	{
		UpdatePath();

		UpdateVelocity();
	}
	else
	{
		delayedRemovalTime -= globals->GetFrametime();

		if (delayedRemovalTime <= 0.0f)
		{
			Remove();
		}
	}

	UpdateTileChange();
}

void CreepBase::SetScale(float scale)
{
	this->scale.Set(scale);
}

void CreepBase::SetSpeed(float speed)
{
	this->maxSpeed.Set(speed);
}

void CreepBase::SetColor(const vec3 &color)
{
	this->color.Set(color);
}

void CreepBase::UpdatePhysicsState()
{
	vec3 position, velocity;

	physics->GetObjectPosition(physicsObject, position);
	physics->GetObjectVelocity(physicsObject, velocity);

	BaseClass::SetOrigin(position);
	BaseClass::SetVelocity(velocity);
}

void CreepBase::UpdatePath()
{
	if (!pathPoints.empty())
	{
		vec3 delta = pathPoints.front() - GetOrigin();
		const float maxDistance = WORLD_TILE_SCALE * 0.45f;

		if (abs(delta.x()) < maxDistance
			&& abs(delta.y()) < maxDistance)
		{
			pathPoints.erase(pathPoints.begin());
		}

		return;
	}

	IWorld *world = engine->GetWorld();

	// TODO: cache end tiles or get from pathfinder..
	for (int i = 0; i < world->GetTileCount(); i++)
	{
		const WorldTile &tile = world->GetTile(i);

		if ((tile.flags & TileFlags::END) == 0)
		{
			continue;
		}

		int currentTile = world->GetTileIndexAtPosition(GetOrigin());

		if (currentTile < 0)
		{
			currentTile = lastTileIndex;
		}

		if (currentTile == i)
		{
			Remove();
		}
		else if (currentTile >= 0
			&& Game::GetInstance()->ConstructPath(currentTile, i, pathPoints))
		{
			//for (unsigned int i = 0; i < pathPoints.size() - 1; i++)
			//{
			//	const vec3 &start = pathPoints[i];
			//	const vec3 &end = pathPoints[i + 1];

			//	vec3 offset(0.0f, 0.0f, 10.0f);

			//	debugDraw->DrawLine(start + offset, end + offset, vec3(1, 1, 0), 1, 10);
			//	debugDraw->DrawLine(start + offset, start + offset + vec3(0, 0, 50), vec3(1, 0, 1), 1, 10);
			//}

			// remove first point so we don't do backtracking when
			// the path is being updated suddenly
			if (pathPoints.size() > 1)
			{
				pathPoints.erase(pathPoints.begin());
			}

			pathDistanceMajor = 0.0f;
			pathDistanceMinor = 0.0f;
			for (unsigned int p = 1; p < pathPoints.size() - 1; p++)
			{
				pathDistanceMajor += (pathPoints[p] - pathPoints[p + 1]).norm();
			}

			if (!pathPoints.empty())
			{
				pathDistanceMinor = (pathPoints.front() - GetOrigin()).norm();
			}

			creepState.Set(CreepStates::WALKING);
			physics->SetObjectLinearDamping(physicsObject, 1.0f);
		}
		else
		{
			creepState.Set(CreepStates::IDLE);
			physics->SetObjectLinearDamping(physicsObject, 10.0f);
		}

		break;
	}
}

void CreepBase::UpdateVelocity()
{
	if (pathPoints.empty())
	{
		pathDistanceMinor = 0.0f;
		return;
	}

	vec3 delta = pathPoints.front() - GetOrigin();

	pathDistanceMinor = delta.norm();
	delta /= pathDistanceMinor;

	vec3 desiredVelocity = delta * maxSpeed.Get();
	delta = desiredVelocity - GetVelocity();

	physics->ApplyObjectImpulse(physicsObject, delta * globals->GetFrametime());
}

void CreepBase::UpdateTileChange()
{
	int newTileIndex = engine->GetWorld()->GetTileIndexAtPosition(GetOrigin());

	if (newTileIndex == lastTileIndex)
	{
		return;
	}

	if (lastTileIndex >= 0)
	{
		Game::GetInstance()->ModifyNodeCreepCount(lastTileIndex, -1);
	}

	if (newTileIndex >= 0)
	{
		Game::GetInstance()->ModifyNodeCreepCount(newTileIndex, 1);
	}

	lastTileIndex = newTileIndex;
}

PhysicsBody CreepBase::GetPhysicsObject()
{
	return physicsObject;
}

void CreepBase::OnEvent(KeyValues *data)
{
	const char *name = data->GetName();

	if (G_StrEq("tower_created", name))
	{
		// TODO: read tower pos and figure out if our path is affected at all...
		pathPoints.clear();
	}
}