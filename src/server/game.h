#ifndef GAME_H
#define GAME_H

#include "shared/pathfinder.h"

class TowerBase;

struct WaveTestData
{
	WaveTestData()
	{
		spawnTimer = 0.0f;
		spawnedCount = 0;
	}

	float starttime;
	int count;
	float duration;

	float speed;
	int health;
	int gold;
	float scale;
	vec3 color;

	float spawnTimer;
	int spawnedCount;
};

class Game : public IGameSystem
{
	DECLARE_CLASS_NOBASE(Game);
	DECLARE_SINGLETON(Game);
public:
	~Game();

	virtual bool Init();

	virtual void Update(float frametime);

	virtual void OnLevelInit();
	virtual void OnLevelShutdown();

	CON_COMMAND_OBJECT(Game, CommandCreateTower);
	CON_COMMAND_OBJECT(Game, CommandSellTower);
	CON_COMMAND_OBJECT(Game, CommandUpgradeTower);

	bool ConstructPath(int tileStart, int tileGoal, std::vector<vec3> &points);
	void ModifyNodeCreepCount(int tileIndex, int delta);

	void ToggleDrawDeathMap();

	void UnlistTower(TowerBase *tower);

private:

	void DrawDeathMap();

	void OnEvent(KeyValues *data);

	DECLARE_CALLBACK(callbackEvent, KeyValues);

	std::vector<const WorldTile*> startTiles;
	std::vector<const WorldTile*> endTiles;

	std::vector<TowerBase *> towers;

	PathFinder pathFinder;
	float deathDecayTime;
	bool drawDeathMap;

	std::vector<WaveTestData> waveTestData;
	float currentWaveDelay;
};


#endif