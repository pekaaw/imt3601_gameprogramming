
#include "pch.h"
#include "player.h"
#include "game.h"

REGISTER_SERVERENTITY_CLASS(player, Player);

IMPLEMENT_NETWORKTABLE_BASE(Player)
	NETWORK_VARIABLE(gold);
END_NETWORKTABLE();

Player::Player()
{
	gold.Set(250);
}

Player::~Player()
{
}

Uint16 Player::GetGold() const
{
	return gold.Get();
}

void Player::SetGold(Uint16 gold)
{
	this->gold.Set(gold);
}

void Player::AddGold(Uint16 amount)
{
	SetGold(GetGold() + amount);
}

void Player::RemoveGold(Uint16 amount)
{
	int newGold = GetGold() - amount;
	newGold = MAX(0, newGold);
	gold.Set(newGold);
}