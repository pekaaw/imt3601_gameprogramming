#ifndef TOWERBASE_H
#define TOWERBASE_H

class CreepBase;

class TowerBase : public ServerEntity
{
	~TowerBase();

	DECLARE_CLASS(TowerBase, ServerEntity);
	DECLARE_NETWORKTABLE_BASE();
public:
	TowerBase();

	virtual void SetOrigin(const vec3 &origin);

	virtual void Simulate();

	virtual void SetTileIndex(int index);
	virtual int GetTileIndex() const;

	virtual void SetTowerType(int type);

	const ResourceTower *GetTowerScript() { return towerScript; }

protected:
	virtual void OnSpawn();
	virtual void OnRelease();
	virtual void UpdateFire();

	virtual PhysicsBody GetPhysicsObject();

	virtual CreepBase *GetCreepTarget();

private:

	PhysicsBody physicsObject;
	const ResourceTower *towerScript;

	NetworkVariable<Uint8> towerType;

	int tilePosition;

	float fireDelay;
};

#endif