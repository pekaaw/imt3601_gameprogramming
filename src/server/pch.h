#ifndef PCH_H
#define PCH_H

#include "util\platform.h"

#pragma warning( disable: 4996 )
#pragma warning( disable: 4530 )

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//#include <fstream>
//#include <direct.h>
#include <vector>
#include <unordered_map>
#include <stack>
#include <unordered_set>
#include <memory>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"
#include "util\bytebuffer.h"
#include "util\factory.h"

#include <shareddefs.h>

#include "appinterface\iappinterface.h"
#include "engine\iglobals.h"
#include "engine\iengineserver.h"
#include "engine\iworld.h"
#include "cvar\icvarinterface.h"
#include "physics/iphysics.h"
#include "engine/idebugdraw.h"
#include "engine/ievents.h"

#include "shared\damageinfo.h"
#include "shared\buttons.h"
#include "shared\gamesystem.h"
#include "shared\resource.h"
#include "shared\networktable.h"

#include "serverentity.h"
#include "serverentityfactory.h"


extern IGlobals *globals;
extern IEngineServer *engine;
extern IPhysics *physics;
extern IDebugDraw *debugDraw;
extern IEvents *events;



#endif