#include <assert.h>			// assert()
#include <iostream>			// cin, cout

#include "client.h"

#include "messagetypes.h"	// MTYPE_ macros
#include "message.h"		// Message class
#include "util\util.h"
#include "shareddefs.h"

// Constructor
Client::Client()
{
	status = CSTATUS_NOT_CONNECTED;
	threadIsRunning = false;
	timeoutTime = 2000;
	packetBuffer = new Message();
}

Client::~Client()
{
	Disconnect();

	delete packetBuffer;
}

// Methods: Connection
bool Client::Connect(IPaddress address, bool block)
{
	// Open socket
	sendSocket = SDLNet_UDP_Open(0);
	if (!sendSocket)
	{
		status = CSTATUS_OPEN_SOCKET_FAILED;
		return false;
	}

	// Try allocating the packet-buffer for sending
	sendPacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE);
	if (!sendPacket)
	{
		status = CSTATUS_PACKET_ALLOC_FAILED;
		return false;
	}

	sendPacket->address = address;
	ipAddress = address;

	// Try allocating the oen for receiving
	receivePacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE);
	if (!receivePacket)
	{
		SDLNet_FreePacket(sendPacket);
		status = CSTATUS_PACKET_ALLOC_FAILED;
		return false;
	}

	if ((address.host == 0) && (address.port == 0))
	{
		status = CSTATUS_CONNECTIONLESS;
		threadIsRunning = true;
		thread = new std::thread(&Client::_WorkerThreadFunction, this);
		return true;
	}

	// Set the send-packet's address
	connectionIDentifier = rand();

	// Construct a raw connection emssage
	Message *initMessage = new Message();
	initMessage->WriteUint16(0U);
	initMessage->WriteUint8(1U);
	initMessage->WriteUint8(MTYPE_CONNECTION_INIT);
	initMessage->WriteUint16(connectionIDentifier);

	// Send it
	Networking::SendUdpPacket(sendSocket, ipAddress, sendPacket, initMessage);
	initMessage->TryKill();

	// Set the status
	connectionStartTime = time(NULL);
	status = CSTATUS_PENDING;

	threadIsRunning = true;
	thread = new std::thread(&Client::_WorkerThreadFunction, this);

	// Block until connect
	if (block)
	{
		while (status == CSTATUS_PENDING)
		{
			if ((time(0) - connectionStartTime) > timeoutTime)
			{
				connectionMutex.lock();
				status = CSTATUS_TIMEOUT;
				connectionMutex.unlock();

				Disconnect();

				return false;
			}

			Sleep(5);
		}
	}

	return true;
}

bool Client::Connect(const char *host, unsigned short portNumber, bool block)
{
	if (SDLNet_ResolveHost(&ipAddress, host, 0) == 0)
	{
		SDLNet_Write16(portNumber, &ipAddress.port);
		return Connect(ipAddress, block);
	}
	else
	{
		status = CSTATUS_COULDNT_RESOLVE_HOST;
		return false;
	}
}

void Client::Disconnect()
{
	if (sendSocket == nullptr)
	{
		return;
	}

	// Make disconnection notice
	Message *disconnectMessage = new Message();
	disconnectMessage->WriteUint16(0U);
	disconnectMessage->WriteUint8(1U);
	disconnectMessage->WriteUint8(MTYPE_DISCONNECTION);
	Networking::SendUdpPacket(sendSocket, ipAddress, sendPacket, disconnectMessage);

	disconnectMessage->TryKill();

	// Signal the thread to stop its loop
	threadIsRunning = false;
	thread->join();

	delete thread;

	// Close the socket
	SDLNet_UDP_Close(sendSocket);

	// Try deleting any incoming and outgoing messages
	for (inQueue.begin(); !inQueue.end(); inQueue.next())
	{
		inQueue.getCurrent()->TryKill();
	}
	for (outQueue.begin(); !outQueue.end(); outQueue.next())
	{
		outQueue.getCurrent()->second->TryKill();
		delete outQueue.getCurrent();
	}

	inQueue.clear();
	outQueue.clear();

	SDLNet_FreePacket(sendPacket);
	SDLNet_FreePacket(receivePacket);

	sendSocket = nullptr;

	if (status == CSTATUS_CONNECTED)
	{
		status = CSTATUS_NOT_CONNECTED;
	}
}

ConnectionStatus Client::GetConnectionStatus()
{
	if (status == CSTATUS_PENDING)
	{
		if ((time(0) - connectionStartTime) > timeoutTime)
		{
			connectionMutex.lock();
			status = CSTATUS_TIMEOUT;
			connectionMutex.unlock();

			Disconnect();
		}
	}

	return status;
}

bool Client::IsConnected()
{
	return(GetConnectionStatus() == CSTATUS_CONNECTED);
}

// Methods: Connectionless
bool Client::SetupConnectionless()
{
	static IPaddress noAddress = { 0U, 0U };

	return Connect(noAddress, false);
}

bool Client::SendConnectionless(IPaddress target, Message *message)
{
	Message packetWrapper(message->GetSize() + 10U);

	// Construct header
	packetWrapper.WriteUint16(0U);
	packetWrapper.WriteUint8(1U);
	packetWrapper.WriteUint8(0U);
	packetWrapper.WriteUint16(0U);
	packetWrapper.WriteUint16(message->GetSize());

	// Copy message
	packetWrapper.WriteBytes(message->GetData(), message->GetSize());

	bool res = Networking::SendUdpPacket(sendSocket, target, sendPacket, &packetWrapper);

	return(res);
}

bool Client::SendConnectionless(const char *targetHost, unsigned short targetPort, Message *message)
{
	IPaddress addr;

	if (SDLNet_ResolveHost(&addr, targetHost, 0) == 0)
	{
		SDLNet_Write16(targetPort, &addr.port);
		return SendConnectionless(addr, message);
	}
	else
	{
		return false;
	}
}

// Methods: Settings
void Client::SetTimeoutThreshold(int milliseconds)
{
	timeoutTime = milliseconds;
}

bool Client::ResizePacketBuffer(size_t newSize)
{
	connectionMutex.lock();
	int result = SDLNet_ResizePacket(sendPacket, newSize);
	connectionMutex.unlock();

	return(result == newSize);
}

// Methods: Interface specific overlaods
void Client::Release()
{
	delete this;
}

Byte *Client::HasNewMessage(Uint32 &sizeOutput)
{
	Message *newMessage = Connection::HasNewMessage();

	if (newMessage != nullptr)
	{
		packetBuffer->OverwriteData(newMessage->GetData(), newMessage->GetSize());

		// Return
		sizeOutput = packetBuffer->GetSize();
		return (Byte *)packetBuffer->GetData();
	}
	else
	{
		return nullptr;
	}
}

void Client::QueueForSending(Byte *data, size_t size, bool reliable)
{
	Connection::QueueForSending(new Message((char *)data, size), reliable ? SYNCMODE_RELIABLE_NOORDER : SYNCMODE_UNRELIABLE_NOORDER);
}

bool Client::Connect(Uint32 address, Uint16 port, bool block)
{
	IPaddress addr;

	addr.host = address;
	addr.port = port;

	return Connect(addr, block);
}

bool Client::SendConnectionless(Uint32 targetAddress, Uint16 targetPort, Byte *data, Uint32 size)
{
	IPaddress addr;

	addr.host = targetAddress;
	addr.port = targetPort;

	return Networking::SendUdpPacket(sendSocket, addr, sendPacket, data, size);
}

bool Client::HasTimedOut()
{
	if ((time(0) - lastHeardFrom) > timeoutTime)
		return true;
	else
		return false;
}

void Client::Ping()
{
	QueuePingMessage();
	SendMessages();
}

// Private methods
void Client::_WorkerThreadFunction()
{
	// Variables: Thread function content
	char *tempBuffer = new char[NETWORKING_MAX_PACKET_SIZE];	// For reads to not cause thread issues with Message's default buffer
	Message packetWrapper(NETWORKING_MAX_PACKET_SIZE);		// For using the Message class methods for reading packet data.
	Message response(16);				// For connection responses

	// Variables: Pointers to thread data, to make code easier
	UDPpacket *responsePacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE);	// For respones, and to not interfere with regular sending.
	UDPsocket socket = sendSocket;

	SDLNet_SocketSet set = SDLNet_AllocSocketSet(1);
	SDLNet_UDP_AddSocket(set, socket);

	assert(responsePacket != nullptr);

	// Variables: For reading packets
	unsigned __int16 lastAck;
	unsigned __int32 ackBitField;
	unsigned __int8 messageCount;
	unsigned __int8 messageType;
	unsigned __int16 messageLength;
	unsigned __int16 sequenceNumber;

	// Main loop
	while (threadIsRunning)
	{
		// Reset bit field, as it's not in all packets
		ackBitField = 0U;

		SDLNet_CheckSockets(set, 20);

		// Check incomming packets
		//connectionMutex.lock();
		int recvNumber = SDLNet_UDP_Recv(socket, receivePacket);
		//connectionMutex.unlock();

		if (recvNumber > 0)
		{
			// Take the data
			packetWrapper.OverwriteData((char *)receivePacket->data, receivePacket->len);

			// Read ack data
			lastAck = packetWrapper.ReadUint16(tempBuffer);
			if (lastAck != 0U)
			{
				ackBitField = packetWrapper.ReadUint32(tempBuffer);
			}
			// TODO: Use ack data

			// Read messages
			messageCount = packetWrapper.ReadUint8();
			for (Uint32 i = 0; i < messageCount; i++)
			{
				messageType = packetWrapper.ReadUint8();

				switch (messageType)
				{
				case MTYPE_NORMAL:
				case MTYPE_RELIABLE:					// This is the bulk of the communication - might split them up when doing acks
					{
						sequenceNumber = packetWrapper.ReadInt16(tempBuffer);
						messageLength = packetWrapper.ReadUint16(tempBuffer);
						_PassMessage(messageType, new Message(packetWrapper.ReadBytes(messageLength, tempBuffer), messageLength), sequenceNumber);

						break;	
					}
				case MTYPE_CONNECTION_REJECT:
					{
						if (status != CSTATUS_CONNECTIONLESS)
						{
							status = CSTATUS_REJECTED;
						}
						break;
					}
				case MTYPE_CONNECTION_ACCEPT:
					{
						if (status != CSTATUS_CONNECTIONLESS)
						{
							response.ResetWriteCursor();
							response.WriteUint16(0U);
							response.WriteUint8(1U);
							response.WriteUint8(MYTPE_CONNECTION_ACK);
							Networking::SendUdpPacket(socket, receivePacket->address, responsePacket, &response);

							status = CSTATUS_CONNECTED;
						}

						break;
					}
				case MTYPE_PING:	// Immediate response seperate from other communication
					{
						// Reset response and construct header
						response.ResetWriteCursor();
						response.WriteUint16(0U);
						response.WriteUint8(1U);
						response.WriteUint8(MTYPE_PONG);

						// Write the PING identifier
						response.WriteUint16(packetWrapper.ReadUint16());

						// Send it right back
						Networking::SendUdpPacket(socket, receivePacket->address, responsePacket, &response);

						break;
					}
				case MTYPE_PONG:
					{
						_PassPingResponse(time(0), packetWrapper.ReadUint16());
						break;
					}
				case MTYPE_DISCONNECTION:
					{
						status = CSTATUS_DROPPED;
						break;
					}
				}
			}
		}
		else if (recvNumber == -1)
		{
			// TODO: Error handling.
		}

	}

	// Free up data.
	SDLNet_FreeSocketSet(set);
	SDLNet_FreePacket(responsePacket);
	delete[] tempBuffer;
	// Messages and buffers will be deleted.
}
