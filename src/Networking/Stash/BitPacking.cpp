#include "BitPacking.h"
#include <iostream>

void initiliazeBitPacking()
{
	if (masks == nullptr) {
		// Make the masks, and set the first to 0
		masks = new	unsigned long long[65];
		masks[0] = 0;

		// Loop, fill the array
		for (int i = 1; i < 65; i++) {
			// Take the previous value
			masks[i] = masks[i - 1];

			// Shift it to the left, and set the least significant bit to 1
			masks[i] <<= 1; // Ex: ...0001 -> ...0010
			masks[i] |= 1;  // Ex: ...0010 -> ...0011
		}
	}
}

unsigned long long* masks = nullptr;