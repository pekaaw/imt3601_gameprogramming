#include "Message.h"
#include <assert.h>

//// Includes
#include <iostream>
#include "shareddefs.h"

using namespace std;

//// Constructor/Destructor
Message::Message(size_t startSize)
{
	// Allocate data
	data = new char[startSize];
	capacity = startSize;

	// Set cursor and size
	readCursor = 0;
	writeCursor = 0;
	currentSize = 0;

	lives = 1;
}

Message::Message(char *data, size_t size)
{
	this->data = new char[size];
	memcpy(this->data, data, size);

	capacity = size;
	currentSize = size;
	readCursor = 0;
	writeCursor = size;
	lives = 1;
}

Message::~Message()
{
	delete [] data;
}

//// Functions
// Cursor Functions
void Message::SetReadCursor(size_t pos)
{
	assert(pos < capacity);
	this->readCursor = pos;
}

void Message::ResetReadCursor()
{
	this->readCursor = 0;
}

size_t Message::GetReadCursor()
{
	return this->readCursor;
}

void Message::ResetWriteCursor()
{
	this->writeCursor = 0;
	this->currentSize = 0;
}

// Basic Read/Write
char *Message::ReadBytes(size_t size, char *buffer)
{
	assert(buffer != nullptr);
	assert(size > 0);

	memcpy(buffer, data + readCursor, size);
	readCursor += size;

	return buffer;
}

void Message::WriteBytes(char *bytes, size_t size)
{
	assert(bytes != nullptr);
	assert(size > 0);

	// Check if there's a need to reallocate
	if (writeCursor + size >= capacity)
	{
		_ReAllocate(capacity * 2);
	}

	// Write
	memcpy(data + writeCursor, bytes, size);

	// Move the cursor
	writeCursor += size;
	if (writeCursor >= currentSize)
	{
		currentSize = writeCursor;
	}
}

// Specific Write
void Message::WriteInt8(char value)
{
	if (writeCursor + 1 >= capacity)
	{
		_ReAllocate(capacity * 2);
	}

	data[writeCursor++] = value;

	if (writeCursor >= currentSize)
	{
		currentSize = writeCursor;
	}
}

void Message::WriteUint8(unsigned char value)
{
	if (writeCursor + 1 >= capacity)
	{
		_ReAllocate(capacity * 2);
	}

	((unsigned char *)data)[writeCursor++] = value;

	if (writeCursor >= currentSize)
	{
		currentSize = writeCursor;
	}
}

void Message::WriteInt16(short value)
{
	_TemplateWrite<short>(value);
}

void Message::WriteUint16(unsigned short value)
{
	_TemplateWrite<unsigned short>(value);
}

void Message::WriteInt32(long value)
{
	_TemplateWrite<long>(value);
}

void Message::WriteUint32(unsigned long value)
{
	_TemplateWrite<unsigned long>(value);
}

void Message::WriteInt64(long long value)
{
	_TemplateWrite<long long>(value);
}

void Message::WriteUint64(unsigned long long value)
{
	_TemplateWrite<unsigned long long>(value);
}

// Specific Read
char Message::ReadInt8()
{
	return data[readCursor++];
}

unsigned char Message::ReadUint8()
{
	return *(unsigned char *)(data + (readCursor++));
}

short Message::ReadInt16(char *buffer)
{
	return _TemplateRead<short>(buffer);
}

unsigned short Message::ReadUint16(char *buffer)
{
	return _TemplateRead<unsigned short>(buffer);
}

long Message::ReadInt32(char *buffer)
{
	return _TemplateRead<long>(buffer);
}

unsigned long Message::ReadUint32(char *buffer)
{
	return _TemplateRead<unsigned long>(buffer);
}

long long Message::ReadInt64(char *buffer)
{
	return _TemplateRead<long long>(buffer);
}

unsigned long long Message::ReadUint64(char *buffer)
{
	return _TemplateRead<unsigned long long>(buffer);
}

// Strings (16-bit prefix)
char *Message::ReadString(unsigned short *lengthOutput, char *buffer)
{
	assert(buffer != nullptr);

	unsigned short length = ReadUint16(buffer);
	return ReadBytes(length, buffer);

	if (lengthOutput)
	{
		*lengthOutput = length;
	}
}

void Message::WriteString(char *str)
{
	assert(str != nullptr);

	unsigned short length = strlen(str) + 1;
	WriteUint16(length);
	WriteBytes(str, length);
}

// Over-writing (For reuse of Message-objects to save allocations)
void Message::OverwriteData(char *source, size_t size)
{
	// Check for need of reallocation
	if (size > capacity)
	{
		_ReAllocate(size);
	}

	// Move the cursors and set size
	currentSize = size;
	writeCursor = size;
	readCursor = 0;

	// Copy the memory to it
	memcpy(data, source, size);
}

void Message::OverwritePart(char *source, size_t index, size_t size)
{
	if (index + size > capacity)
	{
		_ReAllocate(index + size + 64);
	}

	memcpy(data + index, source, size);
}

// Export-related functions
size_t Message::GetSize() const
{
	return currentSize;
}

char *Message::CopyTo(char *buffer)
{
	memcpy(buffer, data, currentSize);
	return buffer;
}

char *Message::GetData() const
{
	return data;
}

// Safe deletion related functions
void Message::AddLives(size_t amount)
{
	lives += amount;
}

void Message::AddLife()
{
	lives++;
}

bool Message::TryKill()
{
	// If it's on the last life.
	if (--lives <= 0)
	{
		delete this;
		return true;
	}

	// If not...
	return false;
}

//// Private methods
// Re-allocation
void Message::_ReAllocate(size_t newSize)
{
	// Do some debug assertions
	assert(data != nullptr);	// That there's data to reallocate
	assert(newSize > capacity);	// That it's not a shrinking

	// Allocate new memory
	char *newData = new char[newSize];

	// Copy the old data over
	memcpy(newData, data, capacity);

	// Delete the old data
	delete[] data;

	// Take the new data
	data = newData;
	capacity = newSize;
}

//// Static definition
char Message::defaultBuffer[NETWORKING_MAX_PACKET_SIZE];
