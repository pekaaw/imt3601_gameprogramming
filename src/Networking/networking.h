#ifndef NETWORKING_H
#define NETWORKING_H

#include "networking\inetworking.h"
#include "util\macros.h"
#include "util/Singleton.h"

#include <winsock.h>
#include <SDL_net.h>
#include <mutex>

// Prototypes
class Message;

class Networking : public INetworking
{
	DECLARE_SINGLETON(Networking);

private:

	// Static members
	static std::mutex sendMutex;

public:
	bool Init();
	void Shutdown();

	INetworkServer *CreateServer(Uint16 port);
	INetworkClient *CreateClient();

	unsigned short GetNetworkOrderShort(unsigned short value);
	unsigned int GetNetworkOrderUInt(unsigned int value);

	static bool SendUdpPacket(UDPsocket socket, IPaddress destination, UDPpacket *packet, Message *message);
	static bool SendUdpPacket(UDPsocket socket, IPaddress destination, UDPpacket *packet, Byte *data, Uint32 size);
};
#endif
