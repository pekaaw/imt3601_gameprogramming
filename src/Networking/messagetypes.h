/*
 * Here are the IDs used in the messages,
 * attempted ordered by how common they are
 *
 *  Normal messages
 *  0x00 - Normal message
 *      (User Data)
 *  0x01 - Guaranteed message
 *      (User Data)
 *
 *  Keep-alive and to measure RTT
 *  0x02 - Ping
 *      Int16 ID
 *  0x03 - Pong
 *      Int16 ID
 *
 *  Connection three-way handshake
 *  0x04 - Connection Init
 *      Int16 identifier
 *  0x05 - Connection Accept
 *      Int16 identifier
 *  0x06 - Connection Reject
 *      Int16 identifier
 *  0x07 - Connection Acknowledge
 *      Int16 identifier
 *
 *  Clean disconnection
 *  0x08 - Disconnect
 */

#ifndef MTYPE_NORMAL
#define MTYPE_NORMAL					0x00U
#define MTYPE_RELIABLE				0x01U
#define MTYPE_PING					0x02U
#define MTYPE_PONG					0x03U
#define MTYPE_CONNECTION_INIT		0x04U
#define MTYPE_CONNECTION_ACCEPT		0x05U
#define MTYPE_CONNECTION_REJECT		0x06U
#define MYTPE_CONNECTION_ACK			0x07U
#define MTYPE_DISCONNECTION			0x08U
#endif
