#ifndef SERVER_H
#define SERVER_H

// Inclsion				-- Reasons for inclusion --
#include "linkedlist.h"	// List<T>
#include <thread>		// thread
#include <vector>		// connection list

#include "Networking.h"	// SyncMode, Netwok::SendUdpPacket()
#include "networking\iserver.h" // INetworkServer

// Prototypes
class Message;
class Connection;

// Main Server class
class Server : public INetworkServer
{
private:
	// Data
	std::vector<Connection *> clients;	// Conencted clients
	Message* packetBuffer;

	// Settings
	unsigned short port;	
	int timeOut;
	bool started;
	bool rejectConenctions;
	bool allowConnectionless;

	// Thread data
	std::thread *thread;			// Thread
	std::mutex mutex;				// Keeps the shared data safe
	std::mutex contentMutex;		// For altering the two lists, and passing messages
	bool running;					// Variable to signal the thread whether it is still to run

	// Lists
	List<int> newConnections;
	List<int> newDisconnections;
	List<std::pair<IPaddress, Message*>> newConnectionlessMessages;

	// SDL_Net specific data
	UDPsocket socket;
	UDPpacket *receivePacket;
	UDPpacket *sendPacket;

	// Private methods
	Connection *_GetByIP(const IPaddress ip) const;
	void _AddMessage(unsigned char type, const IPaddress address, Message *message, unsigned short sequenceNumber);
	void _AddConnection(const IPaddress ip);
	void _AddNewDisconnection(const IPaddress ip);
	void _AddPingResponse(IPaddress ip, time_t timeStamp, unsigned __int16 identifier);
	void _WorkerThreadFunction();

public:
	Server(unsigned short port);
	~Server();
	// Put in interface CreateSever, Networking
	bool Start();	// Opens socket, allocates buffers, and starts listening for packets
	void Stop();	// Notifies the clients, frees up memory and closes the socket.
	void Release(); // INTERFACE

	// Limits
	void SetRejectionMode(bool enable);
	void SetConnectionlessMode(bool enable);
	void SetTimeOutThreshold(int milliseconds);

	// Send methods
	void QueueForSending(Uint32 connectionID, Message *message, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER);
	void QueueForSendingToAll(Message *message, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER, int rttFilter = 20000);
	void QueueForSending(Uint32 connectionID, Byte *data, size_t size, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER);
	void QueueForSendingToAll(Byte *data, size_t size, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER, int rttFilter = 20000);
	bool SendQueuedMessages();
	
	// Ping methods
	void QueuePingMessage(Uint32 connectionID);
	void QueuePingMessageToAll();

	// Interactions with individual clients (RECOMMENDED)
	float GetRTT(Uint32 connectionID);
	bool HasTimedOut(Uint32 connectionID);
	bool HasDisconnected(Uint32 connectionID, bool includeTimeOut = true);
	Byte *HasNewMessage(Uint32 connectionID, Uint32& sizeOutput);
	Byte *HasNewMessage(int& clientReferenceOutput, Uint32& sizeOutput, Uint32& addressOutput, Uint16& portOutput);
	Message *PollMessage(Uint32 connectionID);
	void Drop(Uint32 connectionID);

	// Interactions with all clients.
	bool HasNewConnection(int &connectionIDOutput);
	bool HasNewDisconnection(int &connectionIDOutput, bool includeTimeOut = true);
	Message* HasNewConnectionlessMessage(IPaddress& outputAddress);
	
	// Connectionless interaction
	bool SendConnectionlessMessage(Uint32 address, Uint16 port, Byte *data, size_t size);
	Byte *HasNewConnectionlessMessage(Uint32& outputAddress, Uint16& outputPort, Uint32& outputSize);
};

// Data for keeping track of new connection handshakes
struct NewConenction
{
	IPaddress	ip;
	short		magicNumber;
	bool		accepted;
};
#endif
