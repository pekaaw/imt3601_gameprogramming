
#include "pch.h"
#include "guimain.h"

IRenderContext *renderContext;
ITextureDict *textureDict;
IMaterialDict *materialDict;
IGlobals *globals;

SINGLETON_INSTANCE(GUIMain);

EXPOSE_APP(INTERFACE_IGUI_VERSION, GUIMain::GetInstance());

GUIMain::GUIMain()
{
}

GUIMain::~GUIMain()
{
}

void GUIMain::Init()
{
#ifdef DEBUG
	Assert(TTF_Init() == 0);
#else
	TTF_Init();
#endif

	renderContext = (IRenderContext *)GetAppInterface()->QueryApp(INTERFACE_IRENDERCONTEXT_VERSION);
	textureDict = (ITextureDict *)GetAppInterface()->QueryApp(INTERFACE_ITEXTUREDICT_VERSION);
	materialDict = (IMaterialDict *)GetAppInterface()->QueryApp(INTERFACE_IMATERIALDICT_VERSION);
	globals = (IGlobals *)GetAppInterface()->QueryApp(INTERFACE_IGLOBALS_GUI_VERSION);

	Assert(renderContext != nullptr);
	Assert(textureDict != nullptr);
	Assert(materialDict != nullptr);
	Assert(globals != nullptr);

	FontManager::GetInstance()->Init();
	Graphics::GetInstance()->Init();
}

void GUIMain::Shutdown()
{
	Graphics::GetInstance()->Shutdown();
	FontManager::GetInstance()->Shutdown();
}

void GUIMain::Resize(int width, int height)
{
	FontManager::GetInstance()->OnResize();
}