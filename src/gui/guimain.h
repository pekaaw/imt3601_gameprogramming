#ifndef GUIMAIN_H
#define GUIMAIN_H

#include "gui/igui.h"

class GUIMain : public IGUI
{
	DECLARE_SINGLETON(GUIMain);

public:
	~GUIMain();

	virtual void Init();
	virtual void Shutdown();

	virtual void Resize(int width, int height);
};


#endif