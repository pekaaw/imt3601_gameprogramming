#ifndef GRAPHICS_H
#define GRAPHICS_H


#include "gui/igraphics.h"

class PRE_ALIGN_16 Graphics : public IGraphics
{
	DECLARE_SINGLETON(Graphics);
public:
	~Graphics();

	void Init();
	void Shutdown();

	// IGraphics
	virtual int GetValueScaled(int value);
	virtual int GetValueNormalized(int value);

	virtual float GetFrametime();
	virtual float GetTime();

	virtual void Push2DViewport(int x, int y, int w, int h);
	virtual void Pop2DViewport();

	virtual void SetDrawColor(float r, float g, float b, float a = 1.0f);
	virtual void SetDrawLineWidth(float width);

	virtual void DrawFilledRect(int x0, int y0, int x1, int y1);
	virtual void DrawTexturedRect(int x0, int y0, int x1, int y1, IMaterial *mat);
	virtual void DrawRect(int x0, int y0, int x1, int y1);
	virtual void DrawLine(int x0, int y0, int x1, int y1);

private:

	int viewportDepth;

	IMaterial *flatMaterial;
	vec4 currentColor;
	float currentLineWidth;
}
POST_ALIGN_16;
#endif
