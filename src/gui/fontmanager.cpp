
#include "pch.h"
#include "fontmanager.h"

SINGLETON_INSTANCE(FontManager);

EXPOSE_APP(INTERFACE_IFONTMANAGER_VERSION, FontManager::GetInstance());



FontManager::FontManager()
	: currentFont(FONT_KEY_INVALID)
	, fontMaterial(nullptr)
	, currentPositionX(0)
	, currentPositionY(0)
	, currentColor (vec4::Ones())
	, currentScale(vec2::Ones())
{
}

FontManager::~FontManager()
{
	Assert(fonts.size() == 0);
}

void FontManager::Init()
{
	fontMaterial = materialDict->FindMaterial("gui/font");

	Assert(fontMaterial != nullptr && !fontMaterial->IsErrorMaterial());

	if (fontMaterial != nullptr)
	{
		fontMaterial->IncrementReferenceCount();
	}

	LoadFontScript("font_manifest.xml");
	CreateFontMaps();
}

void FontManager::Shutdown()
{
	ReleaseAll();

	if (fontMaterial != nullptr)
	{
		fontMaterial->DecrementReferenceCount();
	}
}

void FontManager::OnResize()
{
	ReleaseFontMaps();

	textureDict->ReleaseUnreferencedTextures(TextureGroups::FONTS);

	CreateFontMaps();
}

void FontManager::ReleaseAll()
{
	for (auto s : fontScripts)
	{
		delete [] s.name;
		delete [] s.filename;
	}

	fontScripts.clear();

	ReleaseFontMaps();
}

void FontManager::ReleaseFontMaps()
{
	for (auto &s : fontScripts)
	{
		s.fontEntry = FONT_ENTRY_INVALID;
	}

	for (auto f : fonts)
	{
		f.texture->DecrementReferenceCount();

		delete [] f.name;
	}

	fonts.clear();
}

void FontManager::CreateFontMaps()
{
	for (auto &s : fontScripts)
	{
		if (s.fontEntry == FONT_ENTRY_INVALID)
		{
			int tall = s.tall;

			if (s.relative)
			{
				tall = VALUE_SCALED(tall, renderContext->GetScreenHeight());
			}

			s.fontEntry = LoadFont(s.filename, tall);
		}
	}
}

void FontManager::LoadFontScript(const char *filename)
{
	char fullPath[MAX_PATH_GAME];
	G_StrAbsContentPath(ContentDirectories::RESOURCES, filename, fullPath, sizeof(fullPath));
	G_StrFixSlashes(fullPath);

	CXMLDocument doc;

	if (!LoadXMLFile(fullPath, doc))
	{
		Assert(0);
		return;
	}

	rapidxml::xml_node<> *root = doc.GetDocumentRoot();

	for (rapidxml::xml_node<> *fontNode = root->first_node("font", 0U, false);
		fontNode != nullptr;
		fontNode = fontNode->next_sibling("font", 0U, false))
	{
		// require
		rapidxml::xml_node<> *nameNode = fontNode->first_node("name", 0U, false);
		rapidxml::xml_node<> *tallNode = fontNode->first_node("tall", 0U, false);
		rapidxml::xml_node<> *fileNode = fontNode->first_node("file", 0U, false);

		// optional
		rapidxml::xml_node<> *relativeNode = fontNode->first_node("relative", 0U, false);

		if (nameNode == nullptr
			|| tallNode == nullptr
			|| fileNode == nullptr)
		{
			continue;
		}

		const char *name = nameNode->value();
		const char *file = fileNode->value();
		const char *tall = tallNode->value();

		if (!*name
			|| !*file
			|| !*tall)
		{
			continue;
		}

		ScriptEntry entry;
		entry.fontEntry = FONT_ENTRY_INVALID;
		entry.relative = (relativeNode != nullptr) ? atoi(relativeNode->value()) != 0 : false;
		entry.tall = atoi(tall);

		if (entry.tall <= 0)
		{
			continue;
		}

		entry.name = G_StrCreateCopy(name);
		entry.filename = G_StrCreateCopy(file);
		fontScripts.push_back(entry);
	}
}

GUI::FONT_KEY FontManager::LoadFont(const char *filename, int tall)
{
	Assert(filename && *filename);
	Assert(tall > 0 );

	char filenameFixed[MAX_PATH_GAME];
	G_StrAbsContentPath(ContentDirectories::RESOURCES, filename, filenameFixed, sizeof(filenameFixed));
	G_StrFixSlashes(filenameFixed);

	// font already loaded
	for (unsigned int i = 0; i < fonts.size(); i++)
	{
		const FontEntry &font = fonts[i];

		if (G_StrEq(filenameFixed, font.name)
			&& tall == font.tall)
		{
			return i;
		}
	}

	// create new font
	TTF_Font *ttfFont = TTF_OpenFont(filenameFixed, tall);

	// failed to load, use default
	if (ttfFont == nullptr)
	{
		return 0;
	}

	FontEntry entry;

	std::vector<SDL_Surface*> surfaces;

	const int firstCharacter = FIRST_PRINTABLE_CHARACTER;
	const int lastCharacter = 255;
	const int characterCount = lastCharacter - firstCharacter;
	const int lineCount = CHARACTER_MAP_LINE_COUNT;
	const int charactersPerLine = characterCount / (lineCount - 1);

	char characterMap[lineCount][charactersPerLine + 1];
	
	int w = 0, h = 0;

	SDL_Color color;
	color.a = color.r = color.g = color.b = 255;

	for (int i = 0; i < lineCount; i++)
	{
		const int lineFirstCharacter = firstCharacter + i * charactersPerLine;
		const int lineLastCharacter = MIN(256, lineFirstCharacter + charactersPerLine);

		int xPosition = 0;
		int c = 0;

		for (;c < (lineLastCharacter - lineFirstCharacter); c++)
		{
			unsigned char character = lineFirstCharacter + c;
			characterMap[i][c] = character;

			int xmin, xmax, ymin, ymax, advance;

			Assert(character - FIRST_PRINTABLE_CHARACTER < ARRAYSIZE(entry.characters));
			CharacterPosition &characterInfo = entry.characters[character - FIRST_PRINTABLE_CHARACTER];

			if (TTF_GlyphMetrics(ttfFont, character, &xmin, &xmax,
				&ymin, &ymax, &advance) == 0)
			{
				characterInfo.x = xPosition;
				characterInfo.w = advance;
				characterInfo.line = i;

				xPosition += advance;
			}
			else
			{
				characterInfo.x = 0;
				characterInfo.w = 0;
				characterInfo.line = 0;
			}
		}

		characterMap[i][c] = 0;

		SDL_Surface *surface = TTF_RenderText_Blended(ttfFont, characterMap[i], color);

		Assert(surface != nullptr);
		Assert(surface->format->BytesPerPixel == 4);
		//Assert(surface->w == xPosition);

		entry.lines[i].y = h;
		entry.lines[i].h = surface->h;

		surfaces.push_back(surface);

		w = MAX(w, surface->w);
		h += surface->h;
	}

	w = GetNextPowerOfTwo(w);
	h = GetNextPowerOfTwo(h);

	int *data = new int[w * h];

	memset(data, 0, sizeof(int) * w * h);

	SDL_Rect destRect, srcRect;
	destRect.y = destRect.x = 0;
	srcRect.x = srcRect.y = 0;

	destRect.w = w;
	destRect.h = h;

	int yPosition = 0;

	for (int i = 0; i < lineCount; i++)
	{
		const SDL_Surface *surface = surfaces[i];

		srcRect.w = surface->w;
		srcRect.h = surface->h;

		BlitFontMapLine(data, surface->pixels, destRect, srcRect, yPosition);

		yPosition += surface->h;
	}

	char fontTextureName[128];
	G_StrSnPrintf(fontTextureName, sizeof(fontTextureName), "__%s_%i", filename, tall);

	ITexture *texture = textureDict->CreateTexture(fontTextureName, data, w, h,
		TextureFlags::CLAMP_S | TextureFlags::CLAMP_T | TextureFlags::NO_FILTERING | TextureFlags::NO_MIPMAP,
		TextureFormats::BGRA_8, TextureGroups::FONTS);
	texture->IncrementReferenceCount();

	for (auto s : surfaces)
	{
		SDL_FreeSurface(s);
	}

	delete [] data;
	TTF_CloseFont(ttfFont);

	entry.name = G_StrCreateCopy(filenameFixed);
	entry.tall = tall;
	entry.texture = texture;

	fonts.push_back(entry);

	return fonts.size() - 1;
}

GUI::FONT_KEY FontManager::GetFont(const char *name)
{
	for (const auto &s : fontScripts)
	{
		if (G_StrEq(s.name, name))
		{
			return s.fontEntry;
		}
	}

	return FONT_KEY_INVALID;
}

void FontManager::SetDrawTextFont(GUI::FONT_KEY fontKey)
{
	currentFont = fontKey;
}

void FontManager::SetDrawTextColor(float r, float g, float b, float a)
{
	currentColor = vec4(r, g, b, a);
}

void FontManager::SetDrawTextPos(int x, int y)
{
	currentPositionX = x;
	currentPositionY = y;
}

void FontManager::SetDrawTextScale(float scaleWidth, float scaleHeight)
{
	currentScale = vec2(scaleWidth, scaleHeight);
}

int FontManager::GetTextWidth(const char *text)
{
	int length = G_StrLen(text);
	int width = 0;

	const FontEntry &font = fonts[currentFont];

	for (int i = 0; i < length; i++)
	{
		const unsigned char &character = *(text + i);

		if (character < FIRST_PRINTABLE_CHARACTER)
		{
			continue;
		}

		const CharacterPosition &info = font.characters[character - FIRST_PRINTABLE_CHARACTER];

		if (info.w < 1)
		{
			continue;
		}

		width += info.w;
	}

	return width;
}

void FontManager::DrawTextString(const char *text)
{
	Assert(currentFont != FONT_KEY_INVALID);
	Assert(currentFont < int(fonts.size()));
	Assert(fontMaterial != nullptr);

	if (currentFont >= int(fonts.size()))
	{
		return;
	}

	int length = G_StrLen(text);

	if (length == 0)
	{
		return;
	}

	const FontEntry &font = fonts[currentFont];

	static MATERIAL_VAR_HANDLE handleAlbedoTexture = MATERIAL_VAR_HANDLE_DEFAULT;
	IMaterialVar *textureVar = fontMaterial->FindMaterialVarFast(handleAlbedoTexture, "albedotexture");

	textureVar->SetTexture(font.texture);

	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::TRIANGLES,
		VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F | VertexFormat::COLOR_4F,
		2 * length);

	MeshBuilder builder;
	builder.AttachModify(mesh);

	float cx = (float)currentPositionX;
	float cy = (float)currentPositionY;

	const float textureWidth = (float)font.texture->GetWidth();
	const float textureHeight = (float)font.texture->GetHeight();

	for (int i = 0; i < length; i++)
	{
		const unsigned char &character = *(text + i);

		if (character < FIRST_PRINTABLE_CHARACTER)
		{
			continue;
		}

		const CharacterPosition &info = font.characters[character - FIRST_PRINTABLE_CHARACTER];

		if (info.w < 1)
		{
			continue;
		}

		const LinePosition &line = font.lines[info.line];

		float width = info.w * currentScale.x();
		float height = line.h * currentScale.y();

		builder.Position3f(cx, cy, 0.0f);
		builder.Texcoord2f(info.x / textureWidth, line.y/textureWidth);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();
		builder.Position3f(cx, cy + height, 0.0f);
		builder.Texcoord2f(info.x / textureWidth, (line.y + line.h)/textureHeight);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();
		builder.Position3f(cx + width, cy, 0.0f);
		builder.Texcoord2f((info.x + info.w) / textureWidth, line.y/textureWidth);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();
	
		builder.Position3f(cx + width, cy, 0.0f);
		builder.Texcoord2f((info.x + info.w) / textureWidth, line.y/textureWidth);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();
		builder.Position3f(cx, cy + height, 0.0f);
		builder.Texcoord2f(info.x / textureWidth, (line.y + line.h)/textureHeight);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();
		builder.Position3f(cx + width, cy + height, 0.0f);
		builder.Texcoord2f((info.x + info.w) / textureWidth, (line.y + line.h)/textureHeight);
		builder.Color4v(currentColor);
		builder.AdvanceVertex();

		cx += width;
	}

	builder.Detach();

	renderContext->BindMesh(mesh);
	fontMaterial->Draw();

	// need to free reference again
	textureVar->SetTexture(nullptr);
}

void FontManager::BlitFontMapLine(void *dest, void *src,
		SDL_Rect &destDimensions, SDL_Rect &srcDimensions, int yPosition)
{
	Assert(yPosition + srcDimensions.y < destDimensions.h);
	Assert(destDimensions.w >= srcDimensions.w);

	int lineSize = srcDimensions.w * sizeof(int);

	for (int i = 0; i < srcDimensions.h; i++)
	{
		// BGRA expected - 32 bits
		int *intSrc = ((int*)src) + i * srcDimensions.w;
		int *intDest = ((int*)dest) + (i + yPosition) * destDimensions.w;

		memcpy(intDest, intSrc, lineSize);
	}
}