#ifndef PCH_H
#define PCH_H

#include <unordered_map>
#include <vector>
#include <cmath>
#include <math.h>

#include "sdl_ttf.h"

#include "util/singleton.h"
#include "util/stringutil.h"
#include "util/xmlutil.h"
#include "util/mathutil.h"

#include "appinterface/iappinterface.h"
#include "materialsystem/materialsystem.h"
#include "materialsystem/itexture.h"
#include "materialsystem\itexturedict.h"
#include "materialsystem/imaterial.h"
#include "materialsystem/imaterialvar.h"
#include "materialsystem/imaterialdict.h"
#include "materialsystem/irendercontext.h"
#include "materialsystem/meshbuilder.h"
#include "engine/iglobals.h"

#include "gui/gui.h"

#include "fontmanager.h"
#include "graphics.h"

extern IRenderContext *renderContext;
extern ITextureDict *textureDict;
extern IMaterialDict *materialDict;
extern IGlobals *globals;


#endif