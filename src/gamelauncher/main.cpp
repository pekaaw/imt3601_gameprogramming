
#include "util\platform.h"

#ifdef PLATFORM_WIN32
#include <Windows.h>
#endif

#include <string>
#include "util\stringutil.h"
#include "util\launcherutil.h"
#include "launchermain\launchermain.h"


#ifdef PLATFORM_WIN32
int CALLBACK WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance,
					 _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
#else
#error add_entry_point_signature
#endif
{
#ifdef PLATFORM_WIN32
	// force all libraries to be loaded with full path names
	SetDllDirectory("");

	// load all globally required dlls here.
	// SetDllDirectory doesn't work reliably and allows binary planting!
	HMODULE launcherLibraries[launcherLibraryCount];

	{
		char cwd[MAX_PATH_GAME];
		GetCWD(cwd, sizeof(cwd));
		for (int i = 0; i < launcherLibraryCount; i++)
		{
			char libraryPath[MAX_PATH_GAME];
			G_StrSnPrintf(libraryPath, sizeof(libraryPath),
				"%s\\bin\\%s", cwd, launcherLibraryNames[i]);

			launcherLibraries[i] = LoadLibrary(libraryPath);

			if (launcherLibraries[i] == nullptr)
			{
				Assert(0);
				return 1;
			}
		}
	}

	// can't use SDL for library loading yet so use platform specific solution
	HMODULE launcherMainModule = LoadLibrary("bin\\launchermain.dll");
	Assert(launcherMainModule != nullptr);

	if (launcherMainModule == nullptr)
		return 2;

	LauncherMainFunc launcher = (LauncherMainFunc)GetProcAddress(launcherMainModule, "LauncherMain");
	Assert(launcher != nullptr);
	
	if (launcher == nullptr)
		return 3;
#else
#error load_so_dylib_for_posix
#endif

	// enter launcher
	launcher();

#ifdef PLATFORM_WIN32
	FreeLibrary(launcherMainModule);

	for (int i = launcherLibraryCount - 1; i >= 0; i--)
	{
		FreeLibrary(launcherLibraries[i]);
	}
#else
#error unload_libraries_for_posix
#endif

	return 0;
}
