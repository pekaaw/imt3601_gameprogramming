#ifndef EIGEN_INC
#define EIGEN_INC

#include "util\platform.h"
#include <Eigen\Eigen>
#include <Eigen\StdVector>

// shortcuts for vector and matrix classes
typedef Eigen::Vector2f					vec2;
typedef Eigen::Vector3f					vec3;
typedef Eigen::Vector4f					vec4;

typedef Eigen::Matrix2f					mat2;
typedef Eigen::Matrix3f					mat3;
typedef Eigen::Matrix4f					mat4;
typedef Eigen::Matrix<float, 4, 3>		mat4x3;
typedef Eigen::Matrix<float, 3, 4>		mat3x4;

typedef Eigen::Quaternionf				quat;
typedef Eigen::Vector3f					euler;

// M_PI as a float
#define M_PI_F			3.14159265358979323846f

// log 2 bases
#define M_LOG_2E		1.44269504088896340736
#define M_LOG_2EF		1.44269504088896340736f

// log2(x) = log(x) / log(2)
inline long double log2(const long double &x)
{
	return log(x) * M_LOG_2E;
}

inline float log2f(const float x)
{
	return log(x) * M_LOG_2EF;
}

inline float Sign(float value)
{
	return (value >= 0.0f) ? 1.0f : -1.0f;
}

// platform specific align macros
#ifdef PLATFORM_WIN32
#define PRE_ALIGN_16	__declspec(align(16))
#define POST_ALIGN_16
#else
#error do_post_align_for_posix
#endif

//#define FLOAT_EPSILON Eigen::NumTraits<float>::epsilon() //
#define FLOAT_EPSILON    0.0001f

// convert an offset from a vector to a translation matrix
inline mat4 translationMatrix4(const vec3 &offset)
{
	return Eigen::Affine3f(Eigen::Translation3f(offset)).matrix();
}

// translate a matrix by an offset vector
inline mat4 translate(const mat4 &matrix, const vec3 &offset)
{
	return matrix * translationMatrix4(offset);
}

// rotate matrices
template<typename M>
inline M rotate(const M &matrix, float angle, const vec3 &normal)
{
	return matrix * Eigen::Affine3f(Eigen::AngleAxisf(angle, normal)).matrix();
}

template<typename M>
inline M rotate(const M &matrix, const quat &quaternion)
{
	return matrix * Eigen::Affine3f(quaternion.matrix()).matrix();
}

inline mat3 rotate(const mat3 &matrix, float angle, const vec3 &normal)
{
	return matrix * Eigen::AngleAxisf(angle, normal).matrix();
}

inline vec3 rotate(const vec3 &vector, float angle, const vec3 &normal)
{
	mat4 m = mat4::Identity();

	m = rotate(m, angle, normal);
	m = translate(m, vector);
	return vec3(m.col(3).data());
}

// extract rotation information from a 4x4 matrix
inline mat3 rotationMatrix(const mat4 &matrix)
{
	mat3 rot;

	rot.col(0) = vec3(matrix(0, 0), matrix(1, 0), matrix(2, 0));
	rot.col(1) = vec3(matrix(0, 1), matrix(1, 1), matrix(2, 1));
	rot.col(2) = vec3(matrix(0, 2), matrix(1, 2), matrix(2, 2));
	return rot;
}

inline vec3 rotate(const vec3 &vector, const mat4 &matrix)
{
	mat3 rot = rotationMatrix(matrix);

	return rot * vector;
}

template<typename M>
inline M rotate(const M &matrix, const euler &angles)
{
	M m = matrix;

	m = rotate(m, DEG2RAD(angles[2]), vec3::UnitX());
	m = rotate(m, DEG2RAD(angles[0]), vec3::UnitY());
	m = rotate(m, DEG2RAD(angles[1]), vec3::UnitZ());

	//vec3 _fwd = vec3(m(0, 0), m(0, 1), m(0, 2));
	//m = rotate(m, DEG2RAD(angles.z()), _fwd);
	return m;
}

inline quat quaternionFromVector(const vec3 &vector)
{
	return quat().setFromTwoVectors(vector, vec3(1, 0, 0));
}

template<typename M>
inline M scale(const M &matrix, const vec3 &scales)
{
	return matrix * Eigen::Affine3f(Eigen::AlignedScaling3f(scales)).matrix();
}

template<typename M>
inline M scale(const M &matrix, float x, float y, float z)
{
	return matrix * Eigen::Affine3f(Eigen::AlignedScaling3f(x, y, z)).matrix();
}

// create a perspective matrix
inline mat4 perspective(float fov, float ratio, float zNear, float zFar)
{
	float theta = DEG2RAD(fov) * 0.5f;
	float range = zFar - zNear;
	float invtan = 1.0f / tan(theta);

	mat4 matProj = mat4::Identity();

	matProj(0, 0) = invtan / ratio;
	matProj(1, 1) = invtan;
	matProj(2, 2) = -(zNear + zFar) / range;
	matProj(3, 2) = -1;
	matProj(2, 3) = -2 * zNear * zFar / range;
	matProj(3, 3) = 0;

	return matProj;
}

// create an orthogonal matrix
inline mat4 orthographic(float left, float top, float right, float bottom, float zNear, float zFar)
{
	mat4 matProj = mat4::Identity();

	matProj(0, 0) = 2.0f / (right - left);
	matProj(1, 1) = 2.0f / (top - bottom);
	matProj(2, 2) = -2.0f / (zFar - zNear);
	matProj(0, 3) = -(right + left) / (right - left);
	matProj(1, 3) = -(top + bottom) / (top - bottom);
	matProj(2, 3) = -(zFar + zNear) / (zFar - zNear);

	return matProj;
}

// magic matrix used to convert opengl unit cube coords to UVs
// -1 - 1, 1 - -1
// to 0 - 1, 0 - 1
inline mat4 shadowRemapMatrix()
{
	mat4 shadowUVMatrix = mat4::Zero();

	shadowUVMatrix(0, 0) = 0.5f;
	shadowUVMatrix(1, 1) = 0.5f;
	shadowUVMatrix(2, 2) = 0.5f;
	shadowUVMatrix(3, 3) = 1.0f;
	shadowUVMatrix(0, 3) = 0.5f;
	shadowUVMatrix(1, 3) = 0.5f;
	shadowUVMatrix(2, 3) = 0.5f;
	return shadowUVMatrix;
}

inline mat4x3 cast_mat4x3(const mat4 &m)
{
	mat4x3 o;

	o(0) = m(0);
	o(1) = m(1);
	o(2) = m(2);

	o(3) = m(4);
	o(4) = m(5);
	o(5) = m(6);

	o(6) = m(8);
	o(7) = m(9);
	o(8) = m(10);

	o(9) = m(12);
	o(10) = m(13);
	o(11) = m(14);

	return o;
}

inline void AngleVectors(const euler &angles, vec3 *fwd)
{
	Assert(fwd != nullptr);

	mat3 m(mat3::Identity());

	m = rotate(m, DEG2RAD(angles.x()), vec3(0, 1, 0));
	m = rotate(m, DEG2RAD(angles.y()), vec3(0, 0, 1));

	*fwd = vec3(m(0, 0), m(0, 1), m(0, 2));
}

inline void AngleVectors(const euler &angles, vec3 *fwd, vec3 *left, vec3 *up)
{
	mat3 m(mat3::Identity());

	m = rotate(m, DEG2RAD(angles.z()), vec3::UnitX());
	m = rotate(m, DEG2RAD(angles.x()), vec3::UnitY());
	m = rotate(m, DEG2RAD(angles.y()), vec3::UnitZ());

	//vec3 _fwd = vec3(m(0, 0), m(0, 1), m(0, 2));
	//m = rotate(m, DEG2RAD(angles.z()), _fwd);

	if (fwd != nullptr)
	{
		*fwd = vec3(m(0, 0), m(0, 1), m(0, 2));
	}

	if (left != nullptr)
	{
		*left = vec3(m(1, 0), m(1, 1), m(1, 2));
	}

	if (up != nullptr)
	{
		*up = vec3(m(2, 0), m(2, 1), m(2, 2));
	}
}

inline float ClampAngle(float value)
{
	if ((value > -180.0f) && (value <= 180.0f))
	{
		return value;
	}

	if (value < 0.0f)
	{
		value = fmodf(value - 180.0f, 360.0f);
		return value + 180.0f;
	}
	else
	{
		value = fmodf(value + 180.0f, 360.0f);
		return value - 180.0f;
	}
}

inline float ClampAngleRadians(float value)
{
	if ((value > -M_PI_F) && (value <= M_PI_F))
	{
		return value;
	}

	if (value < 0.0f)
	{
		value = fmodf(value - M_PI_F, M_PI_F * 2.0f);
		return value + M_PI_F;
	}
	else
	{
		value = fmodf(value + M_PI_F, M_PI_F * 2.0f);
		return value - M_PI_F;
	}
}

inline float ClampAnglePositive(float value)
{
	value = fmodf(value, 360.0f);
	return value;
}

inline float AngleDiff(float src, float dest)
{
	dest = ClampAngle(dest);
	src = ClampAngle(src);

	float delta = dest - src;

	return ClampAngle(delta);
}

inline float AngleDiffRadians(float src, float dest)
{
	dest = ClampAngleRadians(dest);
	src = ClampAngleRadians(src);

	float delta = dest - src;

	return ClampAngleRadians(delta);
}

inline float Approach(float target, float current, float speed)
{
	float delta = target - current;

	if (delta > speed)
	{
		return current + speed;
	}
	else if (delta < -speed)
	{
		return current - speed;
	}
	else
	{
		return target;
	}
}

inline float ApproachAngle(float target, float current, float speed)
{
	float delta = AngleDiff(current, target);

	if (delta > speed)
	{
		return current + speed;
	}
	else if (delta < -speed)
	{
		return current - speed;
	}
	else
	{
		return target;
	}
}

inline float ApproachAngleRadians(float target, float current, float speed)
{
	float delta = AngleDiffRadians(current, target);

	if (delta > speed)
	{
		return current + speed;
	}
	else if (delta < -speed)
	{
		return current - speed;
	}
	else
	{
		return target;
	}
}

inline bool FloatsEqual(float a, float b, float tolerance = FLOAT_EPSILON)
{
	return abs(a - b) < tolerance;
}

inline bool VectorsEqual(const vec4 &a, const vec4 &b, float tolerance = FLOAT_EPSILON)
{
	return FloatsEqual(a[0], b[0], tolerance) &&
		   FloatsEqual(a[1], b[1], tolerance) &&
		   FloatsEqual(a[2], b[2], tolerance) &&
		   FloatsEqual(a[3], b[3], tolerance);
}

inline bool VectorsEqual(const vec3 &a, const vec3 &b, float tolerance = FLOAT_EPSILON)
{
	return FloatsEqual(a[0], b[0], tolerance) &&
		   FloatsEqual(a[1], b[1], tolerance) &&
		   FloatsEqual(a[2], b[2], tolerance);
}

inline bool VectorsEqual(const vec2 &a, const vec2 &b, float tolerance = FLOAT_EPSILON)
{
	return FloatsEqual(a[0], b[0], tolerance) &&
		   FloatsEqual(a[1], b[1], tolerance);
}

// converts 0 - width, 0 - height, origin top left (Screen)
// to -1 - 1, -1 - 1, origin center (NDC)
inline mat4 ScreenMatrix(int width, int height)
{
	mat4 screenMatrix = translate(mat4::Identity(), vec3(-1.0f, 1.0f, 0.0f));

	return scale(screenMatrix, 2.0f / width, -2.0f / height, 1.0f);
}
#endif
