#ifndef MACROS_H
#define MACROS_H

#include "util\platform.h"

// declares private cctor and implements public, default ctor
#define NO_COPY_DEFAULT_CTOR(x)	\
private:						\
	x(const x &other);			\
	x(x && other);				\
public:							\
	x() {}

// declares private cctor
#define NO_COPY(x)	   \
private:			   \
	x(const x &other); \
	x(x && other);	   \
public:

// use Assert to trigger assertions in debug mode
// use AssertCall to call function which return a bool
// to check if that bool returned true while debugging
#if _DEBUG
#define Assert(x) \
	_ASSERTE(x)
#define AssertCall(x) \
	_ASSERTE(x)
#else
#define Assert(x) \
	((void)0)
#define AssertCall(x) \
	x
#endif

// invalid model index
#define MDX_INVALID_INDEX    0xFFFFFFFF

#define MIN(x, y) \
	(((x) > (y)) ? (y) : (x))

#define MAX(x, y) \
	(((x) > (y)) ? (x) : (y))

#define SEL(v, a, b) \
	((v)>=0?(a):(b))

#define DBGMSG(x) \
	OutputDebugStringA(x)

// define BaseClass and ThisClass types isnide this class
// makes it easier to change baseclasses later!
#define DECLARE_CLASS(thisClass, baseClass)	\
	typedef baseClass	BaseClass;			\
	typedef thisClass	ThisClass;

#define DECLARE_CLASS_NOBASE(thisClass)	\
	typedef thisClass	ThisClass;

#ifdef PLATFORM_WIN32
#define FORCEINLINE		__forceinline
#else
#define FORCEINLINE		inline
#endif

// degree to radians
#define DEG2RAD(x) \
	(((x) / 180.0f) * (float)M_PI)

// radians to degrees
#define RAD2DEG(x) \
	((x * 180.0f) / (float)M_PI)

// common max string length (MAX_PATH is Win XP limit, 260)
#define MAX_PATH_GAME    (MAX_PATH * 4)

// validates naive cast of a class by using dynamic cast in debug mode
#ifdef _DEBUG
template<typename D, typename S>
inline D assert_cast(S *s)
{
	Assert(static_cast<D>(s) == dynamic_cast<D>(s));
	return static_cast<D>(s);
}

#else
#define assert_cast    static_cast
#endif

#define CLAMP(value, min, max) \
	(((value) < (min)) ? (min) : (((value) > (max)) ? (max) : (value)))
#endif

#define VECTOR_CONTAINS(vector, value) \
	(std::find(vector.begin(), vector.end(), value) != vector.end())

#define VECTOR_REMOVE(vector, value) \
	(vector.erase(std::find(vector.begin(), vector.end(), value)))

#define VECTOR_FIND(vector, value) \
	std::find(vector.begin(), vector.end(), value)

#define VECTOR_IS_INDEX_VALID(vector, index) \
	(index >= 0 && index < int(vector.size()))

#define LIST_CONTAINS(list, value) \
	(std::find(list.begin(), list.end(), value) != list.end())

#define MAP_CONTAINS(map, key) \
	(map.find(key) != map.end())

#define XY( vec ) \
	vec.x(), vec.y()

#define XYZ( vec ) \
	vec.x(), vec.y(), vec.z()

#define XYZW( vec ) \
	vec.x(), vec.y(), vec.z(), vec.w()

#define SWIZZLE_VECTOR(vecA, vecB, vecC) \
	vec3(vecA.x(), vecB.y(), vecC.z())