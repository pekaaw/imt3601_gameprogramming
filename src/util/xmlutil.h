#ifndef XMLUTIL_H
#define XMLUTIL_H


#include "fileutil.h"
#include "xml/rapidxml.hpp"

class CXMLDocument
{
public:
	CXMLDocument()
		: data(nullptr)
	{
	}
	~CXMLDocument()
	{
		delete [] data;
	}

	inline void Init(const char *text, int length)
	{
		data = new char[length + 1];
		G_StrCpy(data, text);

		doc.parse<rapidxml::parse_trim_whitespace | rapidxml::parse_validate_closing_tags>(data);

		documentRoot = doc.first_node();
		Assert(documentRoot != nullptr);
	}

	inline rapidxml::xml_node<> *GetDocumentRoot()
	{
		return documentRoot;
	}

	rapidxml::xml_document<> *operator->()
	{
		Assert(data != nullptr);
		return &doc;
	}

private:
	char *data;
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<> *documentRoot;
};


inline bool LoadXMLFile(const char *path, CXMLDocument &doc)
{
	std::string file = LoadFile(path);

	if (file.empty())
		return false;

	doc.Init(file.c_str(), file.length());
	return true;
}

inline bool LoadXMLString(const char *text, CXMLDocument &doc)
{
	doc.Init(text, strlen(text));
	return true;
}


inline bool GetXMLAttrInt(rapidxml::xml_node<> *parent, const char *childName, int &out)
{
	rapidxml::xml_attribute<> *child = parent->first_attribute(childName, 0U, false);

	if (child == nullptr)
	{
		return false;
	}

	out = atoi(child->value());
	return true;
}

inline const char *GetXMLNode(rapidxml::xml_node<> *parent, const char *childName, const char *defaultValue = "")
{
	rapidxml::xml_node<> *child = parent->first_node(childName, 0U, false);

	if (child != nullptr)
	{
		return child->value();
	}

	return defaultValue;
}

inline bool GetXMLNodeString(rapidxml::xml_node<> *parent, const char *childName, std::string &out)
{
	rapidxml::xml_node<> *child = parent->first_node(childName, 0U, false);

	if (child == nullptr)
	{
		return false;
	}

	out = child->value();
	return true;
}

inline bool GetXMLNodeInt(rapidxml::xml_node<> *parent, const char *childName, int &out)
{
	rapidxml::xml_node<> *child = parent->first_node(childName, 0U, false);

	if (child == nullptr)
	{
		return false;
	}

	out = atoi(child->value());
	return true;
}

inline bool GetXMLNodeFloat(rapidxml::xml_node<> *parent, const char *childName, float &out)
{
	rapidxml::xml_node<> *child = parent->first_node(childName, 0U, false);

	if (child == nullptr)
	{
		return false;
	}

	out = float(atof(child->value()));
	return true;
}

#endif