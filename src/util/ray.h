#ifndef RAY_H
#define RAY_H

#include "eigen.h"

struct Ray
{
	inline void Init(vec3 origin, vec3 delta)
	{
		this->origin = origin;
		this->delta = delta;
	};

	inline void InitFromPoints(vec3 start, vec3 end)
	{
		origin = start;
		delta = end - start;
	};

	vec3 origin;
	vec3 delta;
};




#endif