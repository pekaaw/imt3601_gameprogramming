#ifndef CALLBACKUTIL_H
#define CALLBACKUTIL_H

/// Generic callback interface
class ICallback
{
protected:
	virtual ~ICallback() {}

public:
	/// Callback function with unknown parameter type
	virtual void Run(void *p) = 0;
};


template< typename P >
class ICallbackConcrete : public ICallback
{
protected:
	virtual ~ICallbackConcrete() {}
};


/// Templated callback that points to a member function of an object
template< typename C, typename P >
class Callback : public ICallbackConcrete< P >
{
public:
	/// Type declaration of callback function that will be executed
	typedef void (C::*CallbackFunc)(P *p);

	Callback()
	{
		object = nullptr;
		func = nullptr;
	}

	/// Initializes the callback on construction
	Callback(C *object, CallbackFunc func)
	{
		this->object = object;
		this->func = func;
	}

	/// Set callback target and function
	virtual void Set(C *object, CallbackFunc func)
	{
		this->object = object;
		this->func = func;
	}

	/// Executes the callback on the specified object
	virtual void Run(void *p)
	{
		Assert(object != nullptr);
		Assert(func != nullptr);

		(object->*func)((P*)p);
	}

protected:
	C *object;
	CallbackFunc func;
};

#define DECLARE_CALLBACK(name, type) \
	Callback<ThisClass, type> name;

#endif