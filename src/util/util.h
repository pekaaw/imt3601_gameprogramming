#ifndef UTIL_H
#define UTIL_H

#include "macros.h"
#include "platform.h"

#include <stdarg.h>

inline void DBGMSGF(const char *msg, ...)
{
	unsigned int const STRING_BUFFER(4096);
	char text[STRING_BUFFER];
	va_list list;

	if (msg != NULL)
	{
		va_start(list, msg);
		vsprintf_s(text, msg, list);
		va_end(list);

		DBGMSG(text);
	}
}

// random float in 0 - 1 range (inclusive)
inline float randf()
{
	return float(rand() / (float)RAND_MAX);
}

// random int in x - y range (inclusive)
inline int randRange(int min, int max)
{
	// not very uniform..
	// return min + rand() % (max - min + 1);
	return int(min + (max - min + 1) * float(rand() / float(RAND_MAX + 1.0f)));
}

inline float randfRange(float min, float max)
{
	// not very uniform..
	// return min + rand() % (max - min + 1);
	return min + (max - min) * randf();
}

// random bool
inline bool randb()
{
	return randf() > 0.5f;
}

// random int sign
inline int randsign()
{
	return randb() ? 1 : -1;
}

// linear interpolation
template<typename T>
inline T Lerp(float flAmount, T flValue0, T flValue1)
{
	return T(flValue0 + (flValue1 - flValue0) * flAmount);
}

// linear interpolation
inline int Lerp(float flAmount, int flValue0, int flValue1)
{
	return flValue0 + int(float(flValue1 - flValue0) * flAmount);
}

// swap two values
template<typename T>
inline void Swap(T &a, T &b)
{
	T c(std::move(a));

	a = std::move(b);
	b = std::move(c);
}

#endif
