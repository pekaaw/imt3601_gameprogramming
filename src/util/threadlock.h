#ifndef THREADLOCK_H
#define THREADLOCK_H


#include <mutex>
#include <condition_variable>

class ThreadLock
{
	NO_COPY(ThreadLock);
public:
	ThreadLock()
		: requestLock(false)
	{
	}

	// foreign thread calls these to control locking
	void RequestLock();
	void ReleaseLock();

	// should only be called from the thread that requested the lock
	bool IsLocked();

	// owning thread calls this function every frame to
	// listen for lock requests
	void CheckForLock();

private:
	std::mutex threadMutex;
	std::condition_variable requestVar;
	std::condition_variable confirmVar;

	volatile bool requestLock;
};

inline void ThreadLock::RequestLock()
{
	std::unique_lock<std::mutex> threadLock(threadMutex);

	requestLock = true;

	confirmVar.wait(threadLock);
}

inline void ThreadLock::ReleaseLock()
{
	std::unique_lock<std::mutex> threadLock(threadMutex);

	requestLock = false;

	requestVar.notify_all();
}

inline bool ThreadLock::IsLocked()
{
	return requestLock;
}

inline void ThreadLock::CheckForLock()
{
	if (requestLock)
	{
		std::unique_lock<std::mutex> threadLock(threadMutex);

		confirmVar.notify_all();

		requestVar.wait(threadLock);
	}
}

#endif