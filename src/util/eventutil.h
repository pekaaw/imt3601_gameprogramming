#ifndef EVENTUTIL_H
#define EVENTUTIL_H

#include <unordered_set>
#include "callbackutil.h"

class IEvent
{
protected:
	virtual ~IEvent() {}
public:
	virtual void RegisterCallback(void *callback) = 0;
	virtual void OnCallbackDestroyed(void *callback) = 0;
};

template<typename P>
class IEventListener : public ICallbackConcrete<P>
{
	IEventListener(IEventListener &&other);
protected:
	virtual ~IEventListener()
	{
		for (auto itr : events)
			itr->OnCallbackDestroyed(this);
	}

public:
	IEventListener() {}
	IEventListener(const IEventListener &other)
	{
		for (auto itr : other.events)
			itr->RegisterCallback(this);
	}

	std::unordered_set<IEvent *> events;
};

template<typename T>
class Event : public IEvent
{
	Event(Event &&other);
public:
	Event() {}
	Event(const Event &other)
	{
		for (auto itr : other.callbacks)
		{
			callbacks.insert(itr);
			itr->events.insert(this);
		}
	}

	~Event()
	{
		for (auto itr : callbacks)
			itr->events.erase(this);
	}

	inline void operator+=(IEventListener<T> *callback)
	{
		callbacks.insert(callback);
		callback->events.insert(this);
	}

	inline void operator+=(IEventListener<T> &callback)
	{
		callbacks.insert(&callback);
		callback.events.insert(this);
	}

	inline void operator-=(IEventListener<T> *callback)
	{
		callback->events.erase(this);
		callbacks.erase(callback);
	}

	inline void operator-=(IEventListener<T> &callback)
	{
		callback.events.erase(this);
		callbacks.erase(&callback);
	}

	inline void operator()(T param)
	{
		for (auto itr : callbacks)
			itr->Run(&param);
	}

	virtual void RegisterCallback(void *callback)
	{
		IEventListener<T> *concreteCallback = (IEventListener<T> *)callback;
		callbacks.insert(concreteCallback);
		concreteCallback->events.insert(this);
	}

	virtual void OnCallbackDestroyed(void *callback)
	{
		callbacks.erase((IEventListener<T> *)callback);
	}

private:
	std::unordered_set<IEventListener<T> *> callbacks;
};

#define LISTENER(thisType, handler, type)					 \
	class __L ## handler ## __ : public IEventListener<type> \
	{														 \
public:														 \
		__L ## handler ## __(thisType * obj)				 \
			: _obj(obj) {}									 \
		virtual void Run(void *param)						 \
		{													 \
			_obj->handler(*((type *)param));				 \
		}													 \
		thisType *_obj;										 \
	};														 \
	__L ## handler ## __ L ## handler;

#define EVENTHANDLER(name, type)						  \
	void name(type Param);								  \
	class __L ## name ## __ : public IEventListener<type> \
	{													  \
public:													  \
		__L ## name ## __(ThisClass * obj)				  \
			: _obj(obj) {}								  \
		virtual void Run(void *param)					  \
		{												  \
			_obj->name(*((type *)param));				  \
		}												  \
		ThisClass *_obj;								  \
	};													  \
	__L ## name ## __ L ## name;
#endif
