#ifndef FILEUTIL_H
#define FILEUTIL_H


#include <string>
#include <fstream>

// load a file and convert its content to a string
inline std::string LoadFile(const std::string &filename)
{
	std::ifstream file(filename.c_str());

	if (!file.is_open())
	{
		return "";
	}

	std::string ret = std::string((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());

	file.close();
	return ret;
}

// check if a file exists
inline bool FileExists(const char *path)
{
	if (FILE *file = fopen(path, "r"))
	{
		fclose(file);
		return true;
	}

	return false;
}

// parse various datatypes from a file input stream
inline bool ParseUInt(std::ifstream &file, Uint32 &v)
{
	file.read((char*)&v, sizeof(Uint32));
	return file.good();
}

inline bool ParseUInt16(std::ifstream &file, Uint16 &v)
{
	file.read((char*)&v, sizeof(Uint16));
	return file.good();
}

inline bool ParseUInt8(std::ifstream &file, Uint8 &v)
{
	file.read((char*)&v, sizeof(Uint8));
	return file.good();
}

inline bool ParseString(std::ifstream &file, char *out, int sizeInBytes)
{
	file.read(out, sizeInBytes);
	return file.good();
}

inline float ParseFloat(std::ifstream &file, float &v)
{
	file.read((char*)&v, sizeof(float));
	return file.good();
}


#endif