#ifndef PLATFORM_H
#define PLATFORM_H

#ifdef WIN32
#ifndef PLATFORM_WIN32
#define PLATFORM_WIN32
#endif
#endif


#ifdef PLATFORM_WIN32
#include <windows.h>
#include <minwindef.h>

#if DEBUG
#include <crtdbg.h>
#endif
#endif


#ifndef Uint8
typedef unsigned __int8 Uint8;
#endif

#ifndef Uint16
typedef unsigned __int16 Uint16;
#endif

#ifndef Uint32
typedef unsigned __int32 Uint32;
#endif

#ifndef Uint64
typedef unsigned __int64 Uint64;
#endif

#ifndef Int8
typedef signed __int8 Int8;
#endif

#ifndef Int16
typedef signed __int16 Int16;
#endif

#ifndef Int32
typedef signed __int32 Int32;
#endif

#ifndef Int64
typedef signed __int64 Int64;
#endif

#ifndef UByte
typedef unsigned __int8 UByte;
#endif

#ifndef Byte
typedef signed __int8 Byte;
#endif


#endif