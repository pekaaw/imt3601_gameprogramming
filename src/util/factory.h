#ifndef FACTORY_H
#define FACTORY_H

#include <string>
#include <unordered_map>

#include "macros.h"

// utilities for factory lists

// common linked list for factory init.
// classes can register statically by using this.
template<class T, class C>
struct function_register_template
{
	T			function;
	const char	*nameLiteral;
	C			*next;
};

// convert list of above struct to a map<string, factoryFunction>
// and delete instances of that list
template<class T, class D>
inline void ConvertFactoryListToMap(D *first,
									std::unordered_map<std::string, T> &map)
{
	D *cur = first->next;

	while (cur != nullptr)
	{
		D *deleteMe = cur;

		Assert(map.find(cur->nameLiteral) == map.end());
		map[cur->nameLiteral] = cur->function;

		cur = cur->next;
		delete deleteMe;
	}
	first->next = nullptr;
}

// insert new factory to list during dll init
template<class T, class D>
inline void RegisterFactory(D *first,
							T function, const char *name)
{
	D *f = new D;

	f->next = nullptr;
	f->nameLiteral = name;
	f->function = function;

	D *cur = first;
	while (cur->next != nullptr)
	{
		cur = cur->next;
	}
	cur->next = f;
}

// destroy a whole factory list
template<class D>
inline void DestroyFactoryList(D *first)
{
	D *cur = first->next;

	first->next = nullptr;

	while (cur != nullptr)
	{
		D *deleteMe = cur;
		cur = cur->next;
		delete deleteMe;
	}
}

// find a factory from a static list
// don't use this if you want to go fast. Use ConvertFactoryListToMap during init instead!
template<class D, class R>
inline R *ExecuteFactoryFunction(D *first, const char *name)
{
	D *cur = first->next;

	while (cur != nullptr)
	{
		if (_stricmp(cur->nameLiteral, name) == 0)
		{
			return cur->function();
		}

		cur = cur->next;
	}

	return nullptr;
}
#endif	// FACTORY_H
