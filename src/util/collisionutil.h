#ifndef COLLISIONUTIL_H
#define COLLISIONUTIL_H

#include "ray.h"
#include "plane.h"

// returns true if there's an intersection
// only initializes intersectionPoint and distance if there's an intersection
inline bool IntersectRayWithPlane(const Ray &ray, const Plane &plane, vec3 *intersectionPoint = nullptr, float *distance = nullptr)
{
	float angle = ray.delta.dot(plane.normal);

	if (angle == 0.0f)
	{
		return false;
	}

	float factor = ( -( ray.origin.dot(plane.normal) - plane.distance ) ) / angle;

	if (factor < 0.0f)
	{
		return false;
	}
	else
	{
		if (intersectionPoint != nullptr)
		{
			*intersectionPoint = ray.origin + ray.delta * factor;
		}

		if (distance != nullptr)
		{
			*distance = factor;
		}

		return true;
	}
}


inline bool IntersectScreenRayWithWorld(int screenx, int screeny, int screenWidth, int screenHeight,
										const mat4 &viewProjection, vec3 &intersectionPointOut)
{
	float mposx = screenx / float(screenWidth);
	mposx = mposx * 2.0f - 1.0f;

	float mposy = 1.0f - screeny / float(screenHeight);
	mposy = mposy * 2.0f - 1.0f;

	const mat4 &viewProj = viewProjection;
	mat4 viewProjInv = viewProj.inverse().eval();

	// create a ray in NDCs
	// project the ray into the world
	// convert to vec3
	vec4 ray0 = viewProjInv * vec4(mposx, mposy, -1.0f, 1.0f);
	vec3 origin = vec3(ray0.x(), ray0.y(), ray0.z()) / ray0.w();
	vec4 ray1 = viewProjInv * vec4(mposx, mposy, 1.0f, 1.0f);
	vec3 end = vec3(ray1.x(), ray1.y(), ray1.z()) / ray1.w();

	vec3 delta = end - origin;

	Ray ray;
	ray.InitFromPoints(vec3(ray0.x(), ray0.y(), ray0.z()) / ray0.w(),
					   vec3(ray1.x(), ray1.y(), ray1.z()) / ray1.w());

	Plane plane;

	// now collide that world space ray with the plane n = 0,0,1, distance = 0, which is the world plane
	return IntersectRayWithPlane(ray, plane, &intersectionPointOut);
}


#endif