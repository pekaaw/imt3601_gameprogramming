
#include "xml/rapidxml.hpp"

void rapidxml::parse_error_handler(const char *what, void *where)
{
	assert(what && where && false);
	std::exit(1);
}