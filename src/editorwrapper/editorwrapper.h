#pragma once
#using <System.Windows.Forms.DLL>

class IEngine;
class IEngineTools;
struct SDL_Window;

public ref class EditorWrapper
{
public:
	delegate void TileSelectHandler(int x, int y);

	EditorWrapper(void);
	~EditorWrapper(void);
	void Init(void* handle, int width, int height);
	void Shutdown();
	void WindowResize(int width, int height);

	void Lock(bool switchContext);
	void Unlock();

	void LoadMap(System::String^ filename);
	//void SetGlobalLight();
	bool GetTileAtPos(int mouseX, int mouseY, int %indexX, int %indexY);
	void ChangeTile(int x, int y, float, float);
	void ReloadAtlas(array<System::Byte> ^ atlas, array<System::Byte> ^ normalAtlas, int width, int height);
	void ChangeEditMode(int mode);
	void ChangeTileFlag(int x, int y, bool set);

	void OnKeyDown(System::Windows::Forms::KeyEventArgs^ args);
	void OnKeyUp(System::Windows::Forms::KeyEventArgs^ args);
	void MouseDown(System::Windows::Forms::MouseEventArgs^ args);
	void MouseUp(System::Windows::Forms::MouseEventArgs^ args);
	
	void MouseEnter();
	void MouseLeave();
	
	event TileSelectHandler ^ TileSelected;
	event System::Action<int> ^ EntitySelected;
	event System::Action ^ Initalized;

private:

	void LockOnDemand(bool switchContext);
	void UnlockOnDemand();

	SDL_Keycode GetSdlKey(System::Windows::Forms::Keys code);

	bool isLockingExternal;

	IEngine *engine;
	IEngineTools *engineTools;
};

