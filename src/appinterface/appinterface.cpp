
#include <stdlib.h>

#include <SDL.h>

#include "appinterface.h"
#include "util\macros.h"
#include "util\stringutil.h"

SINGLETON_INSTANCE(AppInterface);

APPINTERFACE_API IAppInterface *GetAppInterface()
{
	return AppInterface::GetInstance();
}

AppInterface::AppInterface()
{
}

AppInterface::~AppInterface()
{
	Assert(libraryHandles.empty());
}

bool AppInterface::LoadSharedLibrary(const char *libraryName)
{
	// construct absolute path
	char fullPath[MAX_PATH_GAME];
	GetCWD(fullPath, sizeof(fullPath));
	G_StrNCat(fullPath, sizeof(fullPath), PATHSEPARATOR_STRING "bin" PATHSEPARATOR_STRING);
	G_StrNCat(fullPath, sizeof(fullPath), libraryName);
	G_StrFixSlashes(fullPath);

	void *handle = SDL_LoadObject(fullPath);

	Assert(handle != nullptr);

	if (handle == nullptr)
		return false;

	libraryHandles.push_back(handle);
	return true;
}

void AppInterface::Shutdown()
{
	for (auto l : libraryHandles)
		SDL_UnloadObject(l);

	libraryHandles.clear();
	applications.clear();
}

void AppInterface::RegisterApp(const char *identifier, void *target)
{
	Assert(applications.find(identifier) == applications.end());

	applications[identifier] = target;
}

void *AppInterface::QueryApp(const char *identifier)
{
	Assert(applications.find(identifier) != applications.end());

	auto itr = applications.find(identifier);

	if (itr == applications.end())
		return nullptr;

	return itr->second;
}