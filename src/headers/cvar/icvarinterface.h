#ifndef ICVAR_H
#define ICVAR_H


#ifdef ICVARINTERFACE_EXPORTS
#define ICVARINTERFACE_API	__declspec(dllexport)
#else
#define ICVARINTERFACE_API	__declspec(dllimport)
#endif

#include "util/callbackutil.h"

namespace CvarExecutionTypes
{
	enum CvarExecutionType_e
	{
		ALL = 0,
		CLIENT,
		SERVER,
		CLIENTCOMMAND,

		COUNT,
	};
}
typedef CvarExecutionTypes::CvarExecutionType_e CvarExecutionTypeId;

//#ifdef CLIENT_DLL
//#define CVAR_DEFAULT_EXECUTION_FLAG CvarExecutionFlags::CLIENT
//#elif SERVER_DLL
//#define CVAR_DEFAULT_EXECUTION_FLAG CvarExecutionFlags::SERVER
//#endif

/// Arguments parsed from a command string
class ICommandArgs
{
protected:
	virtual ~ICommandArgs() {}

public:
	virtual int GetArgCount() = 0;
	virtual const char *GetArg(int index) = 0;
};


/// A console variable. Has a unique name and contains a value
class ICvar : public ICallback
{
protected:
	virtual ~ICvar() {}

public:
	virtual const char *GetName() = 0;

	virtual const char *GetString() = 0;
	virtual void SetString(const char *string) = 0;
};


/// Interface to access application wide cvars and cmds
class ICvarInterface
{
protected:
	virtual ~ICvarInterface() {}

public:
	virtual void Init() = 0;

	/// Parses a string and dispatches all known callbacks in it (cmds, cvars)
	virtual void ExecuteBuffer(const char *buffer, CvarExecutionTypeId executionType = CvarExecutionTypes::ALL) = 0;

	/// Finds all commands that begin with a specific partial string
	virtual void GetCommandsStartingWith(const char *text, std::vector<std::string> &commandsOut) = 0;
};

#define INTERFACE_ICVAR_VERSION "icvar001"

/// Queues a cvar/cmd on library load
ICVARINTERFACE_API void RegisterCommand(const char *name, CvarExecutionTypeId executionType, ICallback *command);

/// Helper class to register a global cmd
class Command : public ICallback
{
public:
	/// Type of global function to be executed
	typedef void (*CommandFunc)(ICommandArgs *args);

	Command(CommandFunc func, const char *name)
	{
		this->func = func;

		RegisterCommand(name, CvarExecutionTypes::ALL, this);
	}

	/// Executes the global function
	virtual void Run(void *p)
	{
		func((ICommandArgs*)p);
	}

private:
	CommandFunc func;
};

/// Cmd callback that can be executed on an instance of an object.
/// Object should never be destructed, e.g. use with singletons
template< typename C >
class CommandCallback : public Callback<C, ICommandArgs>
{
public:
	/// Function def generated from Callback
	typedef Callback<C, ICommandArgs>::CallbackFunc CommandFunc;

	CommandCallback()
	{
	}

	CommandCallback(C *object, CommandFunc func, const char *name,
		CvarExecutionTypeId executionType = CvarExecutionTypes::ALL)
		: Callback<C, ICommandArgs>(object, func)
	{
		RegisterCommand(name, executionType, this);
	}

	void Set(C *object, CommandFunc func, const char *name,
		CvarExecutionTypeId executionType = CvarExecutionTypes::ALL)
	{
		Callback<C, ICommandArgs>::Set(object, func);

		RegisterCommand(name, executionType, this);
	}

private:
	CommandFunc func;
};

/// Creates a global cmd
#define CON_COMMAND(x) \
	void __cmdfunc ## x(ICommandArgs *args); \
	static Command __cmdInstance ## x(__cmdfunc ## x, #x); \
	void __cmdfunc ## x(ICommandArgs *args)

/// Registers a global cmd
#define CON_COMMAND_PARAMS(name, func) \
	static Command __cmdInstance ## func(func, #name)

/// Declares a cmd inside a class
#define CON_COMMAND_OBJECT(className, func) \
	void func(ICommandArgs *args); \
	CommandCallback<className> func ## Callback;

#endif