#ifndef SHAREDDEFS_H
#define SHAREDDEFS_H

/// Max players supported by a server
#define MAX_PLAYERS 2

#define MAX_ENTITY_CLASSNAME_LENGTH 64
#define MAX_NETWORKMEMBER_LENGTH 64

/// max amount of bones a model can have
#define MODEL_MAX_BONES 64
/// max amount of concurrent animation layers for transitions
#define MODEL_MAX_TRANSITION_LAYERS 3

/// tile count along x and y for a tile cluster mesh
#define WORLD_CLUSTER_SIZE 20
/// tile size in world units
#define WORLD_TILE_SCALE 128.0f
/// z pos for world tiles
#define WORLD_POS_Z 0.0f
/// atlas texture scale
#define WORLD_ATLAS_TEXTURE_SIZE 1024
/// texture size of a tile
#define WORLD_TILE_TEXTURE_SIZE 64
/// normalized size of a tile
#define WORLD_TILE_UV_SIZE (WORLD_TILE_TEXTURE_SIZE / float(WORLD_ATLAS_TEXTURE_SIZE))
/// normalized inset to hide bleeding due to bilinear interpolation
#define WORLD_TILE_UV_INSET (1.5f / float(WORLD_ATLAS_TEXTURE_SIZE))
/// extension for maps
#define WORLD_EXT "level"
/// map name + suffix = material name
#define WORLD_ATLAS_SUFFIX "_atlas"

/// texture res used for orthogonal shadow map
#define SHADOWMAP_RESOLUTION 1024

/// dynamic box2d objects should operate around 1.0 scale
#define PHYSICS_SCALE (1.0f / 20.0f)
#define PHYSICS_SCALE_INVERSE (1.0f / PHYSICS_SCALE)

/// Fixed simulation speed for physics/server
#define SIMULATION_FRAMESPEED (1.0f/30.0f)

/// Max count for networked entities
#define NETWORKED_ENTITY_COUNT 4096

#define DYNAMIC_MESH_MAX_VERTICES (1 << 13)

#define NETWORKING_MAX_PACKET_SIZE 1472

/// Game tile flags set in editor
namespace TileFlags
{
	enum TileFlags_e
	{
		NONE = 0,					/// Nothing special
		WALKABLE = (1 << 0),		/// Creeps can walk here
		BUILDABLE = (1 << 1),		/// Player can build here
		SPAWN = (1 << 2),			/// Creeps can spawn here
		END = (1 << 3),				/// Goal tile for creeps
		MOUNTAIN = (1 << 4),
		WATER = (1 << 5),

		/// update by hand!
		COUNT = 6,
	};
}
typedef TileFlags::TileFlags_e TileFlagId;

#define PLAYER_0_COLOR vec3(1.0f, 0.2f, 0.0f)
#define PLAYER_1_COLOR vec3(0.05f, 0.2f, 1.0f)

#endif