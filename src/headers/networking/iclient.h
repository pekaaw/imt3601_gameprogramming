#ifndef INETCLIENT_H
#define INETCLIENT_H

#include "util\macros.h"
#include "util\platform.h"
#include "networking\enums.h"

class INetworkClient
{
public:
	// Frees up memory used by the Client. Disconnecting is optional, as this one will do that as well
	virtual void Release() = 0;

	// Queue messages for sending. It is recommended you keep the size below 500 bytes to properly split into packets
	virtual void QueueForSending(Byte *data, size_t size, bool reliable = false) = 0;
	// Sends queued messages. If you just want to ping the server, you can call this without any messages queued. If the amount of messages exceeds the packet limit, more packets are sent.
	virtual bool SendMessages() = 0;
	// Returns a pointer to a new message's data, or nullptr if there are no new messages.
	// NOTE:
	//   The buffer will be reused the next time this fucntion is called for this Client.
	// EXAMPLE:
	//   Uint32 messageSize;
	//   Byte *messageData;
	//   while(messageData = client->HasNewMessage(messageSize)) {...}
	virtual Byte *HasNewMessage(Uint32 &sizeOutput) = 0;
	// Gets the round-trip-time for the client. This value may not reflect the actual RTT, as it is smoothed out.
	virtual float GetRTT() const = 0;
	// Checks if the client has timed out.
	virtual bool HasTimedOut() = 0;


	/* Yes, the below code is ugly, I know, but the Client inheriting from Connection made it necessary */

	// Conenct to the server. Correct use have for example 127.0.0.1 as 7f000001, and port as big endian. I recommend using the other overload.
	virtual bool Connect(Uint32 address, Uint16 port, bool block) { return false; }
	// Connect to a client by host and port (little endian).
	virtual bool Connect(const char *host, unsigned short portNumber, bool block) { return false; }
	// Disconnects from the server, passing a goodbye-message before closing the socket.
	virtual void Disconnect() {}
	// Gets the status of the client, see ConnectioNStatus.
	virtual ConnectionStatus GetConnectionStatus() { return CSTATUS_NOT_CLIENT_CLASS; }
	// Checks whether the client is connected.
	virtual bool IsConnected() { return false; }
	// Sets up the socket for sending, but does not connect to a specific server.
	virtual bool SetupConnectionless() { return false; }
	// Sends a packet to a target. NOTE: It uses a header the Server class understands.
	virtual bool SendConnectionless(Uint32 targetAddress, Uint16 targetPort, Byte *data, Uint32 size) { return false; }
	// Sends a packet to a target. NOTE: It uses a header the Server class understands. This overload of the function resolves a host or IP and calls the other.
	virtual bool SendConnectionless(const char *targetHost, unsigned short targetPort, Byte *data, Uint32 size) { return false; }
	// Set the amount of time before the client times out
	virtual void SetTimeoutThreshold(int milliseconds) {}
	// Increases the buffer for sending packets. Note: This does not up the limit, use the static methods Connection::SetMaxPacketSize(size_t) or Client::SetMaxPacketSize(size_t) for that.
	virtual bool ResizePacketBuffer(size_t newSize = 512U) { return false; }
	// Ping the server.
	virtual void Ping() {};
};

#define INTERFACE_NETCLIENT_VERSION    "inetworkingclient001"
#endif	//ICLIENT_H
