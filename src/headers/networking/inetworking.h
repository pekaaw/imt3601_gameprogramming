#ifndef INETWORKING_H
#define INETWORKING_H

#include "util\platform.h"
#include "networking\iclient.h"
#include "networking\iserver.h"

class INetworkServer;
class INetworkClient;

class INetworking
{
protected:
	virtual ~INetworking()
	{
	}

public:
	// Initializes networking
	virtual bool Init() = 0;
	// Shuts down networking
	virtual void Shutdown() = 0;

	// Creates a server instance and returns the interface to it.
	virtual INetworkServer *CreateServer(Uint16 port) = 0;

	// Creates a client isntance and returns the interface.
	virtual INetworkClient *CreateClient() = 0;

	// Byte ordering
	virtual unsigned short GetNetworkOrderShort(unsigned short value) = 0;
	virtual unsigned int GetNetworkOrderUInt(unsigned int value) = 0;
};

#define INTERFACE_NETWORKING_VERSION    "inetworking001"
#endif	//INETWORKING_H
