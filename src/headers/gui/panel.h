#ifndef PANEL_H
#define PANEL_H

#include "gui.h"
#include "control.h"
#include "color.h"
#include "util/eventutil.h"

namespace GUI
{
class Panel : public Control, IInputMouseListener
{
public:
	Panel();
	virtual ~Panel();

	virtual void DrawSelf();
	virtual void UpdateSelf();
	Color color;
	Color borderColor;

	Event<Control *> Enter;
	Event<Control *> Leave;
	Event<Control *> Click;

	virtual void OnMouseDown(int mouseButton);
	virtual void OnMouseUp(int mouseButton);

	void SetMaterial(IMaterial *mat);
private:
	void Construct();
	bool mouseIn;
	bool leftMouseDown;
	IMaterial *mat;
};
}
#endif
