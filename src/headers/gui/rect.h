#ifndef RECT_H
#define RECT_H

#include "gui.h"

namespace GUI
{
struct Rect
{
public:
	Rect(int x, int y, int w, int h);
	Rect();

	bool Contains(int x, int y);

	int& operator[] (const int idx);
	int x, y, w, h;
};
}
#endif
