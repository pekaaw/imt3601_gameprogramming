#ifndef LAYOUTPANEL_H
#define LAYOUTPANEL_H

#include "gui/button.h"
#include "gui/label.h"
#include "gui/panel.h"

namespace GUI {
class LayoutPanel : public Panel
{
public:
	LayoutPanel();
	virtual ~LayoutPanel();
	void InvokeResize();

protected:
	virtual void Resize(int width, int height);
	virtual void InitComponents() = 0;
	virtual void SetPositions(int width, int height) = 0;
};
}
#endif
