#ifndef CONTROL_H
#define CONTROL_H

#include <list>
#include "gui/rect.h"
#include "gui.h"

#define SCALE(x) \
	VALUE_SCALED(x, renderContext->GetScreenHeight())

namespace GUI
{
class Control
{
public:
	Control(Rect rect);
	Control();
	virtual ~Control();

	void Update();
	void Draw();
	virtual void DrawSelf();
	virtual void UpdateSelf();
	virtual void AddChild(Control *child);
	virtual void RemoveChild(Control *child);
	virtual void Resize(int width, int height);

	Rect GetRecursiveRectPos();
	Rect GetRect();
	const std::list<Control *> &GetAllChildren();
	virtual Control *GetFocusPanel(int x, int y);
	virtual bool IsInputEnabled() const;

	//Setter
	virtual void SetRect(Rect rect);
	virtual bool IsVisible() { return visible; }
	virtual void SetVisible(bool visible);
	virtual void SetInputEnabled(bool enabled);

protected:
	Control *parent;
	std::list<Control *> children;
	Rect rect;
	bool visible;
	bool inputEnabled;

private:
	void Init();
};
}
#endif
