#ifndef IGRAPHICS_H
#define IGRAPHICS_H


/// 2D Graphics API for GUI rendering
class IGraphics
{
protected:
	virtual ~IGraphics() {}

public:
	/// Scale normalized value up (640x480 for 4:3, so y max is 480)
	virtual int GetValueScaled(int value) = 0;
	/// Scale value into normalized range (value/screenheight * 480)
	virtual int GetValueNormalized(int value) = 0;

	/// Get GUI frametime (not reset when map loads)
	virtual float GetFrametime() = 0;
	/// Get GUI time (not reset when map loads)
	virtual float GetTime() = 0;

	/// Pushes a 2D viewport so you can render in 0 - screenwidth, 0 - screenheight range
	virtual void Push2DViewport(int x, int y, int w, int h) = 0;
	virtual void Pop2DViewport() = 0;

	/// Set current drawing color for primitives
	virtual void SetDrawColor(float r, float g, float b, float a = 1.0f) = 0;
	/// Set current drawing line width
	virtual void SetDrawLineWidth(float width) = 0;

	/// Draws a solid rect
	virtual void DrawFilledRect(int x0, int y0, int x1, int y1) = 0;
	//Draw a rect with a given material
	virtual void DrawTexturedRect(int x0, int y0, int x1, int y1, IMaterial *mat) = 0;
	/// Draws a non-filled rect
	virtual void DrawRect(int x0, int y0, int x1, int y1) = 0;
	/// Draws a line
	virtual void DrawLine(int x0, int y0, int x1, int y1) = 0;
};

#define INTERFACE_IGRAPHICS_VERSION    "igraphics001"
#endif
