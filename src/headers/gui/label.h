#ifndef LABEL_H
#define LABEL_H

#include "gui.h"
#include "control.h"
#include "color.h"

namespace GUI
{
	enum Alignment{
		Left, Center
	};

class Label : public Control
{
public:
	Label();
	virtual ~Label();

	virtual void DrawSelf();

	void SetFont(const char *fontname);
	void SetText(const char *text);
	void SetPos(int x, int y);
	void SetAlignment(Alignment alignment);

	virtual void Resize(int width, int height);
	
	Color color;
protected:
	Alignment alignment;
	char *text;
	FONT_KEY font;
	Rect originalPos;
};
}
#endif
