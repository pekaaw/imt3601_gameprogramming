#ifndef GUI_DEFS_H
#define GUI_DEFS_H

#include "util/platform.h"

namespace GUI
{
	typedef Uint32 FONT_KEY;
#define FONT_KEY_INVALID 0xFFFFFFFF

	void Init();
	void Shutdown();
}

#define VALUE_SCALED(x, screenheight) \
	int((x) * ((screenheight)/480.0f))

#define VALUE_NORMALIZED(x, screenheight) \
	int((x) * (480.0f/(screenheight)))


#endif