#ifndef IFBODICT_H
#define IFBODICT_H

#include "util\eigen.h"
#include "materialsystem\materialsystem.h"

class ITexture;

/// System to handle FBOs
class IFBODict
{
protected:
	virtual ~IFBODict() {}

public:
	virtual FBO_HANDLE CreateFBO() = 0;
	virtual void PushFBO(FBO_HANDLE handle) = 0;
	virtual void PopFBO() = 0;

	virtual RB_HANDLE CreateRenderBuffer(TextureFormatId format, FramebufferResizeModeId resizeMode, int multisampleCount = 0, int width = -1, int height = -1) = 0;
	/// Binds color buffer to current FBO
	virtual void BindRenderBufferColor(RB_HANDLE handle, int index = 0) = 0;
	/// Binds depth buffer to current FBO
	virtual void BindRenderBufferDepth(RB_HANDLE handle) = 0;

	virtual RT_HANDLE CreateRenderTarget(const char *name, TextureFormatId format, FramebufferResizeModeId resizeMode, int width = -1, int height = -1,
		int flags = TextureFlags::CLAMP_S | TextureFlags::CLAMP_T | TextureFlags::NO_MIPMAP) = 0;
	/// Binds color target to current FBO
	virtual void BindRenderTargetColor(RT_HANDLE handle, int index = 0) = 0;
	/// Binds depth target to current FBO
	virtual void BindRenderTargetDepth(RT_HANDLE handle) = 0;
	/// Gets texture from specific render target
	virtual ITexture *GetRenderTargetTexture(RT_HANDLE handle) = 0;

	/// Blits one FBO to another one. Make sure that dimensions and format are matching
	virtual void CopyFBOtoFBO(FBO_HANDLE src, FBO_HANDLE dest, FramebufferResizeModeId mode) = 0;

	virtual void ReadFBOColor(void *pixels, int x, int y, int w, int h, int attachmentIndex) = 0;
};

#define INTERFACE_IFBODICT_VERSION "ifbodict001"

#endif