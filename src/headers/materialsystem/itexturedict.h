#ifndef ITEXTUREDICT_H
#define ITEXTUREDICT_H

class ITexture;

/// Manages texture loading, releasing and sharing
class ITextureDict
{
protected:
	virtual ~ITextureDict() {}

public:
	virtual void ReleaseUnreferencedTextures(TextureGroupId group = TextureGroups::NONE) = 0;

	/// Find an existing texture. Does not load it
	virtual ITexture *FindTexture(const char *path) = 0;

	/// Find a texture or load it if it's not precached yet
	virtual ITexture *FindOrLoadTexture(const char *path,
		TextureGroupId group = TextureGroups::OTHER,
		TextureTypeId type = TextureTypes::TEXTURE_2D,
		TextureFormatId format = TextureFormats::RGBA_8,
		int flags = 0) = 0;

	/// Creates a new procedural texture
	virtual ITexture *CreateTexture(const char *path,
		void *data, int width, int height, int flags,
		TextureFormatId format = TextureFormats::RGBA_8,
		TextureGroupId group = TextureGroups::OTHER) = 0;
};

#define INTERFACE_ITEXTUREDICT_VERSION "itexturedict001"

#endif