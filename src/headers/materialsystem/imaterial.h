#ifndef IMATERIAL_H
#define IMATERIAL_H

class IMaterialVar;

/// A Material that contains a shader and references a variable number of textures
class IMaterial
{
protected:
	virtual ~IMaterial() {}

public:
	virtual void IncrementReferenceCount() = 0;
	virtual void DecrementReferenceCount() = 0;

	virtual const char *GetName() const = 0;
	/// Check if this material is the error material
	virtual bool IsErrorMaterial() const = 0;

	/// Draw the material using the currently bound mesh
	virtual void Draw() = 0;

	/// Find a material var. Iterates all vars, don't use every frame!
	virtual IMaterialVar *FindMaterialVar(const char *name) = 0;
	/// Tries to find the material var on first call and every time materials are
	/// reloaded. Caches results to handle, use for calls that happen every frame
	virtual IMaterialVar *FindMaterialVarFast(MATERIAL_VAR_HANDLE &handle, const char *name) = 0;
};



#endif