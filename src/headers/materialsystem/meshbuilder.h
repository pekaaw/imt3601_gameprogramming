#ifndef MESHBUILDER_H
#define MESHBUILDER_H

#include "util\eigen.h"
#include "materialsystem.h"
#include "imesh.h"

/// Helper class to build new meshes
class MeshBuilder
{
	NO_COPY(MeshBuilder);
public:
	MeshBuilder()
		: mesh(nullptr)
		, vertexSize(0)
		, meshData(nullptr)
	{}

	/// Attach to a fresh mesh
	void Attach(IMesh *mesh);
	/// Attach to an existing mesh that was built before
	void AttachModify(IMesh *mesh, int offset = -1, int range = -1);
	/// Finish building/modifying
	void Detach();

	void GenerateNormals();
	void SmoothNormals();
	/// Create tangent and binormal automagically. Requires mesh to consist of triangles
	void GenerateTangentSpace();

	/// Write data to the next vertex
	void AdvanceVertex(int count = 1);

	void Position3f(float x, float y, float z);
	void Position3p(const float *xyz);
	void Position3v(const vec3 &xyz);

	void Position2f(float x, float y);
	void Position2p(const float *xy);

	void Normal3f(float x, float y, float z);
	void Normal3p(const float *xyz);

	void Tangent3f(float x, float y, float z);
	void Tangent3p(const float *xyz);

	void Binormal3f(float x, float y, float z);
	void Binormal3p(const float *xyz);

	void Color3f(float r, float g, float b);
	void Color3p(const float *rgb);

	void Color4f(float r, float g, float b, float a);
	void Color4p(const float *rgba);
	void Color4v(const vec4 &rgba);

	void Texcoord2f(float s, float t);
	void Texcoord2p(const float *st);

	void Boneindices3f(float a, float b, float c);
	void Boneindices3p(const float *abc);

private:
	void ResetWriters();

	inline float *GetMeshData()
	{
		if (meshData != nullptr)
			return meshData;

		return mesh->GetData();
	}

	IMesh *mesh;
	float *meshData;

	int vertexSize;
	float *ptrWrite[VertexFormat::NUM_BITS];
};

inline void MeshBuilder::Attach(IMesh *mesh)
{
	Assert(this->mesh == nullptr);
	Assert(mesh != nullptr);

	memset(this, 0, sizeof(MeshBuilder));

	this->mesh = mesh;
	meshData = nullptr;

	ResetWriters();
}

inline void MeshBuilder::AttachModify(IMesh *mesh, int offset, int range)
{
	Assert(this->mesh == nullptr);
	Assert(mesh != nullptr);

	memset(this, 0, sizeof(MeshBuilder));

	this->mesh = mesh;
	meshData = mesh->BindMap(offset, range);

	ResetWriters();
}

inline void MeshBuilder::Detach()
{
	Assert(mesh != nullptr);

	if (meshData != nullptr)
	{
		mesh->BindUnmap();
	}
	else
	{
		mesh->Upload();
		mesh->FlushCache();
	}

	mesh = nullptr;
	meshData = nullptr;
}

inline void MeshBuilder::ResetWriters()
{
	Assert(mesh != nullptr);

	int vertexFormat = mesh->GetVertexFormat();
	vertexSize = VertexFormat::GetVertexFormatStride(vertexFormat);
	float *writeOffset = GetMeshData();

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int element = VertexFormat::GetVertexElementFlag(i);

		if (VertexFormat::HasElement(vertexFormat, element))
		{
			ptrWrite[i] = writeOffset;

			writeOffset += VertexFormat::GetVertexElementStride(element);
		}
	}
}

inline void MeshBuilder::AdvanceVertex(int count)
{
	Assert(mesh != nullptr);

	// validate offsets
#if _DEBUG
	bool bFound = false;

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int element = VertexFormat::GetVertexElementFlag(i);

		if (VertexFormat::HasElement(mesh->GetVertexFormat(), element))
		{
			float *write = ptrWrite[i];
			int currentCount = write - GetMeshData();
			currentCount /= (VertexFormat::GetVertexFormatStride(mesh->GetVertexFormat()));

			Assert(currentCount < mesh->GetVertexCount());

			bFound = true;

			// only check the first offset so the index can be reconstructed
			break;
		}
	}

	Assert(bFound);
#endif

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
		ptrWrite[i] += vertexSize * count;

	// validate offsets after move
#if _DEBUG
	bFound = false;

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int element = VertexFormat::GetVertexElementFlag(i);

		if (VertexFormat::HasElement(mesh->GetVertexFormat(), element))
		{
			float *write = ptrWrite[i];
			int currentCount = write - GetMeshData();
			currentCount /= (VertexFormat::GetVertexFormatStride(mesh->GetVertexFormat()));

			Assert(currentCount <= mesh->GetVertexCount());

			bFound = true;

			// only check the first offset so the index can be reconstructed
			break;
		}
	}

	Assert(bFound);
#endif
}

inline void MeshBuilder::GenerateNormals()
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::NORMAL_3F));
	Assert((mesh->GetVertexCount()%3) == 0);
	Assert(mesh->GetFaceType() == FaceTypes::TRIANGLES);

	ResetWriters();

	for (int i = 0; i < mesh->GetVertexCount(); i += 3)
	{
		float *readerPos0 = ptrWrite[VertexFormat::INDEX_POSITION_3F];
		float *readerPos1 = ptrWrite[VertexFormat::INDEX_POSITION_3F] + vertexSize;
		float *readerPos2 = ptrWrite[VertexFormat::INDEX_POSITION_3F] + vertexSize * 2;

		vec3 v0 = vec3(*readerPos0, *(readerPos0+1), *(readerPos0+2));
		vec3 v1 = vec3(*readerPos1, *(readerPos1+1), *(readerPos1+2));
		vec3 v2 = vec3(*readerPos2, *(readerPos2+1), *(readerPos2+2));

		vec3 normal = (v1 - v0).cross(v2 - v0);
		normal.normalize();

		memcpy(ptrWrite[VertexFormat::INDEX_NORMAL_3F], normal.data(), sizeof(float) * 3);
		memcpy(ptrWrite[VertexFormat::INDEX_NORMAL_3F] + vertexSize, normal.data(), sizeof(float) * 3);
		memcpy(ptrWrite[VertexFormat::INDEX_NORMAL_3F] + vertexSize * 2, normal.data(), sizeof(float) * 3);

		AdvanceVertex(3);
	}
}

inline void MeshBuilder::SmoothNormals()
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::NORMAL_3F));
	Assert((mesh->GetVertexCount()%3) == 0);
	Assert(mesh->GetFaceType() == FaceTypes::TRIANGLES);

	ResetWriters();

	const int vertexCount = mesh->GetVertexCount();

	std::vector<vec3> knownPositions;
	std::vector<int> normalIndices;
	std::vector<std::vector<vec3>> sharedNormals;

	knownPositions.reserve(vertexCount);
	sharedNormals.reserve(vertexCount);

	// collect all normals of a vertex (and close vertices)
	for (int i = 0; i < vertexCount; i++)
	{
		float *readerPos0 = ptrWrite[VertexFormat::INDEX_POSITION_3F];
		float *readerNormal0 = ptrWrite[VertexFormat::INDEX_NORMAL_3F];

		vec3 v0 = vec3(*readerPos0, *(readerPos0+1), *(readerPos0+2));
		vec3 n0 = vec3(*readerNormal0, *(readerNormal0+1), *(readerNormal0+2));

		int index = -1;

		for (unsigned int x = 0; x < knownPositions.size(); x++)
		{
			if ((knownPositions[x] - v0).squaredNorm() < 0.5f)
			{
				index = x;
				break;
			}
		}

		if (index < 0)
		{
			index = knownPositions.size();
			knownPositions.push_back(v0);
			sharedNormals.push_back(std::vector<vec3>());
		}

		normalIndices.push_back(index);

		auto &normalVector = sharedNormals[index];
		bool containsNormal = false;

		for (auto v : normalVector)
		{
			if (v.dot(n0) >= 0.95f)
			{
				containsNormal = true;
			}
		}

		if (!containsNormal)
		{
			normalVector.push_back(n0);
		}

		AdvanceVertex();
	}

	// average the normals
	for (auto &sn : sharedNormals)
	{
		Assert(!sn.empty());

		vec3 normal(vec3::Zero());

		for (const auto &v : sn)
		{
			normal += v;
		}

		normal.normalize();
		sn[0] = normal;
	}

	ResetWriters();

	// apply normals
	for (int i = 0; i < vertexCount; i++)
	{
		int normalIndex = normalIndices[i];
		const vec3 &normal = sharedNormals[normalIndex][0];

		memcpy(ptrWrite[VertexFormat::INDEX_NORMAL_3F], normal.data(), sizeof(float) * 3);

		AdvanceVertex();
	}
}

inline void MeshBuilder::GenerateTangentSpace()
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::NORMAL_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TANGENT_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::BINORMAL_3F));
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TEXTURE_COORD_2F));
	Assert((mesh->GetVertexCount()%3) == 0);
	Assert(mesh->GetFaceType() == FaceTypes::TRIANGLES);

	ResetWriters();

	for (int i = 0; i < mesh->GetVertexCount(); i += 3)
	{
		// reader for vertices
		float *readerPos0 = ptrWrite[VertexFormat::INDEX_POSITION_3F];
		float *readerPos1 = ptrWrite[VertexFormat::INDEX_POSITION_3F] + vertexSize;
		float *readerPos2 = ptrWrite[VertexFormat::INDEX_POSITION_3F] + vertexSize * 2;

		float *readerNormal0 = ptrWrite[VertexFormat::INDEX_NORMAL_3F];
		float *readerNormal1 = ptrWrite[VertexFormat::INDEX_NORMAL_3F] + vertexSize;
		float *readerNormal2 = ptrWrite[VertexFormat::INDEX_NORMAL_3F] + vertexSize * 2;

		float *readerUV0 = ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F];
		float *readerUV1 = ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] + vertexSize;
		float *readerUV2 = ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] + vertexSize * 2;

		// Shortcuts for positions
		vec3 v0 = vec3(*readerPos0, *(readerPos0+1), *(readerPos0+2));
		vec3 v1 = vec3(*readerPos1, *(readerPos1+1), *(readerPos1+2));
		vec3 v2 = vec3(*readerPos2, *(readerPos2+1), *(readerPos2+2));

		// Shortcuts for UVs
		vec2 uv0 = vec2(*readerUV0,*(readerUV0+1));
		vec2 uv1 = vec2(*readerUV1,*(readerUV1+1));
		vec2 uv2 = vec2(*readerUV2,*(readerUV2+1));

		// Shortcuts for normals
		vec3 n0 = vec3(*readerNormal0, *(readerNormal0+1), *(readerNormal0+2));
		vec3 n1 = vec3(*readerNormal1, *(readerNormal1+1), *(readerNormal1+2));
		vec3 n2 = vec3(*readerNormal2, *(readerNormal2+1), *(readerNormal2+2));

		// Edges of the triangle : postion delta
		vec3 deltaPos1 = v1 - v0;
		vec3 deltaPos2 = v2 - v0;

		assert(deltaPos1.norm() > 0.0f);
		assert(deltaPos2.norm() > 0.0f);

		// UV delta
		vec2 deltaUV1 = uv1 - uv0;
		vec2 deltaUV2 = uv2 - uv0;

		float r = 1.0f / (deltaUV1.x() * deltaUV2.y() - deltaUV1.y() * deltaUV2.x());
		vec3 tangent = (deltaPos1 * deltaUV2.y() - deltaPos2 * deltaUV1.y()) * r;
		vec3 bitangent = (deltaPos2 * deltaUV1.x() - deltaPos1 * deltaUV2.x()) * r;

		assert(tangent.norm() > 0.0f);
		assert(bitangent.norm() > 0.0f);

		tangent.normalize();
		bitangent.normalize();

		// Set the same tangent for all three vertices of the triangle.
		// They will be merged later, in vboindexer.cpp
		vec3 t = (tangent - n0 * n0.dot(tangent)).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_TANGENT_3F], t.data(), sizeof(float) * 3);
		t = (tangent - n1 * (n1.dot(tangent))).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_TANGENT_3F] + vertexSize, t.data(), sizeof(float) * 3);
		t = (tangent - n1 * n1.dot(tangent)).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_TANGENT_3F] + vertexSize * 2, t.data(), sizeof(float) * 3);

		// Same thing for binormals
		vec3 bn = (bitangent - n0 * n0.dot(bitangent)).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_BINORMAL_3F], bn.data(), sizeof(float) * 3);
		bn = (bitangent - n1 * n1.dot(bitangent)).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] + vertexSize, bn.data(), sizeof(float) * 3);
		bn = (bitangent - n2 * n2.dot(bitangent)).normalized();
		memcpy(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] + vertexSize * 2, bn.data(), sizeof(float) * 3);

		AdvanceVertex();
		AdvanceVertex();
		AdvanceVertex();
	}
}

inline void MeshBuilder::Position3f(float x, float y, float z)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(ptrWrite[VertexFormat::INDEX_POSITION_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_POSITION_3F] = x;
	*(ptrWrite[VertexFormat::INDEX_POSITION_3F] + 1) = y;
	*(ptrWrite[VertexFormat::INDEX_POSITION_3F] + 2) = z;
}

inline void MeshBuilder::Position3p(const float *xyz)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(ptrWrite[VertexFormat::INDEX_POSITION_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_POSITION_3F], xyz, sizeof(float) * 3);
}

inline void MeshBuilder::Position3v(const vec3 &xyz)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_3F));
	Assert(ptrWrite[VertexFormat::INDEX_POSITION_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_POSITION_3F], xyz.data(), sizeof(float) * 3);
}

inline void MeshBuilder::Position2f(float x, float y)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_2F));
	Assert(ptrWrite[VertexFormat::INDEX_POSITION_2F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_POSITION_2F] = x;
	*(ptrWrite[VertexFormat::INDEX_POSITION_2F] + 1) = y;
}

inline void MeshBuilder::Position2p(const float *xy)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::POSITION_2F));
	Assert(ptrWrite[VertexFormat::INDEX_POSITION_2F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_POSITION_2F], xy, sizeof(float) * 2);
}

inline void MeshBuilder::Normal3f(float x, float y, float z)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::NORMAL_3F));
	Assert(ptrWrite[VertexFormat::INDEX_NORMAL_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_NORMAL_3F] = x;
	*(ptrWrite[VertexFormat::INDEX_NORMAL_3F] + 1) = y;
	*(ptrWrite[VertexFormat::INDEX_NORMAL_3F] + 2) = z;
}

inline void MeshBuilder::Normal3p(const float *xyz)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::NORMAL_3F));
	Assert(ptrWrite[VertexFormat::INDEX_NORMAL_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_NORMAL_3F], xyz, sizeof(float) * 3);
}

inline void MeshBuilder::Tangent3f(float x, float y, float z)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TANGENT_3F));
	Assert(ptrWrite[VertexFormat::INDEX_TANGENT_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_TANGENT_3F] = x;
	*(ptrWrite[VertexFormat::INDEX_TANGENT_3F] + 1) = y;
	*(ptrWrite[VertexFormat::INDEX_TANGENT_3F] + 2) = z;
}

inline void MeshBuilder::Tangent3p(const float *xyz)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TANGENT_3F));
	Assert(ptrWrite[VertexFormat::INDEX_TANGENT_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_TANGENT_3F], xyz, sizeof(float) * 3);
}

inline void MeshBuilder::Binormal3f(float x, float y, float z)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::BINORMAL_3F));
	Assert(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_BINORMAL_3F] = x;
	*(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] + 1) = y;
	*(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] + 2) = z;
}

inline void MeshBuilder::Binormal3p(const float *xyz)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::BINORMAL_3F));
	Assert(ptrWrite[VertexFormat::INDEX_BINORMAL_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_BINORMAL_3F], xyz, sizeof(float) * 3);
}

inline void MeshBuilder::Color3f(float r, float g, float b)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::COLOR_3F));
	Assert(ptrWrite[VertexFormat::INDEX_COLOR_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_COLOR_3F] =  r;
	*(ptrWrite[VertexFormat::INDEX_COLOR_3F] + 1) = g;
	*(ptrWrite[VertexFormat::INDEX_COLOR_3F] + 2) = b;
}

inline void MeshBuilder::Color3p(const float *rgb)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::COLOR_3F));
	Assert(ptrWrite[VertexFormat::INDEX_COLOR_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_COLOR_3F], rgb, sizeof(float) * 3);
}

inline void MeshBuilder::Color4f(float r, float g, float b, float a)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::COLOR_4F));
	Assert(ptrWrite[VertexFormat::INDEX_COLOR_4F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_COLOR_4F] =  r;
	*(ptrWrite[VertexFormat::INDEX_COLOR_4F] + 1) = g;
	*(ptrWrite[VertexFormat::INDEX_COLOR_4F] + 2) = b;
	*(ptrWrite[VertexFormat::INDEX_COLOR_4F] + 3) = a;
}

inline void MeshBuilder::Color4p(const float *rgba)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::COLOR_4F));
	Assert(ptrWrite[VertexFormat::INDEX_COLOR_4F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_COLOR_4F], rgba, sizeof(float) * 4);
}

inline void MeshBuilder::Color4v(const vec4 &rgba)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::COLOR_4F));
	Assert(ptrWrite[VertexFormat::INDEX_COLOR_4F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_COLOR_4F], rgba.data(), sizeof(float) * 4);
}

inline void MeshBuilder::Texcoord2f(float s, float t)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TEXTURE_COORD_2F));
	Assert(ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] = s;
	*(ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] + 1) = t;
}

inline void MeshBuilder::Texcoord2p(const float *st)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::TEXTURE_COORD_2F));
	Assert(ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_TEXTURE_COORD_2F], st, sizeof(float) * 2);
}

inline void MeshBuilder::Boneindices3f(float a, float b, float c)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::BONEINDICES_3F));
	Assert(ptrWrite[VertexFormat::INDEX_BONEINDICES_3F] != nullptr);

	*ptrWrite[VertexFormat::INDEX_BONEINDICES_3F] = a;
	*(ptrWrite[VertexFormat::INDEX_BONEINDICES_3F] + 1) = b;
	*(ptrWrite[VertexFormat::INDEX_BONEINDICES_3F] + 2) = c;
}

inline void MeshBuilder::Boneindices3p(const float *abc)
{
	Assert(mesh != nullptr);
	Assert(VertexFormat::HasElement(mesh->GetVertexFormat(), VertexFormat::BONEINDICES_3F));
	Assert(ptrWrite[VertexFormat::INDEX_BONEINDICES_3F] != nullptr);

	memcpy(ptrWrite[VertexFormat::INDEX_BONEINDICES_3F], abc, sizeof(float) * 3);
}

#endif