#ifndef IMDXRENDERABLE_H
#define IMDXRENDERABLE_H

/// A model instance that uses shared geometry/materials internally
class IMDXRenderable
{
protected:
	virtual ~IMDXRenderable() {}

public:
	virtual void Release() = 0;

	virtual void Draw() = 0;
	virtual void DrawSkeleton() = 0;

	/// Force bones to be updated during the next rendering request
	virtual void InvalidateBoneCache() = 0;

	virtual void SetSequence(const char *name) = 0;
	virtual void SetSequence(int index) = 0;
	virtual int GetSequence() const = 0;
	virtual const char *GetSequenceName() const = 0;

	virtual void SetCycle(float cycle) = 0;
	virtual float GetCycle() const = 0;

	/// Set animation speed
	virtual void SetPlaybackRate(float rate) = 0;
	virtual float GetPlaybackRate(float rate) const = 0;

	/// Update the animation
	virtual void AdvanceFrame(float frametime) = 0;

	/// Bounds in local space
	virtual void GetModelExtents(vec3 &min, vec3 &max) = 0;
};

#endif