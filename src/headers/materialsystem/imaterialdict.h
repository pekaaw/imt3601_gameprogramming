#ifndef IMATERIALDICT_H
#define IMATERIALDICT_H

class IMaterial;

/// Manages material caching
class IMaterialDict
{
protected:
	virtual ~IMaterialDict() {}

public:
	/// Find a material by name, load if non existent
	virtual IMaterial *FindMaterial(const char *path) = 0;

	virtual bool ReleaseMaterialIfUnreferenced(IMaterial *material) = 0;
	virtual void ReleaseUnreferencedMaterials() = 0;
	/// Reloads all materials. For debugging purposes only.
	/// @warning DO NOT USE FOR PRODUCTION CODE
	virtual void ReloadAllMaterials() = 0;
};

#define INTERFACE_IMATERIALDICT_VERSION "imaterialdict001"

#endif
