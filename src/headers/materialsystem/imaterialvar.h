#ifndef IMATERIALVAR_H
#define IMATERIALVAR_H

#include "util/eigen.h"
#include "materialsystem\materialsystem.h"

/// A variable from a material script with variables data type
class IMaterialVar
{
protected:
	virtual ~IMaterialVar() {}

public:
	/// Type of this variables
	virtual MaterialVarTypeId GetType() const = 0;
	/// Get the name of the variable
	virtual const char *GetName() const = 0;
	/// Check if the variable was set in the script or if it's the default value
	virtual bool IsDefined() const = 0;

	// Get variable as specific data type
	virtual int GetInt() const = 0;
	virtual bool GetBool() const = 0;
	virtual float GetFloat() const = 0;
	virtual vec2 GetVec2() const = 0;
	virtual vec3 GetVec3() const = 0;
	virtual vec4 GetVec4() const = 0;
	virtual const char *GetString() const = 0;
	virtual ITexture *GetTexture() const = 0;

	// Set variable as specific data type
	virtual void SetInt(int value) = 0;
	virtual void SetBool(bool value) = 0;
	virtual void SetFloat(float value) = 0;
	virtual void SetVec2(const vec2 &value) = 0;
	virtual void SetVec3(const vec3 &value) = 0;
	virtual void SetVec4(const vec4 &value) = 0;
	virtual void SetString(const char *value) = 0;
	virtual void SetTexture(ITexture *value) = 0;
};


#endif