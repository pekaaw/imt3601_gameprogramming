#ifndef MDXINTERPOLATORS_H
#define MDXINTERPOLATORS_H

#include "mdxformat.h"

template<typename T>
T MDXInterpolateNone(Uint32 time, const MDXAnimationNode<T> &Node0, const MDXAnimationNode<T> &Node1)
{
	return Node0.Value;
}

template<typename T>
T MDXInterpolateLinear(Uint32 time, const MDXAnimationNode<T> &Node0, const MDXAnimationNode<T> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);

	return Lerp(cycle, Node0.Value, Node1.Value);
}

template<typename T>
T MDXInterpolateHermite(Uint32 time, const MDXAnimationNode<T> &Node0, const MDXAnimationNode<T> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);
	float FactorX2 = cycle * cycle;

	float Factor1 = FactorX2 * (2.0f * cycle - 3.0f) + 1;
	float Factor2 = FactorX2 * (cycle - 2.0f) + cycle;
	float Factor3 = FactorX2 * (cycle - 1.0f);
	float Factor4 = FactorX2 * (3.0f - 2.0f * cycle);

	return (Node0.Value * Factor1) + (Node0.OutTan * Factor2)
		   + (Node1.InTan * Factor3) + (Node1.Value * Factor4);
}

template<typename T>
T MDXInterpolateBezier(Uint32 time, const MDXAnimationNode<T> &Node0, const MDXAnimationNode<T> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);

	float FactorX2 = cycle * cycle;
	float InverseFactor = 1.0f - cycle;
	float InverseFactorX2 = InverseFactor * InverseFactor;

	float Factor1 = InverseFactorX2 * InverseFactor;
	float Factor2 = 3.0f * cycle * InverseFactorX2;
	float Factor3 = 3.0f * FactorX2 * InverseFactor;
	float Factor4 = FactorX2 * cycle;

	return (Node0.Value * Factor1) + (Node0.OutTan * Factor2)
		   + (Node1.InTan * Factor3) + (Node1.Value * Factor4);
}

quat MDXInterpolateLinear(Uint32 time, const MDXAnimationNode<quat> &Node0, const MDXAnimationNode<quat> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);

	return Node0.Value.slerp(cycle, Node1.Value);
	//return slerp(Node0.Value, Node1.Value, cycle);
}

quat MDXInterpolateHermite(Uint32 time, const MDXAnimationNode<quat> &Node0, const MDXAnimationNode<quat> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);

	quat Quaternion1 = Node0.Value.slerp(cycle, Node1.Value);
	quat Quaternion2 = Node0.OutTan.slerp(cycle, Node1.InTan);

	return Quaternion1.slerp((2.0f * cycle * (1.0f - cycle)), Quaternion2);
}

quat MDXInterpolateBezier(Uint32 time, const MDXAnimationNode<quat> &Node0, const MDXAnimationNode<quat> &Node1)
{
	float cycle = float(time - Node0.Time) / float(Node1.Time - Node0.Time);

	quat Quaternion1 = Node0.Value.slerp(cycle, Node0.OutTan);
	quat Quaternion2 = Node0.OutTan.slerp(cycle, Node1.InTan);
	quat Quaternion3 = Node1.InTan.slerp(cycle, Node1.Value);

	quat QuaternionSub1 = Quaternion1.slerp(cycle, Quaternion2);
	quat QuaternionSub2 = Quaternion2.slerp(cycle, Quaternion3);

	return QuaternionSub1.slerp(cycle, QuaternionSub2);
}

template<typename T>
T MDXAnimateByTrack(const MDXAnimationTrack<T> &source, Uint32 time, int sequence, const T &defaultValue)
{
	int s = source.Helpers.size();
	Assert(sequence >= 0 && sequence < (int)source.Helpers.size());

	const MDXAnimationHelper &helper = source.Helpers[sequence];

	if (source.Tracks.size() < 1
		|| helper.startIndex < 0)
		return defaultValue;

	const MDXAnimationNode<T> *pNode0 = NULL;
	const MDXAnimationNode<T> *pNode1 = NULL;

	pNode0 = &source.Tracks[helper.startIndex];

	if (helper.endIndex < 0)
		return pNode0->Value;

	Assert(helper.endIndex < (int)source.Tracks.size());

	for (int i = helper.startIndex + 1; i <= helper.endIndex; i++)
	{
		const MDXAnimationNode<T> &n = source.Tracks[i];

		if (n.Time >= time)
		{
			pNode0 = &source.Tracks[i - 1];
			pNode1 = &source.Tracks[i];
			break;
		}
	}

	Assert(pNode0 != nullptr);

	if (pNode0 == nullptr)
		return defaultValue;

	if (pNode1 == nullptr)
		return pNode0->Value;

	switch (source.InterpolationType)
	{
	default:
	case 0:
		return MDXInterpolateNone(time, *pNode0, *pNode1);

	case 1:
		return MDXInterpolateLinear(time, *pNode0, *pNode1);

	case 2:
		return MDXInterpolateHermite(time, *pNode0, *pNode1);

	case 3:
		return MDXInterpolateBezier(time, *pNode0, *pNode1);
	}
}

#endif