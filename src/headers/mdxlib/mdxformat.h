#ifndef MDXFORMAT_H
#define MDXFORMAT_H

#include <vector>

#include "util/platform.h"
#include "util/eigen.h"


struct MDXVersionChunk
{
	NO_COPY_DEFAULT_CTOR(MDXVersionChunk);

	Uint32 ChunkSize;
	Uint32 Version;
};

struct MDXModelChunk
{
	NO_COPY_DEFAULT_CTOR(MDXModelChunk);

	Uint32 ChunkSize;

	char Name[80];
	char AnimationFileName[260];

	float BoundsRadius;
	vec3 MinimumExtent;
	vec3 MaximumExtent;
	Uint32 BlendTime;
};

struct MDXSequenceChunk
{
	NO_COPY_DEFAULT_CTOR(MDXSequenceChunk);

	Uint32 ChunkSize;

	struct MDXSequence
	{
		char Name[80];

		Uint32 IntervalStart;
		Uint32 IntervalEnd;
		float MoveSpeed;
		Uint32 Flags;		//0 - Looping
		//1 - NonLooping
		float Rarity;
		Uint32 SyncPoint;

		float BoundsRadius;
		vec3 MinimumExtent;
		vec3 MaximumExtent;
	};

	std::vector<MDXSequence> Sequences;
};

struct MDXGlobalSequence
{
	NO_COPY_DEFAULT_CTOR(MDXGlobalSequence);

	Uint32 ChunkSize;

	std::vector<Uint32> Durations;
};

struct MDXAnimationHelper
{
	int startIndex;
	int endIndex;
};

template< typename T >
struct PRE_ALIGN_16 MDXAnimationNode
{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

	Uint32 Time;
	T Value;

	// if (InterpolationType > 1)
	T InTan;
	T OutTan;
} POST_ALIGN_16;

template< typename T >
struct MDXAnimationTrack
{
	NO_COPY_DEFAULT_CTOR(MDXAnimationTrack);

	Uint32 NrOfTracks;

	//0 - None
	//1 - Linear
	//2 - Hermite
	//3 - Bezier
	Uint32 InterpolationType;
	Uint32 GlobalSequenceId;

	std::vector<MDXAnimationNode<T>, Eigen::aligned_allocator<MDXAnimationNode<T>>> Tracks;
	std::vector<MDXAnimationHelper> Helpers;
};

typedef MDXAnimationTrack<Uint32> MDXAnimationTrackUINT32;
typedef MDXAnimationTrack<float> MDXAnimationTrackFLOAT;
typedef MDXAnimationTrack<vec3> MDXAnimationTrackVEC3;
typedef MDXAnimationTrack<vec4> MDXAnimationTrackVEC4;
typedef MDXAnimationTrack<quat> MDXAnimationTrackQUAT;

typedef MDXAnimationNode<Uint32> MDXAnimationNodeUINT32;
typedef MDXAnimationNode<float> MDXAnimationNodeFLOAT;
typedef MDXAnimationNode<vec3> MDXAnimationNodeVEC3;
typedef MDXAnimationNode<vec4> MDXAnimationNodeVEC4;
typedef MDXAnimationNode<quat> MDXAnimationNodeQUAT;


struct MDXLayerChunk
{
	NO_COPY_DEFAULT_CTOR(MDXLayerChunk);
	~MDXLayerChunk()
	{
		for (unsigned int i = 0; i < Layers.size(); i++)
			delete Layers[i];
		Layers.clear();
	}

	Uint32 NrOfLayers;

	struct MDXLayer
	{
		MDXLayer()
			: pMaterialAlpha(NULL), pMaterialTexture(NULL) {};
		~MDXLayer()
		{
			delete pMaterialAlpha;
			delete pMaterialTexture;
		};
		NO_COPY(MDXLayer);

		Uint32 InclusiveSize;

		//0 - None
		//1 - Transparent
		//2 - Blend
		//3 - Additive
		//4 - AddAlpha
		//5 - Modulate
		//6 - Modulate2x
		Uint32 FilterMode;

		//#1   - Unshaded
		//#2   - SphereEnvironmentMap
		//#4   - ???
		//#8   - ???
		//#16  - TwoSided
		//#32  - Unfogged
		//#64  - NoDepthTest
		//#128 - NoDepthSet
		Uint32 ShadingFlags;

		Uint32 TextureId;
		Uint32 TextureAnimationId;
		Uint32 CoordId;
		float Alpha;

		MDXAnimationTrackFLOAT *pMaterialAlpha;
		MDXAnimationTrackUINT32 *pMaterialTexture;
	};

	std::vector<MDXLayer*> Layers;
};

struct MDXMaterialChunk
{
	NO_COPY_DEFAULT_CTOR(MDXMaterialChunk);
	~MDXMaterialChunk()
	{
		for (unsigned int i = 0; i < Materials.size(); i++)
			delete Materials[i];
		Materials.clear();
	}

	Uint32 ChunkSize;

	struct MDXMaterial
	{
		MDXMaterial()
			: pLayer(NULL) {};
		~MDXMaterial()
		{
			delete pLayer;
		};
		NO_COPY(MDXMaterial);

		Uint32 InclusiveSize;
		Uint32 PriorityPlane;

		//#1  - ConstantColor
		//#2  - ???
		//#4  - ???
		//#8  - SortPrimitivesNearZ
		//#16 - SortPrimitivesFarZ
		//#32 - FullResolution
		Uint32 Flags;
		MDXLayerChunk *pLayer;
	};

	std::vector<MDXMaterial*> Materials;
};

struct MDXTextureChunk
{
	NO_COPY_DEFAULT_CTOR(MDXTextureChunk);
	Uint32 ChunkSize;

	struct MDXTexture
	{
		Uint32 ReplaceableId;

		char FileName[260];

		//#1 - WrapWidth
		//#2 - WrapHeight
		Uint32 Flags;
	};

	std::vector<MDXTexture> Textures;
};

struct MDXGeosetChunk
{
	NO_COPY_DEFAULT_CTOR(MDXGeosetChunk);
	~MDXGeosetChunk()
	{
		for (unsigned int i = 0; i < Geosets.size(); i++)
			delete Geosets[i];
		Geosets.clear();
	}

	Uint32 ChunkSize;

	struct MDXGeoset
	{
		NO_COPY_DEFAULT_CTOR(MDXGeoset);

		Uint32 InclusiveSize;

		static const char *pszExpectedVRTX;
		Uint32 NrOfVertexPositions;

		struct MDXVertexPosition
		{
			vec3 Position;
		};
		std::vector<MDXVertexPosition> VertexPositions;

		static const char *pszExpectedNRMS;
		Uint32 NrOfVertexNormals;

		struct MDXVertexNormal
		{
			vec3 Normal;
		};
		std::vector<MDXVertexNormal> VertexNormals;

		static const char *pszExpectedPTYP;
		Uint32 NrOfFaceTypeGroups;

		struct MDXFaceTypeGroup
		{
			//4   - Triangles
			//??? - Triangle fan
			//??? - Triangle strip
			//??? - Quads
			//??? - Quad strip
			Uint32 FaceType;
		};
		std::vector<MDXFaceTypeGroup> FaceTypeGroups;

		static const char *pszExpectedPCNT;
		Uint32 NrOfFaceGroups;

		struct MDXFaceGroup
		{
			Uint32 NrOfIndexes;
		};
		std::vector<MDXFaceGroup> FaceGroups;

		static const char *pszExpectedPVTX;
		Uint32 TotalNrOfIndexes;

		//TotalNrOfFaces = TotalNrOfIndexes / 3
		struct MDXFace
		{
			Uint16 Index[3];
		};
		std::vector<MDXFace> Faces;

		static const char *pszExpectedGNDX;
		Uint32 NrOfVertexGroups;

		struct MDXVertexGroup
		{
			Uint8 MatrixGroup;
		};
		std::vector<MDXVertexGroup> VertexGroups;

		static const char *pszExpectedMTGC;
		Uint32 NrOfMatrixGroups;

		struct MDXMatrixGroup
		{
			Uint32 MatrixGroupSize;
		};
		std::vector<MDXMatrixGroup> MatrixGroups;

		static const char *pszExpectedMATS;
		Uint32 NrOfMatrixIndexes;

		struct MDXMatrixIndex
		{
			Uint32 MatrixIndex;
		};
		std::vector<MDXMatrixIndex> MatrixIndices;

		Uint32 MaterialId;
		Uint32 SelectionGroup;

		//0  - None
		//#1 - ???
		//#2 - ???
		//#4 - Unselectable
		Uint32 SelectionFlags;

		float BoundsRadius;
		vec3 MinimumExtent;
		vec3 MaximumExtent;

		Uint32 NrOfExtents;

		struct MDXExtent
		{
			float BoundsRadius;
			vec3 MinimumExtent;
			vec3 MaximumExtent;
		};
		std::vector<MDXExtent> Extents;

		static const char *pszExpectedUVAS;
		Uint32 NrOfTextureVertexGroups;

		static const char *pszExpectedUVBS;
		Uint32 NrOfVertexTexturePositions;

		struct MDXVertexTexturePosition
		{
			vec2 TexturePosition;
		};
		std::vector<MDXVertexTexturePosition> VertexTexturePositions;
	};

	std::vector<MDXGeoset*> Geosets;
};

struct MDXGeosetAnimationChunk
{
	NO_COPY_DEFAULT_CTOR(MDXGeosetAnimationChunk);
	~MDXGeosetAnimationChunk()
	{
		for (unsigned int i = 0; i < GeosetAnimations.size(); i++)
			delete GeosetAnimations[i];
		GeosetAnimations.clear();
	};

	Uint32 ChunkSize;

	struct MDXGeosetAnimation
	{
		MDXGeosetAnimation() : pGeosetAlpha(NULL), pGeosetColor(NULL) {}
		~MDXGeosetAnimation()
		{
			delete pGeosetAlpha;
			delete pGeosetColor;
		};
		NO_COPY(MDXGeosetAnimation);


		Uint32 InclusiveSize;

		float Alpha;

		//#1 - DropShadow
		//#2 - Color
		Uint32 Flags;
		vec3 Color;

		Uint32 GeosetId;

		MDXAnimationTrackFLOAT *pGeosetAlpha;
		MDXAnimationTrackVEC3 *pGeosetColor;
	};

	std::vector<MDXGeosetAnimation*> GeosetAnimations;
};

struct MDXNode
{
	MDXNode() : pScaling(NULL), pRotation(NULL), pTranslation(NULL) {}

	~MDXNode()
	{
		delete pScaling;
		delete pRotation;
		delete pTranslation;
	};

	NO_COPY(MDXNode);

	enum MDXNodeFlags
	{
		FL_DONT_INHERIT_TRANSLATION = (1 << 0),
		FL_DONT_INHERIT_ROTATION = (1 << 1),
		FL_DONT_INHERIT_SCALING = (1 << 2),

		FL_BILLBOARDED = (1 << 3),
		FL_BILLBOARDED_LOCKX = (1 << 4),
		FL_BILLBOARDED_LOCKY = (1 << 5),
		FL_BILLBOARDED_LOCKZ = (1 << 6),
		FL_CAMERA_ANCHORED = (1 << 7),
	};

	Uint32 InclusiveSize;

	char Name[80];

	Uint32 ObjectId;
	Uint32 ParentId;

	//0        - Helper
	//#1       - DontInheritTranslation
	//#2       - DontInheritRotation
	//#4       - DontInheritScaling
	//#8       - Billboarded
	//#16      - BillboardedLockX
	//#32      - BillboardedLockY
	//#64      - BillboardedLockZ
	//#128     - CameraAnchored
	//#256     - Bone
	//#512     - Light
	//#1024    - EventObject
	//#2048    - Attachment
	//#4096    - ParticleEmitter
	//#8192    - CollisionShape
	//#16384   - RibbonEmitter
	//#32768   - Unshaded / EmitterUsesMdl
	//#65536   - SortPrimitivesFarZ / EmitterUsesTga
	//#131072  - LineEmitter
	//#262144  - Unfogged
	//#524288  - ModelSpace
	//#1048576 - XYQuad
	Uint32 Flags;

	MDXAnimationTrackVEC3 *pScaling;
	MDXAnimationTrackQUAT *pRotation;
	MDXAnimationTrackVEC3 *pTranslation;
};


struct MDXBoneChunk
{
	NO_COPY_DEFAULT_CTOR(MDXBoneChunk);
	~MDXBoneChunk()
	{
		for (unsigned int i = 0; i < Bones.size(); i++)
			delete Bones[i];
		Bones.clear();
	};

	Uint32 ChunkSize;

	struct MDXBone
	{
		MDXBone() : pNode(NULL) {}
		~MDXBone()
		{
			delete pNode;
		};
		NO_COPY(MDXBone);

		MDXNode *pNode;

		Uint32 GeosetId;
		Uint32 GeosetAnimationId;
	};

	std::vector<MDXBone*> Bones;
};

struct MDXHelperChunk
{
	NO_COPY_DEFAULT_CTOR(MDXHelperChunk);
	~MDXHelperChunk()
	{
		for (unsigned int i = 0; i < Helpers.size(); i++)
			delete Helpers[i];
		Helpers.clear();
	};

	Uint32 ChunkSize;

	std::vector<MDXNode*> Helpers;
};

struct MDXAttachmentChunk
{
	NO_COPY_DEFAULT_CTOR(MDXAttachmentChunk);
	~MDXAttachmentChunk()
	{
		for (unsigned int i = 0; i < Attachments.size(); i++)
			delete Attachments[i];
		Attachments.clear();
	}

	Uint32 ChunkSize;

	struct MDXAttachment
	{
		MDXAttachment() : pNode(NULL), pVisibility(NULL) {}
		~MDXAttachment()
		{
			delete pNode;
			delete pVisibility;
		};
		NO_COPY(MDXAttachment);

		Uint32 InclusiveSize;

		MDXNode *pNode;

		char Path[260];

		//First attachment - 0, second - 1 etc...
		Uint32 AttachmentId;

		MDXAnimationTrackFLOAT *pVisibility;
	};

	std::vector<MDXAttachment*> Attachments;
};

struct MDXPivotPointChunk
{
	NO_COPY_DEFAULT_CTOR(MDXPivotPointChunk);

	Uint32 ChunkSize;

	std::vector<vec3> Pivots;
};

struct MDXLightChunk
{
	NO_COPY_DEFAULT_CTOR(MDXLightChunk);
	~MDXLightChunk()
	{
		for (unsigned int i = 0; i < Lights.size(); i++)
			delete Lights[i];
		Lights.clear();
	}

	Uint32 ChunkSize;

	struct MDXLightData
	{
		MDXLightData() : pNode(nullptr),
			pLightVisibility(nullptr), pLightColor(nullptr), pLightIntensity(nullptr),
			pLightAmbientColor(nullptr), pLightAmbientIntensity(nullptr) {}
		~MDXLightData()
		{
			delete pNode;
			delete pLightVisibility;
			delete pLightColor;
			delete pLightIntensity;
			delete pLightAmbientColor;
			delete pLightAmbientIntensity;
		};
		NO_COPY(MDXLightData);

		Uint32 InclusiveSize;

		MDXNode *pNode;

		//0 - Omnidirectional
		//1 - Directional
		//2 - Ambient
		Uint32 Type;

		Uint32 AttenuationStart;
		Uint32 AttenuationEnd;

		vec3 Color;
		float Intensity;
		vec3 AmbientColor;
		float AmbientIntensity;

		static const char *pszExpectedKLAV;
		MDXAnimationTrackFLOAT *pLightVisibility;

		static const char *pszExpectedKLAC;
		MDXAnimationTrackVEC3 *pLightColor;

		static const char *pszExpectedKLAI;
		MDXAnimationTrackFLOAT *pLightIntensity;

		static const char *pszExpectedKLBC;
		MDXAnimationTrackVEC3 *pLightAmbientColor;

		static const char *pszExpectedKLBI;
		MDXAnimationTrackFLOAT *pLightAmbientIntensity;
	};

	std::vector<MDXLightData *> Lights;
};

// custom optimization chunk
struct MDXDynamicChunk
{
	NO_COPY_DEFAULT_CTOR(MDXDynamicChunk);
	~MDXDynamicChunk()
	{
		for (unsigned int i = 0; i < GeosetHelpers.size(); i++)
			delete GeosetHelpers[i];
		GeosetHelpers.clear();
	};

	// bones and helpers in order
	std::vector<MDXNode*> Nodes;

	struct MDXGeosetHelper
	{
		// nodes that will be used by this geoset
		std::vector<int> NodeIndices;

		// node blend configurations for each matrix group
		std::vector<vec3> NodeBoneIndices;
	};

	std::vector<MDXGeosetHelper*> GeosetHelpers;
};

struct MDXModelData
{
	MDXModelData()
	{
		memset(this, 0, sizeof(MDXModelData));
	}

	~MDXModelData()
	{
		delete pVersion;
		delete pModel;
		delete pSequences;
		delete pMaterials;
		delete pTextures;
		delete pGeosets;
		delete pGeosetAnimations;
		delete pBones;
		delete pHelpers;
		delete pAttachments;
		delete pPivots;
		delete pGlobalSequences;
		delete pLights;
		delete pDynamic;
	}

	NO_COPY(MDXModelData);

	MDXVersionChunk *pVersion;
	MDXModelChunk *pModel;
	MDXSequenceChunk *pSequences;
	MDXMaterialChunk *pMaterials;
	MDXTextureChunk *pTextures;
	MDXGeosetChunk *pGeosets;
	MDXGeosetAnimationChunk *pGeosetAnimations;
	MDXBoneChunk *pBones;
	MDXHelperChunk *pHelpers;
	MDXAttachmentChunk *pAttachments;
	MDXPivotPointChunk *pPivots;

	MDXGlobalSequence *pGlobalSequences;
	MDXLightChunk *pLights;

	MDXDynamicChunk *pDynamic;
};


#endif