#ifndef ICLIENT_H
#define ICLIENT_H


/// Exposed from client to engine
class IClient
{
protected:
	virtual ~IClient() {}

public:
	virtual bool Init(bool enableGUI) = 0;
	virtual void Shutdown() = 0;

	/// Simulates frame in client
	virtual void Simulate() = 0;

	virtual void OnLevelInit() = 0;
	virtual void OnLevelShutdown() = 0;

	virtual void SetLocalClientIndex(int clientIndex) = 0;

	virtual void ApplyNetworkSymbolDefinitions(ByteBuffer &buffer) = 0;
	virtual void CreateNetworkEntity(int networkId, Uint8 networkTableIndex, ByteBuffer &buffer) = 0;
	virtual void DestroyNetworkEntity(int networkId) = 0;

	virtual void ApplyWorldStateUpdates(ByteBuffer &buffer) = 0;
	virtual void ApplyWorldStateComplete(ByteBuffer &buffer) = 0;

	/// Begins rendering
	virtual void RenderBegin() = 0;
	/// Renders the scene
	virtual void RenderScene() = 0;
	/// Renders gui, swaps buffers
	virtual void RenderEnd() = 0;
};

#define INTERFACE_ICLIENT_VERSION "iclient001"


#endif