#ifndef ILOGGER_H
#define ILOGGER_H

#define MESSAGE( text ) \
	logger->Log( text, ILogger::TYPE_INFO, __FILE__, __LINE__ )

class ILogger
{
protected:
	virtual ~ILogger() {}

public:
	enum LogType {
		TYPE_DEBUG = 0,
		TYPE_INFO,
		TYPE_WARNING,
		TYPE_ERROR
	};

	virtual void Log(const char *message, LogType type) = 0;
	virtual void Log(const char *message, LogType type, const char *file, int line) = 0;
};

#define INTERFACE_ILOGGER_VERSION "ilogger001"

#endif
