#ifndef IENGINESERVER_H
#define IENGINESERVER_H

class IWorld;

/// Exposed from engine to server
class IEngineServer
{
protected:
	virtual ~IEngineServer() {}

public:
	virtual IWorld *GetWorld() = 0;
	virtual bool IsInToolsMode() const = 0;
};

#define INTERFACE_IENGINESERVER_VERSION "iengineserver001"

#endif