#ifndef IDEBUGDRAW_H
#define IDEBUGDRAW_H

#include "util/eigen.h"

/// Implements debug rendering with simple primitives. Can be called from any place, objects are cached untril rendering is done.
class IDebugDraw
{
protected:
	virtual ~IDebugDraw() {}

public:
	/// Draws a single line
	/// @param ignoreZ Disable depth testing
	virtual void DrawLine(const vec3 &start, const vec3 &end, const vec3 &color, float alpha, float lifetime = 0.5f, bool ignoreZ = true) = 0;
	/// Draws a line loop
	/// @param ignoreZ Disable depth testing
	virtual void DrawLineLoop(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime = 0.5f, bool ignoreZ = true) = 0;
	/// Draws a polygon via a triangle fan
	/// @param ignoreZ Disable depth testing
	virtual void DrawTriangleFan(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime = 0.5f, bool ignoreZ = true) = 0;
	/// Draws an AABB
	/// @param ignoreZ Disable depth testing
	virtual void DrawBox(const vec3 &mins, const vec3 &maxs, const vec3 &color, float alpha, float lifetime = 0.5f, bool ignoreZ = true) = 0;

};

#define INTERFACE_IDEBUGDRAW_VERSION "idebugdraw001"


#endif