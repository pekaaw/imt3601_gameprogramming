#ifndef IWORLD_H
#define IWORLD_H

class IMesh;
class IMaterial;

/// Information about settings of global light in map
struct GlobalLightParams
{
public:
	vec3 color;
	vec3 direction;
};

/// Information about fog settings in map
struct FogParams
{
public:
	float start;
	float range;
	float amount;
	vec3 color;
};

/// Information about a single tile
struct WorldTile
{
public:
	int x;
	int y;
	int index;

	float uvs;
	float uvt;

	Uint8 flags;
	Uint8 tileType;

	// A*
	WorldTile *links[4]; // left, up, right, bottom
	float costReach;
	float costHeuristic;
	WorldTile *previous;
};

struct WorldWaveUnitType
{
public:
	Uint8 type;
	Uint16 count;
};

struct WorldWave
{
public:
	std::vector<WorldWaveUnitType> unitTypes;
};

/// world geometry, can be retrieved through IWorld as const*
class IWorldGeometry
{
protected:
	virtual ~IWorldGeometry() {}

public:
	/// Amount of world meshes
	virtual int GetWorldListCount() const = 0;
	/// Access a mesh by index
	virtual const IMesh *GetWorldListMesh(int index) const = 0;
	/// Access the bounds of a mesh by index
	virtual void GetWorldListBounds(int index, vec3 &min, vec3 &max) const = 0;

	virtual int GetWaterListCount() const = 0;
	virtual const IMesh *GetWaterListMesh(int index) const = 0;
	virtual void GetWaterListBounds(int index, vec3 &min, vec3 &max) const = 0;
};

/// Main world access, get through IEngineClient or IEngineServer
class IWorld
{
protected:
	virtual ~IWorld() {}

public:
	/// Version of binary format
	virtual int GetVersion() const = 0;
	virtual const char *GetName() const = 0;

	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;

	virtual int GetCameraWidth() const = 0;
	virtual int GetCameraHeight() const = 0;

	virtual const GlobalLightParams &GetGlobalLightParams() const = 0;
	virtual const FogParams &GetFogParams() const = 0;

	virtual int GetTileCount() const = 0;
	virtual const WorldTile &GetTile(int index) const = 0;

	virtual int GetWaveDuration() const = 0;
	virtual int GetPauseDuration() const = 0;
	virtual int GetWaveFlags() const = 0;

	virtual int GetWaveCount() const = 0;
	virtual const WorldWave &GetWave(int index) const = 0;

	virtual const IWorldGeometry *GetGeometry() const = 0;
	virtual IMaterial *GetMaterial() const = 0;
	virtual IMaterial *GetWaterMaterial() const = 0;

	virtual int GetTileIndexAtPosition(const vec3 &worldPosition) const = 0;
	virtual bool GetTileIndexAtPosition(const vec3 &worldPosition, int &index_x, int &index_y) const = 0;
	virtual vec3 GetTilePosition(int index) const = 0;

	virtual void GetElevationHeights(int index, vec4 &heights, bool ignoreWater = false) const = 0;
	virtual float GetElevationHeight(const vec3 &worldPosition, bool ignoreWater = false) const = 0;
	virtual IMesh *CreateProjectedDecal(const vec3 &origin, const euler &angles, const vec3 &size, bool ignoreWater = false) = 0;
};


#endif