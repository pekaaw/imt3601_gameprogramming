#ifndef IENGINECLIENT_H
#define IENGINECLIENT_H

class IWorld;

/// Exposed from engine to client
class IEngineClient
{
protected:
	virtual ~IEngineClient() {}

public:
	virtual IWorld *GetWorld() = 0;
	virtual bool IsInToolsMode() const = 0;

	/// Send command to server
	virtual void SendClientCommand(const char *command) = 0;
};

#define INTERFACE_IENGINECLIENT_VERSION "iengineclient001"

#endif